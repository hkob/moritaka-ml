# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

base_path = File.join(Rails.root, 'db', 'seeds', 'locales')
dst_path = File.join(Rails.root, 'config', 'locales')
Dir.glob(File.join(base_path, '**', '*.yml')) do |file|
  dir = File.dirname(file.gsub(base_path, ''))
  ja_path = File.join(dst_path, dir, 'ja.yml')
  en_path = File.join(dst_path, dir, 'en.yml')
  mt = File.mtime(file)
  if mt > File.mtime(ja_path) || mt > File.mtime(en_path)
    File.open(file) do |f|
      File.open(ja_path, 'w') do |fja|
        File.open(en_path, 'w') do |fen|
          puts file
          fja.puts 'ja:'
          fen.puts 'en:'
          while line = f.gets
            if /(.*):(.*)/ =~ line
              key = $1
              body = $2
              ja, en = body.split('|')
              if ja.present?
                fja.puts "#{ key }: #{ ja }"
                fen.puts "#{ key }: #{ en }"
              else
                fja.puts "#{ key }:"
                fen.puts "#{ key }:"
              end
            end
          end
        end
      end
    end
  end
end

def already_created?(obj, file)
  obj && obj.updated_at.to_time.to_datetime > File::stat(file).mtime.to_time.to_datetime
end

require 'factory_bot'
Dir[Rails.root.join('spec/support/fb_find_or_create.rb')].each { |f| require f }
Dir[Rails.root.join('spec/support/create_factories/*.rb')].each {|f| require f }

@vc = %i( VOCAL iChisatoMoritaka )
@vcah = %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito]
@mc = ListContent.title_only :MC
@encore = ListContent.title_only 'アンコール', 'ENCORE'
@double_encore = ListContent.title_only 'ダブルアンコール', 'DOUBLE ENCORE'
@medley_in = ListContent.medley_in
@medley_out = ListContent.title_only :MEDLEY_OUT
@mc_member = ListContent.title_only :MC, :MC, 'メンバー紹介', 'Introduction of band members'
@konomachi_home_mix = [[:konomachi, [], '[HOME MIX]', '[HOME MIX]']]

Dir.glob(File.join(Rails.root, 'db', 'seeds', '*.rb')).sort.each do |file|
  print File.basename(file)
  load(file)
  print "\n"
end

