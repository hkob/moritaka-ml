# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2017_09_24_072152) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.string "key", null: false
    t.integer "activity_type", null: false
    t.string "j_comment"
    t.string "e_comment"
    t.integer "sort_order"
    t.integer "title_id", null: false
    t.integer "song_id"
    t.integer "company_id"
    t.integer "from_id"
    t.integer "to_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["activity_type", "sort_order"], name: "index_activities_on_activity_type_and_sort_order", unique: true
    t.index ["from_id"], name: "index_activities_on_from_id"
    t.index ["key"], name: "index_activities_on_key", unique: true
    t.index ["song_id"], name: "index_activities_on_song_id"
  end

  create_table "activity_subs", force: :cascade do |t|
    t.string "key"
    t.string "j_title"
    t.string "e_title"
    t.string "j_comment"
    t.string "e_comment"
    t.integer "activity_id"
    t.integer "from_id"
    t.integer "to_id"
    t.integer "song_id"
    t.integer "sort_order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["activity_id", "sort_order"], name: "index_activity_subs_on_activity_id_and_sort_order", unique: true
    t.index ["from_id"], name: "index_activity_subs_on_from_id"
    t.index ["key"], name: "index_activity_subs_on_key", unique: true
  end

  create_table "band_members", force: :cascade do |t|
    t.integer "band_id", null: false
    t.integer "person_id", null: false
    t.integer "instrumental_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["band_id", "instrumental_id"], name: "band_members_bi"
    t.index ["band_id", "person_id", "instrumental_id"], name: "band_members_bpi", unique: true
    t.index ["instrumental_id"], name: "band_members_i"
    t.index ["person_id", "instrumental_id"], name: "band_members_pi"
  end

  create_table "bands", force: :cascade do |t|
    t.string "key", null: false
    t.integer "title_id", null: false
    t.string "j_comment"
    t.string "e_comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_bands_on_key", unique: true
    t.index ["title_id"], name: "index_bands_on_title_id", unique: true
  end

  create_table "books", force: :cascade do |t|
    t.string "key", null: false
    t.integer "book_type", null: false
    t.string "isbn"
    t.string "price"
    t.string "j_comment"
    t.string "e_comment"
    t.string "link"
    t.integer "sort_order"
    t.integer "title_id", null: false
    t.integer "publisher_id", null: false
    t.integer "author_id"
    t.integer "photographer_id"
    t.integer "seller_id"
    t.integer "event_date_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id"], name: "index_books_on_author_id"
    t.index ["book_type", "sort_order"], name: "index_books_on_book_type_and_sort_order", unique: true
    t.index ["event_date_id"], name: "index_books_on_event_date_id"
    t.index ["key"], name: "index_books_on_key", unique: true
    t.index ["photographer_id"], name: "index_books_on_photographer_id"
    t.index ["publisher_id"], name: "index_books_on_publisher_id"
    t.index ["seller_id"], name: "index_books_on_seller_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string "key"
    t.integer "title_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_companies_on_key", unique: true
    t.index ["title_id"], name: "index_companies_on_title_id"
  end

  create_table "concert_halls", force: :cascade do |t|
    t.integer "concert_id", null: false
    t.integer "sort_order", null: false
    t.integer "hall_id", null: false
    t.integer "event_date_id"
    t.integer "list_id"
    t.integer "device_id"
    t.string "j_hall_sub"
    t.string "e_hall_sub"
    t.string "j_comment"
    t.string "e_comment"
    t.string "j_product"
    t.string "e_product"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["concert_id", "sort_order", "hall_id"], name: "index_concert_halls_on_concert_id_and_sort_order_and_hall_id", unique: true
    t.index ["event_date_id"], name: "index_concert_halls_on_event_date_id"
    t.index ["list_id"], name: "index_concert_halls_on_list_id"
  end

  create_table "concert_videos", force: :cascade do |t|
    t.integer "concert_list_content_id", null: false
    t.integer "video_list_content_id", null: false
    t.integer "concert_hall_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["concert_hall_id"], name: "index_concert_videos_on_concert_hall_id"
    t.index ["concert_list_content_id"], name: "index_concert_videos_on_concert_list_content_id"
    t.index ["video_list_content_id"], name: "index_concert_videos_on_video_list_content_id"
  end

  create_table "concerts", force: :cascade do |t|
    t.string "key", null: false
    t.string "j_subtitle"
    t.string "e_subtitle"
    t.string "j_comment"
    t.string "e_comment"
    t.integer "concert_type", null: false
    t.boolean "has_song_list", null: false
    t.boolean "has_product", null: false
    t.integer "num_of_performances", null: false
    t.integer "num_of_halls", null: false
    t.integer "sort_order", null: false
    t.integer "title_id", null: false
    t.integer "from_id", null: false
    t.integer "to_id"
    t.integer "band_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["concert_type", "sort_order"], name: "index_concerts_on_concert_type_and_sort_order", unique: true
    t.index ["from_id"], name: "index_concerts_on_from_id"
    t.index ["key"], name: "index_concerts_on_key", unique: true
    t.index ["title_id"], name: "index_concerts_on_title_id", unique: true
  end

  create_table "device_activities", force: :cascade do |t|
    t.integer "device_id", null: false
    t.integer "activity_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["activity_id"], name: "index_device_activities_on_activity_id"
    t.index ["device_id", "activity_id"], name: "index_device_activities_on_device_id_and_activity_id", unique: true
  end

  create_table "devices", force: :cascade do |t|
    t.string "key", null: false
    t.string "type", null: false
    t.integer "device_type", null: false
    t.integer "minutes"
    t.integer "seconds"
    t.integer "sort_order", null: false
    t.string "number"
    t.string "j_comment"
    t.string "e_comment"
    t.string "link"
    t.integer "title_id", null: false
    t.integer "singer_id"
    t.integer "event_date_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_date_id"], name: "index_devices_on_event_date_id"
    t.index ["singer_id"], name: "index_devices_on_singer_id"
    t.index ["title_id"], name: "index_devices_on_title_id"
    t.index ["type", "device_type", "event_date_id"], name: "index_devices_on_type_and_device_type_and_event_date_id"
  end

  create_table "event_dates", force: :cascade do |t|
    t.string "key"
    t.date "date"
    t.integer "year_id", null: false
    t.integer "month"
    t.integer "day"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["date"], name: "index_event_dates_on_date"
    t.index ["key"], name: "index_event_dates_on_key", unique: true
    t.index ["year_id", "date", "month", "day"], name: "index_event_dates_on_year_id_and_date_and_month_and_day"
  end

  create_table "halls", force: :cascade do |t|
    t.string "key", null: false
    t.integer "sort_order", null: false
    t.integer "title_id", null: false
    t.integer "prefecture_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_halls_on_key", unique: true
    t.index ["prefecture_id", "sort_order"], name: "index_halls_on_prefecture_id_and_sort_order", unique: true
    t.index ["title_id"], name: "index_halls_on_title_id", unique: true
  end

  create_table "instrumentals", force: :cascade do |t|
    t.string "key", null: false
    t.integer "sort_order", null: false
    t.integer "title_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_instrumentals_on_key", unique: true
    t.index ["sort_order"], name: "index_instrumentals_on_sort_order", unique: true
    t.index ["title_id"], name: "index_instrumentals_on_title_id", unique: true
  end

  create_table "list_contents", force: :cascade do |t|
    t.string "j_title"
    t.string "e_title"
    t.string "j_ver"
    t.string "e_ver"
    t.integer "sort_order", null: false
    t.integer "list_id", null: false
    t.integer "song_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "j_comment"
    t.string "e_comment"
    t.index ["list_id", "sort_order"], name: "index_list_contents_on_list_id_and_sort_order", unique: true
    t.index ["song_id"], name: "index_list_contents_on_song_id"
  end

  create_table "lists", force: :cascade do |t|
    t.string "key", null: false
    t.string "keyword", null: false
    t.integer "device_id"
    t.integer "concert_id"
    t.integer "book_id"
    t.integer "sort_order", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "activity_sub_id"
    t.index ["book_id", "sort_order"], name: "index_lists_on_book_id_and_sort_order"
    t.index ["concert_id", "sort_order"], name: "index_lists_on_concert_id_and_sort_order"
    t.index ["device_id", "sort_order"], name: "index_lists_on_device_id_and_sort_order"
    t.index ["key"], name: "index_lists_on_key", unique: true
  end

  create_table "lyrics", force: :cascade do |t|
    t.string "key", null: false
    t.integer "song_id", null: false
    t.integer "person_id", null: false
    t.integer "sort_order", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["person_id"], name: "index_lyrics_on_person_id"
    t.index ["song_id", "person_id", "sort_order"], name: "index_lyrics_on_song_id_and_person_id_and_sort_order"
    t.index ["song_id", "sort_order"], name: "index_lyrics_on_song_id_and_sort_order"
  end

  create_table "media", force: :cascade do |t|
    t.string "key", null: false
    t.string "medium_device", null: false
    t.string "code"
    t.string "release"
    t.boolean "now_sale"
    t.string "price"
    t.integer "sort_order", null: false
    t.integer "company_id"
    t.integer "device_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id", "code"], name: "index_media_on_company_id_and_code"
    t.index ["device_id", "sort_order"], name: "index_media_on_device_id_and_sort_order"
    t.index ["key"], name: "index_media_on_key", unique: true
  end

  create_table "musics", force: :cascade do |t|
    t.string "key", null: false
    t.integer "song_id", null: false
    t.integer "person_id", null: false
    t.integer "sort_order", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["person_id"], name: "index_musics_on_person_id"
    t.index ["song_id", "person_id", "sort_order"], name: "index_musics_on_song_id_and_person_id_and_sort_order"
    t.index ["song_id", "sort_order"], name: "index_musics_on_song_id_and_sort_order"
  end

  create_table "people", force: :cascade do |t|
    t.string "key", null: false
    t.integer "title_id", null: false
    t.integer "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_people_on_key", unique: true
    t.index ["parent_id"], name: "index_people_on_parent_id"
    t.index ["title_id"], name: "index_people_on_title_id", unique: true
  end

  create_table "prefectures", force: :cascade do |t|
    t.string "key", null: false
    t.integer "title_id", null: false
    t.integer "region", null: false
    t.string "j_capital", null: false
    t.string "e_capital", null: false
    t.integer "sort_order", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_prefectures_on_key", unique: true
    t.index ["region", "sort_order"], name: "index_prefectures_on_region_and_sort_order"
    t.index ["sort_order"], name: "index_prefectures_on_sort_order", unique: true
    t.index ["title_id"], name: "index_prefectures_on_title_id", unique: true
  end

  create_table "references", force: :cascade do |t|
    t.string "key", null: false
    t.integer "reference_type", null: false
    t.string "link", null: false
    t.integer "title_id", null: false
    t.integer "sort_order", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_references_on_key", unique: true
    t.index ["reference_type", "sort_order"], name: "index_references_on_reference_type_and_sort_order", unique: true
    t.index ["title_id"], name: "index_references_on_title_id", unique: true
  end

  create_table "song_people", force: :cascade do |t|
    t.integer "list_content_id", null: false
    t.integer "person_id", null: false
    t.integer "instrumental_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["list_content_id", "instrumental_id"], name: "song_people_li"
    t.index ["list_content_id", "person_id", "instrumental_id"], name: "song_people_lpi"
    t.index ["person_id", "instrumental_id"], name: "song_people_pi"
  end

  create_table "songs", force: :cascade do |t|
    t.string "key", null: false
    t.integer "title_id", null: false
    t.integer "parent_id"
    t.integer "event_date_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_date_id"], name: "index_songs_on_event_date_id"
    t.index ["key"], name: "index_songs_on_key", unique: true
    t.index ["parent_id"], name: "index_songs_on_parent_id"
    t.index ["title_id"], name: "index_songs_on_title_id", unique: true
  end

  create_table "titles", force: :cascade do |t|
    t.string "key"
    t.string "japanese", null: false
    t.string "english", null: false
    t.string "yomi", null: false
    t.string "yomi_suuji", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_titles_on_key"
    t.index ["yomi_suuji"], name: "index_titles_on_yomi_suuji"
  end

  create_table "years", force: :cascade do |t|
    t.integer "year", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["year"], name: "index_years_on_year", unique: true
  end

  create_table "youtubes", force: :cascade do |t|
    t.string "key"
    t.integer "youtube_type"
    t.string "number"
    t.integer "event_date_id"
    t.string "j_title"
    t.string "e_title"
    t.integer "song_id"
    t.integer "sort_order"
    t.string "link"
    t.text "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_date_id"], name: "index_youtubes_on_event_date_id"
    t.index ["key"], name: "index_youtubes_on_key"
    t.index ["song_id"], name: "index_youtubes_on_song_id"
    t.index ["sort_order"], name: "index_youtubes_on_sort_order"
    t.index ["youtube_type", "number"], name: "index_youtubes_on_youtube_type_and_number"
  end

end
