class CreatePrefectures < ActiveRecord::Migration[5.1]
  def change
    create_table :prefectures do |t|
      t.string :key, null: false
      t.integer :title_id, null: false
      t.integer :region, null: false
      t.string :j_capital, null: false
      t.string :e_capital, null: false
      t.integer :sort_order, null: false

      t.timestamps
    end
    add_index :prefectures, %i[key], unique: true
    add_index :prefectures, %i[region sort_order]
    add_index :prefectures, %i[sort_order], unique: true
    add_index :prefectures, %i[title_id], unique: true
  end
end
