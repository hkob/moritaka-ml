class CreateTitles < ActiveRecord::Migration[5.1]
  def change
    create_table :titles do |t|
      t.string :key
      t.string :japanese, null: false
      t.string :english, null: false
      t.string :yomi, null: false
      t.string :yomi_suuji, null: false

      t.timestamps
    end
    add_index :titles, %i[key]
    add_index :titles, %i[yomi_suuji]
  end
end
