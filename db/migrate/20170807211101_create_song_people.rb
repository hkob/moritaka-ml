class CreateSongPeople < ActiveRecord::Migration[5.1]
  def change
    create_table :song_people do |t|
      t.integer :list_content_id, null: false
      t.integer :person_id, null: false
      t.integer :instrumental_id, null: false

      t.timestamps
    end
    add_index :song_people, %i[list_content_id person_id instrumental_id], name: :song_people_lpi
    add_index :song_people, %i[list_content_id instrumental_id], name: :song_people_li
    add_index :song_people, %i[person_id instrumental_id], name: :song_people_pi
  end
end
