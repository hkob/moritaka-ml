class CreateListContents < ActiveRecord::Migration[5.1]
  def change
    create_table :list_contents do |t|
      t.string :j_title
      t.string :e_title
      t.string :j_ver
      t.string :e_ver
      t.integer :sort_order, null: false
      t.integer :list_id, null: false
      t.integer :song_id

      t.timestamps
    end
    add_index :list_contents, %i[list_id sort_order], unique: true
    add_index :list_contents, %i[song_id]
  end
end
