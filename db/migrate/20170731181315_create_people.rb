class CreatePeople < ActiveRecord::Migration[5.1]
  def change
    create_table :people do |t|
      t.string :key, null: false
      t.integer :title_id, null: false
      t.integer :parent_id

      t.timestamps
    end
    add_index :people, %i[key], unique: true
    add_index :people, %i[title_id], unique: true
    add_index :people, %i[parent_id]
  end

end
