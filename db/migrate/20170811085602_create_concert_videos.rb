class CreateConcertVideos < ActiveRecord::Migration[5.1]
  def change
    create_table :concert_videos do |t|
      t.integer :concert_list_content_id, null: false
      t.integer :video_list_content_id, null: false
      t.integer :concert_hall_id, null: false

      t.timestamps
    end
    add_index :concert_videos, %i[concert_list_content_id]
    add_index :concert_videos, %i[video_list_content_id]
    add_index :concert_videos, %i[concert_hall_id]
  end
end
