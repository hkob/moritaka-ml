class CreateYoutubes < ActiveRecord::Migration[5.1]
  def change
    create_table :youtubes do |t|
      t.string :key
      t.integer :youtube_type
      t.string :number
      t.integer :event_date_id
      t.string :j_title
      t.string :e_title
      t.integer :song_id
      t.integer :sort_order
      t.string :link
      t.text :comment

      t.timestamps
    end
    add_index :youtubes, %i[key]
    add_index :youtubes, %i[event_date_id]
    add_index :youtubes, %i[youtube_type number]
    add_index :youtubes, %i[song_id]
    add_index :youtubes, %i[sort_order]
  end
end
