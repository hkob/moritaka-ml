class CreateBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :books do |t|
      t.string :key, null: false
      t.integer :book_type, null: false
      t.string :isbn
      t.string :price
      t.string :j_comment
      t.string :e_comment
      t.string :link
      t.integer :sort_order
      t.integer :title_id, null: false
      t.integer :publisher_id, null: false
      t.integer :author_id
      t.integer :photographer_id
      t.integer :seller_id
      t.integer :event_date_id

      t.timestamps
    end
    add_index :books, %i[key], unique: true
    add_index :books, %i[author_id]
    add_index :books, %i[publisher_id]
    add_index :books, %i[photographer_id]
    add_index :books, %i[seller_id]
    add_index :books, %i[book_type sort_order], unique: true
    add_index :books, %i[event_date_id]
  end
end
