class AddActivitySubIdToList < ActiveRecord::Migration[5.1]
  def change
    add_column :lists, :activity_sub_id, :integer
  end
end
