class CreateCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :companies do |t|
      t.string :key
      t.integer :title_id

      t.timestamps
    end
    add_index :companies, %i[key], unique: true
    add_index :companies, %i[title_id]
  end
end
