class AddJCommentECommentToListContent < ActiveRecord::Migration[5.1]
  def change
    add_column :list_contents, :j_comment, :string
    add_column :list_contents, :e_comment, :string
  end
end
