class CreateLyrics < ActiveRecord::Migration[5.1]
  def change
    create_table :lyrics do |t|
      t.string :key, null: false
      t.integer :song_id, null: false
      t.integer :person_id, null: false
      t.integer :sort_order, null: false

      t.timestamps
    end
    add_index :lyrics, %i[song_id person_id sort_order]
    add_index :lyrics, %i[song_id sort_order]
    add_index :lyrics, %i[person_id]
  end
end
