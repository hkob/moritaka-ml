class CreateBandMembers < ActiveRecord::Migration[5.1]
  def change
    create_table :band_members do |t|
      t.integer :band_id, null: false
      t.integer :person_id, null: false
      t.integer :instrumental_id, null: false

      t.timestamps
    end
    add_index :band_members, %i[band_id person_id instrumental_id], name: :band_members_bpi, unique: true
    add_index :band_members, %i[band_id instrumental_id], name: :band_members_bi
    add_index :band_members, %i[person_id instrumental_id], name: :band_members_pi
    add_index :band_members, %i[instrumental_id], name: :band_members_i
  end
end
