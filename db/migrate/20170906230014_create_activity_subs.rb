class CreateActivitySubs < ActiveRecord::Migration[5.1]
  def change
    create_table :activity_subs do |t|
      t.string :key
      t.string :j_title
      t.string :e_title
      t.string :j_comment
      t.string :e_comment
      t.integer :activity_id
      t.integer :from_id
      t.integer :to_id
      t.integer :song_id
      t.integer :sort_order

      t.timestamps
    end
    add_index :activity_subs, %i[key], unique: true
    add_index :activity_subs, %i[activity_id sort_order], unique: true
    add_index :activity_subs, %i[from_id]
    add_index :activities, %i[song_id]
  end
end
