class CreateBands < ActiveRecord::Migration[5.1]
  def change
    create_table :bands do |t|
      t.string :key, null: false
      t.integer :title_id, null: false
      t.string :j_comment
      t.string :e_comment

      t.timestamps
    end
    add_index :bands, %i[key], unique: true
    add_index :bands, %i[title_id], unique: true
  end
end
