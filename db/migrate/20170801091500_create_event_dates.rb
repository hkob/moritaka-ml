class CreateEventDates < ActiveRecord::Migration[5.1]
  def change
    create_table :event_dates do |t|
      t.string :key
      t.date :date
      t.integer :year_id, null: false
      t.integer :month
      t.integer :day

      t.timestamps
    end
    add_index :event_dates, %i[key], unique: true
    add_index :event_dates, %i[year_id date month day]
    add_index :event_dates, %i[date]
  end
end
