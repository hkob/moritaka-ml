class CreateSongs < ActiveRecord::Migration[5.1]
  def change
    create_table :songs do |t|
      t.string :key, null: false
      t.integer :title_id, null: false
      t.integer :parent_id
      t.integer :event_date_id, null: false

      t.timestamps
    end
    add_index :songs, %i[key], unique: true
    add_index :songs, %i[event_date_id]
    add_index :songs, %i[parent_id]
    add_index :songs, %i[title_id], unique: true
  end
end
