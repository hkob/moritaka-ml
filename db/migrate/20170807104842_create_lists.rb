class CreateLists < ActiveRecord::Migration[5.1]
  def change
    create_table :lists do |t|
      t.string :key, null: false
      t.string :keyword, null: false
      t.integer :device_id
      t.integer :concert_id
      t.integer :book_id
      t.integer :sort_order, null: false

      t.timestamps
    end
    add_index :lists, %i[key], unique: true
    add_index :lists, %i[device_id sort_order]
    add_index :lists, %i[concert_id sort_order]
    add_index :lists, %i[book_id sort_order]
  end
end
