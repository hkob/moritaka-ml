class CreateHalls < ActiveRecord::Migration[5.1]
  def change
    create_table :halls do |t|
      t.string :key, null: false
      t.integer :sort_order, null: false
      t.integer :title_id, null: false
      t.integer :prefecture_id, null: false

      t.timestamps
    end
    add_index :halls, %i[key], unique: true
    add_index :halls, %i[title_id], unique: true
    add_index :halls, %i[prefecture_id sort_order], unique: true
  end
end
