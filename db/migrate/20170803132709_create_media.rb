class CreateMedia < ActiveRecord::Migration[5.1]
  def change
    create_table :media do |t|
      t.string :key, null: false
      t.string :medium_device, null: false
      t.string :code
      t.string :release
      t.boolean :now_sale
      t.string :price
      t.integer :sort_order, null: false
      t.integer :company_id
      t.integer :device_id, null: false

      t.timestamps
    end
    add_index :media, %i[key], unique: true
    add_index :media, %i[device_id sort_order]
    add_index :media, %i[company_id code]
  end
end
