class CreateConcertHalls < ActiveRecord::Migration[5.1]
  def change
    create_table :concert_halls do |t|
      t.integer :concert_id, null: false
      t.integer :sort_order, null: false
      t.integer :hall_id, null: false
      t.integer :event_date_id
      t.integer :list_id
      t.integer :device_id
      t.string :j_hall_sub
      t.string :e_hall_sub
      t.string :j_comment
      t.string :e_comment
      t.string :j_product
      t.string :e_product

      t.timestamps
    end
    add_index :concert_halls, %i[concert_id sort_order hall_id], unique: true
    add_index :concert_halls, %i[event_date_id]
    add_index :concert_halls, %i[list_id]
  end
end
