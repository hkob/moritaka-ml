class CreateActivities < ActiveRecord::Migration[5.1]
  def change
    create_table :activities do |t|
      t.string :key, null: false
      t.integer :activity_type, null: false
      t.string :j_comment
      t.string :e_comment
      t.integer :sort_order
      t.integer :title_id, null: false
      t.integer :song_id
      t.integer :company_id
      t.integer :from_id
      t.integer :to_id

      t.timestamps
    end
    add_index :activities, %i[key], unique: true
    add_index :activities, %i[activity_type sort_order], unique: true
    add_index :activities, %i[from_id]
  end
end
