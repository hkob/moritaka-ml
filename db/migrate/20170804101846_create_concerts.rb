class CreateConcerts < ActiveRecord::Migration[5.1]
  def change
    create_table :concerts do |t|
      t.string :key, null: false
      t.string :j_subtitle
      t.string :e_subtitle
      t.string :j_comment
      t.string :e_comment
      t.integer :concert_type, null: false
      t.boolean :has_song_list, null: false
      t.boolean :has_product, null: false
      t.integer :num_of_performances, null: false
      t.integer :num_of_halls, null: false
      t.integer :sort_order, null: false
      t.integer :title_id, null: false
      t.integer :from_id, null: false
      t.integer :to_id
      t.integer :band_id

      t.timestamps
    end
    add_index :concerts, %i[key], unique: true
    add_index :concerts, %i[title_id], unique: true
    add_index :concerts, %i[concert_type sort_order], unique: true
    add_index :concerts, %i[from_id]
  end
end
