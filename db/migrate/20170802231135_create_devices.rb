class CreateDevices < ActiveRecord::Migration[5.1]
  def change
    create_table :devices do |t|
      t.string :key, null: false
      t.string :type, null: false
      t.integer :device_type, null: false
      t.integer :minutes
      t.integer :seconds
      t.integer :sort_order, null: false
      t.string :number
      t.string :j_comment
      t.string :e_comment
      t.string :link
      t.integer :title_id, null: false
      t.integer :singer_id
      t.integer :event_date_id

      t.timestamps
    end
    add_index :devices, %i[event_date_id]
    add_index :devices, %i[singer_id]
    add_index :devices, %i[title_id]
    add_index :devices, %i[type device_type event_date_id]
  end
end
