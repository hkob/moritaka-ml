class CreateDeviceActivities < ActiveRecord::Migration[5.1]
  def change
    create_table :device_activities do |t|
      t.integer :device_id, null: false
      t.integer :activity_id, null: false

      t.timestamps
    end
    add_index :device_activities, %i[device_id activity_id], unique: true
    add_index :device_activities, %i[activity_id]
  end
end
