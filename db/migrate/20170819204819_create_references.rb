class CreateReferences < ActiveRecord::Migration[5.1]
  def change
    create_table :references do |t|
      t.string :key, null: false
      t.integer :reference_type, null: false
      t.string :link, null: false
      t.integer :title_id, null: false
      t.integer :sort_order, null: false

      t.timestamps
    end
    add_index :references, %i[key], unique: true
    add_index :references, %i[title_id], unique: true
    add_index :references, %i[reference_type sort_order], unique: true
  end
end
