class CreateYears < ActiveRecord::Migration[5.1]
  def change
    create_table :years do |t|
      t.integer :year, null: false

      t.timestamps
    end
    add_index :years, %i[year], unique: true
  end
end
