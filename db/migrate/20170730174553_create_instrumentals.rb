class CreateInstrumentals < ActiveRecord::Migration[5.1]
  def change
    create_table :instrumentals do |t|
      t.string :key, null: false
      t.integer :sort_order, null: false
      t.integer :title_id, null: false

      t.timestamps
    end
    add_index :instrumentals, %i[key], unique: true
    add_index :instrumentals, %i[sort_order], unique: true
    add_index :instrumentals, %i[title_id], unique: true
  end
end
