key = :album_new_season
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :new_season do
    {
      device_type: :album,
      event_date_id: '1987/7/25',
      minutes: 42,
      seconds: 25,
      sort_order: 198707250,
      number: '1st',
      singer_id: :iChisatoMoritaka,
      j_comment: 'デビューアルバム',
      e_comment: 'Debut Album',
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4101.html'
    }
  end

  album.media_from_array [
    ["#{key}_lp", :lp, 'K-12533', :first, false, :warner_pioneer, '2,800'],
    ["#{key}_ct", :ct, 'LKF-8176', :first, false, :warner_pioneer, '2,800'],
    ["#{key}_cd1", :cd, '32XL-216', :first, false, :warner_pioneer, '3,200 3,008'],
    ["#{key}_cd2", :cd, 'WPCL-502', :second, true, :warner_music_japan, '2,400'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/new-season/id528972314', :first, true, :warner_music_japan, '1,900'],
  ]

  list = list_factory("#{key}_common") { {device_id: album, keyword: :common, sort_order: 1} }

  list.list_contents_from_array [
    [:namida_good_bye, %i[VOCAL iChisatoMoritaka ARRANGE iWise GUITAR iKazuhikoIwami KEYBOARDS iYasuakiMaejima KEYBOARDS iYoichiroKakizaki BASS iHiroshiSawada DRUMS iMansakuKimura PERCUSSION iShingoKanno SAXOPHONE iKazuoKamata CHORUS iTheThreePinkies SYNPR iTomoakiArima FRSOLO iChisatoMoritaka]],
    [:yumeno_owari, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito GUITAR iHideoSaito KEYBOARDS iNobitaTsukada CYMBALS iRyoSaito TOMS iRyoSaito SIMMONS iRyoSaito CHORUS iHideoSaito]],
    [:ringoshuno_rule, %i[VOCAL iChisatoMoritaka ARRANGE iKenShima GUITAR iAkiraWada KEYBOARDS iKenShima BASS iKenjiTakamizu DRUMS iShuichiPONTAMurakami PERCUSSION iShingoKanno SAXOPHONE iShigeoFuchino CHORUS iTheThreePinkies STRINGS iMAEDAStrings SYNPR iTakayoshiUmeno]],
    [:otisreddingni_kanpai, %i[VOCAL iChisatoMoritaka ARRANGE iHiromichiTsugaki KEYBOARDS iHiromichiTsugaki SAXOPHONE iShigeoFuchino TROMBONE iHarumiMita CHORUS iTheThreePinkies SYNPR iTomoakiArima]],
    [:ways, %i[VOCAL iChisatoMoritaka ARRANGE iHiromichiTsugaki KEYBOARDS iHiromichiTsugaki CHORUS iTheThreePinkies SYNPR iTomoakiArima]],
    [:period, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito GUITAR iHideoSaito KEYBOARDS iNobitaTsukada CYMBALS iRyoSaito TOMS iRyoSaito SIMMONS iRyoSaito CHORUS iHideoSaito], '(アルバム・ヴァージョン)', '(Album version)'],
    [:anohino_photograph, %i[VOCAL iChisatoMoritaka ARRANGE iKenShima APIANO iChisatoMoritaka GUITAR iMakotoMatsushita KEYBOARDS iKenShima BASS iYasuoTomikura DRUMS iEijiShimamura PERCUSSION iNobuSaito STRINGS iTOMODAStrings SYNPR iTakayoshiUmeno]],
    [:miss_lady, %i[VOCAL iChisatoMoritaka ARRANGE iWise GUITAR iKazuhikoIwami KEYBOARDS iYasuakiMaejima BASS iHiroshiSawada DRUMS iMansakuKimura PERCUSSION iShingoKanno SAXOPHONE iKazuoKamata TROMBONE iKoyoMurakami BGVOCAL iHiromasaIjichi SYNPR iTomoakiArima]],
    [ :new_season, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito SKEYSOLO iChisatoMoritaka GUITAR iHideoSaito KEYBOARDS iNobitaTsukada KEYBOARDS iYasuhikoFukuda BASS iChiharuMikuzuki DRUMS iRyubenTsujino CHORUS iHideoSaito BGVOCAL iYukariFujio], '(ロング・ヴァージョン)', '(long version)']
  ]
end
