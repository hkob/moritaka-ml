key = :album_thema_songs80_96
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :thema_songs80_96 do
    {
      device_type: :lyric_album,
      event_date_id: '1996/9/10',
      sort_order: 199609100,
      singer_id: :iAkikoWada
    }
  end

  album.media_from_array [
    ["#{key}_cd", :cd, 'WPC6-8228', :first, false, :wea_japan, '3,000'],
  ]

  va = %i[VOCAL iAkikoWada]
  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_with_performer(va, 'Mother') +
    [[:saa_boukenda, %i[VOCAL iAkikoWada ARRANGE iKomeKomeClub]]] +
    ListContent.title_with_performer(va, '約束の夢', '{YAKUSOKU-NO YUME}') +
    ListContent.title_with_performer(va, 'やじろべえ', '{YAJIROBEE}') +
    ListContent.title_with_performer(va, '愛、とどきますか', '{AI, TODOKIMASUKA}') +
    ListContent.title_with_performer(va, '黄昏に歌わせて', '{TASOGARE-NI UTAWASETE}') +
    ListContent.title_with_performer(va, 'あの鐘を鳴らすのはあなた', '{ANO KANE-O NARASUNO-HA ANATA}') +
    ListContent.title_with_performer(va, 'TIME GOES BY') +
    ListContent.title_with_performer(va, 'ダ・ダ・ダ・ダ・ダイエット［アッコにおまかせ合唱団］', 'DA DA DA Diet') +
    ListContent.title_with_performer(va, '抱擁', '{HOYO}') +
    ListContent.title_with_performer(va, '日曜日のきまぐれ', '{NICHIYOBI-NO KIMAGURE}') +
    ListContent.title_with_performer(va, 'バ・カ・ダ・ネ', '{BA.KA.DA.NE}') +
    ListContent.title_with_performer(va, '君が野に咲くバラなら', '{KIMI-GA NO-NI SAKU BARA-NARABA}') +
    ListContent.title_with_performer(va, '風の見える町', '{KAZE-NO MIERU MACHI}')
  )
end

