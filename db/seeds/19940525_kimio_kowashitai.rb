key = :single_kimio_kowashitai
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :kimio_kowashitai do
    {
      device_type: :part_single,
      event_date_id: '1994/5/25',
      sort_order: 199405250,
      number: '9th',
      singer_id: :iHaruhikoAndo,
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'PODH-1205', '1st', false, :polydor, '1,000'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:kimio_kowashitai, %i[VOCAL iHaruhikoAndo ARRANGE iHaruhikoAndo ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka]],
    [:kiss, %i[VOCAL iHaruhikoAndo ARRANGE iHaruhikoAndo ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka]],
    [:kimio_kowashitai, %i[VOCAL iKaraoke ARRANGE iHaruhikoAndo ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka], '[オリジナル・カラオケ]', '[Original karaoke]']
  ]
end

