key = :single_umimade_5fun
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :umimade_5fun do
    {
      device_type: :single,
      event_date_id: '1998/7/15',
      sort_order: 199807150,
      number: '36th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4294.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDA-1004', '1st', false, :zetima, '1,020'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:umimade_5fun, %i[ARRANGE iYasuakiMaejima VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka STEELDRUMS iChisatoMoritaka CHORUS iChisatoMoritaka AGUITAR iYuichiTakahashi KEYBOARDS iYasuakiMaejima EGUITAR iMasahiroInaba CONGAS iGenOgimi TIMBALES iGenOgimi SHAKER iGenOgimi PROGRAMMING iYasuakiMaejima TRUMPET iToshioAraki TRUMPET iAkiraOkumura TROMBONE iYohichiMurata TENORSAX iTakuoYamamoto]],
    [:sleepless_night_blues, %i[DRUMS iChisatoMoritaka AGUITAR iYuichiTakahashi PROGRAMMING iYuichiTakahashi APIANO iYasuakiMaejima EGUITAR iYukioSeto]],
    [:umimade_5fun, %i[VOCAL iKaraoke DRUMS iChisatoMoritaka STEELDRUMS iChisatoMoritaka CHORUS iChisatoMoritaka AGUITAR iYuichiTakahashi KEYBOARDS iYasuakiMaejima EGUITAR iMasahiroInaba CONGAS iGenOgimi TIMBALES iGenOgimi SHAKER iGenOgimi PROGRAMMING iYasuakiMaejima TRUMPET iToshioAraki TRUMPET iAkiraOkumura TROMBONE iYohichiMurata TENORSAX iTakuoYamamoto], '(オリジナル・カラオケ)', '(Original karaoke)']
]
  device_activity_factory single, :thema_song_umimade_5fun
end



