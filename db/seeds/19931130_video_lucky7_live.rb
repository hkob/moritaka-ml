key = :video_lucky7_live
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key, :lucky7_live do
    {
      device_type: :live_video,
      event_date_id: '1994/2/25',
      minutes: 108,
      sort_order: 199402250,
      number: '6th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4335.html'
    }
  end

  video.media_from_array [
    ["#{key}_vhs1", :vhs, 'EPVA-1', :first, false, :warner_music_japan, '5,000'],
    ["#{key}_ld1", :ld, 'EPLA-1', :first, false, :warner_music_japan, '5,000'],
  ]

  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }
  list.list_contents_from_array ListContent.title_only('オープニング', 'Opening') + [
    [:lucky7_blues, [], '(INSTRUMENTAL)', '(INSTRUMENTAL)'],
    [:teo_tatako, @vc],
    [:rockn_roll_kencho_shozaichi, @vc],
    [:fight, @vc],
    [:jimina_onna, @vc],
    [:tomodachino_kare, @vc],
    [:writer_shibo, @vc],
    [:michi, @vc],
    [:the_stress, @vc],
    [:watashino_natsu, @vc],
    [:kazeni_fukarete, @vc],
    [:watarasebashi, @vc],
    [:sayonara_watashino_koi, @vc],
    [:i_love_you, @vc],
    [:haeotoko, @vc],
    [:watashiga_obasanni_nattemo, @vc],
    [:teriyaki_burger, @vc],
    [:ame, @vc],
    [:memories, @vc],
    [:konomachi, @vc],
    [:concert_no_yoru, @vc],
    [:watarasebashi, @vc],
  ]

  concert = concert_factory :tour_lucky7
  concert_hall = concert_hall_factory concert, 60
  concert_list = list_factory(:tour_lucky7_D)
  concert_list_contents = concert_list.list_contents.order_sort_order

  csos = { 1 => 0, 2 => 1, 3 => 2, 4 => 3, 5 => 5, 6 => 6, 7 => 7, 8 => 9, 9 => 10, 10 => 11, 11 => 13, 12 => 14, 13 => 15, 14 => 16, 15 => 18, 16 => 19, 17 => 20, 18 => 22, 19 => 23, 20 => 25, 21 => 27 }
  list.list_contents.order_sort_order.each_with_index do |vsls, i|
    concert_video_factory concert_list_contents[csos[i]], vsls, concert_hall if csos[i]
  end
end

