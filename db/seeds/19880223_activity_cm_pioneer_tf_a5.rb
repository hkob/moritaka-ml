pkey = :cm_pioneer
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  company_factory :pioneer
  activity_factory pkey do
    {
      activity_type: :cm,
      title_id: 'exist|pioneer',
      company_id: :pioneer,
      sort_order: 198802230
    }
  end
end

key = :cm_pioneer_tf_a5
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_pioneer,
      j_title: 'パイオニア 「留守番テレフォン TF-A5」',
      e_title: "PIONEER 'telephone system with answering service TF-A5",
      from_id: '1988/2/23',
      to_id: '1988/11',
      song_id: :mi_ha_,
      sort_order: 198802230
    }
  end
end
