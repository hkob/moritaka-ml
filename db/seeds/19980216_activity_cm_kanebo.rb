pkey = :cm_kanebo
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  company_factory :kanebo
  activity_factory pkey do
    {
      activity_type: :cm,
      title_id: 'exist|kanebo',
      company_id: :kanebo,
      sort_order: 199807010,
    }
  end
end

key = :cm_kanebo_sala
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_kanebo,
      j_title: 'カネボウ「SALA」',
      e_title: 'Kanebo "SALA"',
      from_id: '1998/2/16',
      to_id: '1998/7',
      sort_order: 199807010,
      song_id: :telephone,
      j_comment: '「いいシャンプーで洗った」編 [15 秒]|「いいシャンプーで洗おう」編 [15 秒]|沖縄にて撮影',
      e_comment: '{II SHAMPU-DE ARATTA} version [15 seconds]|{II SHAMPU-DE ARAOU} version [15 seconds]|Make a film in Okinawa',
    }
  end
end

key = :cm_kanebo_none
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_kanebo,
      j_title: 'カネボウ「海のうるおい藻」',
      e_title: 'Kanebo "{UMI-NO URUOI SOU}"',
      from_id: '2001/3',
      to_id: '2004/3',
      sort_order: 200103010,
    }
  end
end



