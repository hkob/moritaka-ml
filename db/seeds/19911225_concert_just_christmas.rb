key = :live_just_christmas
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  concert = concert_factory key, :just_christmas do
    {
      concert_type: :live,
      from_id: '1991/12/25',
      has_song_list: true,
      sort_order: 199112250,
      num_of_performances: 1,
      num_of_halls: 1,
    }
  end

  list = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }
  list.list_contents_from_array %i[hitorigurashi period good_bye_season kondo_watashi_dokoka aru_olno_seishun seishun alone seventeen benkyono_uta fight funkey_monkey_baby sonogono_watashi get_smile teriyaki_burger] + @encore + %i[ame mi_ha_] + @konomachi_home_mix + @double_encore + %i[new_season]

  concert.concert_halls_from_array [
    ['1991/12/25', :横浜文化体育館, list],
  ]
end


