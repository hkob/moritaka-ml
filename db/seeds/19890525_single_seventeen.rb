key = :single_seventeen
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :seventeen do
    {
      device_type: :single,
      event_date_id: '1989/5/25',
      sort_order: 198905250,
      number: '7th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4167.html'
    }
  end

  single.media_from_array [
    ["#{key}_ct", :ct, '09L5-4084', '1st', false, :warner_pioneer, '937'],
    ["#{key}_cds1", :cds, '09L3-4084', '1st', false, :warner_pioneer, '937'],
    ["#{key}_cds2", :cds, '09L3-4084', '2nd', false, :warner_music_japan, '937'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/17cai-single/id528969258', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }

  list.list_contents_from_array [
    [:seventeen, @vcah],
    [:hatachi, @vcah]
  ]
end
