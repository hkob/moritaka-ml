key = :album_step_by_step
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :step_by_step do
    {
      device_type: :album,
      event_date_id: '1994/7/25',
      minutes: 53,
      seconds: 30,
      sort_order: 199407250,
      number: '11th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4127.html',
      j_comment: '初回限定 32ページカラー写真集',
      e_comment: '32 page photo book(1st lot only)',
    }
  end

  album.media_from_array [
    ["#{key}_ct", :ct, 'EPTA-7002', :first, false, :one_up_music, '2,900'],
    ["#{key}_cd1", :cd, 'EPCA-7002', :first, false, :one_up_music, '2,900'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/step-by-step/id278884152', :first, true, :up_front_works, '2,000'],
  ]

  list_cd = list_factory("#{key}_cd") { {device_id: album, keyword: :cd, sort_order: 1} }
  list_ct = list_factory("#{key}_ct") { {device_id: album, keyword: :ct, sort_order: 2} }
  performer = %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito]
  list_cd.list_contents_from_array [
    [:kibun_soukai, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka GUITAR iYuichiTakahashi CHORUS iYuichiTakahashi GUITAR iHiroyoshiMatsuo BASS iMasafumiYokoyama], '(Album Version)', '(Album Version)'],
    [:wakasano_hiketsu, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka GUITARSOLO iChisatoMoritaka SHAKER iChisatoMoritaka TAMBOURINE iChisatoMoritaka APF iChisatoMoritaka GUITAR iYuichiTakahashi SYNTHESIZER iYuichiTakahashi CHORUS iYuichiTakahashi RGUITAR iYukoNohji APF iYasuakiMaejima BASS iYukioSeto]],
    [:zuruyasumi, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka APF iChisatoMoritaka GUITAR iYuichiTakahashi BASS iYuichiTakahashi SYNTHESIZER iYuichiTakahashi CHORUS iYuichiTakahashi GUITARSOLO iHiroyoshiMatsuo]],
    [:otokonara, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito SITAR iHideoSaito SYNTHESIZER iHideoSaito]],
    [:natsuno_hi, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka GUITAR iHideoSaito KEYBOARDS iHideoSaito CHORUS iHideoSaito TAMBOURINE iHideoSaito]],
    [:everybodys_got_something, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka GUITAR iYuichiTakahashi BELL iSeijiIshikawa BASS iYukioSeto]],
    [:torikago, %i[VOCAL iChisatoMoritaka ARRANGE iYasuakiMaejima ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka APF iYasuakiMaejima SYNTHESIZER iYasuakiMaejima GUITAR iYuichiTakahashi GUITAR iHiroyoshiMatsuo BASS iYukioSeto]],
    [:hoshino_ojisama, %i[VOCAL iChisatoMoritaka ARRANGE iYasuakiMaejima DRUMS iChisatoMoritaka APF iYasuakiMaejima EPF iYasuakiMaejima SYNTHESIZER iYasuakiMaejima GUITAR iYukioSeto]],
    [:watashino_daijina_hito, %i[VOCAL iChisatoMoritaka ARRANGE iYasuakiMaejima DRUMS iChisatoMoritaka APF iYasuakiMaejima EPF iYasuakiMaejima SYNTHESIZER iYasuakiMaejima]],
    [:ichido_asobini_kiteyo, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito TAMBOURINE iHideoSaito SYNTHESIZER iHideoSaito APF iYasuakiMaejima]],
    [:office_gaino_koi, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka GUITAR iYuichiTakahashi SYNTHESIZER iYuichiTakahashi CHORUS iYuichiTakahashi EPF iYasuakiMaejima BASS iYukioSeto GUITAR iYukioSeto]],
    [:taifu, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka APF iChisatoMoritaka GUITAR iYuichiTakahashi GUITARSOLO iHiroyoshiMatsuo GUITAR iEijiOgata APF iHiyoshimaru BASS iYukioSeto SYNTHESIZER iYukioSeto]],
    [:kazeni_fukarete, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka BASS iHideoSaito SYNTHESIZER iHideoSaito TAMBOURINE iHideoSaito AGUITAR iHideoSaito AGUITAR iHiroyoshiMatsuo AGUITAR iYuichiTakahashi AGUITAR iJunTakahashi]],
    [:step_by_step_kareno_jinsei, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka APF iChisatoMoritaka GUITAR iYuichiTakahashi APF iYuichiTakahashi SYNTHESIZER iYuichiTakahashi BASS iYukioSeto]],
  ]
  list_ct.list_contents_from_array [
    [:kibun_soukai, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka GUITAR iYuichiTakahashi CHORUS iYuichiTakahashi GUITAR iHiroyoshiMatsuo BASS iMasafumiYokoyama], '(Album Version)', '(Album Version)'],
    [:wakasano_hiketsu, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka GUITARSOLO iChisatoMoritaka SHAKER iChisatoMoritaka TAMBOURINE iChisatoMoritaka APF iChisatoMoritaka GUITAR iYuichiTakahashi SYNTHESIZER iYuichiTakahashi CHORUS iYuichiTakahashi RGUITAR iYukoNohji APF iYasuakiMaejima BASS iYukioSeto]],
    [:zuruyasumi, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka APF iChisatoMoritaka GUITAR iYuichiTakahashi BASS iYuichiTakahashi SYNTHESIZER iYuichiTakahashi CHORUS iYuichiTakahashi GUITARSOLO iHiroyoshiMatsuo]],
    [:otokonara, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito SITAR iHideoSaito SYNTHESIZER iHideoSaito]],
    [:natsuno_hi, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka GUITAR iHideoSaito KEYBOARDS iHideoSaito CHORUS iHideoSaito TAMBOURINE iHideoSaito]],
    [:everybodys_got_something, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka GUITAR iYuichiTakahashi BELL iSeijiIshikawa BASS iYukioSeto]],
    [:torikago, %i[VOCAL iChisatoMoritaka ARRANGE iYasuakiMaejima ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka APF iYasuakiMaejima SYNTHESIZER iYasuakiMaejima GUITAR iYuichiTakahashi GUITAR iHiroyoshiMatsuo BASS iYukioSeto]],
    [:hoshino_ojisama, %i[VOCAL iChisatoMoritaka ARRANGE iYasuakiMaejima DRUMS iChisatoMoritaka APF iYasuakiMaejima EPF iYasuakiMaejima SYNTHESIZER iYasuakiMaejima GUITAR iYukioSeto]],
    [:watashino_daijina_hito, %i[VOCAL iChisatoMoritaka ARRANGE iYasuakiMaejima DRUMS iChisatoMoritaka APF iYasuakiMaejima EPF iYasuakiMaejima SYNTHESIZER iYasuakiMaejima]],
    [:ichido_asobini_kiteyo, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito TAMBOURINE iHideoSaito SYNTHESIZER iHideoSaito APF iYasuakiMaejima]],
    [:office_gaino_koi, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka GUITAR iYuichiTakahashi SYNTHESIZER iYuichiTakahashi CHORUS iYuichiTakahashi EPF iYasuakiMaejima BASS iYukioSeto GUITAR iYukioSeto]],
    [:taifu, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka APF iChisatoMoritaka GUITAR iYuichiTakahashi GUITARSOLO iHiroyoshiMatsuo GUITAR iEijiOgata APF iHiyoshimaru BASS iYukioSeto SYNTHESIZER iYukioSeto]],
    [:kazeni_fukarete, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka BASS iHideoSaito SYNTHESIZER iHideoSaito TAMBOURINE iHideoSaito AGUITAR iHideoSaito AGUITAR iHiroyoshiMatsuo AGUITAR iYuichiTakahashi AGUITAR iJunTakahashi]],
    [:step_by_step_kareno_jinsei, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka APF iChisatoMoritaka GUITAR iYuichiTakahashi APF iYuichiTakahashi SYNTHESIZER iYuichiTakahashi BASS iYukioSeto]],
    [:kibun_soukai, %i[VOCAL iKaraoke ARRANGE iYuichiTakahashi], '(オリジナル・カラオケ)', '(Original Karaoke)'],
    [:kazeni_fukarete, %i[VOCAL iKaraoke ARRANGE iHideoSaito], '[オリジナル・カラオケ]', '[Original Karaoke]'],
  ]
end

