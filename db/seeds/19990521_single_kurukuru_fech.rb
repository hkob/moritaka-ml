key = :single_kurukuru_fech
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :kurukuru_fech do
    {
      device_type: :part_single,
      event_date_id: '1999/5/21',
      sort_order: 199905210,
      number: '3rd',
      singer_id: :iCoil,
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'TECN-12551', '1st', false, :teichiku_records, '1,200'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  vc = %i[VOCAL iCoil]
  list.list_contents_from_array [
    [:kurukuru_fech, %i[VOCAL iCoil DRUMS iChisatoMoritaka]],
    [nil, vc, nil, nil, '(仮) タイパニック', '({KARI}) {TAIPANIKKU}'],
    [nil, vc, nil, nil, 'Cloudy', 'Cloudy']
  ]
end




