key = :video_clip_the_mi_ha_
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key, :the_mi_ha_special_mi_ha_mix do
    {
      device_type: :video_clip,
      event_date_id: '1988/9/10',
      minutes: 7,
      sort_order: 198809100,
      number: '1st',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4311.html'
    }
  end

  video.media_from_array [
    ["#{key}_vhs1", :vhs, '12L9-8002', :first, false, :warner_pioneer, '1,236'],
    ["#{key}_beta", :beta, '12L8-8002', :first, false, :warner_pioneer, '1,236'],
    ["#{key}_vhs2", :vhs, '12L9-8002', :second, false, :warner_music_japan, '1,236 1,200'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/music-video/za-miha-supesharu-miha-mikkusu/id534978390', :first, true, :warner_music_japan, 400],
  ]

  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }

  list.list_contents_from_array [
    [:the_mi_ha_, %i[VOCAL iChisatoMoritaka EDRUMS iChisatoMoritaka], '(スペシャル・ミーハー・ミックス)', '[Special {MI-HA-} mix]']
  ]
end
