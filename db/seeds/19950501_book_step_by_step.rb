key = :picture_book_step_by_step
obj = Book.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  book = book_factory key do
    {
      book_type: :picture_book,
      publisher_id: :up_front_books,
      seller_id: :wani_books,
      photographer_id: :iMasashigeOgata,
      isbn: '4-8275-1339-2 C0072 P2300E',
      price: '2,300',
      event_date_id: '1995/5/1',
      sort_order: 199505010,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4469.html'
    }
  end
end
