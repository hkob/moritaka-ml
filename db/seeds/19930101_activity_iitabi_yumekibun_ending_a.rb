pkey = :thema_song_iitabi_yumekibun
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_factory pkey do
    {
      activity_type: :thema_song,
      title_id: %q(旅行番組 「いい旅・夢気分」|TV program (travel) "{II-TABI YUME-KIBUN}|いいたび　ゆめきぶん),
      company_id: :tv_tokyo,
      sort_order: 199301010
    }
  end
end

key = :thema_song_iitabi_yumekibun_a
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :thema_song_iitabi_yumekibun,
      j_title: '旅行番組 「いい旅・夢気分」エンディングテーマ',
      e_title: 'Ending thema song of TV program (travel) "{II-TABI YUME-KIBUN}',
      from_id: '1993',
      to_id: '1993',
      song_id: :watarasebashi,
      sort_order: 199301010
    }
  end
end


