key = :single_natsuno_hi
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :natsuno_hi do
    {
      device_type: :single,
      event_date_id: '1994/5/10',
      sort_order: 199405100,
      number: '23rd',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4248.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDA-4', '1st', false, :one_up_music, '1,000'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/xiano-ri-single/id531611154', '1st', true, :warner_music_japan, '750']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:natsuno_hi, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito KEYBOARDS iHideoSaito CHORUS iHideoSaito TAMBOURINE iHideoSaito]],
    [:kowai_yume, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito APIANO iHideoSaito]],
    [:natsuno_hi, %i[VOCAL iKaraoke ARRANGE iHideoSaito DRUMS iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito KEYBOARDS iHideoSaito CHORUS iHideoSaito TAMBOURINE iHideoSaito], '[オリジナル・カラオケ]', '[Original karaoke]'],
  ]
  device_activity_factory single, :thema_song_asakusabashi_young
end
