key = :tour_the_moritaka
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  band = band_factory(:janet_jacksons)
  band.band_members_from_array %i[VOCAL iChisatoMoritaka GUITAR iHiroyoshiMatsuo BASS iMasafumiYokoyama DRUMS iMakotoGeorgeYoshihara KEYBOARDS iShinKohno GUITAR iShinKohno KEYBOARDS iYasuakiMichaelMaejima]

  concert = concert_factory key do
    {
      j_subtitle: "森高千里サマーコンサートツアー'91",
      e_subtitle: "Chisato Moritaka summer concert tour'91",
      concert_type: :tour,
      from_id: '1991/7/22',
      to_id: '1991/8/28',
      has_song_list: true,
      sort_order: 199107220,
      num_of_performances: 11,
      num_of_halls: 8,
    }
  end

  list_a, list_b = %i[A B].map.with_index { |k, i| list_factory("#{key}_#{k}") { {concert_id: concert, keyword: k, sort_order: i+1} } }
  list_a.list_contents_from_array %i[kanojo mijikai_natsu the_benkyono_uta seishun aru_olno_seishun the_mi_ha_] + [[nil, [], '', '', '夏の歌', 'A summer song']] + [[:ame, [], '[ロック・ヴァージョン]', '[Rock version]']] + %i[the_nozokanaide the_stress] + [[:daite, [], '[ラスベガス・ヴァージョン]', '[Las Vegas version]']] + [[:kusaimononiwa_futaoshiro, [], '[おじさんヴァージョン]', '[{OJISAN} version]']] + %i[seventeen sonogono_watashi yoruno_entotsu] + [[:get_smile, [], '[コンサート・アレンジ・ヴァージョン]', '[Concert arrange version]']] + @encore + %i[hachigatsuno_koi funkey_monkey_baby teriyaki_burger] + @double_encore + [[:konomachi, [], '[ザ・森高・ヴァージョン + ホーム・ミックス]', '[The Moritaka version + Home Mix]']]
  list_b.list_contents_from_array %i[kanojo mijikai_natsu the_benkyono_uta seishun aru_olno_seishun the_mi_ha_] + [[nil, [], '', '', '夏の歌', 'A summer song']] + [[:ame, [], '[ロック・ヴァージョン]', '[Rock version]']] + %i[the_nozokanaide the_stress new_season] + [[:kusaimononiwa_futaoshiro, [], '[おじさんヴァージョン]', '[{OJISAN} version]']] + %i[sonogono_watashi yoruno_entotsu] + [[:get_smile, [], '[コンサート・アレンジ・ヴァージョン]', '[Concert arrange version]']] + @encore + %i[hachigatsuno_koi funkey_monkey_baby teriyaki_burger] + @double_encore + [[:konomachi, [], '[ザ・森高・ヴァージョン + ホーム・ミックス]', '[The Moritaka version + Home Mix]']]

  concert.concert_halls_from_array [
    ['1991/7/22', :渋谷公会堂, list_a],
    ['1991/7/23', :渋谷公会堂, list_b],
    ['1991/7/25', :名古屋市民会館, list_b],
    ['1991/7/28', :神戸国際会館, list_b],
    ['1991/8/1', :北海道厚生年金会館, list_b],
    ['1991/8/7', :東京厚生年金会館, list_b],
    ['1991/8/12', :神奈川県民ホール, list_b],
    ['1991/8/16', :メルパルクホール福岡, list_b],
    ['1991/8/21', :渋谷公会堂, list_b],
    ['1991/8/22', :渋谷公会堂, list_b, nil, nil, '<「ザ・勉強の歌」ビデオ収録>', '<Video recording(The Study Song, {BENKYO-NO UTA})>'],
    ['1991/8/28', :大阪厚生年金会館, list_b],
  ]
end




