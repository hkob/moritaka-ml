key = :cm_pioneer_tf_a50
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_pioneer,
      j_title: 'パイオニア 「留守番電話 TF-A50」',
      e_title: 'Pioneer "telephone system with answering service TF-A50',
      from_id: '1988/11',
      to_id: '1990/2/28',
      song_id: :stress,
      sort_order: 198811010
    }
  end
end

