pkey = :cm_kirin_beverage
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  company_factory :kirin_beverage
  activity_factory pkey do
    {
      activity_type: :cm,
      title_id: 'exist|kirin_beverage',
      company_id: :kirin_beverage,
      sort_order: 199901010,
    }
  end
end

key = :cm_kirin_beverage_watashinoyouni
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_kirin_beverage,
      j_title: 'キリンビバレッジ「ナチュラルズ」',
      e_title: 'Kirin beverage',
      from_id: '1999/1',
      to_id: '1999/3',
      sort_order: 199901010,
      song_id: :watashinoyouni
    }
  end
end

