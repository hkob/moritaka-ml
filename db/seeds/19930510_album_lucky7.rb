key = :album_lucky7
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :lucky7 do
    {
      device_type: :album,
      event_date_id: '1993/5/10',
      minutes: 60,
      seconds: 00,
      sort_order: 199305100,
      number: '10th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4030.html',
      j_comment: '初回限定 32ページカラー写真集',
      e_comment: '32 page photo book(1st lot only)',
    }
  end

  album.media_from_array [
    ["#{key}_ct", :ct, 'WPCT-760', :first, false, :warner_music_japan, '2,900'],
    ["#{key}_cd1", :cd, 'WPCL-760', :first, false, :warner_music_japan, '2,900'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/lucky-7/id278605574', :first, true, :warner_music_japan, '2,000'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  performer = %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito]
  list.list_contents_from_array [
    [:teo_tatako, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PIANO iChisatoMoritaka CHORUS iChisatoMoritaka GUITARSOLO iChisatoMoritaka GUITAR iYuichiTakahashi BASS iYukioSeto]],
    [:tankiwa_sonki, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka PIANO iChisatoMoritaka GUITAR iYuichiTakahashi BASS iYuichiTakahashi SYNTHESIZER iYuichiTakahashi]],
    [:hareta_nichiyoubi, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka PIANO iChisatoMoritaka SYNTHESIZER iYuichiTakahashi GUITAR iYukioSeto]],
    [:jimina_onna, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito SYNTHESIZER iHideoSaito]],
    [:toi_mukashi, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka PIANO iChisatoMoritaka GUITAR iYuichiTakahashi SYNTHESIZER iYuichiTakahashi BASS iYukioSeto]],
    [:bassari_yatteyo, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka PIANO iChisatoMoritaka GUITAR iYuichiTakahashi SYNTHESIZER iYuichiTakahashi CHORUS iYuichiTakahashi BASS iYukioSeto]],
    [:watashino_natsu, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka PIANO iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito SYNTHESIZER iHideoSaito], '(アルバム・ヴァージョン)', '(Album version)'],
    [:i_love_you, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PIANO iChisatoMoritaka GUITAR iYuichiTakahashi SYNTHESIZER iYuichiTakahashi CHORUS iYuichiTakahashi]],
    [:haeotoko, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PIANOLC iChisatoMoritaka GUITAR iYuichiTakahashi GUITAR iEijiOgata PIANORC iShinHashimoto BASS iYukioSeto]],
    [:watarasebashi, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka PIANO iChisatoMoritaka RECORDER iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito TAMBOURINE iHideoSaito SYNTHESIZER iHideoSaito]],
    [:sayonara_watashino_koi, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito PIANO iHideoSaito SYNTHESIZER iHideoSaito AGUITAR iJunTakahashi]],
    [:tomodachino_kare, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka GUITAR iYuichiTakahashi BASS iYuichiTakahashi SYNTHESIZER iYuichiTakahashi GUITAR iYukioSeto]],
    [:otokono_roman, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka PIANO iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito ORGAN iHideoSaito SYNTHESIZER iHideoSaito]],
    [:memories, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito TOMTOMSOLO iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito SYNTHESIZER iHideoSaito]],
  ]
end


