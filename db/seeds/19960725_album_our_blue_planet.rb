key = :album_our_blue_planet
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :our_blue_planet do
    {
      device_type: 'compilation_album',
      event_date_id: '1996/7/25',
      minutes: 61,
      seconds: 35,
      singer_id: :iVariousArtists,
      sort_order: 199607250,
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'PHCL-5027', :first, false, :mercury_music_entertainment, '3,000'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_only('Crazy Shopper / 松田聖子', 'Crazy Shopper / Seiko Matsuda') +
    ListContent.title_only('STEP BY STEP / ZIGGY', 'STEP BY STEP / ZIGGY') +
    [[:futariwa_koibito, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito]]] +
    ListContent.title_only('ズルい女 / シャ乱Q', '{ZURUI-ONNA} / Sharam Q') +
    ListContent.title_only('ゆずれない願い / 田村直美', '{YUZURENAI NEGAI} / Naomi Tamura') +
    ListContent.title_only('夏の午後 / 小林武史', '{NATSU-NO GOGO} / Takeshi Kobayashi') +
    ListContent.title_only('あじさい / サニーデイ・サービス', '{AJISAI} / Sunnyday Service') +
    ListContent.title_only('木蘭の涙 / スターダスト・レビュー', '{MOKUREN-NO NAMIDA} / Stardust Revue') +
    ListContent.title_only('愛までもうすぐだから / 織田祐二', '{AI-MADE MOUSUGU-DAKARA} / Yuji Oda') +
    ListContent.title_only('東京に来い / KAN', '{Tokyo-NI KOI} / KAN') +
    ListContent.title_only('NAKED BLUE / CRAZE', 'NAKED BLUE / CRAZE') +
    ListContent.title_only('本気でも嘘でもいい / BEREEVE', '{HONKI-DEMO USO-DEMO II} / BEREEVE') +
    ListContent.title_only('Winter Kiss / DUAL DREAM', 'Winter Kiss / DUAL DREAM') +
    ListContent.title_only('高速道路の星 / 王様', '{KOSOKUDORO-NO HOSHI} / Osama')
  )
end

