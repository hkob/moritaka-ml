key = :album_minna_daisuki
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :minna_daisuki do
    {
      device_type: :part_album,
      event_date_id: '1997/11/1',
      minutes: 63,
      seconds: 38,
      sort_order: 199711010,
      number: '25th',
      singer_id: :iTakuroYoshida
    }
  end

  album.media_from_array [
    ["#{key}_cd", :cd, 'FLCF-3702', :first, false, :for_life_records, '2,854'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  vt = %i[VOCAL iTakuroYoshida]
  list.list_contents_from_array(
    ListContent.title_with_performer(vt, '伽草子', '{OTOGI-ZOSHI}') +
    ListContent.title_with_performer(vt, '結婚しようよ', '{KEKKON SHIYOUYO}') +
    ListContent.title_with_performer(vt, 'おきざりにした悲しみは', '{OKIZARI-NI SHITA KANASHIMI-HA}') +
    ListContent.title_with_performer(vt, 'こっちを向いてくれ', '{KOCCHI-O MUITEKURE}') +
    ListContent.title_with_performer(vt, '旅の宿', '{TABI-NO YADO}') +
    ListContent.title_with_performer(vt, 'ひらひら', '{HIRAHIRA}') +
    ListContent.title_with_performer(vt, '春だったね', '{HARU-DATTANE}') +
    [[:wagayoki_tomoyo, %i[VOCAL iTakuroYoshida DRUMS iChisatoMoritaka]]] +
    ListContent.title_with_performer(vt, 'せんこう花火', '{SENKO-HANABI}') +
    ListContent.title_with_performer(vt, 'マーク II', 'Mark II') +
    ListContent.title_with_performer(vt, 'ともだち', '{TOMODACHI}') +
    ListContent.title_with_performer(vt, 'たどり着いたらいつも雨降り', '{TADORI-TSUITARA ITSUMO AME-FURI}') +
    ListContent.title_with_performer(vt, '夏休み', '{NATSUYASUMI}') +
    ListContent.title_with_performer(vt, 'どうしてこんなに悲しいんだろう', '{DOUSHITE KONNANI KANASIIN-DARO}') +
    ListContent.title_with_performer(vt, '人生を語らず', '{JINSEI-O KATARAZU}') +
    ListContent.title_with_performer(vt, '野の仏', '{NO-NO HOTOKE}')
  )
end
