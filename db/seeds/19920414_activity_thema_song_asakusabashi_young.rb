key = :thema_song_asakusabashi_young
obj = Activity.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_factory key do
    {
      activity_type: :thema_song,
      title_id: %q(「浅草橋ヤング洋品店」|TV program "{ASAKUSA-BASHI Young YOHINTEN}"|あさくさばし　やんぐようひんてん),
      j_comment: 'エンディング・テーマ',
      e_comment: 'Ending thema',
      company_id: :tv_tokyo,
      from_id: '1992/4/14',
      to_id: '1996/3/31',
      song_id: :natsuno_hi,
      sort_order: 199204140
    }
  end
end



