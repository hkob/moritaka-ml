key = :album_pepperland
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :pepperland do
    {
      device_type: :album,
      event_date_id: '1992/11/18',
      minutes: 42,
      seconds: 23,
      sort_order: 199211180,
      number: '9th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4124.html'
    }
  end

  album.media_from_array [
    ["#{key}_ct", :ct, 'WPTL-707', :first, false, :warner_music_japan, '2,900'],
    ["#{key}_cd1", :cd, 'WPCL-707', :first, false, :warner_music_japan, '2,900'],
    ["#{key}_cd2", :cd, 'WPC6-8323', :second, false, :warner_music_japan, '2,447'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/ペパーランド-pepperland/id528972208', :first, true, :warner_music_japan, '2,100'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  performer = %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito]
  list.list_contents_from_array [
    [:pepperland, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PIANO iChisatoMoritaka BASS iChisatoMoritaka RGUITAR iChisatoMoritaka AGUITAR iChisatoMoritaka CHORUS iChisatoMoritaka GUITAR iShinKohno GUITAR iYuichiTakahashi]],
    [:docchimo_docchi, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PIANO iChisatoMoritaka RGUITAR iChisatoMoritaka GUITAR iYuichiTakahashi BASS iYuichiTakahashi CHORUS iYuichiTakahashi]],
    [:atamaga_itai, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PIANO iChisatoMoritaka CHORUS iChisatoMoritaka BASS iYuichiTakahashi GUITAR iYukioSeto]],
    [:sunrise, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PIANO iChisatoMoritaka RGUITAR iChisatoMoritaka GUITARSOLO iChisatoMoritaka BASS iMasafumiYokoyama GUITAR iYuichiTakahashi CHORUS iYuichiTakahashi GUITAR iIzutsuya]],
    [:rockn_roll_kencho_shozaichi, %i[VOCAL iChisatoMoritaka ALLINST iChisatoMoritaka CHORUS iChisatoMoritaka]],
    [:ameno_asa, %i[VOCAL iChisatoMoritaka TWINDRUMS iChisatoMoritaka FRHODES iYasuakiMaejima SYNTHESIZER iYasuakiMaejima]],
    [:tokonatsuno_paradise, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka RGUITAR iChisatoMoritaka CHORUS iChisatoMoritaka BASS iMasafumiYokoyama FRHODES iShinKohno GUITAR iYuichiTakahashi GUITAR iIzutsuya]],
    [:uturn_wagaya, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PIANO iChisatoMoritaka GUITAR iYuichiTakahashi BASS iYuichiTakahashi CHORUS iYuichiTakahashi]],
    [:gokigenna_asa, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka PIANO iChisatoMoritaka BASS iChisatoMoritaka SYNTHESIZER iHideoSaito GUITAR iYuichiTakahashi GUITAR iIzutsuya]],
    [:rock_alarm_clock, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka FGUITAR iChisatoMoritaka GUITAR iMasataroNaoe SYNTHESIZER iMasataroNaoe BASS iMasafumiYokoyama GUITAR iYuichiTakahashi PIANO iYuichiTakahashi GUITARSOLO iIzutsuya]],
    [:aoi_umi, %i[VOCAL iChisatoMoritaka ARRANGE iHiroyoshiMatsuo DRUMS iChisatoMoritaka BASS iChisatoMoritaka RGUITAR iChisatoMoritaka CHORUS iChisatoMoritaka GUITARSOLO iHiroyoshiMatsuo FRHODES iYukieMatsuo]],
  ]
    device_activity_factory album, :thema_song_quiz_junsui_danjo_kouyuu
end

