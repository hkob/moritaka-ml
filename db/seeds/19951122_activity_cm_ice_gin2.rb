key = :cm_suntory_ice_gin2
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_suntory,
      j_title: 'サントリー 「アイスジン」',
      e_title: 'Suntory "ICE GIN"',
      from_id: '1995/11/22',
      to_id: '1995/12/25',
      sort_order: 199511220,
      song_id: :gin_gin_ginglebell
    }
  end
end


