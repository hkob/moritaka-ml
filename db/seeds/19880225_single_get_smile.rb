key = :single_get_smile
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :get_smile do
    {
      device_type: 'ep_single|single',
      event_date_id: '1988/2/25',
      sort_order: 198802250,
      number: '3rd',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4012.html'
    }
  end

  single.media_from_array [
    ["#{key}_ep", :ep, 'K-1567', '1st', false, :warner_pioneer, '700'],
    ["#{key}_ct", :ct, 'LKC-2056', '1st', false, :warner_pioneer, '1,000'],
    ["#{key}_cds1", :cds, '10SL-2', '1st', false, :warner_pioneer, '937'],
    ["#{key}_cds2", :cds, '10SL-2', '2nd', false, :warner_music_japan, '937'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/get-smile-single/id528975338', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }

  list.list_contents_from_array [
    [:get_smile, %i[VOCAL iChisatoMoritaka ARRANGE iKenShima]],
    [:good_bye_season, %i[VOCAL iChisatoMoritaka ARRANGE iTakumiYamamoto]]
  ]
end
