pkey = :cm_asahi_beer
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  company_factory :asahi_beer
  activity = activity_factory pkey do
    {
      activity_type: :cm,
      title_id: 'exist|asahi_beer',
      company_id: :asahi_beer,
      sort_order: 199401010
    }
  end
end

key = :cm_asahi_beer_z1
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_asahi_beer,
      j_title: 'アサヒビール 「アサヒ生ビールＺ」(春夏編)',
      e_title: 'Asahi beer "Asahi draft beer Z" (Spring & Summer version)',
      from_id: '1994/1',
      to_id: '1994/9/3',
      j_comment: 'ヨット編 (だるま落し) [15秒]|砂浜版 その1 [15秒]|砂浜版 その2 [15秒]|海上編 その1 (カヌー) [15秒]|海上編 その2 (水上スキー) [15秒]',
      e_comment: "{DARUMA OTOSHI} version [15 seconds]|`in the beach' version part 1 [15 seconds]|`in the beach' version part 2[15 seconds]|`on the sea' version part 1 [15 seconds]|`on the sea' version part 2 [15 seconds]",
      song_id: :kibun_soukai,
      sort_order: 199401010
    }
  end
end

