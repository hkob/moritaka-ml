pkey = :cm_suntory
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  company_factory :suntory
  activity_factory pkey do
    {
      activity_type: :cm,
      title_id: 'exist|suntory',
      company_id: :suntory,
      sort_order: 199504140,
    }
  end
end

key = :cm_suntory_ice_gin_1
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_suntory,
      j_title: 'サントリー 「アイスジン」',
      e_title: 'Suntory "ICE GIN"',
      from_id: '1995/4/14',
      to_id: '1995/7/20',
      j_comment: '1995年度CM音楽大賞 第16回広告音楽大競技会|第一カテゴリー(15秒以内)　金賞受賞|大魔 GIN 編 [15 秒]|外国 GIN 編 [15 秒]',
      e_comment: "The CM got the first prize(Gold) in the first Category (within 15 seconds)|of the 16th Advertismental Music Contest(in 1995 CM Music Contest).|{DAI-MA GIN} version [15 seconds]|Foreigner's (GAIKOKU-GIN) version [15 seconds]",
      sort_order: 199504140,
      song_id: :gin_gin_gin
    }
  end
end
