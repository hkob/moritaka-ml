key = :tour_sava_sava
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  band = band_factory(:utopia)
  band.band_members_from_array %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka RECORDER iChisatoMoritaka GUITAR iChisatoMoritaka KEYBOARDS iChisatoMoritaka DRUMS iMansakuKimura GUITAR iMasahiroInaba BASS iMasafumiYokoyama KEYBOARDS iShinKohno KEYBOARDS iYasuakiMaejima BANDLEADER iYasuakiMaejima]

  concert = concert_factory key do
    {
      j_subtitle: "Chisato Moritaka Concert Tour '98",
      e_subtitle: "Chisato Moritaka Concert Tour '98",
      concert_type: :tour,
      from_id: '1998/9/16',
      to_id: '1998/11/23',
      has_song_list: true,
      has_product: true,
      sort_order: 199809160,
      num_of_performances: 27,
      num_of_halls: 23,
      band_id: :utopia
    }
  end

  list_a, list_b, list_c = %i[A B C].map.with_index { |k, i| list_factory("#{key}_#{k}") { {concert_id: concert, keyword: k, sort_order: i+1} } }

  list_a.list_contents_from_array %i[utopia] + [[:telephone, [], '(Album Version)', '(Album Version)']] + @mc + %i[sweet_candy futariwa_koibito watarasebashi] + ListContent.title_only('衣裳替え', 'Costume change') + %i[umimade_5fun] + @mc + %i[zarude_mizukumu_koigokoro rockn_roll_kencho_shozaichi kikenna_hodou] + @mc + %i[tsumetai_tsuki tanpopono_tane la_la_sunshine] + ListContent.title_only('衣裳替え', 'Costume change') + %i[konomachi] + @mc + %i[nagasarete anatawa_ninkimono watashiga_obasanni_nattemo] + @encore + %i[snow_again] + [[:seventeen, [], '(カーネーション・ヴァージョン)', '(Carnation Version)']] + @mc + %i[kibun_soukai] + @double_encore + %i[watashiga_obasanni_nattemo]
  list_b.list_contents_from_array %i[utopia] + [[:telephone, [], '(Album Version)', '(Album Version)']] + @mc + %i[sweet_candy futariwa_koibito watarasebashi] + ListContent.title_only('衣裳替え', 'Costume change') + %i[umimade_5fun] + @mc + %i[zarude_mizukumu_koigokoro rockn_roll_kencho_shozaichi kikenna_hodou] + @mc + %i[tsumetai_tsuki tanpopono_tane la_la_sunshine] + ListContent.title_only('衣裳替え', 'Costume change') + %i[konomachi] + @mc + %i[nagasarete anatawa_ninkimono watashiga_obasanni_nattemo] + @encore + %i[snow_again] + [[:seventeen, [], '(カーネーション・ヴァージョン)', '(Carnation Version)']] + @mc + %i[kibun_soukai] + @double_encore + %i[ame]
  list_c.list_contents_from_array %i[utopia] + [[:telephone, [], '(Album Version)', '(Album Version)']] + @mc + %i[sweet_candy futariwa_koibito watarasebashi] + ListContent.title_only('衣裳替え', 'Costume change') + %i[umimade_5fun] + @mc + %i[zarude_mizukumu_koigokoro rockn_roll_kencho_shozaichi kikenna_hodou] + @mc + ListContent.title_only('夏の扉', 'Door for summer') + @mc + %i[tsumetai_tsuki tanpopono_tane la_la_sunshine] + ListContent.title_only('衣裳替え', 'Costume change') + %i[konomachi] + @mc + %i[nagasarete anatawa_ninkimono watashiga_obasanni_nattemo] + @encore + %i[snow_again] + [[:seventeen, [], '(カーネーション・ヴァージョン)', '(Carnation Version)']] + @mc + %i[kibun_soukai] + @double_encore + %i[ame]

  concert.concert_halls_from_array [
    ConcertHall.mk('1998/9/16', :川口リリア・メインホール, list_a, 'とんこつラーメン', '{TONKOTSU-RAMEN}'),
    ConcertHall.mk('1998/9/22', :仙台サンプラザホール, list_b, '笹かま', '{SASA-KAMABOKO}(boiled fish paste)'),
    ConcertHall.mk('1998/9/24', :岩手県民会館, list_b, 'わんこそば', '{WANKO-SOBA}'),
    ConcertHall.mk('1998/9/29', :大宮ソニックシティ, list_b, 'とんこつラーメン', '{TONKOTSU-RAMEN}'),
    ConcertHall.mk('1998/10/1', :中野サンプラザ, list_b, 'とんこつラーメン', '{TONKOTSU-RAMEN}'),
    ConcertHall.mk('1998/10/4', :中野サンプラザ, list_b, 'とんこつラーメン', '{TONKOTSU-RAMEN}'),
    ConcertHall.mk('1998/10/7', :新潟県民会館, list_b, 'こしひかり', '{KOSHIHIKARI}'),
    ConcertHall.mk('1998/10/15', :大阪厚生年金会館, list_b, 'お好み焼き', '{OKONOMIYAKI}'),
    ConcertHall.mk('1998/10/16', :大阪厚生年金会館, list_b, 'お好み焼き', '{OKONOMIYAKI}'),
    ConcertHall.mk('1998/10/21', :アクトシティ浜松, list_b, 'うなぎ', 'Eels'),
    ConcertHall.mk('1998/10/23', :神奈川県民ホール, list_b, '中華', 'China'),
    ConcertHall.mk('1998/10/26', :広島厚生年金会館, list_b, 'お好み焼き', '{OKONOMIYAKI}'),
    ConcertHall.mk('1998/10/28', :鳥取県立県民文化会館, list_b, '松葉ガニ', '{MATSUBA-GANI}'),
    ConcertHall.mk('1998/11/2', :金沢市観光会館, list_b, 'かぶらずし', '{KABURA-ZUSHI}'),
    ConcertHall.mk('1998/11/3', :長野県県民文化会館, list_b, '戸隠 (とがくし) そば', '{TOGAKUSHI-SOBA}'),
    ConcertHall.mk('1998/11/5', :群馬県民会館, list_b, '下仁田のネギ', '{SHIMONITA-NO NEGI}'),
    ConcertHall.mk('1998/11/10', :大分県立総合文化センター, list_b, '城下かれい', '{SHIROSHITA KAREI}'),
    ConcertHall.mk('1998/11/12', :福岡サンパレス, list_b, 'とんこつラーメン', '{TONKOTSU-RAMEN}'),
    ConcertHall.mk('1998/11/16', :鹿児島市民文化ホール, list_b, '薩摩揚げ', '{SATSUMAAGE}'),
    ConcertHall.mk('1998/11/21', :釧路市民文化会館, list_b, 'メンメ', '{MENME}'),
    ConcertHall.mk('1998/11/23', :北海道厚生年金会館, list_c, '札幌ラーメン', '{SAPPORO-RAMEN}'),
    ConcertHall.mk('1998/11/27', :香川県県民ホール, list_b, '讃岐うどん', '{SANUKI-UDON}'),
    ConcertHall.mk('1998/11/30', :神戸国際会館ハーバーランドプラザ, list_b, '神戸牛', '{KOBE-GYU}'),
    ConcertHall.mk('1998/12/3', :名古屋センチュリーホール, list_b, '味噌煮込みうどん', '{MISO-NIKOMI UDON}'),
    ConcertHall.mk('1998/12/4', :名古屋センチュリーホール, list_b, 'きしめんと味噌カツ', '{KISHIMEN} and {MISOKATSU}'),
    ConcertHall.mk('1998/12/7', :東京国際フォーラム, list_b, 'とんこつラーメン', '{TONKOTSU-RAMEN}', 'ホールA', 'Hall A'),
    ConcertHall.mk('1998/12/8', :東京国際フォーラム, list_b, '馬刺', '{BASASHI}' ,'ホールA', 'Hall A'),
  ]
end

