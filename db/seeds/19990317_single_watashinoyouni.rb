key = :single_watashinoyouni
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :watashinoyouni do
    {
      device_type: :single,
      event_date_id: '1999/3/17',
      sort_order: 199903170,
      number: '38th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4298.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDE-1023', '1st', false, :zetima, '1,020'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:watashinoyouni, %i[VOCAL iChisatoMoritaka ARRANGE iShinKohno DRUMS iChisatoMoritaka EGUITAR iCornellDupree RHODES iShinKohno PROGRAMMING iShinKohno AGUITAR iYuichiTakahashi]],
    [:space, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka PROGRAMMING iYuichiTakahashi GUITAR iYukioSeto]],
    [:watashinoyouni, %i[ARRANGE iShinKohno VOCAL iKaraoke DRUMS iChisatoMoritaka EGUITAR iCornellDupree RHODES iShinKohno PROGRAMMING iShinKohno AGUITAR iYuichiTakahashi], '(ORIGINL KARAOKE)', '(ORIGINL KARAOKE)'],
  ]
end
