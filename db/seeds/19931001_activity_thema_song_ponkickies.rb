pkey = :thema_song_ponkickies
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_factory pkey do
    {
      activity_type: :thema_song,
      title_id: %q{テレビ番組 (kids) 「ポンキッキーズ」|TV program (kids) "Ponkickies"|ぽんきっきーず},
      company_id: :fuji_tv,
      sort_order: 199501010
    }
  end
end

key = :thema_song_ponkickies_ro
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :thema_song_ponkickies,
      j_title: 'テレビ番組 (kids) 「ポンキッキーズ」挿入歌',
      e_title: 'Featured songs for TV program (kids) "Ponkickies"',
      from_id: '1995',
      to_id: '1995',
      song_id: :saa_boukenda,
      sort_order: 199501010
    }
  end
end
