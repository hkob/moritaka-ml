key = :score_piano_mini_selection_the_best_vol1
obj = Book.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  book = book_factory key do
    {
      book_type: :score_book,
      publisher_id: :fairy_inc,
      isbn: '4-938770-72-5 C0073 P669E',
      price: '669',
      event_date_id: '1995/5/15',
      sort_order: 199505150,
    }
  end

  list = list_factory("#{key}_A") { {book_id: book, keyword: :A, sort_order: 1} }
  list.list_contents_from_array %i[futariwa_koibito  sutekina_tanjoubi kibun_soukai]
end
