key = :cm_lawson_fresh_flutes
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_lawson,
      j_title: 'ローソン 「フレッシュフルーツ」',
      e_title: 'LAWSON "Fresh Flutes"',
      from_id: '1997/7/15',
      to_id: '1997/8/30',
      j_comment: '共演: 細野晴臣, 高嶋政伸|[15 秒]|[30 秒]',
      e_comment: 'Coactor: Haruomi Hosono, Masanobu Takashima|[15 seconds]|[30 seconds]',
      sort_order: 199707150,
      song_id: :sweet_candy,
    }
  end
end
key = :cm_lawson_natural_bakery2
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_lawson,
      j_title: 'ローソン 「ナチュラルベーカリー」',
      e_title: 'LAWSON "Natural Bakery"',
      from_id: '1997/7/15',
      to_id: '1997/8/30',
      j_comment: '共演: 細野晴臣, 高嶋政伸|[15 秒]|[30 秒]',
      e_comment: 'Coactor: Haruomi Hosono, Masanobu Takashima|[15 seconds]|[30 seconds]',
      sort_order: 199707160,
      song_id: :sweet_candy,
    }
  end
end

