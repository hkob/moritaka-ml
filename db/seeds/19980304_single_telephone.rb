key = :single_telephone
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :telephone do
    {
      device_type: :single,
      event_date_id: '1998/3/4',
      sort_order: 199803040,
      number: '35th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4292.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDA-57', '1st', false, :one_up_music, '1,020'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:telephone, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka ACCORDION iChisatoMoritaka CHORUS iChisatoMoritaka AGUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi APIANO iShinHashimoto KEYBOARDS iShinHashimoto BASS iYukioSeto PERCUSSION iYukioSeto]],
    [:dragon, %i[DRUMS iChisatoMoritaka PIANICA iChisatoMoritaka ACCORDION iChisatoMoritaka BASS iYukioSeto GUITAR iYukioSeto]],
    [:telephone, %i[VOCAL iKaraoke DRUMS iChisatoMoritaka ACCORDION iChisatoMoritaka CHORUS iChisatoMoritaka AGUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi APIANO iShinHashimoto KEYBOARDS iShinHashimoto BASS iYukioSeto PERCUSSION iYukioSeto], '(オリジナル・カラオケ)', '(Original karaoke)'],
  ]
  device_activity_factory single, :cm_kanebo
end


