key = :picture_book_i_Realite
obj = Book.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  book = book_factory key, :i_Realite do
    {
      book_type: :picture_book,
      title_id: :i_Realite,
      publisher_id: :sony_magazines,
      photographer_id: :iKatsumiOhmura,
      isbn: '4-592-73090-9 C0072 P2000E',
      price: '2,000',
      event_date_id: '1991/2/4',
      sort_order: 199102040,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4464.html'
    }
  end

  list = list_factory("#{key}_A") { {book_id: book, keyword: :A, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_only("SCENE 1 in MACAU") +
    ListContent.title_only("　She's just hunging around on the Street.") +
    ListContent.title_only("SCENE 2 on STAGE") +
    ListContent.title_only("　Yes, She's got a have it")
  )
end

