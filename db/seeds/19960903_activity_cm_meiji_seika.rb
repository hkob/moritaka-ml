pkey = :cm_meiji_seika
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  company_factory :meiji_seika
  activity_factory pkey do
    {
      activity_type: :cm,
      title_id: 'exist|meiji_seika',
      company_id: :meiji_seika,
      sort_order: 199609030,
    }
  end
end

key = :cm_meiji_seika_milk_chocolate
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_meiji_seika,
      j_title: '明治製菓「ミルクチョコレート」',
      e_title: 'Meiji-Seika "Milk Chocolate"',
      from_id: '1996/9/3',
      to_id: '1998/8',
      j_comment: '15 秒版 / 30 秒版|足利にて撮影',
      e_comment: '15 seconds version / 30 seconds version|Make a film in Ashikaga',
      sort_order: 199609030,
      song_id: :milk_choko
    }
  end
end

