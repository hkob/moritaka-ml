key = :concert_nagoya_university
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  concert = concert_factory key do
    {
      concert_type: :etc,
      from_id: '1989/6/8',
      to_id: '1989/8/4',
      has_song_list: false,
      sort_order: 198906080,
      num_of_performances: 4,
      num_of_halls: 4
    }
  end

  concert.concert_halls_from_array [
    ['1989/6/8', :名古屋大学],
    ['1989/7/30', :熊本テクノリサーチパーク, nil, 'TKUサマーブリーズ', 'TKU summer breeze'],
    ['1989/8/2', :広島蒲苅, nil, '海と島の博覧会', 'Exhibition of sea and island'],
    ['1989/8/4', :三浦海岸, nil, 'ラジオ日本公録', 'Public recording for Radio-Nippon'],
  ]
end
