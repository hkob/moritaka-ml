yt = :sc161

yt_hash = {
  '2015/2/21' => {
    link: 'sKJhGT4_npQ',
    number: '161',
    song_id: :haeotoko,
    comment: "作詞・作曲：森高千里　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」161曲目は、\n1993年発表のアルバム「LUCKY 7」に収録され、その後シングルカットされた「ハエ男」！\nドラムは森高千里によるループドラム！\n\n衣装協力：GALSTAR／GROWZE"
  },
  '2015/3/2' => {
    link: 'lQG76fv9egE',
    number: '162',
    song_id: :i_love_you,
    comment: "作詞：森高千里　作曲：伊秩弘将\n\n公式チャンネル独占企画「200曲セルフカヴァー」162曲目は、\n1993年発表のアルバム「LUCKY 7」から「I LOVE YOU」！\nドラムは森高千里による新録音です！\n\n衣装協力：ANOTHER BRANCH BY OLIVEdesOLIVE / Demi-Luxe BEAMS GINZA"
  },
  '2015/3/24' => {
    link: 'WQvjaNgVzSA',
    number: '163',
    song_id: :rock_alarm_clock,
    comment: "作詞：森高千里　作曲：直枝政太郎　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」163曲目は、\n1992年発表のアルバム「ペパーランド」から「ROCK ALARM CLOCK」です。\nドラムは森高千里によるループドラム！\n\n衣装協力：原宿シカゴ／Lilidia",
  },
  '2015/4/1' => {
    link: 'mvv2MpmL94Y',
    number: '164',
    song_id: :bassari_yatteyo,
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」164曲目は、\n1993年発表のアルバム「LUCKY 7」から「ばっさりやってよ」！\nドラムは森高千里による新録音です！\n\n衣装協力：GROWZE／原宿シカゴ 神宮前店"
  },
  '2015/4/7' => {
    link: 'EWcIAU9hrQM',
    number: '165',
    song_id: :ringoshuno_rule,
    j_title: "森高千里 『林檎酒のルール (2015 Ver.)』 【セルフカヴァー】",
    e_title: "Chisato Moritaka 『{RINGOSHU-NO rule} (2015 Ver.): [Rule of cider] (2015 Ver.)』 【Self cover】",
    comment: "作詞：高柳恋　作曲：島健　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」165曲目は、\n1987年発表のアルバム「NEW SEASON」から「林檎酒のルール」！\nセルフカヴァーでは二回目の登場ですが、背景を変え、サウンドもリアレンジしました！\n衣装協力：EBELE MOTION／GAL STAR"
  },
  '2015/4/18' => {
    link: 'gL0SbbthLEg',
    number: '166',
    song_id: :kataomoi,
    j_title: "森高千里 『片思い (2015 Ver.)』 【セルフカヴァー】",
    e_title: "Chisato Moritaka 『{KATAOMOI} (2015 Ver.): [one‐sided love] (2015 Ver.)』 【Self cover】",
    comment: "作詞：森高千里　作曲：河野伸　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」166曲目は、\n1997年発表のアルバム「PEACHBERRY」から「片思い」！\nセルフカヴァーでは二回目の登場ですが、背景を変え、サウンドもリアレンジしました！\n衣装協力：Nina man",
  },
  '2015/4/25' => {
    link: 'LTTOEtxavnk',
    number: '167',
    song_id: :hong_kong,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」167曲目は、\n1990年発表のアルバム「古今東西」から「香港」！\nドラムは森高千里による新録音です！！\n\n衣装協力：Nina man／お世話や"
  },
  '2015/5/2' => {
    link: 'pIRpQmt8UD8',
    number: '168',
    song_id: :otokono_roman,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」168曲目は、\n1993年発表のアルバム「LUCKY 7」から「男のロマン」！\nドラムは森高千里による新録音です！！\n\n衣装協力：LIVIANA CONTI"
  },
  '2015/5/9' => {
    link: 'lLKV3MPEUUc',
    number: '169',
    song_id: :hatachi,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」169曲目は、\n1989年発表のシングル「17才」のカップリング曲「20才」！\nドラムは森高千里によるループドラム！\n\n衣装協力：TITE IN THE STORE／Nina man"
  },
  '2015/5/15' => {
    link: '-Faa0eVrTnk',
    number: '170',
    song_id: :step_by_step_kareno_jinsei,
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」170曲目は、\n1994年発表のアルバム「STEP BY STEP」からタイトル曲「STEP BY STEP」！\nドラムは森高千里による新録音です！！\n\n衣装協力：EBELE MOTION／GAL STAR"
  },
  '2015/5/30' => {
    link: 'IBLfww6I4Sw',
    number: '171',
    song_id: :gin_gin_gin,
    j_title: "森高千里 『人 人 人』【セルフカヴァー】",
    e_title: "Chisato Moritaka 『JIN JIN JIN』【Self cover】",
    comment: "作詞・作曲：森高千里\n\n公式チャンネル独占企画「200曲セルフカヴァー」171曲目は、\n1996年発表のアルバム「TAIYO」から「人 人 人」！\nドラムは森高千里によるループドラム！\n\n衣装協力：原宿シカゴ表参道店／Nina mew／TITE IN THE STORE"
  },
  '2015/6/12' => {
    link: 'uggzI4qZYjI',
    number: '172',
    song_id: :mi_ha_,
    j_title: "森高千里 『ミーハー (2015 Ver.)』 【セルフカヴァー】",
    e_title: "Chisato Moritaka 『Mi-HA- (2015 Ver.)』 【Self cover】",
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」172曲目は、\n1988年発表のアルバム「ミーハー」からタイトル曲「ミーハー」！\nセルフカヴァーでは二回目の登場ですが、背景を変え、サウンドもリアレンジしました！\n\n衣装協力：Million Carats／EBELE MOTION"
  },
  '2015/6/19' => {
    link: 'V9FRpw9K60U',
    number: '173',
    song_id: :daibouken,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」173曲目は、\n1990年発表のアルバム「古今東西」から「大冒険」！\nドラムは森高千里による新録音です！！\n\n衣装協力：原宿シカゴ／EBELE MOTION／お世話や"
  },
  '2015/6/27' => {
    link: 'fz4EpYE1X08',
    number: '174',
    song_id: :modorenai_natsu,
    j_title: "森高千里 『戻れない夏 (2015 Ver.)』 【セルフカヴァー】",
    e_title: "Chisato Moritaka 『{MODORENAI NATSU} (2015 Ver.): [Summer not to be able to come back] (2015 Ver.)』 【Self cover】",
    comment: "作詞：久和カノン　作曲：島健　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」174曲目は、\n1988年発表の3rdアルバム『見て』から「戻れない夏」！\nセルフカヴァーでは二回目の登場ですが、背景を変え、サウンドもリアレンジしました！\n\n衣装協力：one way"
  },
  '2015/7/4' => {
    link: 'Q69qDB7Ozi0',
    number: '175',
    song_id: :hijitsuryokuha_sengen,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」175曲目は、\n1989年発表のアルバム「非実力派宣言」からタイトル曲の「非実力派宣言」！\nドラムは森高千里による新録音です！！\n\nGuitar Solo : 鈴木マリア\n\n衣装協力：リエディ／DABAgirl"
  },
  '2015/7/18' => {
    link: 'isNRroQX3BQ',
    number: '176',
    song_id: :rockn_roll_kencho_shozaichi,
    j_title: "森高千里 『ロックンロール県庁所在地 (2015 Ver.)』【セルフカヴァー】",
    e_title: "Chisato Moritaka 『Rock'n Roll {KENCHO-SHOZAICHI} (2015 Ver.): [Rock'n Roll prefectural capital] (2015 Ver.)』【Self cover】",
    comment: "作詞・作曲：森高千里\n\n公式チャンネル独占企画「200曲セルフカヴァー」176曲目は、\n1992年発表のアルバム「ペパーランド」から「ロックンロール県庁所在地」！\nドラムは森高千里による新録音です！！\n\n衣装協力：EBELE MOTION"
  },
  '2015/7/25' => {
    link: 'Nl0mXrTpgHE',
    number: '177',
    song_id: :natsuwa_pararaylon,
    j_title: "森高千里 『夏はパラレイロン (2015 Ver.)』【セルフカヴァー】",
    e_title: "Chisato Moritaka 『{NATSU-HA Pararaylon} (La La SunShine Part 2, 2015 Ver.): [Pararaylon in summer (La La SunShine Part 2, 2015 Ver.)]』【Self cover】",
    comment: "作詞：森高千里　作曲：伊秩弘将\n\n公式チャンネル独占企画「200曲セルフカヴァー」177曲目は、\n1996年発表のアルバム「TAIYO」から「夏はパラレイロン」！\nセルフカヴァーでは二回目の登場ですが、背景を変え、サウンドもリアレンジしました！ドラムは森高千里によるループドラムです！！\n\n衣装協力：原宿シカゴ表参道店／リエディ"
  },
  '2015/8/1' => {
    link: 'xaLWvHnQpME',
    number: '178',
    song_id: :pepperland,
    comment: "作詞：森高千里　作曲：河野伸\n\n公式チャンネル独占企画「200曲セルフカヴァー」178曲目は、\n1992年発表のアルバム「ペパーランド」からタイトル曲「ペパーランド 〜PEPPER LAND〜」！\nドラムは森高千里による新録音です！！\n\n衣装協力：rienda／Million Carats"
  },
  '2015/8/10' => {
    link: 'qjACdqQ4bvw',
    number: '179',
    song_id: :sleepless_night_blues,
    comment: "作詞・作曲：森高千里\n\n公式チャンネル独占企画「200曲セルフカヴァー」179曲目は、\n1998年発表のシングル「海まで5分」のカップリング曲「Sleepless Night Blues」！\nドラムは森高千里によるループドラムです！！\n\n衣装協力：Nima mew"
  },
  '2015/8/16' => {
    link: '9Mh9u8VhAPU',
    number: '180',
    song_id: :busters_blues,
    j_title: "森高千里 『ザ・バスターズ・ブルース (2015 Ver.)』【セルフカヴァー】",
    e_title: "Chisato Moritaka 『The Busters Blues(2015 Ver.)』【Self cover】",
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」180曲目は、\n1990年発表のアルバム「古今東西」から「ザ・バスターズ・ブルース」！\n低音が再生できる環境で視聴することをオススメします！"
  },
}
create_youtube_factory_from_hash(yt, yt_hash)
