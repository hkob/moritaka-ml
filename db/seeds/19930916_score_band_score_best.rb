key = :score_band_score_best
obj = Book.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  book = book_factory key do
    {
      book_type: :score_book,
      publisher_id: :shinko_music,
      isbn: '4-401-34719-6 C0073 P2987E',
      price: '2,987',
      event_date_id: '1993/9/16',
      sort_order: 199309160,
    }
  end

  list = list_factory("#{key}_A") { {book_id: book, keyword: :A, sort_order: 1} }
  list.list_contents_from_array %i[the_mi_ha_ new_season stress seventeen michi kusaimononiwa_futaoshiro benkyono_uta fight watashiga_obasanni_nattemo watarasebashi watashino_natsu haeotoko]
end



