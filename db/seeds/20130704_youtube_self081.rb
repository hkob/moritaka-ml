yt = :sc081

yt_hash = {
  '2013/7/4' => {
    link: 'XdjdYUTDkl0',
    number: '81',
    song_id: :obasan,
    comment: "作詞：森高千里　作曲：伊秩弘将　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」81曲目は、\n1992年発表のアルバム「ROCK ALIVE」から『叔母さん』！\nドラムは森高千里による新録音！\n\n衣装協力：　goa／パチュリ（フラッパーズ）／Westwood Outfitters(カイタックインターナショナル)／MESMO"
  },
  '2013/7/9' => {
    link: 'IEYkrVW6Oz4',
    number: '82',
    song_id: :tsukiyono_kokai,
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」82曲目は、\n1990年発表のアルバム「古今東西」から『月夜の航海』！\n\n衣装協力：NOLLEY'S／FREDY&GLOSTER"
  },
  '2013/7/13' => {
    link: 'gYiXln4FZwg',
    number: '83',
    song_id: :writer_shibo,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」83曲目は、\n1993年発表の両A面シングル「渡良瀬橋／ライター志望」から『ライター志望』！\nドラムは森高千里による新録音！\n\n衣装協力：CARBOOTS"
  },
  '2013/7/17' => {
    link: '8DWl1Po2L6k',
    number: '84',
    song_id: :sunrise,
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」84曲目は、\n1992年発表のアルバム「ペパーランド」から『サンライズ』！\nドラムは森高千里による新録音！\n\n衣装協力：Rie miller／RICORDI"
  },
  '2013/7/20' => {
    link: 'kmYRZ1axDSM',
    number: '85',
    song_id: :miracle_light,
    comment: "作詞：森高千里　作曲：細野晴臣\n\n公式チャンネル独占企画「200曲セルフカヴァー」85曲目は、\n1997年発表のシングル『ミラクルライト』！\nドラムは森高千里によるループドラム！\n\n衣装協力：manics／バウレットキー"
  },
  '2013/7/26' => {
    link: 'nLBXVpM6QNU',
    number: '86',
    song_id: :botto_shitemiyou,
    comment: "作詞・作曲：森高千里　編曲：dessert,desert\n\n公式チャンネル独占企画「200曲セルフカヴァー」86曲目は、\n1996年発表のアルバム「TAIYO」から「ボーッとしてみよう」！\nドラムは森高千里による新録音！\n\n衣装協力：NOLLEY'S sophi／MET"
  },
  '2013/7/31' => {
    link: 'ZIJ1kivfoBM',
    number: '87',
    song_id: :telephone,
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」87曲目は、\n1998年発表のシングル『電話』！\nドラムは森高千里による新録音！\n\n衣装協力：grintmati／NOLLEY'S"
  },
  '2013/8/3' => {
    link: 'uR-8ztnCYLQ',
    number: '88',
    song_id: :kibun_soukai,
    comment: "作詞：森高千里　作曲：黒沢健一　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」88曲目は、\n1994年発表のシングル『気分爽快』！\nドラムは森高千里による新録音！\n\n衣装協力：FREDY&GROSTER／Damuy&Ahne"
  },
  '2013/8/7' => {
    link: 'Fqsks9-8nHI',
    number: '89',
    song_id: :watashiwa_onchi,
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」89曲目は、\n1989年発表のアルバム「非実力派宣言」から『私はおんち』！\nドラムは森高千里による新録音！\n\n衣装協力：Damuy&Ahne／RICORDI"
  },
  '2013/8/10' => {
    link: 'qqmilG2va0A',
    number: '90',
    song_id: :tereya,
    comment: "作詞：森高千里　作曲：真田カオル　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」90曲目は、\n1996年発表のアルバム「TAIYO」から『照れ屋』！\nドラムは森高千里による新録音！\n\n衣装協力：rich／Met"
  },
  '2013/8/16' => {
    link: 'ALr2nGCWLE8',
    number: '91',
    song_id: :so_blue,
    comment: "作詞：森高千里　作曲：伊秩弘将　編曲：dessert,desert\n\n公式チャンネル独占企画「200曲セルフカヴァー」91曲目は、\n1996年発表のシングル『SO BLUE』！\nドラムは森高千里による新録音！\n\n衣装協力：NOLLEY'S／grintmati"
  },
  '2013/8/29' => {
    link: 'hyw38Pfy9Gc',
    number: '92',
    song_id: :lets_go,
    comment: "作詞：森高千里　作曲：伊秩弘将　リアレンジ：S&T\n\n公式チャンネル独占企画「200曲セルフカヴァー」92曲目は、\n1997年発表のシングル『Let's Go!』！\nドラムは森高千里による新録音！\n\n衣装協力：RICORDI／MET"
  },
  '2013/9/6' => {
    link: 'z-yvWqDe3zA',
    number: '93',
    song_id: :zarude_mizukumu_koigokoro,
    comment: "作詞：森高千里　作曲：COIL　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」93曲目は、\n1998年発表のアルバム「Sava Sava」から『ザルで水くむ恋心』！\nこの曲のドラムトラックは今年春に行われたライブで実際に叩いたものを使用し、またボーカルトラック及びその他の楽器は新たに録音したものを使用しています。\n\n衣装協力：BUONA GIORNATA／YEVS"
  },
  '2013/9/12' => {
    link: 'ImlbMUnmX_I',
    number: '94',
    song_id: :wakarimashita,
    comment: "作詞：森高千里　作曲：横山雅史　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」94曲目は、\n1992年発表のアルバム「ROCK ALIVE」から『わかりました』！\nドラムは森高千里による新録音！\n\n衣装協力：American Apparel／RICORDI"
  },
  '2013/9/18' => {
    link: 'rold1BuFxfk',
    number: '95',
    song_id: :weekend_blue,
    comment: "作詞：伊秩弘将　作曲：斉藤英夫　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」95曲目は、\n1988年発表のアルバム「ミーハー」から『WEEKEND BLUE』！\nドラムは森高千里による新録音！\n\n衣装協力：American Apparel／C.C.CROSS"
  },
  '2013/9/23' => {
    link: 'Q50tGv3tdIA',
    number: '96',
    song_id: :yokohama_one_night,
    comment: "作詞：伊秩弘将　作曲：斉藤英夫　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」96曲目は、\n1988年発表のアルバム「ミーハー」から『YOKOHAMA ONE NIGHT』！\nドラムは森高千里による新録音！\n\n衣装協力：DECEIVE／YEVS"
  },
  '2013/9/28' => {
    link: 'HuTHhO3zKG8',
    number: '97',
    song_id: :watashinoyouni,
    comment: "作詞：森高千里 　作曲：河野伸　編曲：S&T\n\n公式チャンネル独占企画「200曲セルフカヴァー」97曲目は、\n1999年発表のシングル「私のように」！\nドラムは森高千里による新録音！\nGuitar：太田雄二\n\n衣装協力：AGNOST／Immanoel"
  },
  '2013/10/4' => {
    link: 'IxFqwkV12KA',
    number: '98',
    song_id: :otisreddingni_kanpai,
    comment: "作詞：高柳恋　作曲：津垣博通　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」98曲目は、\n1987年発表の1stアルバム「NEW SEASON」から『オーティスレディングに乾杯』！\nドラムは森高千里による新録音！\n\n衣装協力：American Apparel／Baulette Key"
  },
  '2013/10/8' => {
    link: 'mzEQkRcQaA4',
    number: '99',
    song_id: :teriyaki_burger,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」99曲目は、\n1990年発表のアルバム「古今東西」から『テリヤキ・バーガー』！\nドラムは森高千里による新録音！\n\n衣装協力：madarie／GROWZE"
  },
  '2013/10/11' => {
    link: 'uUMtFzExwJI',
    number: '100',
    song_id: :uturn_wagaya,
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」100曲目は、\n1992年発表のアルバム「ペパーランド」から『Uターン ～我が家～』！\nドラムは森高千里による新録音！\n\n衣装協力：Language"
  },

}
create_youtube_factory_from_hash(yt, yt_hash)
