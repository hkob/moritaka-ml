key = :thema_song_count_down_tv
obj = Activity.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity = activity_factory key do
    {
      activity_type: :thema_song,
      title_id: %q(Count Down TV|Count Down TV|かうんとだうんてぃーびー),
      j_comment: 'オープニング・テーマ',
      e_comment: 'Opening Song',
      company_id: :tbs,
      from_id: '1996/2',
      song_id: :so_blue,
      sort_order: 199602010
    }
  end
end
