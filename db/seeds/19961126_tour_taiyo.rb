key = :tour_taiyo
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  band = band_factory(:smilyMaejimaHelloGoodBye2)
  band.band_members_from_array %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka RECORDER iChisatoMoritaka GUITAR iChisatoMoritaka KEYBOARDS iChisatoMoritaka DRUMS iToshihiroTsuchiya GUITAR iToruHasegawa BASS iMasafumiYokoyama KEYBOARDS iShinKohno KEYBOARDS iYasuakiMaejima BANDLEADER iYasuakiMaejima]

  concert = concert_factory key do
    {
      j_subtitle: "Chisato Moritaka Concert Tour '96",
      e_subtitle: "Chisato Moritaka Concert Tour '96",
      concert_type: :tour,
      from_id: '1996/11/26',
      to_id: '1996/12/5',
      has_song_list: true,
      has_product: true,
      sort_order: 199611260,
      num_of_performances: 4,
      num_of_halls: 3,
      band_id: :smilyMaejimaHelloGoodBye2
    }
  end

  list_a, list_b, list_c = %i[A B C].map.with_index { |k, i| list_factory("#{key}_#{k}") { {concert_id: concert, keyword: k, sort_order: i+1} } }
  list_a.list_contents_from_array %i[natsuwa_pararaylon watashino_natsu fight] + @mc + %i[new_season seventeen so_blue stress] + @mc + %i[ame watarasebashi kibun_soukai] + ListContent.title_only('ビデオ', 'Video') + %i[teo_tatako rockn_omelette tereya futariwa_koibito] + @mc + %i[toga_tatsu] + [[:milk_choko, [], '(アカペラ)', '(a cappella)']] + ListContent.medley_in('ジンジンジンメドレー', 'GIN GIN GIN Medley') + %i[gin_gin_gin jin_jin_jinglebell] + @medley_out + %i[la_la_sunshine konomachi] + @encore + [[:taiyouto_aoi_tsuki, [], '(太陽神ヴァージョン)', '(Sungod version)']] + %i[ginirono_yume watashiga_obasanni_nattemo] + @double_encore + @mc_member + %i[kyoukara]
  list_b.list_contents_from_array %i[natsuwa_pararaylon watashino_natsu fight] + @mc + %i[new_season seventeen so_blue stress] + @mc + %i[ame watarasebashi kibun_soukai] + ListContent.title_only('ビデオ', 'Video') + %i[teo_tatako rockn_omelette tereya] + @mc + [[:milk_choko, [], '(アカペラ)', '(a cappella)']] + %i[futariwa_koibito toga_tatsu] + ListContent.medley_in('ジンジンジンメドレー', 'GIN GIN GIN Medley') + %i[gin_gin_gin jin_jin_jinglebell] + @medley_out + %i[la_la_sunshine konomachi] + @encore + [[:taiyouto_aoi_tsuki, [], '(太陽神ヴァージョン)', '(Sungod version)']] + %i[ginirono_yume watashiga_obasanni_nattemo] + @double_encore + @mc_member + %i[kyoukara]
  list_c.list_contents_from_array %i[natsuwa_pararaylon watashino_natsu fight] + @mc + %i[new_season seventeen so_blue stress] + @mc + %i[ame watarasebashi kibun_soukai] + ListContent.title_only('ビデオ', 'Video') + %i[teo_tatako rockn_omelette tereya] + @mc + %i[futariwa_koibito toga_tatsu ginirono_yume] + @mc + ListContent.medley_in('ジンジンジンメドレー', 'GIN GIN GIN Medley') + %i[gin_gin_gin jin_jin_jinglebell] + @medley_out + %i[la_la_sunshine konomachi] + @encore + [[:taiyouto_aoi_tsuki, [], '(太陽神ヴァージョン)', '(Sungod version)']] + @mc_member + %i[watashiga_obasanni_nattemo] + @double_encore + [[:milk_choko, [], '(アカペラ)', '(a cappella)']] + %i[kyoukara]

  concert.concert_halls_from_array [
    ConcertHall.mk('1996/11/26', :名古屋レインボーホール, list_a, '味噌煮込みうどん', '{MISONIKOMI}-Noodles'),
    ConcertHall.mk('1996/11/29', :大阪城ホール, list_b, 'たこ焼きやお好み焼き', '{TAKOYAKI} and {OKONOMIYAKI}'),
    ConcertHall.mk('1996/12/4', :日本武道館, list_c, 'とんこつラーメン', '{TONKOTSU-RAMEN}'),
    ConcertHall.mk('1996/12/5', :日本武道館, list_c, 'とんこつラーメン', '{TONKOTSU-RAMEN}'),
  ]
end


