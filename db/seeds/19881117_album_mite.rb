key = :album_mite
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :mite do
    {
      device_type: :album,
      event_date_id: '1988/11/17',
      minutes: 42,
      seconds: 50,
      sort_order: 198811170,
      number: '3rd',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4036.html'
    }
  end

  album.media_from_array [
    ["#{key}_lp", :lp, '28L1-0027', :first, false, :warner_pioneer, '2,800'],
    ["#{key}_ct", :ct, '28L4-0027', :first, false, :warner_pioneer, '2,800'],
    ["#{key}_cd1", :cd, '32L2-0027', :first, false, :warner_pioneer, '3,200 3,008'],
    ["#{key}_cd2", :cd, 'WPCL-504', :second, true, :warner_music_japan, '2,400'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/jiante/id528970392', :first, true, :warner_music_japan, '2,100']
  ]

  list = list_factory("#{key}_common") { {device_id: album, keyword: :common, sort_order: 1} }

  list.list_contents_from_array [
    [:omoshiroi, %i[VOCAL iChisatoMoritaka GUITAR iHideoSaito SYNPR iHideoSaito DRUMPR iHideoSaito]],
    [:wakareta_onna, %i[VOCAL iChisatoMoritaka GUITAR iHideoSaito SYNPR iHideoSaito DRUMPR iHideoSaito]],
    [:watashiga_hen, %i[VOCAL iChisatoMoritaka ARRANGE iTakumiYamamoto GUITAR iTakumiYamamoto DRUMPR iTakumiYamamoto CHORUS iShinjiYasuda SYNPR iTomoakiArima SYNPR iNaokiSuzuki]],
    [:alone, %i[VOCAL iChisatoMoritaka ARRANGE iShinjiYasuda CHORUS iShinjiYasuda SYNPR iNaokiSuzuki]],
    [:stress, %i[VOCAL iChisatoMoritaka GUITAR iHideoSaito SYNPR iHideoSaito DRUMPR iHideoSaito]],
    [:detagari, %i[VOCAL iChisatoMoritaka GUITAR iHideoSaito SYNPR iHideoSaito DRUMPR iHideoSaito]],
    [:modorenai_natsu, %i[VOCAL iChisatoMoritaka ARRANGE iKenShima KEYBOARDS iKenShima BASS iKenjiTakamizu PERCUSSION iShingoKanno SYNPR iKeijiToriyama SYNPR iNaokiSuzuki]],
    [:mite, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito GUITAR iHideoSaito SYNPR iHideoSaito DRUMPR iHideoSaito]],
    [:let_me_go, %i[VOCAL iChisatoMoritaka ARRANGE iTakumiYamamoto]]
  ]
end
