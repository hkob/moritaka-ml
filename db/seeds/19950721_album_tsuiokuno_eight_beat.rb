key = :album_tsuiokuno_eight_beat
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :tsuiokuno_eight_beat do
    {
      device_type: 'part_album',
      event_date_id: '1995/7/21',
      minutes: 39,
      seconds: 17,
      sort_order: 199507210,
      number: '18th',
      singer_id: :iShigeruIzumiya
    }
  end

  album.media_from_array [
    ["#{key}_cd", :cd, 'VICL-657', :first, false, :victor_entertainment, '3,000'],
  ]

  vs = %i[VOCAL iShigeruIzumiya]
  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_with_performer(vs, 'I Like a SONG', 'I Like a SONG') +
    ListContent.title_with_performer(vs, '永遠の約束', '{EIEN-NO YAKUSOKU}') +
    ListContent.title_with_performer(vs, 'CITY / 無防備都市', 'CITY / {MUBOBI-TOSHI}') +
    ListContent.title_with_performer(vs, 'レイン', 'Rain') +
    [[:true_love, %i[VOCAL iShigeruIzumiya ARRANGE iKenYoshida ARRANGE iShigeruIzumiya CHORUS iChisatoMoritaka]]] +
    ListContent.title_with_performer(vs, 'ヘッドライト', 'Headlight') +
    ListContent.title_with_performer(vs, '鯛とロープ', '{TAI-TO ROPE}') +
    ListContent.title_with_performer(vs, 'ゲットー', '{GETTO-}') +
    ListContent.title_with_performer(vs, "It's gonna be ALRIGHT", "It's gonna be ALRIGHT")
  )
end
