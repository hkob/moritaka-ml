key = :score_doremi1
obj = Book.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  book = book_factory key do
    {
      book_type: :score_book,
      publisher_id: :doremi_shuppan,
      isbn: '4-8108-1178-6 C0073 P1545E',
      price: '1,545',
      event_date_id: '1993/12/30',
      sort_order: 199312300,
    }
  end

  list = list_factory("#{key}_A") { {book_id: book, keyword: :A, sort_order: 1} }
  list.list_contents_from_array %i[kazeni_fukarete kyushu_sodachi watashino_natsu watarasebashi writer_shibo watashiga_obasanni_nattemo jimina_onna haeotoko sayonara_watashino_koi ame benkyono_uta concert_no_yoru kondo_watashi_dokoka kusaimononiwa_futaoshiro] + [[:daite, [], '[ラスベガス・ヴァージョン]', '[Las Vegas version]']] + %i[furusatono_sora uturn_wagaya fight hachigatsuno_koi seishun michi] + [[:the_stress, [], '[ストレス中近東ヴァージョン]', '[Stress {CHUUKINTOU} version]']] + %i[alone mi_ha_ new_season get_smile seventeen]
end


