key = :album_hijitsuryokuha_sengen
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :hijitsuryokuha_sengen do
    {
      device_type: :album,
      event_date_id: '1989/7/25',
      minutes: 55,
      seconds: 32,
      sort_order: 198907250,
      number: '4th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4038.html'
    }
  end

  album.media_from_array [
    ["#{key}_ct", :ct, '25L4-85', :first, false, :warner_pioneer, '2,560'],
    ["#{key}_cd1", :cd, '29L2-85', :first, false, :warner_pioneer, '3,008'],
    ["#{key}_cd2", :cd, 'WPCL-505', :second, true, :warner_music_japan, '2,400'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/fei-shi-li-pai-xuan-yan/id528974085', :first, true, :warner_music_japan, '2,100'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:seventeen, %i[VOCAL iChisatoMoritaka ARRANGE iMasataroNaoe AGUITAR iMasataroNaoe EGUITAR iGiroBando BASS iYujiMada KEYBOARDS iYuichiTanaya DRUMS iHiroshiYabe], '[カーネーション・ヴァージョン]', '[Carnation version]'],
    [:korekkiri_byebye, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito]],
    [:daite, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi CHORUS iChisatoMoritaka GUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi CHORUS iYuichiTakahashi DRUMS iNaokiKimura BASS iKazuyoshiYamauchi TROMBONE iCarlosKanno SYNPR iNaokiSuzuki]],
    [:hijitsuryokuha_sengen, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito CHORUS iChisatoMoritaka CHORUS iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito]],
    [:kondo_watashi_dokoka, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito]],
    [:hadakaniwa_naranai, %i[VOCAL iChisatoMoritaka ARRANGE iMasataroNaoe AGUITAR iMasataroNaoe CHORUS iMasataroNaoe EGUITAR iGiroBando BASS iYujiMada KEYBOARDS iYuichiTanaya CHORUS iYuichiTanaya DRUMS iHiroshiYabe]],
    [:watashiwa_onchi, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi GUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi CHORUS iYuichiTakahashi]],
    [:shiritagari, %i[VOCAL iChisatoMoritaka ARRANGE iCarlosKanno ARRANGE iYasuakiMaejima SPERCUSSION iChisatoMoritaka PERCUSSION iCarlosKanno PIANO iYasuakiMaejima GUITAR iAkiraWada DRUMS iMansakuKimura BASS iHiroshiSawada SYNTHESIZER iHiromichiTsugaki TRUMPET iKenjiYoshida TRUMPET iMasahiroKobayashi TROMBONE iOsamuMatsuki ALTOSAX iHisashiYoshinaga TENORSAX iTakeruMuraoka]],
    [:wakasugita_koi, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi GUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi CHORUS iYuichiTakahashi DRUMS iNaokiKimura BASS iKazuyoshiYamauchi SYNPR iNaokiSuzuki]],
    [:akunno_higeki, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito CHORUS iHideoSaito]],
    [:yoruno_entotsu, %i[VOCAL iChisatoMoritaka ARRANGE iCarnation AGUITAR iMasataroNaoe EGUITAR iMasataroNaoe CHORUS iMasataroNaoe EGUITAR iGiroBando BASS iYujiMada CHORUS iYujiMada KEYBOARDS iYuichiTanaya CHORUS iYuichiTanaya DRUMS iHiroshiYabe]],
    [:sonogono_watashi, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito CHORUS iHideoSaito]],
    [:yumeno_nakano_kiss, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka ORGAN iChisatoMoritaka BASS iYuichiTakahashi CHORUS iYuichiTakahashi TAMBOURINE iYuichiTakahashi GUITAR iYukioSeto]],
    [:seventeen, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito], '[オレンジ・ミックス]', '[Orange Mix]']
  ]
end
