yt = :sc181

yt_hash = {
  '2015/12/19' => {
    link: '2tK0o6mUtkk',
    number: '181',
    song_id: :jin_jin_jinglebell,
    j_title: "森高千里 『ジン ジン ジングルベル 2015』 【セルフカヴァー】",
    e_title: "Chisato Moritaka 『JIN JIN JINGLEBELL 2015』 【Self cover】",
    comment: "作詞・作曲：森高千里　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」181曲目は、\n1995年発表のシングル「ジン ジン ジングルベル」！\n今年も新バージョンをお楽しみ下さい！\nドラムは森高千里によるループドラム！"
  },
  '2015/12/31' => {
    link: 'QbE32cIMMtc',
    number: '182',
    song_id: :ichigatsu_ichijitsu,
    j_title: "森高千里 『一月一日 2016』【セルフカヴァー】",
    e_title: "Chisato Moritaka 『{ICHIGATSU ICHIJITSU} 2016』【Self cover】",
    comment: "作詞：千家尊福　作曲：上真行　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」182曲目は、\n1995年発表のシングル「ジンジン ジングルベル」のC/W「一月一日」です。2016年バージョンをお楽しみ下さい。\nドラムは森高千里によるループドラムです！\n\n撮影協力：ゲストハウスティータイム"
  },
  '2016/3/12' => {
    link: 'iQTSspAf5jg',
    number: '183',
    song_id: :kanojo,
    comment: "作詞：森高千里　作曲：斉藤英夫　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」183曲目は、1991年発表のアルバム「ザ・森高」から『彼女』！\n \nドラムは森高千里による新録音です！！\n\n衣装協力：EDDY△GRACE／デミルクス△ビームス"
  },
  '2016/3/22' => {
    link: '-ekEMQ22s60',
    number: '184',
    song_id: :sonogono_watashi,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」184曲目は、1989年発表のアルバム「非実力派宣言」から『その後の私（森高コネクション）』！\nドラムは森高千里によるループドラムです！！\n\n衣装協力：EDDY GRACE"
  },
  '2016/3/28' => {
    link: 'pFd1EFpLvs4',
    number: '185',
    song_id: :wasurekaketeta_yume,
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」185曲目は、1998年発表のアルバム「Sava Sava」から『忘れかけてた夢』！\nドラムは森高千里による新録音です！！\n\n衣装協力：Munich／MICA&DEAL"
  },
  '2016/5/7' => {
    link: 'FrWGKNTZJmI',
    number: '186',
    song_id: :a_bientot,
    comment: "作詞：森高千里　作曲：コシミハル　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」186曲目は、1998年発表のアルバム「今年の夏はモア・ベター」から『ア・ビアント』！\nドラムは森高千里によるループドラムです！！\n\n衣装協力：デミルクス ビームス／Million Carats"
  },
  '2016/7/2' => {
    link: 'hNvK0ofQrQU',
    number: '187',
    song_id: :taiyouto_aoi_tsuki,
    comment: "作詞・作曲：森高千里\n\n公式チャンネル独占企画「200曲セルフカヴァー」187曲目は、1996年発表のアルバム「TAIYO」から『太陽と青い月』！\nドラムは森高千里によるループドラムです！！\n\n衣装協力：トランスコンチネンツ（ピーチ）"
  },
  '2016/8/19' => {
    link: '5pdRDTY4DD4',
    number: '188',
    song_id: :two_of_me,
    comment: "作詞・作曲：森高千里　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」188曲目は、1998年発表のアルバム「Sava Sava」から『Two of me』！\n映像は「JCB Presents Chisato Moritaka Special Live vol.3 in Blue Note TOKYO」より！\n\nドラムは森高千里によるループドラムです！！"
  },
  '2016/8/26' => {
    link: '_3sPmwUkoaE',
    number: '189',
    song_id: :akunno_higeki,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」189曲目は、1989年発表のアルバム「非実力派宣言」から『A君の悲劇』！\n映像は「JCB Presents Chisato Moritaka Special Live vol.3 in Blue Note TOKYO」より！\n\nドラムは森高千里によるループドラムです！！"
  },
  '2016/9/2' => {
    link: 'n1ST87NTyvk',
    number: '190',
    song_id: :utopia,
    comment: "作詞：森高千里　作曲：河野伸\n\n公式チャンネル独占企画「200曲セルフカヴァー」190曲目は、1998年発表のアルバム「Sava Sava」から『ユートピア』！\n映像は「JCB Presents Chisato Moritaka Special Live vol.3 in Blue Note TOKYO」より！\n\nドラムは森高千里によるループドラムです！！"
  },
  '2016/9/9' => {
    link: 'ZdAJLKIsvw4',
    number: '191',
    song_id: :nozokanaide,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」191曲目は、1990年発表のシングル「臭いものにはフタをしろ!!」のカップリング曲「のぞかないで」！\n\n映像は2016年4月24日 Zepp Tokyoで行われたLIVE「森高千里 47 HARD NIGHTS」より！"
  },
  '2016/9/16' => {
    link: 'rJp-m4nsUu0',
    number: '192',
    song_id: :toga_tatsu,
    comment: "作詞：森高千里　作曲：伊秩弘将\n\n公式チャンネル独占企画「200曲セルフカヴァー」192曲目は、1996年発表のアルバム「TAIYO」から「薹が立つ」！\n\n映像は2015年10月27日Zepp DiverCity(TOKYO)で行われたLIVE「The Plain Moritaka Nights」より！"
  },
  '2016/12/17' => {
    link: 'jkSUpGSMnuo',
    number: '193',
    song_id: :jin_jin_jinglebell,
    j_title: "森高千里 『ジン ジン ジングルベル 2016』 【セルフカヴァー】",
    e_title: "Chisato Moritaka 『JIN JIN JINGLEBELL 2016』 【Self cover】",
    comment: "作詞・作曲：森高千里　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」193曲目は、\n1995年発表のシングル「ジン ジン ジングルベル」！\n今年もお待たせしました新バージョンです！\nドラムは森高千里によるループドラム！"
  },
  '2016/12/31' => {
    link: 'yKBorkIh4b0',
    number: '194',
    song_id: :ichigatsu_ichijitsu,
    j_title: "森高千里 『一月一日 2017』【セルフカヴァー】",
    e_title: "Chisato Moritaka 『{ICHIGATSU ICHIJITSU} 2017』【Self cover】",
    comment: "作詞：千家尊福　作曲：上真行　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」194曲目は、\n1995年発表のシングル「ジンジン ジングルベル」のC/W「一月一日」です。2017年の新バージョンをお楽しみ下さい。\nドラムは森高千里によるループドラムです！"
  },
  '2017/5/1' => {
    link: 'mrHG3MCOk6M',
    number: '195',
    song_id: :chounanto_inakamon,
    comment: "作詞：森高千里　作曲：伊秩弘将\n\n公式チャンネル独占企画「200曲セルフカヴァー」195曲目は、\n1996年発表のアルバム「TAIYO」から「長男と田舎もん」！\nドラムは森高千里による新録音！\n\n衣装協力：YANUK"
  },
  '2017/5/10' => {
    link: 'OHiZDJzW2yc',
    number: '196',
    song_id: :hikisakanaide_futario,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」196曲目は、\n1992年に加藤紀子のシングル用に書き下ろし、当時の森高が歌唱したバージョンを1999年のアルバム「harvest time」に収録した「引き裂かないで二人を」！\nドラムは森高千里によるループドラム！\n\n衣装協力：YANUK/原宿シカゴ表参道店"
  },
  '2017/5/17' => {
    link: '4YxMfYAZR18',
    number: '197',
    song_id: :zoku_aru_olno_seishun,
    comment: "作詞：森高千里　作曲：斉藤英夫　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」197曲目は、\n1992年発表のシングル「コンサートの夜」のカップリング「続・あるOLの青春 〜A子の場合〜」！\nドラムは森高千里による新録音！\n\n衣装協力：ヨーロピアン△カルチャー/ストックマン/LANDS'END"
  },
  '2017/5/24' => {
    link: 'zM3G-nEj4eU',
    number: '198',
    song_id: :hey_vodka,
    comment: "作詞：ドーシー魚塚　作曲：森高千里\n\n公式チャンネル独占企画「200曲セルフカヴァー」198曲目は、\n1996年発表のアルバム「TAIYO」から「HEY! VODKA」！\nドラムは森高千里によるループドラム！\n\n衣装協力：LANDS’END"
  },
  '2017/5/31' => {
    link: 'myUws5VcDU4',
    number: '199',
    song_id: :uwasa,
    comment: "作詞：森高千里　作曲：斉藤英夫　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」199曲目は、\n1989年発表のアルバム「森高ランド」から「うわさ」！\nドラムは森高千里による新録音！\n\n衣装協力：Sov./フィルム＆ECLIN△ルミネ新宿店"
  },
  '2017/6/7' => {
    link: 'WhiZBx2Xcbo',
    number: '200',
    song_id: :osharefu,
    comment: "作詞・作曲：森高千里\n\n公式チャンネル独占企画「200曲セルフカヴァー」200曲目は、\n1997年発表のアルバム「PEACHBERRY」から「おしゃれ風」！\nドラムは森高千里によるループドラム！\n\n衣装協力：ECLIN△ルミネ新宿店／YANUK"
  },
}
create_youtube_factory_from_hash(yt, yt_hash)
