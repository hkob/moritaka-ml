key = :dvd_live_get_smile
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key do
    {
      device_type: :live_video,
      event_date_id: '2000/8/23',
      minutes: 48,
      sort_order: 200008230,
      number: '1st',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4498.html'
    }
  end

  video.media_from_array [
    ["#{key}_dvd", :dvd, 'WPB6-90002', :first, true, :warner_vision_japan, '4,104'],
  ]

  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(%i[good_bye_season namida_good_bye anohino_photograph ringoshuno_rule yumeno_owari weekend_blue get_smile overheat_night_2 new_season].map { |key| [key, @vc] })

  concert = concert_factory :concert_over_get
  concert_hall = concert_hall_factory concert, 2
  concert_list = list_factory(:concert_over_get_A)
  concert_list_contents = concert_list.list_contents.order_sort_order
  list.list_contents.order_sort_order.each_with_index do |vsls, i|
    concert_video_factory concert_list_contents[i], vsls, concert_hall
  end
end

