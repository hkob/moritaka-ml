key = :concert_first_live
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  concert = concert_factory key, :shibuya_live_in_first_live do
    {
      concert_type: :live,
      from_id: '1987/9/7',
      has_song_list: true,
      sort_order: 198709070
    }
  end

  list = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }

  list.list_contents_from_array %i[otisreddingni_kanpai period miss_lady namida_good_bye anohino_photograph ringoshuno_rule yumeno_owari weekend_blue overheat_night new_season]

  concert_hall_factory(concert, 1) { {hall_id: :渋谷ライブイン, event_date_id: '1987/9/7', list_id: list.id} }
end
