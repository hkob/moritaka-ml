key = :concert_festival_1992
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  concert = concert_factory key, :festival_1992 do
    {
      concert_type: :festival,
      from_id: '1992/11/14',
      has_song_list: true,
      has_product: true,
      sort_order: 199211140,
      num_of_performances: 1,
      num_of_halls: 1
    }
  end

  list = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }

  list.list_contents_from_array %i[guitar] + @mc + %i[fight natsuno_umi watashiga_obasanni_nattemo] + @mc + %i[ame the_benkyono_uta mitsuketa_saifu] + ListContent.title_only('衣裳替え', 'Costume change') + %i[kusaimononiwa_futaoshiro rock_alive] + @mc + %i[alone seventeen] + @mc + %i[yacchimaina sonogono_watashi get_smile] + @encore + %i[konomachi] + @mc_member + %i[teriyaki_burger]

  concert.concert_halls_from_array [
    ConcertHall.mk('1992/11/14', :慶応大学_日吉校舎, list, 'ヤキソバ', 'Chow mein'),
  ]
end



