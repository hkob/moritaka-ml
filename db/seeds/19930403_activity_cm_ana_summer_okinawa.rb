pkey = :cm_ana
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  company_factory :ana
  activity_factory pkey do
    {
      activity_type: :cm,
      title_id: 'exist|ana',
      company_id: :ana,
      sort_order: 199304030,
    }
  end
end

key = :cm_ana_summer_okinawa
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_ana,
      j_title: "全日空(ANA) 「ANA'S SUMMER 沖縄」",
      e_title: %q{All Nihon Airline(ANA) "ANA'S SUMMER Okinawa"},
      from_id: '1993/4/3',
      to_id: '1993/7/19',
      j_comment: '砂浜を走りきる版 [15秒]|砂浜でこける版 [30秒]|*** キャンペーン期間: 1993年5月1日〜7月19日 ***',
      e_comment: 'She is running in the beach[15 seconds].|She is running in the beach, but ...[30 seconds].|*** Campain Period: 1993/5/1 -- 1993/7/19 ***',
      sort_order: 1,
      song_id: :watashino_natsu
    }
  end
end

