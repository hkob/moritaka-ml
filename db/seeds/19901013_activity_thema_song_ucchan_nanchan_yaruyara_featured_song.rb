key = :thema_song_ucchan_nanchan_yaruyara_featured_song
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :thema_song_ucchan_nanchan_yaruyara,
      j_title: '「ウッチャンナンチャンのやるならやらねば!」ハエ男コーナー挿入歌',
      e_title: 'Thema song for "{HAEOTOKO}" corner in a TV program "{UCCHAN NANCHAN-NO YARUNARA YARANEBA!}"',
      from_id: '1990/10/13',
      to_id: '1993/6/26',
      song_id: :haeotoko,
      sort_order: 199010140
    }
  end
end
