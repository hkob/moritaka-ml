key = :live_girlpop22
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  concert = concert_factory key do
    {
      concert_type: :etc,
      from_id: '1998/6/1',
      has_song_list: true,
      sort_order: 199806010,
      num_of_performances: 1,
      num_of_halls: 1,
    }
  end

  list_a = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }
  list_a.list_contents_from_array %i[kibun_soukai believe_in_love] + @mc + %i[gakuen_tengoku]


  concert.concert_halls_from_array [
    ['1998/6/1', :日清パワーステーション, list_a],
  ]
end
