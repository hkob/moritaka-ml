key = :single_mi_ha_
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :the_mi_ha_mi_ha_ do
    {
      device_type: 'ep_single|single',
      event_date_id: '1988/4/25',
      sort_order: 198804250,
      number: '4th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4022.html'
    }
  end

  single.media_from_array [
    ["#{key}_ep", :ep, 'K-1568', '1st', false, :warner_pioneer, '700'],
    ["#{key}_ct", :ct, 'LKC-2066', '1st', false, :warner_pioneer, '700'],
    ["#{key}_cds1", :cds, '10SL-16', '1st', false, :warner_pioneer, '937'],
    ["#{key}_cds2", :cds, '10SL-16', '2nd', false, :warner_music_japan, '937'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/za-miha-supesharu-miha-mikkusu/id528980756', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }

  list.list_contents_from_array [
    [:the_mi_ha_, %i[VOCAL iChisatoMoritaka REMIX iHideoSaito GUITAR iHideoSaito CHORUS iHideoSaito DRUMPR iHideoSaito SYNPR iHideoSaito SYNPR iNobitaTsukada KEYBOARDS iNobitaTsukada BGVOCAL iMisaNakayama TIMSOLO iChisatoMoritaka], '[スペシャル・ミーハー・ミックス]', '[Special {MI-HA-} mix]'],
    [:mi_ha_, @vc, '[オリジナル・ヴァージョン]', '[Original version]']
  ]
end
