pkey = :cm_house_foods
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  company_factory :house_foods
  activity_factory pkey do
    {
      activity_type: :cm,
      title_id: 'exist|house_foods',
      company_id: :house_foods,
      sort_order: 200405010,
    }
  end
end

key = :cm_house_foods_java_curry
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_house_foods,
      j_title: 'ハウス食品「ジャワカレー」',
      e_title: 'House foods "Java Curry"',
      from_id: '2004/5',
      to_id: '2009/4',
      sort_order: 200405010,
    }
  end
end

