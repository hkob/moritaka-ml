key = :single_lets_go
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :lets_go do
    {
      device_type: :single,
      event_date_id: '1997/2/25',
      sort_order: 199702250,
      number: '31st',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4284.html',
      j_comment: 'おまけステッカー付き(ローソンでの販売のみ)',
      e_comment: 'include a sticker (only in Lawson)'
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDA-38', '1st', false, :one_up_music, '1,000'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/lets-go!-ep/id531614263', '1st', true, :warner_music_japan, '1,000']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:lets_go, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka AGUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi APIANO iShinHashimoto EPIANO iShinHashimoto KEYBOARDS iShinHashimoto EGUITAR iYukioSeto BASS iYukioSeto WINDCHIME iYukioSeto PERCUSSION iYukioSeto]],
    [:lets_go2, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka AGUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi APIANO iShinHashimoto EPIANO iShinHashimoto KEYBOARDS iShinHashimoto EGUITAR iYukioSeto BASS iYukioSeto WINDCHIME iYukioSeto PERCUSSION iYukioSeto]],
    [:botto_shitemiyou, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka AGUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi APIANO iShinHashimoto EGUITAR iYukioSeto BASS iYukioSeto WINDCHIME iYukioSeto]],
    [:lets_go, %i[VOCAL iKaraoke DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka AGUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi APIANO iShinHashimoto EPIANO iShinHashimoto KEYBOARDS iShinHashimoto EGUITAR iYukioSeto BASS iYukioSeto WINDCHIME iYukioSeto PERCUSSION iYukioSeto], '(オリジナル・カラオケ)', '(Original karaoke)']
  ]
  device_activity_factory single, :cm_lawson
end



