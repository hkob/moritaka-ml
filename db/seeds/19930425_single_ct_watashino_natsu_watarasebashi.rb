key = :single_ct_watashino_natsu_watarasebashi
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :watashino_natsu_watarasebashi do
    {
      device_type: :ct_single,
      event_date_id: '1993/4/25',
      sort_order: 199304250,
      number: 'Special',
      singer_id: :iChisatoMoritaka,
    }
  end

  single.media_from_array [
    ["#{key}_ct", :ct, 'WPSL-4366', '1st', false, :warner_music_japan, '1,000'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:watashino_natsu, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka PIANO iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito SYNTHESIZER iHideoSaito]],
    [:watarasebashi, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka PIANO iChisatoMoritaka RECORDER iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito TAMBOURINE iHideoSaito SYNTHESIZER iHideoSaito]],
    [:watashino_natsu, %i[VOCAL iKaraoke ARRANGE iHideoSaito DRUMS iChisatoMoritaka PIANO iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito SYNTHESIZER iHideoSaito], '[オリジナル・カラオケ]', '[Original karaoke]'],
    [:watarasebashi, %i[VOCAL iKaraoke ARRANGE iHideoSaito DRUMS iChisatoMoritaka PIANO iChisatoMoritaka RECORDER iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito TAMBOURINE iHideoSaito SYNTHESIZER iHideoSaito], '[オリジナル・カラオケ]', '[Original karaoke]']
  ]
  device_activity_factory single, :cm_ana
  device_activity_factory single, :thema_song_iitabi_yumekibun
end

