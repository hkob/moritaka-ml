key = :concert_festival_1988
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  concert = concert_factory key, :festival_1988 do
    {
      concert_type: :festival,
      from_id: '1988/10/9',
      to_id: '1988/12/11',
      has_song_list: true,
      sort_order: 198810090,
      num_of_performances: 13,
      num_of_halls: 13
    }
  end

  list = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }

  list.list_contents_from_array(%i[get_smile overheat_night] + @mc + %i[cant_say_good_bye kiss_the_night weekend_blue] + @mc + %i[stress mite] + @mc + %i[good_bye_season yokohama_one_night] + [[ nil, [], nil, nil, '川崎では1コーラス目を"KAWASAKI ONE NIGHT"と唄う', 'She sang "KAWASAKI ONE NIGHT" instead of "YOKOHAMA ONE NIGHT" at the first chorus.']] + %i[forty_seven_hard_nights let_me_go mi_ha_] + @encore + %i[new_season] + @mc + %i[alone])

  concert.concert_halls_from_array [
    ['1988/10/9', :豊橋科学技術大学],
    ['1988/10/13', :有明MZAコンベンション],
    ['1988/10/28', :新潟薬科大学],
    ['1988/10/29', :金沢大学],
    ['1988/10/30', :高岡短期大学],
    ['1988/10/31', :愛媛文化講堂, nil, '(公録)', '(Public recording)'],
    ['1988/11/1', :滋賀大学],
    ['1988/11/4', :立教女学院],
    ['1988/11/5', :信州大学],
    ['1988/11/6', :新潟産業大学],
    ['1988/11/12', :東京都立科学技術大学, list],
    ['1988/11/13', :東洋大学],
    ['1988/11/15', :クラブチッタ川崎, list],
    ['1988/11/19', :帝京技術大学],
    ['1988/11/21', :九州工業大学],
    ['1988/12/10', :八代厚生年金会館],
    ['1988/12/11', :長崎NBCホール, nil, '(公録)', '(Public recording)']
  ]
end
