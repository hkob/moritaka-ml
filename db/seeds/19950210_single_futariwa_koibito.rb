key = :single_futariwa_koibito
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :futariwa_koibito do
    {
      device_type: :single,
      event_date_id: '1995/2/10',
      sort_order: 199502100,
      number: '25th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4268.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDA-10', '1st', false, :one_up_music, '1,000'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/er-renha-lian-ren-ep/id531608295', '1st', true, :warner_music_japan, '1,000']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:futariwa_koibito, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka COWBELL iChisatoMoritaka APIANO iYasuhikoFukuda GUITAR iHideoSaito BASS iHideoSaito SYNTHESIZER iHideoSaito TAMBOURINE iHideoSaito CHORUS iHideoSaito]],
    [:wakasano_hiketsu, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka RGUITAR iChisatoMoritaka GUITARSOLO iChisatoMoritaka APIANO iYasuakiMaejima GUITAR iYuichiTakahashi BASS iYukioSeto], '(シングル・ヴァージョン)', '(Single version)'],
    [:futariwa_koibito, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka COWBELL iChisatoMoritaka APIANO iYasuhikoFukuda GUITAR iHideoSaito BASS iHideoSaito SYNTHESIZER iHideoSaito TAMBOURINE iHideoSaito CHORUS iHideoSaito], '(ドラマ・オープニング・サイズ)', '(Drama opening size)'],
    [:futariwa_koibito, %i[VOCAL iKaraoke ARRANGE iHideoSaito DRUMS iChisatoMoritaka COWBELL iChisatoMoritaka APIANO iYasuhikoFukuda GUITAR iHideoSaito BASS iHideoSaito SYNTHESIZER iHideoSaito TAMBOURINE iHideoSaito CHORUS iHideoSaito], '(オリジナル・カラオケ)', '(Original karaoke)']
]
  device_activity_factory single, :thema_song_koimo_nidomenara
end

