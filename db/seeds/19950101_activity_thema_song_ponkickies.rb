key = :thema_song_ponkickies_sb
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :thema_song_ponkickies,
      j_title: 'テレビ番組 (kids) 「ポンキッキーズ」主題歌',
      e_title: 'Thema songs for TV program (kids) "Ponkickies"',
      from_id: '1993/10',
      to_id: '1994/3',
      song_id: :rockn_omelette,
      sort_order: 199310010
    }
  end
end

