key = :concert_nihono_sukue
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  concert = concert_factory key, :nihono_sukue do
    {
      j_subtitle: '阪神大震災被災者支援コンサート',
      e_subtitle: 'Concert for supporting victims of Hanshin earthquake',
      concert_type: :etc,
      from_id: '1995/8/12',
      has_song_list: true,
      sort_order: 199508120,
      num_of_performances: 1,
      num_of_halls: 1,
    }
  end

  list_a = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }
  list_a.list_contents_from_array [
    [:true_love, [], '(バック・コーラス)', '(Back Chorus)'],
    [:watashino_natsu],
    [:rockn_roll_nya_kene_kakaru, [], '(ドラム演奏)', '(play the drums)']
  ]

  concert.concert_halls_from_array [
    ['1995/8/12', :メリケンパーク, list_a],
  ]
end

