key = :video_clips_kibun_soukai
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key, :kibun_soukai do
    {
      device_type: :video_clips,
      event_date_id: '1994/11/30',
      minutes: 45,
      sort_order: 199411300,
      number: '5th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4337.html'
    }
  end

  video.media_from_array [
    ["#{key}_vhs1", :vhs, 'EPVA-2', :first, false, :one_up_music, '4,800' ],
    ["#{key}_ld1", :ld, 'EPLA-2', :first, false, :one_up_music, '4,800' ],
  ]

  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:watarasebashi, %i[VOCAL iChisatoMoritaka], nil, nil, 'スポットCM... LIVE ROCK ALIVE', 'TV SPOT CM... LIVE ROCK ALIVE'],
    [:watashino_natsu, %i[VOCAL iChisatoMoritaka], nil, nil, 'スポットCM... 私の夏', 'TV SPOT CM... {WATASHI-NO NATSU}'],
    [:haeotoko, %i[VOCAL iChisatoMoritaka], '(ロング・ヴァージョン)', '(Long version)', 'スポットCM... ハエ男', 'TV SPOT CM... {HAEOTOKO}'],
    [:kazeni_fukarete, %i[VOCAL iChisatoMoritaka], nil, nil, 'スポットCM... 風に吹かれて', 'TV SPOT CM... {KAZE-NI FUKARETE}'],
    [:rockn_omelette, %i[VOCAL iChisatoMoritaka], nil, nil, 'スポットCM... ロックン・オムレツ', "TV SPOT CM... Rock'n Omelette"],
    [:kibun_soukai, %i[VOCAL iChisatoMoritaka], nil, nil, 'スポットCM... 気分爽快(発売前)|スポットCM... 気分爽快(発売後)', 'TV SPOT CM... {KIBUN-SOKAI}(pre release)|TV SPOT CM... {KIBUN-SOKAI}(on sale)'],
    [:natsuno_hi, %i[VOCAL iChisatoMoritaka], nil, nil, 'スポットCM... 夏の日(発売前)|スポットCM... 夏の日(発売後)|スポットCM... STEP BY STEP', 'TV SPOT CM... {NATSU-NO HI}(pre release)|TV SPOT CM... {NATSU-NO HI}(on sale)|TV SPOT CM... STEP BY STEP'],
    [:sutekina_tanjoubi, %i[VOCAL iChisatoMoritaka], nil, nil, 'スポットCM... 素敵な誕生日', 'TV SPOT CM... {SUTEKI-NA TANJOBI}'],
    [:watashino_daijina_hito, %i[VOCAL iChisatoMoritaka], nil, nil, 'スタッフロール BGM「素敵な誕生日(カラオケ)」', 'Staff roll BGM... {SUTEKI-NA TANJOBI} (Karaoke)'],
  ]
end


