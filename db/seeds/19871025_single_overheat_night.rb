key = :single_overheat_night
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :overheat_night_2 do
    {
      device_type: :ep_single,
      event_date_id: '1987/10/25',
      sort_order: 198710250,
      number: '2nd',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4008.html'
    }
  end

  single.media_from_array [
    ["#{key}_ep", :ep, 'K-1566', '1st', false, :warner_pioneer, '700'],
    ["#{key}_ct", :ct, 'LKC-2043', '1st', false, :warner_pioneer, '1,000'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/obahito-naito-single/id528974648', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }

  list.list_contents_from_array [
    [:overheat_night_2, @vcah],
    [:weekend_blue, @vc]
  ]
end
