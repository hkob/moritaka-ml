key = :single_yasumino_gogo
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :yasumino_gogo do
    {
      device_type: :single,
      event_date_id: '1995/10/10',
      sort_order: 199510100,
      number: '26th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4274.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDA-17', '1st', false, :one_up_music, '1,000'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/xiumino-wu-hou-single/id531612857', '1st', true, :warner_music_japan, '750']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:yasumino_gogo, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka GUITARS iHideoSaito BASS iHideoSaito TAMBOURINE iHideoSaito CHORUS iHideoSaito PROGRAMMING iHideoSaito APF iYasuakiMaejima ORGAN iYasuakiMaejima]],
    [:wasuremono, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka ALLINST iHideoSaito]],
    [:yasumino_gogo, %i[VOCAL iKaraoke ARRANGE iHideoSaito DRUMS iChisatoMoritaka GUITARS iHideoSaito BASS iHideoSaito TAMBOURINE iHideoSaito CHORUS iHideoSaito PROGRAMMING iHideoSaito APF iYasuakiMaejima ORGAN iYasuakiMaejima], '(オリジナル・カラオケ)', '(Original karaoke)']
]
  device_activity_factory single, :thema_song_iitabi_yumekibun
end
