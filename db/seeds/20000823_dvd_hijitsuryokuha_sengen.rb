key = :dvd_hijitsuryokuha_sengen
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key do
    {
      device_type: :live_video,
      event_date_id: '2000/8/23',
      minutes: 58,
      sort_order: 200008232,
      number: '3rd',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4496.html'
    }
  end

  video.media_from_array [
    ["#{key}_dvd", :dvd, 'WPB6-90004', :first, true, :warner_vision_japan, '4,104'],
  ]

  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:korekkiri_byebye, @vc],
    [:hijitsuryokuha_sengen, @vc],
    [:hatachi, @vc],
    [:the_stress, @vc, '[ストレス中近東ヴァージョン]', '[Stress {CHUUKINTOU} version]'],
    [:shiritagari, @vc],
    [:watashiwa_onchi, @vc],
    [:seventeen, @vc],
    [:yoruno_entotsu, @vc],
    [:get_smile, @vc],
    [:mite, @vc],
    [:daite, @vc, '[ラスベガス・ヴァージョン]', '[Las Vegas version]'],
    [:seventeen, @vc, '[オレンジ・ミックス]', '[Orange Mix]'],
    [:kaiouseino_densetsu, [], '[インストゥルメンタル]', '[Instrumental]'],
  ]

  concert = concert_factory :tour_hijitsuryokuha_sengen
  concert_hall = concert_hall_factory concert, 2
  concert_list = list_factory(:tour_hijitsuryokuha_sengen_A)
  concert_list_contents = concert_list.list_contents.order_sort_order
  csos = { 0 => 0, 1 => 1, 2 => 2, 3 => 6, 4 => 7, 5 => 8, 6 => 11, 7 => 13, 8 => 15, 9 => 16, 10 => 18, 11 => 19 }

  list.list_contents.order_sort_order.each_with_index do |vsls, i|
    concert_video_factory concert_list_contents[csos[i]], vsls, concert_hall if csos[i]
  end
end


