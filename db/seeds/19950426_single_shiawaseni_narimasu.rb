key = :single_shiawaseni_narimasu
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :shiawaseni_narimasu do
    {
      device_type: :lyric_single,
      event_date_id: '1995/4/26',
      sort_order: 199504260,
      number: '9th',
      singer_id: :iSanaeJonouchi,
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'PSDF-5004', '1st', false, :y_j_sounds, '1,000'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:shiawaseni_narimasu, %i[VOCAL iSanaeJonouchi ARRANGE iHideoSaito]],
    [nil, %i[VOCAL iSanaeJonouchi], nil, nil, '止められない悲しみ', '{TOMERASENAI KANASHIMI}'],
    [:shiawaseni_narimasu, %i[VOCAL iKaraoke ARRANGE iHideoSaito], '(オリジナル・カラオケ)', '(Original karaoke)']
  ]
  device_activity_factory single, :thema_song_iitabi_yumekibun
end


