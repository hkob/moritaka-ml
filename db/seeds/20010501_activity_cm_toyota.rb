pkey = :cm_toyota
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  company_factory :toyota
  activity_factory pkey do
    {
      activity_type: :cm,
      title_id: 'exist|toyota',
      company_id: :toyota,
      sort_order: 200105010,
    }
  end
end

key = :cm_toyota_corolla
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_toyota,
      j_title: 'トヨタ「カローラ SPACIO」',
      e_title: 'TOYOTA "Carolla SPACIO"',
      from_id: '2001/5',
      to_id: '2002/5',
      sort_order: 200105010,
    }
  end
end

