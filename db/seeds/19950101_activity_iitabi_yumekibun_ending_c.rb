key = :thema_song_iitabi_yumekibun_c
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :thema_song_iitabi_yumekibun,
      j_title: '旅行番組 「いい旅・夢気分」エンディングテーマ',
      e_title: 'Ending thema song of TV program (travel) "{II-TABI YUME-KIBUN}',
      from_id: '1995',
      to_id: '1995',
      song_id: :shiawaseni_narimasu,
      sort_order: 199501010
    }
  end
end

key = :thema_song_iitabi_yumekibun_d
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :thema_song_iitabi_yumekibun,
      j_title: '旅行番組 「いい旅・夢気分」エンディングテーマ',
      e_title: 'Ending thema song of TV program (travel) "{II-TABI YUME-KIBUN}',
      from_id: '1995_2',
      to_id: '1995_2',
      song_id: :yasumino_gogo,
      sort_order: 199501020
    }
  end
end



