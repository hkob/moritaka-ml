key = :video_clips_seventeen
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key, :seventeen do
    {
      device_type: :video_clips,
      event_date_id: '1989/11/10',
      minutes: 21,
      sort_order: 198911100,
      number: '2nd',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4323.html'
    }
  end

  video.media_from_array [
    ["#{key}_vhs1", :vhs, '30L8-8055', :first, false, :warner_pioneer, '3,090'],
    ["#{key}_ld1", :ld, '30L6-8055', :first, false, :warner_pioneer, '3,090'],
    ["#{key}_vhs2", :vhs, '30L8-8055', :second, false, :warner_music_japan, '3,090'],
    ["#{key}_ld2", :ld, '30L6-8055', :second, false, :warner_music_japan, '3,090'],
  ]

  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:seventeen, @vc],
    [:daite, @vc, '[ラスベガス・ヴァージョン]', '[Las Vegas version]'],
    [:yoruno_entotsu, @vc],
    [:watashiwa_onchi, @vc, 'ザ・メイキング', 'The making']
  ]
end
