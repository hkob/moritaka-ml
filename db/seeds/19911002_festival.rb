key = :concert_festival_1991
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  concert = concert_factory key, :festival_1991 do
    {
      concert_type: :festival,
      from_id: '1991/10/2',
      to_id: '1991/11/24',
      has_song_list: true,
      sort_order: 199110020,
      num_of_performances: 16,
      num_of_halls: 16
    }
  end

  list_a, list_b = %i[A B].map.with_index { |k, i| list_factory("#{key}_#{k}") { {concert_id: concert, keyword: k, sort_order: i+1} } }

  list_a.list_contents_from_array %i[akunno_higeki uchini_kagitte the_benkyono_uta aru_olno_seishun] + [[nil, [], 'ロックンロール・メドレー(?)', "Rock'n Roll medley(?)", 'MEDLEY_IN', 'MEDLEY_IN']] + %i[kusaimononiwa_futaoshiro rockn_roll_widow] + @medley_out + %i[michi the_nozokanaide kanojo] + [[:konomachi, [], '[HOME MIX]', '[HOME MIX]']] + %i[the_mi_ha_ sonogono_watashi funkey_monkey_baby get_smile] + @encore + %i[ame new_season teriyaki_burger] + @double_encore + %i[akunno_higeki]
  list_b.list_contents_from_array %i[kanojo the_benkyono_uta aru_olno_seishun uchini_kagitte] + [[nil, [], 'ロックンロール・メドレー(?)', "Rock'n Roll medley(?)", 'MEDLEY_IN', 'MEDLEY_IN']] + %i[kusaimononiwa_futaoshiro rockn_roll_widow] + @medley_out + %i[fight the_nozokanaide akunno_higeki new_season the_mi_ha_ sonogono_watashi funkey_monkey_baby get_smile] + @encore + %i[ame] + [[:konomachi, [], '[HOME MIX]', '[HOME MIX]']] + %i[teriyaki_burger]

  concert.concert_halls_from_array [
    ['1991/10/2', :学法福島高校],
    ['1991/10/6', :大阪医科大学, list_a],
    ['1991/10/13', :石巻大学, list_a],
    ['1991/10/15', :秋田経済法科大学, list_a],
    ['1991/10/18', :いわき明星大学, list_a],
    ['1991/10/19', :神奈川大学, list_b],
    ['1991/10/25', :日本大学郡山, list_b],
    ['1991/10/27', :白百合女子大学, list_b],
    ['1991/10/31', :中央大学, list_b],
    ['1991/11/2', :山梨学院大学, list_b],
    ['1991/11/3', :城西大学, list_b],
    ['1991/11/5', :九州産業大学, list_b],
    ['1991/11/7', :岐阜市文化センター, list_b],
    ['1991/11/9', :清泉女子大学, list_b],
    ['1991/11/19', :静岡大学, list_b],
    ['1991/11/24', :鹿児島経済大学, list_b]
  ]
end


