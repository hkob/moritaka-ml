key = :thema_song_video_anataga_shuyaku
obj = Activity.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity = activity_factory key do
    {
      activity_type: :thema_song,
      title_id: 'バラエティ「ビデオ あなたが主役」|A variety show "Video {ANATAGA SHUYAKU}"|びでお　あなたがしゅやく',
      j_comment: 'テーマ曲',
      e_comment: 'Thema song',
      company_id: :tv_asahi,
      from_id: '1990/10',
      to_id: '1992/9',
      song_id: :hachigatsuno_koi,
      sort_order: 199010010
    }
  end
end


