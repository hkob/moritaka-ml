key = :album_the_karaoke_vol1
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :the_karaoke_vol1 do
    {
      device_type: :karaoke_cd,
      event_date_id: '1992/8/25',
      minutes: 49,
      seconds: 31,
      sort_order: 199208250,
      number: '1st',
      singer_id: :iChisatoMoritaka,
      j_comment: '私がオバさんになっても 振り付けイラスト付き',
      e_comment: 'An illustlation of choreograph for "{WATASHI-GA OBASAN-NI NATTEMO}"',
    }
  end

  album.media_from_array [
    ["#{key}_cd", :cd, 'WPCL-677', :first, false, :warner_music_japan, '1,800'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  performer = %i[VOCAL iKaraoke]
  list.list_contents_from_array [
    [:watashiga_obasanni_nattemo, performer, '[シングル・ヴァージョン]', '[Single version]'],
    [:ame, performer],
    [:kusaimononiwa_futaoshiro, performer],
    [:konomachi, performer],
    [:seventeen, performer],
    [:benkyono_uta, performer],
    [:michi, performer],
    [:seishun, performer],
    [:fight, performer, '[アルバム・ヴァージョン]', '[Album version]'],
    [:new_season, performer],
    [:yowaseteyo_konyadake, performer],
  ]
end
