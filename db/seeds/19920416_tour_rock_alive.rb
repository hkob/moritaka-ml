key = :tour_rock_alive
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  band = band_factory(:the_london)
  band.band_members_from_array %i[VOCAL iChisatoMoritaka GUITAR iChisatoMoritaka KEYBOARDS iYasuakiMaejima CHORUS iYasuakiMaejima KEYBOARDS iShinKohno GUITAR iShinKohno CHORUS iShinKohno GUITAR iHiroyoshiMatsuo CHORUS iHiroyoshiMatsuo DRUMS iToshihiroTsuchiya BASS iMasafumiYokoyama CHORUS iMasafumiYokoyama]

  concert = concert_factory key do
    {
      j_subtitle: "森高千里コンサートツアー'92",
      e_subtitle: "Chisato Moritaka concert tour '92",
      concert_type: :tour,
      from_id: '1992/4/16',
      to_id: '1992/10/31',
      has_song_list: true,
      has_product: true,
      sort_order: 199204160,
      num_of_performances: 60,
      num_of_halls: 55,
      j_comment: 'JA共済Presents|女性アーティストとして2番目の全都道府県制覇ツアー',
      e_comment: 'JA cooperative society presents|She is the second female artist that gave the concert in all prefectures.',
      band_id: :the_london
    }
  end

  list_a, list_b, list_c = %i[A B C].map.with_index { |k, i| list_factory("#{key}_#{k}") { {concert_id: concert, keyword: k, sort_order: i+1} } }
  list_a.list_contents_from_array [[:concert_no_yoru, [], '[アルバム・ヴァージョン]', '[Album version]']] + %i[rhythm_to_bass fight natsuno_umi wakarimashita guitar yowaseteyo_konyadake the_blue_blues mitsuketa_saifu benkyono_uta rock_alive kusaimononiwa_futaoshiro ame seventeen yacchimaina sonogono_watashi get_smile amenochi_hare] + @encore + %i[watashiga_obasanni_nattemo misaki konomachi] + @double_encore + %i[new_season]
  list_b.list_contents_from_array [[:concert_no_yoru, [], '[アルバム・ヴァージョン]', '[Album version]']] + %i[rhythm_to_bass] + @mc + %i[fight natsuno_umi watashiga_obasanni_nattemo] + @mc + %i[yowaseteyo_konyadake the_blue_blues benkyono_uta wakarimashita mitsuketa_saifu] + ListContent.title_only('衣裳替え', 'Costume change') + %i[rock_alive kusaimononiwa_futaoshiro] + @mc + %i[ame seventeen yacchimaina sonogono_watashi get_smile amenochi_hare] + @encore + %i[new_season konomachi] + @double_encore + %i[guitar]
  list_c.list_contents_from_array [[:concert_no_yoru, [], '[アルバム・ヴァージョン]', '[Album version]']] + %i[rhythm_to_bass] + @mc + %i[fight natsuno_umi watashiga_obasanni_nattemo] + @mc + %i[yowaseteyo_konyadake the_blue_blues benkyono_uta wakarimashita mitsuketa_saifu] + ListContent.title_only('衣裳替え', 'Costume change') + %i[rock_alive kusaimononiwa_futaoshiro] + @mc + %i[ame seventeen yacchimaina sonogono_watashi get_smile amenochi_hare] + @encore + %i[guitar konomachi] + @double_encore + %i[seishun]

  concert.concert_halls_from_array [
    ConcertHall.mk('1992/4/16', :北とぴあさくらホール, list_a, 'ラーメン', '{RAMEN}'),
    ConcertHall.mk('1992/4/20', :岡山市民会館, nil, '????', '????'),
    ConcertHall.mk('1992/4/21', :広島厚生年金会館, nil, 'お好み焼き', '{OKONOMIYAKI}'),
    ConcertHall.mk('1992/4/23', :徳山市文化会館, nil, '????', '????'),
    ConcertHall.mk('1992/5/1', :宇都宮市文化会館, list_b, 'しもつかれ', '{SHIMOTSUKARE}'),
    ConcertHall.mk('1992/5/6', :鹿児島県文化センター, list_c, 'さつまあげ', '{SATSUMAAGE}'),
    ConcertHall.mk('1992/5/8', :宮崎市民会館, list_c, '冷し汁', '{HIYASHI-JIRU}'),
    ConcertHall.mk('1992/5/10', :大分文化会館, list_c, '団子汁', '{DANGO-JIRU}'),
    ConcertHall.mk('1992/5/13', :静岡市民文化会館, list_c, 'ウナギ', 'Eels'),
    ConcertHall.mk('1992/5/16', :奈良県文化会館, list_c, '芝漬け', '{SHIBAZUKE}'),
    ConcertHall.mk('1992/5/18', :姫路市文化センター, list_c, 'そうめん', '{SOMEN}'),
    ConcertHall.mk('1992/5/19', :和歌山市民会館, list_c, '那智黒', '{NACHIGURO?}'),
    ConcertHall.mk('1992/5/23', :群馬県民会館, list_c, 'だるま弁当', '{DARUMA BENTO}'),
    ConcertHall.mk('1992/5/25', :千葉県文化会館, list_c, '落花生', 'Peanuts'),
    ConcertHall.mk('1992/5/30', :山梨県立県民文化ホール, list_c, 'ほうとう', '{HO-TO-}'),
    ConcertHall.mk('1992/6/3', :中野サンプラザ, list_c, '馬刺し', 'Slices of raw horse'),
    ConcertHall.mk('1992/6/4', :中野サンプラザ, list_c, 'とんこつラーメン', '{TONKOTSU-RAMEN}'),
    ConcertHall.mk('1992/6/15', :京都会館, list_c, '天一ラーメン', "{TEN'ICHI RAMEN}", '第１ホール', 'First hall'),
    ConcertHall.mk('1992/6/17', :沼津市民文化センター, list_c, 'アジの開き', '{AJI-NO HIRAKI}'),
    ConcertHall.mk('1992/6/18', :浜松市民会館, list_c, 'ウナギ', 'Eels'),
    ConcertHall.mk('1992/6/22', :新潟県民会館, list_c, 'コシヒカリ', '{KOSHIHIKARI}(rice)'),
    ConcertHall.mk('1992/6/24', :郡山市民文化センター, list_c, 'イカ人参', '{IKA-NINJIN}'),
    ConcertHall.mk('1992/6/28', :山形市民会館, list_c, '米沢牛', 'Yonezawa Beef'),
    ConcertHall.mk('1992/6/29', :秋田市文化会館, list_c, '????', '????'),
    ConcertHall.mk('1992/7/1', :青森市文化会館, list_c, '????', '????'),
    ConcertHall.mk('1992/7/6', :金沢市文化ホール, list_c, '????', '????'),
    ConcertHall.mk('1992/7/7', :福井市文化会館, list_c, '越前ソバ', "Echizen's Buckwheat"),
    ConcertHall.mk('1992/7/9', :長野県県民文化会館, list_c, '信州ソバ', "Shinsyu's Buckwheat"),
    ConcertHall.mk('1992/7/10', :富山県民会館, list_c, '????', '????'),
    ConcertHall.mk('1992/7/15', :四日市市文化会館, list_c, '赤福', '{AKAFUKU}'),
    ConcertHall.mk('1992/7/16', :岐阜市民会館, list_c, 'アユ', '{AYU}'),
    ConcertHall.mk('1992/7/18', :豊橋勤労福祉会館, list_c, 'ちくわ', '{CHIKUWA}'),
    ConcertHall.mk('1992/7/27', :守山市民ホール, list_c, '鮒寿司', '{FUNAZUSHI}'),
    ConcertHall.mk('1992/7/29', :神戸国際会館, list_c, '神戸牛ステーキ', "Steak of Kobe' beef"),
    ConcertHall.mk('1992/8/7', :米子市公会堂, list_c, '大山そば', "Oyama's Buckwheat"),
    ConcertHall.mk('1992/8/8', :出雲市民会館, list_c, '出雲そば', "Izumo's Buckwheat"),
    ConcertHall.mk('1992/8/17', :大宮ソニックシティ, list_c, '十万石饅頭', '{JUMANGOKU MANJU}'),
    ConcertHall.mk('1992/8/19', :神奈川県民ホール, list_c, '中華料理', 'Chinese cooking'),
    ConcertHall.mk('1992/8/24', :岩手県民会館, list_c, '????', '????'),
    ConcertHall.mk('1992/8/25', :宮城県民会館, list_c, '笹かま', '{SASA-KAMABOKO}(boiled fish paste)'),
    ConcertHall.mk('1992/8/27', :茨城県立県民文化センター, list_c, '納豆', 'Fermented soybeans'),
    ConcertHall.mk('1992/8/30', :高松市民会館, list_c, '讃岐うどん', "Sanuki's noodles"),
    ConcertHall.mk('1992/8/31', :徳島市立文化センター, list_c, '鳴門わかめ', "Naruto's {WAKAME}"),
    ConcertHall.mk('1992/9/2', :松山市民会館, list_c, '????', '????'),
    ConcertHall.mk('1992/9/7', :福岡市民会館, list_c, '辛子めんたい', '{KARASHI-MENTAI}'),
    ConcertHall.mk('1992/9/8', :長崎市公会堂, list_c, '????', '????'),
    ConcertHall.mk('1992/9/10', :熊本市民会館, list_c, '馬刺し', 'Slices of raw horse'),
    ConcertHall.mk('1992/9/12', :佐賀市民会館, list_c, 'むつごろうと有明海苔', "{MUTSUGORO} and Ariake's laver"),
    ConcertHall.mk('1992/9/16', :沖縄市民会館, list_c, 'ソーキそば', '{SOKI-SOBA}'),
    ConcertHall.mk('1992/9/21', :高知県民文化ホール, list_c, '????', '????'),
    ConcertHall.mk('1992/9/24', :大阪厚生年金会館, list_c, 'たこやき', '{TAKOYAKI}'),
    ConcertHall.mk('1992/9/29', :中野サンプラザ, list_c, '東京醤油ラーメン', 'Tokyo soy {RAMEN}'),
    ConcertHall.mk('1992/9/30', :中野サンプラザ, list_c, '馬刺し', 'Slices of raw horse', nil, nil, 'ビデオ収録', 'Video recording'),
    ConcertHall.mk('1992/10/3', :松本文化会館, list_c, '馬刺し', 'Slices of raw horse'),
    ConcertHall.mk('1992/10/6', :伊勢崎市文化会館, list_c, '焼き饅頭', '{YAKI-MANJU}'),
    ConcertHall.mk('1992/10/10', :旭川市民文化会館, list_c, '????', '????'),
    ConcertHall.mk('1992/10/11', :北海道厚生年金会館, list_c, '札幌ラーメン', "Sapporo's {RAMEN}"),
    ConcertHall.mk('1992/10/19', :名古屋センチュリーホール, list_c, 'きしめん', '{KISHIMEN}'),
    ConcertHall.mk('1992/10/25', :中野サンプラザ, list_c, '馬刺し', 'Slices of raw horse'),
    ConcertHall.mk('1992/10/31', :中野サンプラザ, list_c, '馬刺し', 'Slices of raw horse')
  ]
end
