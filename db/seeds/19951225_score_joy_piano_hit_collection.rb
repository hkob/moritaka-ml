key = :score_joy_piano_hit_collection
obj = Book.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  book = book_factory key do
    {
      book_type: :score_book,
      publisher_id: :tokyo_ongaku_shoin,
      isbn: '4-8114-2473-5 C0073 P1442E',
      price: '1,442',
      event_date_id: '1995/12/25',
      sort_order: 199512250,
    }
  end

  list = list_factory("#{key}_A") { {book_id: book, keyword: :A, sort_order: 1} }
  list.list_contents_from_array %i[yasumino_gogo jin_jin_jinglebell futariwa_koibito sutekina_tanjoubi watashino_daijina_hito natsuno_hi kyoukara kibun_soukai rockn_omelette kazeni_fukarete memories watarasebashi ame watashiga_obasanni_nattemo watashino_natsu haeotoko rockn_roll_kencho_shozaichi95]
end

