key = :thema_song_ikenai_joshikou
obj = Activity.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity = activity_factory key do
    {
      activity_type: :thema_song,
      title_id: %q(ドラマ「いけない女子高物語」|TV drama "{IKENAI JOSHIKO MONOGATARI}"|どらま　いけないじょしこうものがたり),
      j_comment: '主題歌',
      e_comment: 'Thema Song',
      company_id: :nihon_tv,
      from_id: '1990/1/13',
      to_id: '1990/3/24',
      song_id: :seishun,
      sort_order: 199001130
    }
  end
end

