key = :album_no1_hits_japan
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :no1_hits_japan do
    {
      device_type: 'compilation_album',
      event_date_id: '1995/10/25',
      minutes: 71,
      seconds: 05,
      singer_id: :iVariousArtists,
      sort_order: 199510250,
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'WPC6-8158', :first, false, :wea_japan, '3,000'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_only('あなた / 小坂明子', '{ANATA} / Akiko Kosaka') +
    ListContent.title_only('精霊流し / さだまさし', '{SHOUROU NAGASHI} / Masashi Sada') +
    ListContent.title_only('池上線 / 西島三重子', '{IKEGAMI SEN} / Mieko Nishijima') +
    ListContent.title_only('酒と涙と男と女 / 河島英五', '{SAKE-TO NAMIDA-TO OTOKO-TO ONNA} / Eigo Kawashima') +
    ListContent.title_only('THEME FROM "PROOF OF THE MAN" / ジョー山中', 'THEME FROM "PROOF OF THE MAN" / Joh Yamanaka') +
    ListContent.title_only('白いページの中に / 柴田まゆみ', '{SHIROI Page-NO NAKANI} / Mayumi Shibata') +
    ListContent.title_only('関白宣言 / さだまさし', '{KANPAKU SENGEN} / Masahi Sada') +
    ListContent.title_only('THIS IS A SONG FOR COCA-COLA / 矢沢永吉', 'THIS IS A SONG FOR COCA-COLA / Eikichi Yazawa') +
    ListContent.title_only('青い瞳のステラ，1962 年夏…  / 柳ジョージ & レイニーウッド', '{AOI HITOMI-NO SUTERA}, Summer 1962... / Johji Yanagi & Rainy Wood') +
    ListContent.title_only('翼の折れたエンジェル / 中村あゆみ', '{TSUBASA-NO ORETA} Angel / Ayumi Nakamura') +
    ListContent.title_only('今夜だけきっと / スターダスト・レビュー', '{KONYA-DAKE KITTO} / Stardust Revue') +
    ListContent.title_only('P.S. I LOVE YOU / ピンク・サファイア', 'P.S. I LOVE YOU / Pink Sapphire') +
    [[:ame, %i[VOCAL iChisatoMoritaka]]] +
    ListContent.title_only('どんなときも / 槇原敬之', '{DONNATOKIMO} / Noriyuki Makihara') +
    ListContent.title_only('ぼくたちの失敗 / 森田童子', '{BOKUTACHI-NO SHIPPAI} / Doji Morita') +
    ListContent.title_only('瞳を僕にちかづけて / コルベッツ', '{HITOMI-WO BOKU-NI CHIKAZUKETE} / Corvettes')
  )
end
