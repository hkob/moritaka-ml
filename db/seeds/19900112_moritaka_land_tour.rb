key = :moritaka_land_tour
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  concert = concert_factory key do
    {
      j_subtitle: "森高千里コンサートツアー '90",
      e_subtitle: "Chisato Moritaka concert tour '90",
      concert_type: :tour,
      from_id: '1990/1/12',
      to_id: '1990/3/7',
      has_song_list: true,
      sort_order: 199001120,
      num_of_performances: 21,
      num_of_halls: 21,
    }
  end

  list = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }
  list.list_contents_from_array %i[nozokanaide uwasa mite akunno_higeki yumeno_nakano_kiss good_bye_season yoruno_entotsu yumeno_owari michi seventeen] + ListContent.medley_in('ピンクレディメドレー', 'Pink Lady medley') + %i[sergent_pepper sos nagisano_sindbad southpaw ufo] + @medley_out + %i[stress mi_ha_ sonogono_watashi overheat_night get_smile seishun] + @encore + %i[alone] + [[:new_season, [], "'90", "'90"]] + @double_encore + %i[daite]

  concert.concert_halls_from_array [
    ['1990/1/12', :久喜総合文化会館, list],
    ['1990/1/18', :熊本市民会館, list],
    ['1990/1/19', :大分文化会館, list],
    ['1990/1/22', :福岡郵便貯金会館, list],
    ['1990/1/23', :広島郵便貯金会館, list],
    ['1990/1/24', :大阪厚生年金会館, list],
    ['1990/1/26', :和歌山県民文化会館, list],
    ['1990/1/31', :青森市文化会館, list],
    ['1990/2/1', :宮城県民会館, list],
    ['1990/2/6', :野洲文化ホール, list],
    ['1990/2/7', :静岡市民文化会館, list],
    ['1990/2/9', :浜松市民会館, list],
    ['1990/2/13', :京都会館, list, '第２ホール', 'Second hall'],
    ['1990/2/14', :名古屋市民会館, list],
    ['1990/2/17', :神奈川県民ホール, list],
    ['1990/2/18', :桐生市産業文化会館, list],
    ['1990/2/28', :札幌市教育文化会館, list],
    ['1990/3/3', :NHKホール, list],
    ['1990/3/5', :東京厚生年金会館, list],
    ['1990/3/6', :東京厚生年金会館, list],
    ['1990/3/7', :東京厚生年金会館, list],
  ]
end


