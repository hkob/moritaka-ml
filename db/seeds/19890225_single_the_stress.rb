key = :single_the_stress
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :the_stress_stress_chuukintou_version do
    {
      device_type: 'ep_single|single',
      event_date_id: '1989/2/25',
      sort_order: 198902250,
      number: '6th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4029.html'
    }
  end

  single.media_from_array [
    ["#{key}_ep", :ep, '07L7-4037', '1st', false, :warner_pioneer, '700'],
    ["#{key}_ct", :ct, '10L5-4037', '1st', false, :warner_pioneer, '1,000'],
    ["#{key}_cds1", :cds, '10L3-4037', '1st', false, :warner_pioneer, '937'],
    ["#{key}_cds2", :cds, '10L3-4037', '2nd', false, :warner_music_japan, '937'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/za-sutoresu-sutoresu-zhong/id528975527', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:the_stress, @vcah, '[ストレス中近東ヴァージョン]', '[Stress {CHUUKINTOU} version]'],
    [:yurusenai, @vcah]
  ]
end
