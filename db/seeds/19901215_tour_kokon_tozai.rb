key = :tour_kokon_tozai
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  band = band_factory(:janet_jacksons)
  band.band_members_from_array %i[VOCAL iChisatoMoritaka GUITAR iHiroyoshiMatsuo BASS iMasafumiYokoyama DRUMS iMakotoGeorgeYoshihara KEYBOARDS iShinKohno GUITAR iShinKohno KEYBOARDS iYasuakiMichaelMaejima]

  concert = concert_factory key do
    {
      j_subtitle: "森高千里コンサートツアー '90-'91",
      e_subtitle: "Chisato Moritaka concert tour '90-'91",
      concert_type: :tour,
      from_id: '1990/12/15',
      to_id: '1991/3/4',
      has_song_list: true,
      sort_order: 199012150,
      num_of_performances: 28,
      num_of_halls: 24,
      band_id: :janet_jacksons
    }
  end

  kisetsu = ListContent.title_only ' 季節の歌など', 'Songs in season'
  rockn = ListContent.medley_in 'ロックンロール・メドレー(?)', "Rock'n Roll medley(?)"
  single = ListContent.medley_in 'シングル・メドレー', 'Single Medley'
  list_a, list_b, list_c, list_d = %i[A B C D].map.with_index { |k, i| list_factory("#{key}_#{k}") { {concert_id: concert, keyword: k, sort_order: i+1} } }
  list_a.list_contents_from_array %i[onitaiji tsukiyono_kokai oye_como_va] + kisetsu + %i[kondo_watashi_dokoka aru_olno_seishun hare] + rockn + %i[kusaimononiwa_futaoshiro rockn_roll_widow] + @medley_out + %i[hong_kong] + single + %i[new_season overheat_night_2 alone michi the_stress get_smile] + @medley_out + %i[seishun konomachi seventeen sonogono_watashi funkey_monkey_baby yoruno_entotsu teriyaki_burger] + @encore + %i[busters_blues ame]
  list_b.list_contents_from_array %i[onitaiji oye_como_va] + kisetsu + %i[kondo_watashi_dokoka aru_olno_seishun hare] + rockn + %i[kusaimononiwa_futaoshiro rockn_roll_widow] + @medley_out + %i[hong_kong] + single + %i[new_season overheat_night_2 alone michi the_stress get_smile] + @medley_out + %i[seishun konomachi seventeen sonogono_watashi funkey_monkey_baby yoruno_entotsu teriyaki_burger] + @encore + %i[busters_blues ame]
  list_c.list_contents_from_array %i[onitaiji] + kisetsu + %i[kondo_watashi_dokoka aru_olno_seishun hare] + rockn + %i[kusaimononiwa_futaoshiro rockn_roll_widow] + @medley_out + %i[hong_kong] + single + %i[new_season overheat_night_2 alone michi the_stress get_smile] + @medley_out + %i[seishun konomachi seventeen sonogono_watashi funkey_monkey_baby yoruno_entotsu teriyaki_burger] + @encore + %i[busters_blues ame]
  list_d.list_contents_from_array %i[onitaiji] + kisetsu + %i[kondo_watashi_dokoka aru_olno_seishun hare] + rockn + %i[kusaimononiwa_futaoshiro rockn_roll_widow] + @medley_out + %i[hong_kong] + single + %i[new_season overheat_night_2 alone michi the_stress get_smile] + @medley_out + %i[benkyono_uta konomachi seventeen sonogono_watashi funkey_monkey_baby yoruno_entotsu teriyaki_burger] + @encore + %i[busters_blues mi_ha_ ame] + @double_encore + %i[mite]

  concert.concert_halls_from_array [
    ['1990/12/15', :福生市民会館, list_a],
    ['1990/12/16', :日野市民会館, list_b],
    ['1990/12/18', :鹿島勤労文化会館, list_c],
    ['1990/12/22', :名古屋市民会館, list_c],
    ['1990/12/23', :横浜文化体育館, list_c],
    ['1991/1/11', :柏崎市民会館, list_c],
    ['1991/1/12', :富山県民会館, list_c],
    ['1991/1/14', :阪南町文化センター, list_c],
    ['1991/1/16', :静岡市民文化会館, list_c],
    ['1991/1/17', :磐田市民文化会館, list_c],
    ['1991/1/19', :伊勢崎市文化会館, list_c],
    ['1991/1/24', :北海道厚生年金会館, list_c],
    ['1991/1/28', :高松市民会館, list_c],
    ['1991/1/29', :メルパルクホール広島, list_c],
    ['1991/1/31', :メルパルクホール福岡, list_c],
    ['1991/2/3', :鹿児島市文化センター, list_c],
    ['1991/2/6', :郡山市民文化センター, list_c],
    ['1991/2/13', :大阪厚生年金会館, list_c],
    ['1991/2/15', :米子市公会堂, list_c],
    ['1991/2/19', :栗原文化会館, list_c],
    ['1991/2/20', :仙台市民会館, list_c],
    ['1991/2/23', :茨城県立県民文化センター, list_c],
    ['1991/2/24', :新潟県民会館, list_c],
    ['1991/2/27', :中野サンプラザ, list_c],
    ['1991/2/28', :中野サンプラザ, list_c],
    ['1991/3/2', :中野サンプラザ, list_c],
    ['1991/3/3', :中野サンプラザ, list_d, nil, nil, '<ビデオ収録>', '<Video recording>'],
    ['1991/3/4', :中野サンプラザ, list_d],
]
end



