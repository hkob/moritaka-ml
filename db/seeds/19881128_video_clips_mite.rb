key = :video_clips_mite
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key, :mite do
    {
      device_type: :video_clips,
      event_date_id: '1988/11/28',
      minutes: 27,
      seconds: 15,
      sort_order: 198811280,
      number: '1st',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4315.html',
      j_comment: 'VHS発売: 1988年12月10日',
      e_comment: 'VHS release: Dec. 10, 1988'
    }
  end

  video.media_from_array [
    ["#{key}_vhs1", :vhs, '35L8-8020', :first, false, :warner_pioneer, '3,500'],
    ["#{key}_ld1", :ld, '25L6-8017', :first, false, :warner_pioneer, '2,431'],
    ["#{key}_vhs2", :vhs, '35L8-8020', :second, false, :warner_music_japan, '3,500'],
    ["#{key}_ld2", :ld, '25L6-8017', :second, false, :warner_music_japan, '2,431'],
  ]

  list_vhs = list_factory("#{key}_vhs") { {device_id: video, keyword: :vhs, sort_order: 1} }
  list_ld = list_factory("#{key}_ld") { {device_id: video, keyword: :ld, sort_order: 2} }
  list_vhs.list_contents_from_array(%i[new_season overheat_night_2 get_smile the_mi_ha_ alone].map { |key| [key, @vc] })
  list_ld.list_contents_from_array(%i[new_season the_mi_ha_ alone].map { |key| [key, @vc] })
end
