key = :single_suichuu_megane
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :suichuu_megane do
    {
      device_type: :part_single,
      event_date_id: '1999/7/7',
      sort_order: 199907070,
      number: '3rd',
      singer_id: :iChappie,
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'AIDT-5048,5049', '1st', false, :sony_music_entertainment, '1,529'],
  ]

  list_disk1 = list_factory("#{key}_disk1") { {device_id: single, keyword: :disk1, sort_order: 1} }
  list_disk2 = list_factory("#{key}_disk2") { {device_id: single, keyword: :disk2, sort_order: 2} }
  vc = %i[VOCAL iChappie]
  list_disk1.list_contents_from_array ListContent.title_with_performer(vc, '水中メガネ', '{SUICHUU MEGANE}')
  list_disk2.list_contents_from_array [
    [:tanabatano_yoru_kimini_aitai, %i[VOCAL iChappie TVOCAL iChisatoMoritaka]]
  ]
end
