key = :single_watarasebashi_writer_shibo
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :watarasebashi_writer_shibo do
    {
      device_type: :single,
      event_date_id: '1993/1/25',
      sort_order: 199301250,
      number: '17th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4208.html'
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'WPDL-4239', '1st', false, :warner_music_japan, '900'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/du-liang-lai-qiao-raita-zhi/id531620956', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:watarasebashi, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka PIANO iChisatoMoritaka RECORDER iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito TAMBOURINE iHideoSaito SYNTHESIZER iHideoSaito]],
    [:writer_shibo, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito]],
  ]
  device_activity_factory single, :thema_song_iitabi_yumekibun
  device_activity_factory single, :thema_song_quiz_junsui_danjo_kouyuu
end
