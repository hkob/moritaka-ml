key = :thema_song_koimo_nidomenara
obj = Activity.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_factory key do
    {
      activity_type: :thema_song,
      title_id: %q(ドラマ「恋も二度目なら」|TV drama "{KOI-MO NIDOME-NARA}"|こいもにどめなら),
      j_comment: '主題歌',
      e_comment: 'Thema song',
      company_id: :nihon_tv,
      from_id: '1995/1/11',
      to_id: '1995/3/15',
      song_id: :futariwa_koibito,
      sort_order: 199501110
    }
  end
end
