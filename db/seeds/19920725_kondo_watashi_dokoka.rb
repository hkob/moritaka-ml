key = :single_kondo_watashi_dokoka
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :kondo_watashi_dokoka do
    {
      device_type: 'cover_single|lyric_single',
      event_date_id: '1992/7/25',
      sort_order: 199207250,
      number: '1st',
      singer_id: :iNorikoKato,
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'WPDL-4307', '1st', false, :warner_music_japan, '1,000'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:kondo_watashi_dokoka, %i[VOCAL iNorikoKato ARRANGE iHideoSaito]],
    [:hikisakanaide_futario, %i[VOCAL iNorikoKato ARRANGE iHideoSaito]],
    [:kondo_watashi_dokoka, %i[VOCAL iKaraoke ARRANGE iHideoSaito], 'カラオケ', '[Karaoke]'],
  ]
end



