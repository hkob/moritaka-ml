key = :single_concert_no_yoru
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :concert_no_yoru do
    {
      device_type: :single,
      event_date_id: '1992/2/25',
      sort_order: 199202250,
      number: '15th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4204.html'
    }
  end

  single.media_from_array [
    ["#{key}_cds1", :cds, 'WPDL-4279', '1st', false, :warner_pioneer, '900'],
    ["#{key}_cds2", :cds, 'WPDL-4279', '2nd', false, :warner_music_japan, '900'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/konsatono-ye-single/id528970955', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:concert_no_yoru, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito CHORUS iHideoSaito CHORUS iSeijiMatsuura]],
    [:zoku_aru_olno_seishun, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito CHORUS iHideoSaito CHORUS iSeijiMatsuura]],
  ]
end

