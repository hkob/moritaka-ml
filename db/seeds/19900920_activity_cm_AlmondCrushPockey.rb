key = :cm_ezaki_guriko_pocky_konomachi
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_ezaki_guriko,
      j_title: '江崎グリコ 「アーモンド・クラッシュ・ポッキー」',
      e_title: 'Ezaki Guriko "Almond crush pockey"',
      from_id: '1990/9/20',
      to_id: '1991/1/31',
      song_id: :konomachi,
      sort_order: 199009200,
      j_comment: 'ポッキー版|階段版',
      e_comment: 'Pockey version|Stairs version'
    }
  end
end
