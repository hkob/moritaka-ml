key = :tour_mite
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  concert = concert_factory key do
    {
      j_subtitle: "森高千里全国コンサートツアー'89",
      e_subtitle: "Chisato Moritaka a cross country tour '89",
      concert_type: :tour,
      from_id: '1989/1/12',
      to_id: '1989/3/27',
      num_of_performances: 10,
      num_of_halls: 10,
      has_song_list: true,
      sort_order: 198901120
    }
  end

  list = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }
  list.list_contents_from_array(
    %i[omoshiroi wakareta_onna cant_say_good_bye kiss_the_night yokohama_one_night otisreddingni_kanpai get_smile] +
    ListContent.medley_in('ボサノバ・スペシャル (メドレー)', 'Bossa nova special (Medley)') +
    %i[romantic pi_a_no modorenai_natsu do_you_know_the_way_to_san_jose] +
    [[:anohino_photograph, [], '(ボサノバヴァージョン)', '(Bossa nova version)']] +
    %i[kaigan shizukana_natsu] + @medley_out +
    %i[the_stress detagari new_season weekend_blue forty_seven_hard_nights let_me_go overheat_night] +
    [[:the_mi_ha_, [], "<'89>", "<'89>"]] + @encore + %i[alone mite]
  )

  concert.concert_halls_from_array [
    ['1989/1/12', :渋谷公会堂, list],
    ['1989/3/1', :長野県民文化会館, list, '中ホール', 'Medium hall'],
    ['1989/3/3', :名古屋勤労会館, list],
    ['1989/3/4', :大阪厚生年金会館, list, '中ホール', 'Medium hall'],
    ['1989/3/10', :札幌道新ホール, list],
    ['1989/3/12', :仙台市民会館, list, '大ホール', 'Big hall'],
    ['1989/3/15', :新潟県民会館, list, '大ホール', 'Big hall'],
    ['1989/3/24', :熊本郵便貯金ホール, list],
    ['1989/3/25', :長崎平和会館, list],
    ['1989/3/27', :福岡都久志会館, list],
  ]
end
