key = :tv_hey_hey_hey
obj = Activity.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity = activity_factory key do
    {
      activity_type: :tv_program,
      title_id: 'HEY!HEY!HEY! MUSIC CHAMP|HEY!HEY!HEY! MUSIC CHAMP|へいへいへい　みゅーじっく　ちゃんぷ',
      company_id: :fuji_tv,
      sort_order: 199410170,
    }
  end
end

jt = 'HEY!HEY!HEY! MUSIC CHAMP SPECIAL'
et = 'HEY!HEY!HEY! MUSIC CHAMP SPECIAL'
activity_sub_factories_for_music_tv :tv_hey_hey_hey, {
  '19971201' => ['1997/12/1', %i[snow_again]],
  '19970616' => ['1997/6/16', %i[sweet_candy]],
  '19970310' => ['1997/3/10', %i[lets_go]],
  '19961118' => ['1996/11/18', %i[ginirono_yume]],
  '19960916' => ['1996/9/16', %i[la_la_sunshine], jt, et],
  '19960325' => ['1996/3/25', %i[so_blue], jt, et],
  '19960226' => ['1996/2/26', %i[so_blue]],
  '19941107' => ['1994/11/7', %i[sutekina_tanjoubi]],
}

