key = :thema_song_umimade_5fun
obj = Activity.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity = activity_factory key do
    {
      activity_type: :thema_song,
      title_id: %q(東芝日曜劇場「海まで 5 分」|TV drama {UMI-MADE GO-FUN}|とうしばにちようげきじょう　うみまでごふん),
      company_id: :tbs,
      j_comment: '主題歌',
      e_comment: 'Thema Song',
      from_id: '1998/6/28',
      to_id: '1998/9/20',
      song_id: :umimade_5fun,
      sort_order: 199806280
    }
  end
end

