key = :album_golden_q
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :golden_q do
    {
      device_type: :part_album,
      event_date_id: '1996/12/18',
      minutes: 66,
      seconds: 50,
      sort_order: 199612180,
      number: '7th',
      singer_id: :iSharamQ
    }
  end

  album.media_from_array [
    ["#{key}_cd", :cd, 'BVCR-779', :first, false, :bmg_victor, '3,000'],
  ]

  vt = %i[VOCAL iTsunku]
  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_with_performer(vt, 'Baby! I NEED YOU') +
    ListContent.title_with_performer(vt, '大阪エレジー', 'Osaka elegy') +
    ListContent.title_with_performer(vt, '泣かないで', '{NAKANAIDE}') +
    ListContent.title_with_performer(vt, '脱出', '{DASSHUTSU}') +
    ListContent.title_with_performer(vt, 'こんなにあなたを愛しているのに', '{KONNA-NI ANATA-O AISHITE IRUNONI}') +
    ListContent.title_with_performer(vt, 'ネオン', 'Neon') +
    ListContent.title_with_performer(vt, 'いいわけ', '{IIWAKE}') +
    ListContent.title_with_performer(vt, '純愛II', '{JUNAI} II') +
    ListContent.title_with_performer(vt, 'ジェラシー', 'Jealousy') +
    ListContent.title_with_performer(vt, 'COCORO') +
    ListContent.title_with_performer(vt, '雲', 'A cloud') +
    ListContent.title_with_performer(vt, '泣ける話', '{NAKERU HANASHI}') +
    ListContent.title_with_performer(vt, '涙の影', '{NAMIDA-NO KAGE}') +
    [[:white, %i[VOCAL iTsunku DRUMS iChisatoMoritaka]]]
  )
end

