key = :tv_pop_jam
obj = Activity.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity = activity_factory key do
    {
      activity_type: :tv_program,
      title_id: 'POP JAM|POP JAM|ぽっぷじゃむ',
      company_id: :nhk,
      sort_order: 199304040,
    }
  end
end

jt = 'POP JAM SPECIAL'
et = 'POP JAM SPECIAL'
activity_sub_factories_for_music_tv :tv_pop_jam, {
  '19971129' => ['1997/11/29', %i[snow_again]],
  '19970712' => ['1997/7/12', %i[sweet_candy]],
  '19970614' => ['1997/6/14', %i[sweet_candy]],
  '19970308' => ['1997/3/8', %i[lets_go]],
  '19970301' => ['1997/3/1', %i[lets_go]],
  '19961207' => ['1996/12/7', %i[ginirono_yume]],
  '19961116' => ['1996/11/16', %i[ginirono_yume la_la_sunshine]],
  '19960824' => ['1996/8/24', %i[la_la_sunshine], jt, et],
  '19960817' => ['1996/8/17', %i[la_la_sunshine watarasebashi]],
  '19960713' => ['1996/7/13', %i[la_la_sunshine]],
  '19960622' => ['1996/6/22', %i[la_la_sunshine]],
  '19960330' => ['1996/3/30', %i[so_blue]],
  '19960308' => ['1996/3/8', %i[so_blue]],
  '19950304' => ['1995/3/4', %i[futariwa_koibito]],
  '19941126' => ['1994/11/26', %i[sutekina_tanjoubi]],
}
