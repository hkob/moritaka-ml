key = :live_kanagawa_kenmin_hall
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  concert = concert_factory key, :kanagawa_kenmin_hall do
    {
      concert_type: :live,
      from_id: '1991/11/28',
      has_song_list: false,
      sort_order: 199111280,
      num_of_performances: 1,
      num_of_halls: 1,
    }
  end

  concert.concert_halls_from_array [
    ['1991/11/28', :神奈川県民ホール],
  ]
end
