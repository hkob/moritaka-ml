key = :album_the_singles
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :the_singles do
    {
      device_type: :album,
      event_date_id: '2012/8/8',
      minutes: 195,
      seconds: 0,
      sort_order: 201208080,
      number: '20th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4156.html',
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'WPCL-11128-30', :first, false, :warner_music_japan, '4,280'],
  ]

  vc = %i[VOCAL iChisatoMoritaka]
  list_disk1 = list_factory("#{key}_disk1") { {device_id: album, keyword: :disk1, sort_order: 1} }
  list_disk2 = list_factory("#{key}_disk2") { {device_id: album, keyword: :disk2, sort_order: 2} }
  list_disk3 = list_factory("#{key}_disk3") { {device_id: album, keyword: :disk3, sort_order: 3} }
  list_disk1.list_contents_from_array [
    [:new_season, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka KEYBOARD_SOLO iChisatoMoritaka]],
    [:overheat_night, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka]],
    [:get_smile, %i[ARRANGE iKenShima VOCAL iChisatoMoritaka]],
    [:the_mi_ha_, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka], '[スペシャル・ミーハー・ミックス]', '[Special {MI-HA-} mix]'],
    [:alone, %i[ARRANGE iShinjiYasuda VOCAL iChisatoMoritaka]],
    [:the_stress, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka], '[ストレス中近東ヴァージョン]', '[Stress {CHUUKINTOU} version]'],
    [:seventeen, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka]],
    [:daite, %i[ARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka], '[ラスベガス・ヴァージョン]', '[Las Vegas version]'],
    [:michi, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka KEYBOARD_SOLO iChisatoMoritaka]],
    [:seishun, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka]],
    [:kusaimononiwa_futaoshiro, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka]],
    [:ame, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka]],
    [:benkyono_uta, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka]],
    [:konomachi, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka]],
    [:hachigatsuno_koi, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka]],
  ]
  list_disk2.list_contents_from_array [
    [:fight, %i[ARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka]],
    [:concert_no_yoru, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka]],
    [:watashiga_obasanni_nattemo, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka], '(シングル・ヴァージョン)', '(Single version)'],
    [:watarasebashi, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka]],
    [:writer_shibo, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka]],
    [:watashino_natsu, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka]],
    [:haeotoko, %i[VOCAL iChisatoMoritaka], '(シングル・ヴァージョン)', '(Single version)'],
    [:memories, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka], '(シングル・ヴァージョン)', '(Single version)'],
    [:kazeni_fukarete, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka]],
    [:rockn_omelette, %i[VOCAL iChisatoMoritaka]],
    [:kibun_soukai, %i[ARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka]],
    [:natsuno_hi, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka]],
    [:sutekina_tanjoubi, %i[ARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka]],
    [:watashino_daijina_hito, %i[ARRANGE iYasuakiMaejima VOCAL iChisatoMoritaka], '(シングル・ヴァージョン)', '(Single version)'],
    [:futariwa_koibito, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka]],
  ]
  list_disk3.list_contents_from_array [
    [:yasumino_gogo, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka]],
    [:jin_jin_jinglebell, %i[ARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka]],
    [:so_blue, %i[ARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka]],
    [:la_la_sunshine, %i[STRINGSARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka]],
    [:ginirono_yume, %i[VOCAL iChisatoMoritaka]],
    [:lets_go, %i[VOCAL iChisatoMoritaka]],
    [:sweet_candy, %i[VOCAL iChisatoMoritaka]],
    [:miracle_light, %i[ARRANGE iHaruomiHosono VOCAL iChisatoMoritaka]],
    [:snow_again, %i[VOCAL iChisatoMoritaka]],
    [:telephone, %i[VOCAL iChisatoMoritaka]],
    [:umimade_5fun, %i[ARRANGE iYasuakiMaejima VOCAL iChisatoMoritaka]],
    [:tsumetai_tsuki, %i[ARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka]],
    [:watashinoyouni, %i[ARRANGE iShinKohno VOCAL iChisatoMoritaka]],
    [:mahiruno_hoshi, %i[ARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka]],
    [:ichido_asobini_kiteyo99, %i[ARRANGE iYasuakiMaejima VOCAL iChisatoMoritaka]]
  ]
end
