key = :album_do_the_best
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :do_the_best do
    {
      device_type: :album,
      event_date_id: '1995/3/25',
      minutes: 62,
      seconds: 04,
      sort_order: 199503250,
      number: '12th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4132.html',
      j_comment: '初回限定 36ページカラー写真集',
      e_comment: '36 page photo book(1st lot only)',
    }
  end

  album.media_from_array [
    ["#{key}_ct", :ct, 'EPTA-7003', :first, false, :one_up_music, '3,000'],
    ["#{key}_cd1", :cd, 'EPCA-7003', :first, false, :one_up_music, '3,000'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/do-the-best/id278097087', :first, true, :up_front_works, '2,000'],
  ]

  list_common = list_factory("#{key}_common") { {device_id: album, keyword: :common, sort_order: 1} }
  performer = %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito]
  list_common.list_contents_from_array [
    [:rockn_roll_kencho_shozaichi95, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka APF iChisatoMoritaka GUITARSOLO iChisatoMoritaka WWGUITAR iChisatoMoritaka CHORUS iChisatoMoritaka FRHODES iYasuakiMaejima SYNTHESIZER iYasuakiMaejima BASS iMasafumiYokoyama GUITAR iYuichiTakahashi SYNTHESIZER iYuichiTakahashi ENGINEER iSeijiIshikawa]],
    [:ame, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito CHORUS iSeijiMatsuura ENGINEER iHideoSaito ENGINEER iShinichiKawanaga]],
    [:watashiga_obasanni_nattemo, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito ENGINEER iHideoSaito ENGINEER iShinichiKawanaga], '(アルバム・ヴァージョン)', '(Album version)'],
    [:watarasebashi, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka PIANO iChisatoMoritaka RECORDER iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito TAMBOURINE iHideoSaito SYNTHESIZER iHideoSaito ENGINEER iHideoSaito ENGINEER iShinichiKawanaga]],
    [:watashino_natsu, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka PIANO iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito SYNTHESIZER iHideoSaito ENGINEER iHideoSaito ENGINEER iShinichiKawanaga]],
    [:haeotoko, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PIANORC iChisatoMoritaka GUITAR iYuichiTakahashi GUITAR iEijiOgata PIANOLC iShinHashimoto BASS iYukioSeto ENGINEER iShinichiKawanaga], '(シングル・ヴァージョン)', '(Single version)'],
    [:memories, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito GUITAR iHideoSaito SYNTHESIZER iHideoSaito ENGINEER iShinichiKawanaga ENGINEER iHideoSaito], '(シングル・ヴァージョン)', '(Single version)'],
    [:kazeni_fukarete, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka BASS iHideoSaito SYNTHESIZER iHideoSaito TAMBOURINE iHideoSaito AGUITAR iHideoSaito AGUITAR iHiroyoshiMatsuo AGUITAR iYuichiTakahashi AGUITAR iJunTakahashi ENGINEER iShinichiKawanaga]],
    [:rockn_omelette, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka GUITAR iYuichiTakahashi BASS iYuichiTakahashi PIANO iShinKohno CHORUS iYuichiTakahashi ENGINEER iShinichiKawanaga]],
    [:kibun_soukai, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka GUITAR iYuichiTakahashi CHORUS iYuichiTakahashi PIANO iYuichiTakahashi AGUITAR iHiroyoshiMatsuo BASS iMasafumiYokoyama ENGINEER iShinichiKawanaga]],
    [:natsuno_hi, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito KEYBOARDS iHideoSaito CHORUS iHideoSaito TAMBOURINE iHideoSaito MIXENGINEER iHideoSaito MIXENGINEER iNobuyukiAoyagi RECENGINEER iShinichiKawanaga]],
    [:sutekina_tanjoubi, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka GUITAR iYuichiTakahashi CHORUS iYuichiTakahashi SYNTHESIZER iYuichiTakahashi BASS iYukioSeto ENGINEER iSeijiIshikawa]],
    [:watashino_daijina_hito, %i[VOCAL iChisatoMoritaka ARRANGE iYasuakiMaejima DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka APF iYasuakiMaejima FRHODES iYasuakiMaejima SYNTHESIZER iYasuakiMaejima ENGINEER iSeijiIshikawa], '(シングル・ヴァージョン)', '(Single version)'],
    [:futariwa_koibito, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka COWBELL iChisatoMoritaka APIANO iYasuhikoFukuda GUITAR iHideoSaito SYNTHESIZER iHideoSaito TAMBOURINE iHideoSaito CHORUS iHideoSaito BASS iYukioSeto ENGINEER iSeijiIshikawa], '(Remix)', '(Remix)'],
    [:kyoukara, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka FRHODES iChisatoMoritaka APIANO iYasuakiMaejima ORGAN iTaisei SYNTHESIZER iYuichiTakahashi TAMBOURINE iYuichiTakahashi BASS iYukioSeto ENGINEER iSeijiIshikawa]],
  ]
end


