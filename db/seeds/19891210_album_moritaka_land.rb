key = :album_moritaka_land
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :moritaka_land do
    {
      device_type: :album,
      event_date_id: '1989/12/10',
      minutes: 54,
      seconds: 31,
      sort_order: 198912100,
      number: '5th',
      singer_id: :iChisatoMoritaka,
      j_comment: '1st ベスト・アルバム|初回限定 60ページカラー写真集',
      e_comment: '1st best album|60 page photo book(1st lot only)',
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4040.html'
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, '31LZ-117', :first, false, :warner_pioneer, '3,286'],
    ["#{key}_cd2", :cd, '29L2-117', :second, true, :warner_music_japan, '3,008'],
    ["#{key}_cd3", :cd, 'WPCL-688', :third, true, :warner_music_japan, '2,400'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/森高ランド/id528977791', :first, true, :warner_music_japan, '2,000'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  performer = %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito]
  performerWC = performer + %i[CHORUS iManahoSaito]
  list.list_contents_from_array [
    [:uwasa, performer],
    [:mi_ha_, performer],
    [:stress, performer],
    [:alone, performer],
    [:get_smile, performer],
    [:yumeno_owari, performer],
    [:overheat_night, performer],
    [:seventeen, performerWC],
    [:let_me_go, performer],
    [:good_bye_season, performer],
    [:michi, performer],
    [:new_season, performerWC],
  ]

  device_activity_factory album, :cm_ezaki_guriko
end

