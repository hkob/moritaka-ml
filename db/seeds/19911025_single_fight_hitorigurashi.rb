key = :single_fight_hitorigurashi
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :fight_hitorigurashi do
    {
      device_type: :single,
      event_date_id: '1991/10/25',
      sort_order: 199110250,
      number: '14th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4202.html'
    }
  end

  single.media_from_array [
    ["#{key}_cds1", :cds, 'WPDL-4267', '1st', false, :warner_pioneer, '900'],
    ["#{key}_cds2", :cds, 'WPDL-4267', '2nd', false, :warner_music_japan, '900'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/faito!!-single/id528972030', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:fight, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi CHORUS iChisatoMoritaka KEYBOARDS iYuichiTakahashi PROGRAMS iYuichiTakahashi AGUITAR iYuichiTakahashi CHORUS iYuichiTakahashi APIANO iYasuakiMaejima GUITAR iHiroyoshiMatsuo]],
    [:hitorigurashi, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito]],
  ]
  device_activity_factory single, :thema_song_volleyball_worldcup91
end
