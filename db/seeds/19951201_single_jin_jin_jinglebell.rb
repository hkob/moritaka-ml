key = :single_jin_jin_jinglebell
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :jin_jin_jinglebell do
    {
      device_type: :single,
      event_date_id: '1995/12/1',
      sort_order: 199512010,
      number: '27th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4276.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDA-21', '1st', false, :one_up_music, '1,000'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/jin-jin-jinguruberu-ep/id531613426', '1st', true, :warner_music_japan, '1,000']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:jin_jin_jinglebell, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka APIANO iYasuakiMaejima FRHODES iYasuakiMaejima BASS iMasafumiYokoyama GUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi PROGRAMS iYuichiTakahashi GUITAR iYukioSeto]],
    [:gin_gin_ginglebell, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka APIANO iYasuakiMaejima FRHODES iYasuakiMaejima BASS iMasafumiYokoyama GUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi PROGRAMS iYuichiTakahashi GUITAR iYukioSeto]],
    [:ichigatsu_ichijitsu, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka APIANO iYasuakiMaejima FRHODES iYasuakiMaejima BASS iMasafumiYokoyama GUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi PROGRAMS iYuichiTakahashi GUITAR iYukioSeto]],
    [:jin_jin_jinglebell, %i[VOCAL iKaraoke ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka APIANO iYasuakiMaejima FRHODES iYasuakiMaejima BASS iMasafumiYokoyama GUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi PROGRAMS iYuichiTakahashi GUITAR iYukioSeto], '[オリジナル・カラオケ]', '[Original karaoke]']
  ]
  device_activity_factory single, :cm_suntory
end
