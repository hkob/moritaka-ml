key = :single_sweet_candy
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :sweet_candy do
    {
      device_type: :single,
      event_date_id: '1997/6/11',
      sort_order: 199706110,
      number: '32nd',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4286.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDA-44', '1st', false, :one_up_music, '1,020'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/sweet-candy-single/id531614904', '1st', true, :warner_music_japan, '750']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:sweet_candy, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka KEYBOARDS iYuichiTakahashi EPIANO iShinHashimoto EGUITAR iYukioSeto BASS iYukioSeto]],
    [:mirai, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka EGUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi APIANO iShinHashimoto EPIANO iShinHashimoto EGUITAR iYukioSeto TRUMPET iFutoshiKobayashi TRUMPET iShiroSasaki TROMBONE iWakabaKawai TENORBARITONSAX iNobuyukiMori HORNARRANGEMENT iNobuyukiMori]],
    [:sweet_candy, %i[VOCAL iKaraoke DRUMS iChisatoMoritaka KEYBOARDS iYuichiTakahashi EPIANO iShinHashimoto EGUITAR iYukioSeto BASS iYukioSeto], '(オリジナル・カラオケ)', '(Original karaoke)']
  ]
  device_activity_factory single, :cm_lawson
  device_activity_factory single, :thema_song_kumamoto_mirai_kokutai
end




