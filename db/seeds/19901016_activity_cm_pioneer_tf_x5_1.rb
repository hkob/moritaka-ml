key = :cm_pioneer_tf_x5_1
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_pioneer,
      j_title: 'パイオニア 「留守番電話 TF-X5」',
      e_title: 'Pioneer "telephone system with answering service TF-X5"',
      from_id: '1990/10/25',
      to_id: '1990/11/25',
      song_id: :busters_blues,
      sort_order: 199010250
    }
  end
end
