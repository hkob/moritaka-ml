key = :single_hachigatsuno_koi
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :hachigatsuno_koi do
    {
      device_type: :single,
      event_date_id: '1991/6/25',
      sort_order: 199106250,
      number: '13th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4200.html'
    }
  end

  single.media_from_array [
    ["#{key}_cds1", :cds, 'WPDL-4242', '1st', false, :warner_pioneer, '900'],
    ["#{key}_cds2", :cds, 'WPDL-4242', '2nd', false, :warner_music_japan, '900'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/id528966922', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:hachigatsuno_koi, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito]],
    [:itsumademo, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito]],

  ]
  device_activity_factory single, :thema_song_video_anataga_shuyaku
  device_activity_factory single, :thema_song_mischievous_twins
end

