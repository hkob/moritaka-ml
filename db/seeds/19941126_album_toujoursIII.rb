key = :album_toujoursIII
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :toujoursIII do
    {
      device_type: 'compilation_album',
      event_date_id: '1994/11/26',
      minutes: 71,
      seconds: 8,
      singer_id: :iVariousArtists,
      sort_order: 199411260,
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'TKCR-1300', :first, false, :kitty_records, '2,800'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_only('あの頃に待ち合わせよう / 高橋洋子', '{ANO KORO-NI MACHIAWASEYO} / Yoko Takahashi') +
    ListContent.title_only('CRESCENT MOONSHINE / 平松愛理', 'CRESCENT MOONSHINE / Eri Hiramatsu') +
    ListContent.title_only('MAYBE / Sing Like Talking') +
    ListContent.title_only('20TH CENTURY FLIGHT 光の彼方に / Spiral Life', '20TH CENTURY FLIGHT {HIKARI-NO KANATA-NI} / Spiral Life') +
    ListContent.title_only('青い車 / スピッツ', 'Blue Car, {AOI KURUMA} / Spitz') + 
    [[:tsukiyono_kokai, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi]]] +
    ListContent.title_only('日曜日の後の月曜日 / 柿原朱美', '{NICHIYOBI-NO ATONO GETSUYOBI} / Akemi Kakihara') +
    ListContent.title_only('心のベクトル / 杏子', '{KOKORO-NO Vector} / Kyoko') +
    ListContent.title_only('あなたに騙されたい / 藤谷美和子', '{ANATA-NI DAMASARETAI} / Miwako Takahashi') +
    ListContent.title_only('UNFINISH 〜あの日の君がいる〜 / 中西保志', 'UNFINISH ~{ANO HI-NO KIMI-GA IRU}~ / Yasushi Nakanishi') +
    ListContent.title_only('WILL BE MINE / Date of Birth') +
    ListContent.title_only('恋愛が愛情に変っても / 楠瀬誠志郎', '{RENAI-GA AIJO-NI KAWATTEMO} / Seishiro Kusunose') +
    ListContent.title_only('優しい雨 / 鈴木祥子', '{YASASHII AME} / Shoko Suzuki') +
    ListContent.title_only('君は僕の宝物 / 槇原敬之', '{KIMI-HA BOKU-NO TAKARAMONO} / Noriyuki Makihara')
  )
end

