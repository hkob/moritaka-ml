key = :single_kibun_soukai
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :kibun_soukai do
    {
      device_type: :single,
      event_date_id: '1994/1/31',
      sort_order: 199401310,
      number: '22nd',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4240.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDA-1', '1st', false, :one_up_music, '800'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/qi-fen-shuang-kuai-single/id531611078', '1st', true, :warner_music_japan, '750']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:kibun_soukai, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka GUITAR iYuichiTakahashi CHORUS iYuichiTakahashi PIANO iYuichiTakahashi AGUITAR iHiroyoshiMatsuo BASS iMasafumiYokoyama]],
    [:teo_tatako2, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka GUITAR iYuichiTakahashi PIANO iYuichiTakahashi GUITARSOLO iHiroyoshiMatsuo APIANO iShinHashimoto BASS iYukioSeto]],
    [:kibun_soukai, %i[VOCAL iKaraoke ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka GUITAR iYuichiTakahashi CHORUS iYuichiTakahashi PIANO iYuichiTakahashi AGUITAR iHiroyoshiMatsuo BASS iMasafumiYokoyama], '(オリジナル・カラオケ)', '(Original karaoke)'],
  ]
  device_activity_factory single, :cm_asahi_beer
end


