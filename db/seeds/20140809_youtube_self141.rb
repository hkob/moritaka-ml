yt = :sc141

yt_hash = {
  '2014/8/9' => {
    link: '6pIvx11jPr8',
    number: '141',
    song_id: :cup_myudol,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」141曲目は、\n1990年発表のシングル「雨」のカップリング曲「カップ・ミュードル」！\nドラムは森高千里による新録音です！\n\n衣装協力：EBELE MOTION／GALSTAR"
  },
  '2014/8/16' => {
    link: 'jcEuXjilBWo',
    number: '142',
    song_id: :gokigenna_asa,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」142曲目は、\n1992年発表のアルバム「ペパーランド」から「ごきげんな朝」！\nドラムは森高千里による新録音です！\n\n衣装協力：Demi-Luxe BEAMS／EFFE BEAMS"
  },
  '2014/8/23' => {
    link: 'cSZ-7GGlWTQ',
    number: '143',
    song_id: :wasuremono,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」143曲目は、\n1995年発表のシングル「休みの午後」のカップリング曲「忘れ物」！\nドラムは森高千里による新録音です！\n\n衣装協力：Embellish／Millon Carats"
  },
  '2014/8/29' => {
    link: 'YfjjMTqX99k',
    number: '144',
    song_id: :ameno_asa,
    comment: "作詞：森高千里　作曲：前嶋康明\n\n公式チャンネル独占企画「200曲セルフカヴァー」144曲目は、\n1992年発表のアルバム「ペパーランド」から「雨の朝」！\nドラムは森高千里による新録音です！\n\n衣装協力：原宿シカゴ表参道店"
  },
  '2014/9/7' => {
    link: 'sI6x7WO0MIY',
    number: '145',
    song_id: :yoruno_entotsu,
    j_title: "森高千里 with カーネーション 『夜の煙突』 （森高千里ドラムヴァージョン） 【セルフカヴァー】",
    e_title: "Chisato Moritaka with Carnation 『{YORU-NO ENTOTSU}: [Chimney at night]』 (Chisato Moritaka Drum version) 【Self cover】",
    comment: "作詞・作曲：直枝政太郎\n\n公式チャンネル独占企画「200曲セルフカヴァー」145曲目は、\n1989年発表のアルバム「非実力派宣言」から「夜の煙突」！\nカーネーショント・トリビュート・アルバム\n「なんできみはぼくよりぼくのことくわしいの？」\nに収録されている「夜の煙突」にYouTubeのセルフカバー用に新たに森高千里がドラムをプレイしました。\n\n衣装協力：Demi-Luxe BEAMS／BONICA|DOT／スリーフォータイム"
  },
  '2014/9/13' => {
    link: 'nf1byp7vgG8',
    number: '146',
    song_id: :kikenna_hodou,
    comment: "作詞・作曲：森高千里　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」146曲目は、\n1998年発表のアルバム「Sava Sava」から「危険な舗道」！\nドラムは森高千里による新録音です！\n\n衣装協力：Million Carats／BLONDY ReLISH"
  },
  '2014/9/19' => {
    link: 'R81CV1wpq_M',
    number: '147',
    song_id: :wakasano_hiketsu,
    comment: "作詞・作曲：森高千里\n\n公式チャンネル独占企画「200曲セルフカヴァー」147曲目は、\n1994年発表のアルバム「STEP BY STEP」から「若さの秘訣」！後にシングル「二人は恋人」のカップリング曲にもなりました。\nドラムは森高千里による新録音です！\n\n衣装協力：31 Sons de mode／THE FIRST women／スリーフォータイム」"
  },
  '2014/10/3' => {
    link: 'P-444VSPDn0',
    number: '148',
    song_id: :teriyaki_burger,
    j_title: "森高千里 『テリヤキ・バーガー (BURGER QUEEN Ver.)』 【セルフカヴァー】",
    e_title: "Chisato Moritaka 『TERIYAKI burger (BURGER QUEEN Ver.)』 【Self cover】",
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」148曲目は、\n1990年発表のアルバム「古今東西」から「テリヤキ・バーガー」！\nセルフカヴァーでは二回目の登場ですが、背景を変え、前回よりサウンドもよりブラッシュアップしました！\n\n衣装協力：madarie／GROWZE"
  },
  '2014/10/22' => {
    link: 'ChTRVS5c9LU',
    number: '149',
    song_id: :hoshini_negaio,
    comment: "作詞：森高千里　作曲：伊秩弘将　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」149曲目は、\n1997年発表のアルバム「PEACHBERRY」から「星に願いを」！\n今回の曲は1080pでアップしています。満点の星空をお楽しみ下さい！\nドラムは森高千里による新録音です！\n\n衣装協力：Millon Carats／Lilidia"
  },
  '2014/10/28' => {
    link: 'bJ705NhjnQI',
    number: '150',
    song_id: :get_smile,
    j_title: "森高千里 『GET SMILE (2014 Ver.)』 【セルフカヴァー】",
    e_title: "Chisato Moritaka 『GET SMILE (2014 Ver.)』 【Self cover】",
    comment: "作詞・伊秩弘将　作曲：島健　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」150曲目は、1988年発表の3rdシングル「GET SMILE」です！\nセルフカヴァーでは二回目の登場ですが、背景を変え、サウンドもリアレンジしました！"
  },
  '2014/11/8' => {
    link: 'zlqddfICRmE',
    number: '151',
    song_id: :let_me_go,
    comment: "作詞・伊秩弘将・森高千里　作曲：安田信二　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」151曲目は、1988年発表のアルバム「見て」から「LET ME GO」です！\nドラムは森高千里による新録音です！\n\n衣装協力：BLACK BY MOUSSY／RODEO CROWNS"
  },
  '2014/11/22' => {
    link: 'rbxGzThVT9s',
    number: '152',
    song_id: :teo_tatako,
    comment: "作詞・作曲：森高千里\n\n公式チャンネル独占企画「200曲セルフカヴァー」152曲目は、1993年発表のアルバム「LUCKY 7」から「手をたたこう」です！\nドラムは森高千里による新録音です！\n\n衣装協力：Nina mew"
  },
  '2014/11/28' => {
    link: '9z0JVcR8foA',
    number: '153',
    song_id: :yurusenai,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」153曲目は、1989年発表のシングル「ザ・ストレス」のカップリング曲「ユルセナイ」です！\nドラムは森高千里による新録音！\n\n衣装協力：Nina mew"
  },
  '2014/12/5' => {
    link: '1cHcy3eprNo',
    number: '154',
    song_id: :jin_jin_jinglebell,
    j_title: "森高千里 『ジン ジン ジングルベル 2014』 【セルフカヴァー】",
    e_title: "Chisato Moritaka 『JIN JIN JINGLEBELL 2014』 【Self cover】",
    comment: "作詞・作曲：森高千里　編曲：S&T\n\n公式チャンネル独占企画「200曲セルフカヴァー」154曲目は、\n1995年発表のシングル「ジン ジン ジングルベル」！\n2014年バージョンをお楽しみ下さい！\nドラムは森高千里によるループドラム！"
  },
  '2014/12/12' => {
    link: 'P25tPtYKGWk',
    number: '155',
    song_id: :snow_again,
    j_title: "森高千里 『SNOW AGAIN 2014』 【セルフカヴァー】",
    e_title: "Chisato Moritaka 『SNOW AGAIN 2014』 【Self cover】",
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」155曲目は、\n1997年発表のシングル「SNOW AGAIN」！\n二度目の登場です！\nドラムは森高千里によるループドラム！"
  },
  '2014/12/19' => {
    link: 'Qzk28y6tT9Y',
    number: '156',
    song_id: :alone,
    comment: "作詞：森高千里　作曲：安田信二　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」156曲目は、\n1988年発表のシングル「ALONE」！\nドラムは森高千里による新録音です！\n\n衣装協力：Milion Carats"
  },
  '2014/12/31' => {
    link: 'M0ru1HVUyM8',
    number: '157',
    song_id: :ichigatsu_ichijitsu,
    j_title: '森高千里 『一月一日 2015』【セルフカヴァー】',
    e_title: 'Chisato Moritaka 『{ICHIGATSU ICHIJITSU} 2015』【Self cover】',
    comment: "作詞：千家尊福　作曲：上真行　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」157曲目は、\n1995年発表のシングル「ジンジン ジングルベル」のC/W「一月一日」です。2015年バージョンをお楽しみ下さい。\nドラムは森高千里によるループドラムです！\n\n衣装協力：GOUT COMMUN"
  },
  '2015/1/7' => {
    link: 'KH_wgVXsGZc',
    number: '158',
    song_id: :dekirudesho,
    comment: "作詞：森高千里　作曲：伊秩弘将\n\n公式チャンネル独占企画「200曲セルフカヴァー」158曲目は、\n1996年発表のアルバム「TAIYO」から「出来るでしょ！！」です。\nドラムは森高千里による新録音です！\n\n衣装協力：LADY MADE／BONICA DOT"
  },
  '2015/2/5' => {
    link: '8qRk5svVrWA',
    number: '159',
    song_id: :tanpopono_tane,
    comment: "作詞：森高千里　作曲：スガシカオ　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」159曲目は、\n1998年発表のアルバム「Sava Sava」から「たんぽぽの種」です。\nドラムは森高千里によるループドラム！\n\n衣装協力：Million Carats／Nina mew"
  },
  '2015/2/13' => {
    link: '19fWxwxNO8w',
    number: '160',
    song_id: :tomodachi,
    comment: "作詞：森高千里　作曲：斉藤英夫・安田信二\n\n公式チャンネル独占企画「200曲セルフカヴァー」160曲目は、\n1990年発表のアルバム「古今東西」から「友達」です。\nドラムは森高千里による新録音！\n\n衣装協力：TITE IN THE STORE"
  },
}
create_youtube_factory_from_hash(yt, yt_hash)
