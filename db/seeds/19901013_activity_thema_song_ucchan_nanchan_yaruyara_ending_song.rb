pkey = :thema_song_ucchan_nanchan_yaruyara
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_factory pkey do
    {
      activity_type: :thema_song,
      title_id: %q(「ウッチャンナンチャンのやるならやらねば!」|TV program "{UCCHAN NANCHAN-NO YARUNARA YARANEBA!}" (FUJI-TV)|うっちゃんなんちゃんのやるならやらねば),
      company_id: :fuji_tv,
      sort_order: 199010130
    }
  end
end

key = :thema_song_ucchan_nanchan_yaruyara_ending_song
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :thema_song_ucchan_nanchan_yaruyara,
      j_title: '「ウッチャンナンチャンのやるならやらねば!」エンディングソング',
      e_title: 'Ending song for "{HAEOTOKO}" corner in a TV program "{UCCHAN NANCHAN-NO YARUNARA YARANEBA!}"',
      from_id: '1990/10/13',
      to_id: '1993/6/26',
      song_id: :memories,
      sort_order: 199010130
    }
  end
end
