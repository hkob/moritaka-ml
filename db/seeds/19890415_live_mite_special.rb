key = :live_mite_special
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  band = band_factory(:animals_1)
  band.band_members_from_array %i[VOCAL iChisatoMoritaka SAXOPHONE iGinjiOgawa CHORUS iGinjiOgawa GUITAR iHiroyukiKato DRUMS iNaokiKimura CHORUS iNaokiKimura KEYBOARDS iKenichiMitsuda PERCUSSION iYouichiOkabe]

  concert = concert_factory key, :mite_special_live do
    {
      concert_type: :live,
      from_id: '1989/4/15',
      has_song_list: true,
      sort_order: 198904150,
      num_of_performances: 1,
      num_of_halls: 1,
      band_id: :animals_1
    }
  end

  list = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }
  list.list_contents_from_array %i[the_stress watashiga_hen alone yurusenai cant_say_good_bye kiss_the_night ringoshuno_rule seventeen the_loco_motion overheat_night let_me_go detagari weekend_blue wakareta_onna get_smile new_season] + @encore + %i[mi_ha_ mite alone]

  concert.concert_halls_from_array [
    ['1989/4/15', :汐留_PIT_II, list, nil, nil, 'ビデオ収録|初のチケット即日ソールドアウト', 'Video Recording|It was the first time that the concert ticket was sold out in a day.']
  ]
end
