yt = :sc121

yt_hash = {
  '2014/3/8' => {
    link: 'tdcXkp4cUQQ',
    number: '121',
    song_id: :uchini_kagitte,
    comment: "作詞：森高千里　作曲：直枝政太郎\n\n公式チャンネル独占企画「200曲セルフカヴァー」121曲目は、\n1990年発表のアルバム「古今東西」から『うちにかぎってそんなことはないはず』！\nドラムは森高千里による新録音です！\n\n衣装協力：Fabulous Angela／Million Carats／原宿シカゴ表参道店"
  },
  '2014/3/14' => {
    link: 'cPXN47Xxziw',
    number: '122',
    song_id: :torikago,
    comment: "作詞：森高千里　作曲：前嶋康明　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」122曲目は、\n1994年発表のアルバム「STEP BY STEP」から『鳥かご』！\nドラムは森高千里による新録音です！\n\n衣装協力：Language"
  },
  '2014/3/21' => {
    link: '8N6urm-rnok',
    number: '123',
    song_id: :hadakaniwa_naranai,
    comment: "作詞：森高千里　作曲：直枝政太郎\n\n公式チャンネル独占企画「200曲セルフカヴァー」123曲目は、\n1989年発表のアルバム「非実力派宣言」から『はだかにはならない』！\nドラムは森高千里による新録音です！\n衣装協力：原宿シカゴ表参道店"
  },
  '2014/3/29' => {
    link: '4Eyn_AX5Yys',
    number: '124',
    song_id: :kowai_yume,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」124曲目は、\n1994年発表のシングル「夏の日」のカップリング曲『こわい夢』！\nドラムは森高千里による新録音です！\n\n衣装協力：スリーフォータイム／EBELE MOTION／ノーリーズ／Language"
  },
  '2014/4/3' => {
    link: 'LummlTpk1SY',
    number: '125',
    song_id: :chase,
    comment: "作詞：久和カノン　作曲：山本拓巳　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」125曲目は、\n1988年発表のシングル「ALONE」のカップリング曲『CHASE』！\nドラムは森高千里による新録音です！\n\n衣装協力：Nine mew／ノーリーズ／DABA GIRL"
  },
  '2014/4/11' => {
    link: 'zl73nUIDz8Y',
    number: '126',
    song_id: :kusaimononiwa_futaoshiro,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」126曲目は、\n1990年発表のシングル「臭いものにはフタをしろ!!」！\nドラムは森高千里による新録音です！\n\n衣装協力：Million Carats／原宿シカゴ表参道店"
  },
  '2014/4/25' => {
    link: 'Hx_aeARpecc',
    number: '127',
    song_id: :stress,
    j_title: "森高千里 『ストレス（25周年記念Ver.）』 【セルフカヴァー】",
    e_title: "Chisato Moritaka 『Stress (25 years meomrial Ver.)』 【Self cover】",
    comment: "作詞：森高千里　作曲：斉藤英夫\nremix：dessert,desert\n\n公式チャンネル独占企画「200曲セルフカヴァー」127曲目は、\n1988年発表のアルバム「見て」に初収録され、その後シングルカットされた「ストレス」！\nドラムは森高千里による新録音です！\n\n衣装協力：BEAMS"
  },
  '2014/5/4' => {
    link: '8CxVNBAlY3o',
    number: '128',
    song_id: :wakareta_onna,
    comment: "作詞：森高千里　作曲：斉藤英夫\nremix：dessert,desert\n\n公式チャンネル独占企画「200曲セルフカヴァー」128曲目は、\n1988年発表のアルバム「見て」に収録されている「別れた女」！\nドラムは森高千里による新録音です！\n\n衣装協力：原宿シカゴ表参道"
  },
  '2014/5/10' => {
    link: 'eZpMo2Q2a18',
    number: '129',
    song_id: :docchimo_docchi,
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」129曲目は、\n1992年発表のアルバム「ペパーランド」に収録されている「どっちもどっち」！\nドラムは森高千里による新録音です！\n\n衣装協力：原宿シカゴ表参道"
  },
  '2014/5/17' => {
    link: 'Usj8jJOUp60',
    number: '130',
    song_id: :kyushu_sodachi,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」130曲目は、\n1993年発表のシングル「風に吹かれて」のカップリング曲「九州育ち」！\nドラムは森高千里による新録音です！\n\n衣装協力：Demi-Luxe BEAMS"
  },
  '2014/5/25' => {
    link: 'GM3-hEJhby4',
    number: '131',
    song_id: :jitensha_tsuugaku,
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」131曲目は、\n1992年発表のシングル「私がオバさんになっても」のカップリング曲「自転車通学」！\nドラムは森高千里による新録音です！\n\n衣装協力：NOLLEY'S"
  },
  '2014/5/30' => {
    link: 'IYXLY-iaN8g',
    number: '132',
    song_id: :aoi_umi,
    comment: "作詞：森高千里　作曲：松尾弘良\n\n公式チャンネル独占企画「200曲セルフカヴァー」132曲目は、\n1992年発表のアルバム「ペパーランド」に収録されている「青い海」！\nドラムは森高千里による新録音です！\n\n衣装協力：デイジークレア"
  },
  '2014/6/13' => {
    link: 'uiuuzcRSH2M',
    number: '133',
    song_id: :atamaga_itai,
    comment: "作詞：森高千里　作曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」133曲目は、\n1992年発表のアルバム「ペパーランド」に収録されている「頭が痛い」！\nドラムは森高千里による新録音です！\n\n衣装協力：Demi-Luxe BEAMS／デイジークレア／原宿シカゴ表参道店"
  },
  '2014/6/20' => {
    link: 'cfFslImWGYo',
    number: '134',
    song_id: :jimina_onna,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」134曲目は、\n1993年発表のアルバム「LUCKY 7」に収録されている「地味な女」！\nドラムは森高千里による新録音です！\n\n衣装協力：Demi-Luxe BEAMS"
  },
  '2014/6/28' => {
    link: 'CQKJVwaHacE',
    number: '135',
    song_id: :tomodachino_kare,
    comment: "作詞：森高千里　作曲：伊秩弘将　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」135曲目は、\n1993年発表のアルバム「LUCKY 7」に収録されている「友達の彼」！\nドラムは森高千里による新録音です！\n\n衣装協力：fifth"
  },
  '2014/7/5' => {
    link: 'YmeOba6p2_s',
    number: '136',
    song_id: :watashino_natsu,
    j_title: "森高千里 『私の夏(2014)』 【セルフカヴァー】",
    e_title: "Chisato Moritaka 『{WATASHI-NO NATSU (2014)}: [My Summer (2014)]』 【Self cover】",
    comment: "作詞：森高千里　作曲：斉藤英夫　オリジナルアレンジ:斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」136曲目は1993年発表の18thシングル「私の夏」！\nドラムは森高千里による新録音です！\n\n衣装協力：fifth"
  },
  '2014/7/12' => {
    link: 'bBt0hb0-10I',
    number: '137',
    song_id: :watashiga_hen,
    comment: "作詞：鈴木エキス　作曲：山本拓巳\n\n公式チャンネル独占企画「200曲セルフカヴァー」137曲目は1988年発表の3rdアルバム「見て」から「私が変？」！\nドラムは森高千里による新録音です！\n\n衣装協力：SLY／GROWZE"
  },
  '2014/7/19' => {
    link: 'JA5jcL2rzCo',
    number: '138',
    song_id: :overheat_night,
    comment: "作詞：伊秩弘将　作曲：斉藤英夫　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」138曲目は­1987年発表の2ndシングル「オーバーヒート・ナイト」！\nドラムは森高千里による新録音です！\n\n衣装協力：デイジークレア"
  },
  '2014/7/26' => {
    link: 'tOFVHFO1wto',
    number: '139',
    song_id: :toi_mukashi,
    comment: "作詞：森高千里　作曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」139曲目は、\n1993年発表のアルバム「LUCKY 7」に収録されている「遠い昔」！\nドラムは森高千里による新録音です！\n\n衣装協力：GALSTAR／BONICA DOT"
  },
  '2014/8/2' => {
    link: '850pU_nnyUI',
    number: '140',
    song_id: :hitorigurashi,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」140曲目は、\n1991年発表のシングル「ファイト！」のカップリング曲「ひとり暮らし」！\nドラムは森高千里によるループドラムです！\n\n衣装協力：原宿シカゴ／Million Carats"
  },
}
create_youtube_factory_from_hash(yt, yt_hash)
