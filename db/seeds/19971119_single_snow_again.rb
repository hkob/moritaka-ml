key = :single_snow_again
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :snow_again do
    {
      device_type: :single,
      event_date_id: '1997/11/19',
      sort_order: 199711190,
      number: '34th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4290.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDA-53', '1st', false, :one_up_music, '1,020'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/snow-again-single/id531615565', '1st', false, :up_front_works, '750'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:snow_again, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka AGUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi APIANO iShinHashimoto KEYBOARDS iShinHashimoto BASS iYukioSeto EGUITAR iYukioSeto]],
    [:pa_pa_paya, %i[DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka PIANICA iChisatoMoritaka AGUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi FRHODES iShinHashimoto CLAVINET iShinHashimoto SYNTHESIZER iShinHashimoto EGUITAR iYukioSeto]],
    [:snow_again, %i[VOCAL iKaraoke DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka AGUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi APIANO iShinHashimoto KEYBOARDS iShinHashimoto BASS iYukioSeto EGUITAR iYukioSeto], '(オリジナル・カラオケ)', '(Original karaoke)']
]
  device_activity_factory single, :cm_lawson
end

