key = :concert_event90
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  concert = concert_factory key do
    {
      concert_type: :live,
      from_id: '1990/4/8',
      to_id: '1990/9/15',
      has_song_list: true,
      sort_order: 199004080,
      num_of_performances: 28,
      num_of_halls: 28
    }
  end

  list_a, list_b, list_c, list_d, list_e, list_f = %i[A B C D E F].map.with_index { |k, i| list_factory("#{key}_#{k}") { {concert_id: concert, keyword: k, sort_order: i+1} } }

  list_a.list_contents_from_array(%i[uwasa good_bye_season overheat_night michi stress akunno_higeki seventeen mi_ha_ sonogono_watashi yoruno_entotsu new_season] + @encore + %i[daite])
  list_b.list_contents_from_array(%i[uwasa good_bye_season stress yoruno_entotsu seventeen sonogono_watashi get_smile mite] + @encore + %i[new_season])
  list_c.list_contents_from_array(%i[uwasa hadakaniwa_naranai let_me_go good_bye_season overheat_night kusaimononiwa_futaoshiro seishun michi stress akunno_higeki seventeen mi_ha_ sonogono_watashi yoruno_entotsu mite] + @encore + %i[alone new_season])
  list_d.list_contents_from_array(%i[kusaimononiwa_futaoshiro hadakaniwa_naranai overheat_night michi stress akunno_higeki seventeen sonogono_watashi yoruno_entotsu get_smile new_season])
  list_e.list_contents_from_array(%i[kusaimononiwa_futaoshiro hadakaniwa_naranai overheat_night michi stress seventeen sonogono_watashi yoruno_entotsu get_smile new_season])
  list_f.list_contents_from_array(%i[kusaimononiwa_futaoshiro hadakaniwa_naranai let_me_go overheat_night michi seishun stress seventeen sonogono_watashi yoruno_entotsu get_smile mite] + @encore + %i[new_season])

  concert.concert_halls_from_array [
    ['1990/4/8', :郡山市民文化センター, nil, '中ホール', 'Medium hall'],
    ['1990/4/9', :仙台市青年文化センター],
    ['1990/4/14', :水戸市民会館],
    ['1990/4/28', :京都産業大学],
    ['1990/5/2', :新潟県民会館],
    ['1990/5/12', :五反田ゆうぽうと, list_a, 'クボタ', 'KUBOTA'],
    ['1990/5/19', :中央大学, nil, '学園祭', 'School festival'],
    ['1990/5/20', :船橋札幌ビール工場, list_b],
    ['1990/5/21', :大阪厚生年金会館, nil, 'クボタ？', 'KUBOTA?'],
    ['1990/5/25', :渋谷公会堂, nil, '専修大学 学園祭', 'Sensyu university festival'],
    ['1990/6/1', :長崎大学, nil, '学園祭', 'School festival'],
    ['1990/6/10', :群馬音楽センター, list_c],
    ['1990/6/16', :川崎教育文化会館, list_c, '新入生歓迎会', 'Welcome meeting for newcomers'],
    ['1990/7/7', :日本武道館, list_d, '花王ヘアケア', 'Kao Hair-Care'],
    ['1990/7/11', :江ノ島サーフ, nil, "'90", "'90"],
    ['1990/7/21', :久美浜町公園, nil, '[未確認]', '[unconfirmed]'],
    ['1990/7/26', :大宮ソニックシティ],
    ['1990/7/27', :大阪あじかわ, list_e],
    ['1990/7/29', :嬉野町, list_e],
    ['1990/8/3', :白馬],
    ['1990/8/5', :阿武町],
    ['1990/8/10', :大磯, nil, '(台風のため中止)', 'The event was called off by typoon.'],
    ['1990/8/20', :つま恋, list_f],
    ['1990/8/23', :六日町文化会館],
    ['1990/8/25', :北アルプス文化センター],
    ['1990/8/28', :浜松市民会館, list_c],
    ['1990/9/9', :名古屋市総合体育館, nil, '(ローソン)', '(Lawson)'],
    ['1990/9/15', :平和台球場, nil, '(ローソン)', '(Lawson)'],
  ]
end

