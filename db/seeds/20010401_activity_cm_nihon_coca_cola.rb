pkey = :cm_nihon_coca_cola
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  company_factory :nihon_coca_cola
  activity_factory pkey do
    {
      activity_type: :cm,
      title_id: 'exist|nihon_coca_cola',
      company_id: :nihon_coca_cola,
      sort_order: 200104010,
    }
  end
end

key = :cm_nihon_coca_cola_ashitaga_arusa
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_nihon_coca_cola,
      j_title: '日本コカ・コーラ株式会社「ジョージア　カフェレーチェ」',
      e_title: 'Nihon Coca Cola "Georgia Cafe Leche"',
      from_id: '2001/4',
      to_id: '2001/6',
      sort_order: 200104010,
      song_id: :ashitaga_arusa
    }
  end
end

