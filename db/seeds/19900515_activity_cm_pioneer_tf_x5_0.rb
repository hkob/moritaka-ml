key = :cm_pioneer_tf_x5_0
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_pioneer,
      j_title: 'パイオニア 「留守番できるコードレス TF-X5」',
      e_title: 'Pioneer "codeless telephone system with answering service TF-X5',
      from_id: '1990/5/15',
      to_id: '1990/6/2',
      song_id: :kusaimononiwa_futaoshiro,
      sort_order: 199005150
    }
  end
end
