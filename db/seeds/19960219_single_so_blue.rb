key = :single_so_blue
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :so_blue do
    {
      device_type: :single,
      event_date_id: '1996/2/19',
      sort_order: 199602190,
      number: '28th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4278.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDA-24', '1st', false, :one_up_music, '1,000'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/so-blue-single/id531613809', '1st', true, :warner_music_japan, '750']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:so_blue, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka APIANO iYasuakiMaejima GUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi BASS iYukioSeto]],
    [:chisato_samba, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka APIANO iChisatoMoritaka FRHODES iYasuakiMaejima PERCUSSION iYasuakiMaejima BASS iMasafumiYokoyama KEYBOARDS iYuichiTakahashi]],
    [:so_blue, %i[VOCAL iKaraoke ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka APIANO iYasuakiMaejima GUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi BASS iYukioSeto], '(オリジナル・カラオケ)', '(Original karaoke)']
  ]
  device_activity_factory single, :thema_song_count_down_tv
end

