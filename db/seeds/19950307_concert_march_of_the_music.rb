key = :concert_march_of_the_music
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  concert = concert_factory key, :march_of_the_music do
    {
      j_subtitle: '阪神大震災被災者支援コンサート',
      e_subtitle: 'Concert for supporting victims of Hanshin earthquake',
      concert_type: :etc,
      from_id: '1995/3/7',
      has_song_list: true,
      sort_order: 199503070,
      num_of_performances: 1,
      num_of_halls: 1,
    }
  end

  list_a = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }
  list_a.list_contents_from_array [[:diamonds, [], ' with Princess Princess', ' with Princess Princess']]

  concert.concert_halls_from_array [
    ['1995/3/7', :日本武道館, list_a],
  ]
end
