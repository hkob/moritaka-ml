key = :single_watashiga_obasannni_nattemo
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :watashiga_obasanni_nattemo do
    {
      device_type: :single,
      event_date_id: '1992/6/25',
      sort_order: 199206250,
      number: '16th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4206.html'
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'WPDL-4301', '1st', false, :warner_music_japan, '900'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/私がオバさんになっても-single/id528968719', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:watashiga_obasanni_nattemo, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito], '(シングル・ヴァージョン)', '(Single version)'],
    [:jitensha_tsuugaku, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi]],
  ]
  device_activity_factory single, :thema_song_mattanashi
end


