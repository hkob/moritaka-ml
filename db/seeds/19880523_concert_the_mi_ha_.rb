key = :concert_the_mi_ha_
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  concert = concert_factory key do
    {
      concert_type: :live,
      from_id: '1988/5/23',
      to_id: '1988/8/12',
      has_song_list: true,
      sort_order: 198808120,
      num_of_performances: 10,
      num_of_halls: 10
    }
  end

  list_a = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }
  list_b = list_factory("#{key}_B") { {concert_id: concert, keyword: :B, sort_order: 2} }

  list_a.list_contents_from_array(%i[new_season otisreddingni_kanpai namida_good_bye yumeno_owari do_you_know_the_way_to_san_jose anohino_photograph romantic kiss_the_night good_bye_season yokohama_one_night mi_ha_ overheat_night forty_seven_hard_nights cant_say_good_bye get_smile] + @encore + %i[the_mi_ha_])
  list_b.list_contents_from_array %i[new_season yokohama_one_night romantic kiss_the_night mi_ha_ get_smile]

  concert.concert_halls_from_array [
    ['1988/5/23', :inkstick_芝浦_factory],
    ['1988/6/26', :日比谷野外音楽堂, list_a, '「ザ・ミーハー」', '"The Mi-HA-"'],
    ['1988/7/7', :大阪厚生年金会館, list_a, '中ホール', 'Medium hall'],
    ['1988/7/17', :長野市内ライブハウス, list_a],
    ['1988/7/22', :新潟県民会館, list_a, '大ホール', 'Big hall'],
    ['1988/7/24', :江ノ島シーサイド],
    ['1988/7/26', :向ヶ丘遊園, nil, 'ラジオ日本公録', 'Public recording for Radio-Nippon'],
    ['1988/8/4', :有明MZAコンベンション, nil, "Bay Side Summer Gal's Festival|過労で途中倒れる", "Bay Side Summer Gal's Festival|The overwork incapacitates her for the concert."],
    ['1988/8/11', :大宮ソニックシティ, list_b],
    ['1988/8/12', :渋谷公会堂, nil, 'MUSIC WAVE', 'MUSIC WAVE']
  ]
end
