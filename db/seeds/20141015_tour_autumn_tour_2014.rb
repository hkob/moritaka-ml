key = :tour_autumn_tour_2014
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  band = band_factory(:nazono_enban_ufos)
  band.band_members_from_array %i[VOCAL iChisatoMoritaka RECORDER iChisatoMoritaka DRUMS iToshiyukiTakao GUITAR iMariaSuziki GUITAR iYuichiTakahashi BASS iMasafumiYokoyama KEYBOARDS iDaisukeSakurai BANDLEADER iYuichiTakahashi]

  concert = concert_factory key do
    {
      concert_type: :tour,
      from_id: '2014/10/15',
      to_id: '2014/11/18',
      has_song_list: true,
      has_product: true,
      sort_order: 201410150,
      num_of_performances: 6,
      num_of_halls: 3,
      band_id: :nazono_enban_ufos
    }
  end

  list_a, list_b, list_c, list_d, list_e, list_f = %i[A B C D E F].map.with_index { |k, i| list_factory("#{key}_#{k}") { {concert_id: concert, keyword: k, sort_order: i+1} } }

  list_a.list_contents_from_array %i[wakasano_hiketsu kusaimononiwa_futaoshiro watashiga_obasanni_nattemo] + @mc + %i[forty_seven_hard_nights otisreddingni_kanpai] + @mc + %i[hadakaniwa_naranai wasuremono dekirudesho] + @mc_member + [[:ame, [], '(ロック・ヴァージョン)', '(Rock version)']] + %i[stress hijitsuryokuha_sengen] + ListContent.title_only('MC (グッズ紹介)', 'MC (Introduction of goods)') + %i[mi_ha_ cup_myudol dont_stop_the_music] + [[:la_la_sunshine, [], 'Remixed by tofubeats', 'Remixed by tofubeats']] + @mc + %i[yoruno_entotsu get_smile teriyaki_burger] + @encore + %i[tsumetai_tsuki konomachi] + @mc + %i[watarasebashi] + @double_encore + %i[kibun_soukai]
  list_b.list_contents_from_array %i[wakasano_hiketsu kusaimononiwa_futaoshiro watashiga_obasanni_nattemo] + @mc + %i[kiss_the_night weekend_blue] + @mc + %i[hadakaniwa_naranai wasuremono dekirudesho] + @mc_member + %i[michi stress hijitsuryokuha_sengen] + ListContent.title_only('MC (グッズ紹介)', 'MC (Introduction of goods)') + %i[mi_ha_ cup_myudol dont_stop_the_music] + [[:la_la_sunshine, [], 'Remixed by tofubeats', 'Remixed by tofubeats']] + @mc + %i[yoruno_entotsu get_smile teriyaki_burger] + @encore + %i[tsumetai_tsuki konomachi] + @mc + %i[watarasebashi] + @double_encore + %i[kibun_soukai]
  list_c.list_contents_from_array %i[wakasano_hiketsu kusaimononiwa_futaoshiro watashiga_obasanni_nattemo] + @mc + %i[forty_seven_hard_nights weekend_blue] + @mc + %i[hadakaniwa_naranai wasuremono dekirudesho] + @mc_member + [[:ame, [], '(ロック・ヴァージョン)', '(Rock version)']] + %i[stress hijitsuryokuha_sengen] + ListContent.title_only('MC (グッズ紹介)', 'MC (Introduction of goods)') + %i[mi_ha_ cup_myudol dont_stop_the_music] +[[:la_la_sunshine, [], 'Remixed by tofubeats', 'Remixed by tofubeats']] + @mc + %i[yoruno_entotsu get_smile teriyaki_burger] + @encore + %i[tsumetai_tsuki konomachi] + @mc + %i[watarasebashi] + @double_encore + %i[kibun_soukai]
  list_d.list_contents_from_array %i[wakasano_hiketsu kusaimononiwa_futaoshiro watashiga_obasanni_nattemo] + @mc + %i[kiss_the_night otisreddingni_kanpai] + @mc + %i[hadakaniwa_naranai wasuremono dekirudesho] + @mc_member + %i[michi stress hijitsuryokuha_sengen] + ListContent.title_only('MC (グッズ紹介)', 'MC (Introduction of goods)') + %i[mi_ha_ cup_myudol dont_stop_the_music] + [[:la_la_sunshine, [], 'Remixed by tofubeats', 'Remixed by tofubeats']] + @mc + %i[yoruno_entotsu get_smile teriyaki_burger] + @encore + %i[tsumetai_tsuki konomachi] + @mc + %i[watarasebashi] + @double_encore + %i[kibun_soukai]
  list_e.list_contents_from_array %i[wakasano_hiketsu kusaimononiwa_futaoshiro watashiga_obasanni_nattemo] + @mc + %i[forty_seven_hard_nights otisreddingni_kanpai] + @mc + %i[hadakaniwa_naranai wasuremono dekirudesho] + @mc_member + %i[michi stress hijitsuryokuha_sengen] + ListContent.title_only('MC (グッズ紹介)', 'MC (Introduction of goods)') + %i[mi_ha_ cup_myudol dont_stop_the_music] + [[:la_la_sunshine, [], 'Remixed by tofubeats', 'Remixed by tofubeats']] + @mc + %i[yoruno_entotsu get_smile teriyaki_burger] + @encore + %i[tsumetai_tsuki konomachi] + @mc + %i[watarasebashi] + @mc + %i[mite] + @double_encore + %i[kibun_soukai]
  list_f.list_contents_from_array %i[wakasano_hiketsu kusaimononiwa_futaoshiro watashiga_obasanni_nattemo] + @mc + %i[kiss_the_night weekend_blue] + @mc + %i[hadakaniwa_naranai wasuremono dekirudesho] + @mc_member + [[:ame, [], '(ロック・ヴァージョン)', '(Rock version)']] + %i[stress hijitsuryokuha_sengen] + ListContent.title_only('MC (グッズ紹介)', 'MC (Introduction of goods)') + %i[mi_ha_ cup_myudol dont_stop_the_music] + [[:la_la_sunshine, [], 'Remixed by tofubeats', 'Remixed by tofubeats']] + @mc + %i[yoruno_entotsu get_smile teriyaki_burger] + @encore + %i[tsumetai_tsuki konomachi] + @mc + %i[watarasebashi] + @double_encore + %i[jin_jin_jinglebell kibun_soukai]

  concert.concert_halls_from_array [
    ConcertHall.mk('2014/10/15', :ZeppNamba, list_a, 'ホルモン、串カツ', 'Hormone, Cutlet Skewers'),
    ConcertHall.mk('2014/10/16', :ZeppNagoya, list_b, '味噌カツ、味噌煮込みうどん', '{MISOKATSU}, {MISO-NIKOMI UDON}'),
    ConcertHall.mk('2014/10/27', :ZeppTokyo, list_c, '馬刺し、豚骨ラーメン', '{BASASHI}, {TONKOTSU RA-MEN}'),
    ConcertHall.mk('2014/11/4', :ZeppTokyo, list_d, '馬刺し、ラーメン', '{BASASHI}, {RA-MEN}'),
    ConcertHall.mk('2014/11/5', :ZeppTokyo, list_e, '馬刺し、豚骨ラーメン', '{BASASHI}, {TONKOTSU RA-MEN}'),
    ConcertHall.mk('2014/11/18', :ZeppTokyo, list_f, '馬刺し、豚骨ラーメン', '{BASASHI}, {TONKOTSU RA-MEN}'),
  ]
end
