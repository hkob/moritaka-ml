key = :mezamashi_live_2019
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  concert = concert_factory key do
    {
      concert_type: :live,
      from_id: '2019/3/7',
      has_song_list: true,
      has_product: false,
      sort_order: 201903070,
      num_of_performances: 1,
      num_of_halls: 1,
    }
  end

  list_a, list_b = %i[A].map.with_index { |k, i| list_factory("#{key}_#{k}") { {concert_id: concert, keyword: k, sort_order: i+1} } }

  list_a.list_contents_from_array %i[futariwa_koibito rockn_roll_kencho_shozaichi la_la_sunshine watashiga_obasanni_nattemo watarasebashi] + [[:ame, [], '(ロック・ヴァージョン)', '(Rock version)']] + %i[kibun_soukai teriyaki_burger]

  concert.concert_halls_from_array [
    ConcertHall.mk('2019/3/7', '国技館', list_a, '', ''),
  ]
end

