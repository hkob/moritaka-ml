key = :thema_song_volleyball_worldcup91
obj = Activity.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_factory key do
    {
      activity_type: :thema_song,
      title_id: %q(「バレーボールワールドカップ'91」|"Volleyball world cup '91"|ばれーぼーるわーるどかっぷきゅうじゅういち),
      j_comment: 'イメージソング',
      e_comment: 'Image song',
      company_id: :fuji_tv,
      from_id: '1991/11/8',
      to_id: '1991/12/1',
      sort_order: 199111080,
      song_id: :fight,
    }
  end
end

