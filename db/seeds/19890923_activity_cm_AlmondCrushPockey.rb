pkey = :cm_ezaki_guriko
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  company_factory :ezaki_guriko
  activity_factory pkey do
    {
      activity_type: :cm,
      title_id: 'exist|ezaki_guriko',
      company_id: :ezaki_guriko,
      sort_order: 198909230
    }
  end
end

key = :cm_ezaki_guriko_pocky_michi
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_ezaki_guriko,
      j_title: '江崎グリコ 「アーモンド・クラッシュ・ポッキー」',
      e_title: 'Ezaki Guriko "Almond crush pockey"',
      from_id: '1989/9/23',
      to_id: '1990/12/22',
      song_id: :michi,
      sort_order: 198909230
    }
  end
end
