key = :single_futarino_hokkaido
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :futarino_hokkaido do
    {
      device_type: :lyric_single,
      event_date_id: '1999/7/23',
      sort_order: 199907230,
      number: '1st',
      singer_id: :iCountryMusume,
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'SPRE-002', '1st', false, :potato, '1,020'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:futarino_hokkaido, %i[ARRANGE iYasuakiMaejima VOCAL iCountryMusume]],
    [nil, %i[VOCAL iCountryMusume], nil, nil, 'モーニング牛乳', 'Morning Milk'],
    [:futarino_hokkaido, %i[ARRANGE iYasuakiMaejima VOCAL iKaraoke], '(ORIGINAL KARAOKE)', '(ORIGINAL KARAOKE)'],
  ]
end

