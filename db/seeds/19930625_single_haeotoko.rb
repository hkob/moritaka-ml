key = :single_haeotoko
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :haeotoko do
    {
      device_type: :single,
      event_date_id: '1993/6/25',
      sort_order: 199306250,
      number: '19th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4214.html',
      j_comment: 'おまけステッカー付き',
      e_comment: 'include a sticker',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'WPDL-4351', '1st', false, :warner_music_japan, '900'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/sino-xia-single/id531608790', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:haeotoko, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PIANORC iChisatoMoritaka GUITAR iYuichiTakahashi GUITAR iEijiOgata PIANOLC iShinHashimoto BASS iYukioSeto]],
    [:memories, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito GUITAR iHideoSaito SYNTHESIZER iHideoSaito]],
  ]
  device_activity_factory single, :thema_song_ucchan_nanchan_yaruyara
end


