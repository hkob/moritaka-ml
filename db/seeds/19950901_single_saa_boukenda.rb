key = :single_saa_boukenda
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :saa_boukenda do
    {
      device_type: :lyric_single,
      event_date_id: '1995/9/1',
      sort_order: 199509010,
      number: '59th',
      singer_id: :iAkikoWada,
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'WPD6-9043', '1st', false, :wea_japan, '1,000'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:saa_boukenda, %i[VOCAL iAkikoWada ARRANGE iKomeKomeClub]],
    [:saa_boukenda, %i[VOCAL iAkikoWada ARRANGE iKomeKomeClub], '(アコースティック・ヴァージョン)', '(Acoustic version)'],
    [:saa_boukenda, %i[VOCAL iKaraoke ARRANGE iKomeKomeClub], '(オリジナル・カラオケ)', '(Original karaoke)'],
  ]
  device_activity_factory single, :thema_song_ponkickies
end


