key = :dvd_mite_stress_17sai
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key do
    {
      device_type: :video_clips,
      event_date_id: '2000/9/27',
      minutes: 55,
      seconds: 00,
      sort_order: 200009270,
      number: '1st, 2nd',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4494.html'
    }
  end

  video.media_from_array [
    ["#{key}_dvd", :dvd, 'WPB6-90006', :first, true, :warner_vision_japan, '4,104'],
  ]

  list_mite = list_factory("#{key}_mite") { {device_id: video, keyword: :mite, sort_order: 1} }
  list_stress = list_factory("#{key}_stress") { {device_id: video, keyword: :stress, sort_order: 2} }
  list_seventeen = list_factory("#{key}_seventeen") { {device_id: video, keyword: :seventeen, sort_order: 3} }
  list_mite.list_contents_from_array(%i[new_season the_mi_ha_ alone].map { |key| [key, @vc] })
  list_stress.list_contents_from_array [
    [:overheat_night, @vc],
    [:get_smile, @vc],
    [:the_stress, @vc, '[ストレス中近東ヴァージョン]', '[Stress {CHUUKINTOU} version]'],
    [:modorenai_natsu, @vc, "[メイキング・オブ・「ザ・ストレス」 BGM]", "[Making of `the stress' BGM]"],
  ]
  list_seventeen.list_contents_from_array [
    [:seventeen, @vc],
    [:daite, @vc, '[ラスベガス・ヴァージョン]', '[Las Vegas version]'],
    [:yoruno_entotsu, @vc],
    [:watashiwa_onchi, @vc, 'ザ・メイキング', 'The making']
  ]
end

