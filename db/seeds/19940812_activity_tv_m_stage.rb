key = :tv_m_stage
obj = Activity.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity = activity_factory key do
    {
      activity_type: :tv_program,
      title_id: 'M STAGE|M STAGE|えむ　すてーじ',
      company_id: :nihon_tv,
      sort_order: 199408120,
    }
  end

end

activity_sub_factories_for_music_tv :tv_m_stage, {
  '19940812' => ['1994/8/12',%i[watashino_natsu natsuno_hi]]
}
