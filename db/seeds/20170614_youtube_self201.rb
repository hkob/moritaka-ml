yt = :sc201

yt_hash = {
  '2017/6/15' => {
    link: 'Ny0yDSyIvc8',
    number: '201',
    song_id: :detagari,
    comment: "作詞：森高千里　作曲：斉藤英夫　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」201曲目は、\n1988年発表のアルバム「見て」から「出たがり」！\nドラムは森高千里によるループドラム！\n\n衣装協力：ミューカ／YANUK"
  },
  '2017/6/21' => {
    link: 'NDGdFd2IhVg',
    number: '202',
    song_id: :omoshiroi,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」202曲目は、\n1988年発表のアルバム「見て」から「おもしろい（森高コネクション）」！\nドラムは森高千里によるループドラム！\n\n衣装協力：GHITA/ジータ△オンラインストア"
  },
  '2017/7/13' => {
    link: 'ymLLjMHMw7E',
    number: '203',
    song_id: :korekkiri_byebye,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」203曲目は、\n1989年発表のアルバム「非実力派宣言」から「これっきりバイバイ」！\nドラムは森高千里によるループドラム！\n\n衣装協力：エッフェ ビームス／チノ／ヴァンドーム"
  },
  '2017/7/20' => {
    link: '9sdM4Z2ULM8',
    number: '204',
    song_id: :sayonara_watashino_koi,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」204曲目は、\n1993年発表のアルバム「LUCKY 7」から「さよなら私の恋」！\nドラムは森高千里による新録音！\n\n 衣装協力：ECLIN／マリベル ジーン／プラス ヴァンドーム／ヴァンドーム ブティック"
  },
  '2017/7/27' => {
    link: 'MyOtj7R2O20',
    number: '205',
    song_id: :mitatoriyo_watashi,
    comment: "作詞・作曲：森高千里\n\n公式チャンネル独占企画「200曲セルフカヴァー」205曲目は、\n1997年発表のアルバム「PEACHBERRY」から「見たとおりよ私」！\nドラムは森高千里によるループドラム！\n\n衣装協力：ECLIN／アネモネ"
  },
  '2017/9/29' => {
    link: 'MEqN0U7Vox8',
    number: '206',
    song_id: :watashiga_obasanni_nattemo,
    j_title: "森高千里 『私がオバさんになっても（スローヴァージョン）』 【セルフカヴァー】",
    e_title: "Chisato Moritaka 『{WATASHI-GA OBASAN-NI NATTEMO} (Slow version): [Even if I become a middle age lady] (Slow version)』 【Self cover】",
    comment: "作詞：森高千里　作曲：斉藤英夫　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」ラストの曲は、\n1992年発表のアルバム「ROCK ALIVE」に収録され、その後16枚目のシングルとしてリリースされた「私がオバさんになっても」！\nドラムは森高千里によるループドラム！\n\n衣装協力：デミクルス ビームス／3rd Spring／ヴァンドーム ブティック"
  },
}
create_youtube_factory_from_hash(yt, yt_hash)
