key = :album_acoustic
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :acoustic do
    {
      device_type: :cover_album,
      event_date_id: '1995/8/21',
      sort_order: 199508210,
      singer_id: :iKaraoke
    }
  end

  album.media_from_array [
    ["#{key}_cd", :cd, 'APCE-5411', :first, false, :apollon_inc, '2,500'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array %i[kyoukara futariwa_koibito watashino_daijina_hito sutekina_tanjoubi natsuno_hi] + [[:kibun_soukai, [], '(Album Version)', '(Album Version)']] + %i[kazeni_fukarete memories watashino_natsu watarasebashi watashiga_obasanni_nattemo ame]
end

