key = :thema_song_iitabi_yumekibun_b
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :thema_song_iitabi_yumekibun,
      j_title: '旅行番組 「いい旅・夢気分」エンディングテーマ',
      e_title: 'Ending thema song of TV program (travel) "{II-TABI YUME-KIBUN}',
      from_id: '1994',
      to_id: '1994',
      song_id: :juusangatsuno_ame,
      sort_order: 199401010
    }
  end
end



