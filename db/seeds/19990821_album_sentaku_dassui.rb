key = :album_sentaku_dassui
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :sentaku_dassui do
    {
      device_type: :part_album,
      event_date_id: '1999/8/21',
      minutes: 37,
      seconds: 29,
      singer_id: :iGeorgeTokoro,
      sort_order: 199908210,
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'VPCC-81307', :first, false, :vap, '3,059'],
  ]

  vg = %i[VOCAL iGeorgeTokoro]
  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_with_performer(vg, '駅前のワシントン条約', '{EKIMAE-NO} Washington {JOUYAKU}') +
    ListContent.title_with_performer(vg, '拓郎さんのギターです', '{TAKUROU-SAN-NO} Guitar {DESU}') +
    [[:koino_uta, %i[ARRANGE iKounosukeSakazaki DRUMS iChisatoMoritaka VOCAL iGeorgeTokoro]]] +
    ListContent.title_with_performer(vg, '西瓜', 'Water Melon') +
    ListContent.title_with_performer(vg, 'コンドコソ飛んで行く', '{KONDOKOSO TONDE IKU}') +
    ListContent.title_with_performer(vg, '雪', 'Snow') +
    ListContent.title_with_performer(vg, '化けて出ている', '{BAKETE DETEIRU}') +
    ListContent.title_with_performer(vg, '泳げたいやき屋のおじさん', '{OYOGE TAIYAKI-YA-NO OJISAN}') +
    ListContent.title_with_performer(vg, '酒と肴と酒と酒', '{SAKE-TO SAKANA-TO SAKE-TO SAKE}') +
    ListContent.title_with_performer(vg, '雨が空に戻るまでに', '{AME-GA SORA-NI MODORUMADENI}') +
    ListContent.title_with_performer(vg, '後悔してます', '{KOUKAI SHITEMASU}') +
    ListContent.title_with_performer(vg, '明石家さんまさんに聞いてみてもネ!', '{AKASHIYA SANMA-SAN-NI KIITE MITEMO-NE}!') +
    ListContent.title_with_performer(vg, 'ご自由にどうぞ', '{GOJIYUU-NI DOUZO}')
  )
end

