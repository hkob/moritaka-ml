key = :album_the_best_selection_of_first_moritaka
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :the_best_selection_of_first_moritaka do
    {
      device_type: :album,
      event_date_id: '1999/2/15',
      minutes: 90,
      seconds: 0,
      sort_order: 199902150,
      number: '17th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4147.html',
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'WPC7-8535', :first, false, :warner_music_japan, '3,675'],
  ]

  vc = %i[VOCAL iChisatoMoritaka]
  list_disk1 = list_factory("#{key}_disk1") { {device_id: album, keyword: :disk1, sort_order: 1} }
  list_disk2 = list_factory("#{key}_disk2") { {device_id: album, keyword: :disk2, sort_order: 2} }
  list_disk1.list_contents_from_array [
    [:new_season, vc],
    [:overheat_night, vc],
    [:get_smile, vc],
    [:mi_ha_, vc, '[スペシャル・ミーハー・ミックス]', '[Special {MI-HA-} mix]'],
    [:alone, vc],
    [:stress, vc, '[ストレス中近東ヴァージョン]', '[Stress {CHUUKINTOU} version]'],
    [:seventeen, vc],
    [:daite, vc, '[ラスベガス・ヴァージョン]', '[Las Vegas version]'],
    [:michi, vc],
    [:seishun, vc]
  ]
  list_disk2.list_contents_from_array [
    [:kusaimononiwa_futaoshiro, vc],
    [:ame, vc],
    [:benkyono_uta, vc],
    [:hachigatsuno_koi, vc],
    [:fight, vc],
    [:concert_no_yoru, vc],
    [:watashiga_obasanni_nattemo, vc, '(シングル・ヴァージョン)', '(Single version)'],
    [:watarasebashi, vc],
    [:watashino_natsu, vc],
    [:haeotoko, vc, '(シングル・ヴァージョン)', '(Single version)'],
  ]
end

