key = :score_piano_mini_album
obj = Book.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  book = book_factory key do
    {
      book_type: :score_book,
      publisher_id: :yamaha_music_media,
      price: '669',
      event_date_id: '1995/12/10',
      sort_order: 199512100,
    }
  end

  list = list_factory("#{key}_A") { {book_id: book, keyword: :A, sort_order: 1} }
  list.list_contents_from_array %i[ame watashiga_obasanni_nattemo kazeni_fukarete futariwa_koibito yasumino_gogo]
end
