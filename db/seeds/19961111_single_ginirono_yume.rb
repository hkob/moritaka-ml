key = :single_ginirono_yume
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :ginirono_yume do
    {
      device_type: :single,
      event_date_id: '1996/11/11',
      sort_order: 199611110,
      number: '30th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4282.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDA-32', '1st', false, :one_up_music, '1,000'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/yin-seno-meng-single/id531613939', '1st', true, :warner_music_japan, '750']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:ginirono_yume, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PERCUSSION iChisatoMoritaka CHORUS iChisatoMoritaka GUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi PIANO iShinHashimoto TAISHOGOTO iShinHashimoto BASS iYukioSeto GUITAR iYukioSeto PERCUSSION iYukioSeto]],
    [:taiyouto_aoi_tsuki, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PERCUSSION iChisatoMoritaka CHORUS iChisatoMoritaka KEYBOARDS iYuichiTakahashi PERCUSSION iYuichiTakahashi PIANO iShinHashimoto FRHODES iShinHashimoto KEYBOARDS iShinHashimoto PERCUSSION iShinHashimoto BASS iYukioSeto GUITAR iYukioSeto PERCUSSION iYukioSeto], '(太陽神ヴァージョン)', '(Sungod version)'],
    [:ginirono_yume, %i[VOCAL iKaraoke DRUMS iChisatoMoritaka PERCUSSION iChisatoMoritaka CHORUS iChisatoMoritaka GUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi PIANO iShinHashimoto TAISHOGOTO iShinHashimoto BASS iYukioSeto GUITAR iYukioSeto PERCUSSION iYukioSeto], '(オリジナル・カラオケ)', '(Original karaoke)']
  ]
  device_activity_factory single, :cm_meiji_seika
end


