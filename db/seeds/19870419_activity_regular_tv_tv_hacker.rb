key = :regular_tv_tv_hacker
obj = Activity.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_factory key do
    {
      activity_type: :regular_tv,
      title_id: 'TVハッカー|TV Hacker|てれびはっかー',
      company_id: :fuji_tv,
      from_id: '1987/4/19',
      to_id: '1987/9/27',
      j_comment: '毎週日曜 20:00 - 20:54',
      e_comment: 'Every Sunday, 20:00 - 20:54',
      sort_order: 198704190,
    }
  end
end

