key = :concert_festival_1989
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  concert = concert_factory key, :festival_1989 do
    {
      concert_type: :festival,
      from_id: '1989/10/8',
      to_id: '1989/11/25',
      has_song_list: true,
      sort_order: 198910080,
      num_of_performances: 20,
      num_of_halls: 20
    }
  end

  concert.concert_halls_from_array [
    ['1989/10/8', :金沢医科大学],
    ['1989/10/14', :東京経済大学],
    ['1989/10/15', :東北学院大学],
    ['1989/10/19', :拓殖大学],
    ['1989/10/20', :文教大学, nil, '湘南校舎', 'Shonan Schoolhouse'],
    ['1989/10/22', :足利工業大学],
    ['1989/10/24', :京都府立医科大学],
    ['1989/10/26', :神戸文化ホール, nil, '神戸大学医学部', 'Kobe University School of Medical'],
    ['1989/10/28', :埼玉工業大学],
    ['1989/10/29', :武蔵野女子大学],
    ['1989/10/31', :愛知県立大学],
    ['1989/11/2', :日本大学松戸歯学部],
    ['1989/11/3', :流通経済大学],
    ['1989/11/5', :日本大学経済学部],
    ['1989/11/12', :北里大学],
    ['1989/11/16', :明治薬科大学],
    ['1989/11/19', :九州大学],
    ['1989/11/23', :武蔵工業大学],
    ['1989/11/24', :茨城大学],
    ['1989/11/25', :岐阜経済大学],
  ]
end
