pkey = :thema_song_mischievous_twins
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_factory pkey do
    {
      activity_type: :thema_song,
      title_id: %q(アニメ「おちゃめなふたご クレア学院物語」|An animation program {OCHAME-NA FUTAGO KUREA GAKUIN MONOGATARI}: [Mischievous Twins: The Tales of St. Clare's]|おちゃめなふたご　くれあがくいんものがたり),
      company_id: :nihon_tv,
      sort_order: 199101050,
    }
  end
end

key = :thema_song_mischievous_twins_ending
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :thema_song_mischievous_twins,
      j_title: 'アニメ「おちゃめなふたご クレア学院物語」エンディングソング',
      e_title: "Ending song for animation program {OCHAME-NA FUTAGO KUREA GAKUIN MONOGATARI}: [Mischievous Twins: The Tales of St. Clare's]",
      from_id: '1991/1/5',
      to_id: '1991/11/2',
      sort_order: 199101060,
      song_id: :itsumademo,
    }
  end
end

