key = :album_dancing_with_an_angel
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :dancing_with_an_angel do
    {
      device_type: :cover_album,
      event_date_id: '1991/7/21',
      minutes: 58,
      seconds: 9,
      sort_order: 199107210,
      number: '??th',
      singer_id: :iRitaCoolidge,
    }
  end

  album.media_from_array [
    ["#{key}_cd", :cd, 'ALCB-298', :first, false, :alfa_records, '3,146'],
  ]

  vr = %i[VOCAL iRitaCoolidge]
  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_with_performer(vr, 'I Just Want To Be With You') +
    [[:rain, vr]] +
    ListContent.title_with_performer(vr, 'End of the Summer') +
    ListContent.title_with_performer(vr, 'Endless Nights') +
    ListContent.title_with_performer(vr, 'Another Saturday -- Let It Be Me --') +
    ListContent.title_with_performer(vr, 'Samarkand Blue') +
    ListContent.title_with_performer(vr, 'River of Love') +
    ListContent.title_with_performer(vr, 'Sal y Sombra') +
    ListContent.title_with_performer(vr, 'Rainy Blue') +
    ListContent.title_with_performer(vr, "Please Don't You Cry ") +
    ListContent.title_with_performer(vr, 'Suddenly') +
    ListContent.title_with_performer(vr, 'Circle of Light') +
    ListContent.title_with_performer(vr, '')
  )
end
