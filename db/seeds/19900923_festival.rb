key = :concert_festival_1990
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  concert = concert_factory key, :festival_1990 do
    {
      concert_type: :festival,
      from_id: '1990/9/23',
      to_id: '1990/11/29',
      has_song_list: true,
      sort_order: 199009230,
      num_of_performances: 17,
      num_of_halls: 17
    }
  end

  list_a, list_b, list_c = %i[A B C].map.with_index { |k, i| list_factory("#{key}_#{k}") { {concert_id: concert, keyword: k, sort_order: i+1} } }

  list_a.list_contents_from_array(%i[kusaimononiwa_futaoshiro hadakaniwa_naranai seishun overheat_night aru_olno_seishun ame funkey_monkey_baby stress sonogono_watashi yoruno_entotsu seventeen mi_ha_ get_smile mite] + @encore + %i[michi daite])
  list_b.list_contents_from_array(%i[kusaimononiwa_futaoshiro hadakaniwa_naranai seishun overheat_night aru_olno_seishun ame funkey_monkey_baby stress sonogono_watashi yoruno_entotsu seventeen] + @encore + %i[new_season])
  list_c.list_contents_from_array(%i[kusaimononiwa_futaoshiro hadakaniwa_naranai seishun overheat_night aru_olno_seishun ame] + [[:uchini_kagitte, [],  '(初期だけ)', '(only the early days)']] + %i[yoruno_entotsu stress] + [[:sonogono_watashi, [], '(筑波以外)', '(except for Tsukuba)']] + [[:akunno_higeki, [], '(By 一生懸命, 筑波のみ)', '(By {ISSHOKENMEI}, only Tsukuba)']] + %i[seventeen funkey_monkey_baby mi_ha_ get_smile] + [[:mite, [], '(ツアー前半)', '(The first half of the tour)']] + [[:teriyaki_burger, [], '(ツアー後半)', '(The last half of the tour)']] + @encore + [[:michi, [], '(歌われないこともあり)', '(This song sometimes omitted.)']] + %i[daite])

  concert.concert_halls_from_array [
    ['1990/9/23', :千葉日大第一高等学校],
    ['1990/9/29', :筑波大学, list_a],
    ['1990/10/1', :有明MZAコンベンション, list_b, '(WPB)', '(WPB)'],
    ['1990/10/7', :東京歯科大学, list_c],
    ['1990/10/20', :有明MZAコンベンション, list_c, '[早稲田大学]', '[Waseda university]'],
    ['1990/10/21', :大妻女子短期大学, list_c],
    ['1990/10/28', :福山大学, list_c],
    ['1990/11/1', :皇學館大学, list_c],
    ['1990/11/2', :駒沢大学, list_c],
    ['1990/11/3', :立正大学, list_c],
    ['1990/11/5', :鳴門市文化会館, list_c, '[徳島大学]', '[Tokushima University]'],
    ['1990/11/10', :群馬音楽センター, list_c, '[高崎経済大学]', 'Takasaki City University of Economics]'],
    ['1990/11/14', :四日市市民会館, list_c, '[四日市大学]', '[Yokkaichi University]'],
    ['1990/11/17', :岡山商科大学, list_c],
    ['1990/11/18', :成徳女子短期大学, list_c],
    ['1990/11/21', :愛媛県県民文化会館, list_c, '[松山大学]', '[Matsuyama University]'],
    ['1990/11/29', :宇都宮市文化会館, list_c, '[宇都宮大学]', '[Utsunomiya University]'],
  ]
end

