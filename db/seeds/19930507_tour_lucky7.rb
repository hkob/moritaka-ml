key = :tour_lucky7
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  band = band_factory(:smilyMaejimaHelloGoodBye)
  band.band_members_from_array %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka GUITAR iChisatoMoritaka RECORDER iChisatoMoritaka PIANO iChisatoMoritaka KEYBOARDS iYasuakiMaejima CHORUS iYasuakiMaejima KEYBOARDS iShinKohno GUITAR iShinKohno CHORUS iShinKohno BASS iMasafumiYokoyama CHORUS iMasafumiYokoyama GUITAR iHiroyoshiMatsuo CHORUS iHiroyoshiMatsuo DRUMS iToshihiroTsuchiya BANDLEADER iYasuakiMaejima]

  concert = concert_factory key do
    {
      j_subtitle: "森高千里コンサートツアー'93",
      e_subtitle: "Chisato Moritaka concert tour '93",
      concert_type: :tour,
      from_id: '1993/5/7',
      to_id: '1993/11/30',
      has_song_list: true,
      has_product: true,
      sort_order: 199305070,
      num_of_performances: 61,
      num_of_halls: 57,
      j_comment: 'JA共済Presents',
      e_comment: 'JA cooperative society presents',
      band_id: :smilyMaejimaHelloGoodBye
    }
  end

  list_a, list_b, list_c, list_d = %i[A B C D].map.with_index { |k, i| list_factory("#{key}_#{k}") { {concert_id: concert, keyword: k, sort_order: i+1} } }
  list_a.list_contents_from_array %i[lucky7_blues teo_tatako kusaimononiwa_futaoshiro rockn_roll_kencho_shozaichi fight] + @mc + %i[jimina_onna tomodachino_kare writer_shibo] + @mc + %i[michi watashino_natsu] + @mc + %i[watarasebashi sayonara_watashino_koi i_love_you] + @mc + %i[haeotoko watashiga_obasanni_nattemo yacchimaina get_smile] + @encore + %i[ame memories] + @mc + %i[konomachi] + @double_encore + %i[concert_no_yoru]
  list_b.list_contents_from_array %i[lucky7_blues teo_tatako rockn_roll_kencho_shozaichi fight] + @mc + %i[jimina_onna tomodachino_kare writer_shibo] + @mc + %i[michi stress watashino_natsu] + @mc + %i[watarasebashi sayonara_watashino_koi i_love_you] + @mc + %i[haeotoko watashiga_obasanni_nattemo yacchimaina get_smile] + @encore + %i[ame memories] + @mc + %i[konomachi] + @double_encore + %i[concert_no_yoru]
  list_c.list_contents_from_array %i[lucky7_blues teo_tatako rockn_roll_kencho_shozaichi fight] + @mc + %i[jimina_onna tomodachino_kare writer_shibo] + @mc + %i[michi stress watashino_natsu] + @mc + %i[watarasebashi sayonara_watashino_koi i_love_you] + @mc + %i[haeotoko watashiga_obasanni_nattemo teriyaki_burger] + @mc + %i[ame memories] + @mc + %i[konomachi] + @double_encore + %i[concert_no_yoru]
  list_d.list_contents_from_array %i[lucky7_blues teo_tatako rockn_roll_kencho_shozaichi fight] + @mc + %i[jimina_onna tomodachino_kare writer_shibo] + @mc + %i[michi stress watashino_natsu] + @mc + %i[kazeni_fukarete watarasebashi sayonara_watashino_koi i_love_you] + @mc + %i[haeotoko watashiga_obasanni_nattemo teriyaki_burger] + @encore + %i[ame memories] + @mc + %i[konomachi] + @double_encore + %i[concert_no_yoru]

  concert.concert_halls_from_array [
    ConcertHall.mk('1993/5/7', :綾瀬市文化会館, list_a, 'ラーメン', '{RAMEN}'),
    ConcertHall.mk('1993/5/12', :茨城県立県民文化センター, list_a, '納豆', 'Fermented soybeans'),
    ConcertHall.mk('1993/5/19', :守山市民ホール, list_a, '守山メロン', "Moriyama's melon"),
    ConcertHall.mk('1993/5/21', :沼津市民文化センター, list_a, 'あじの開き', '{AJI-NO HIRAKI}]'),
    ConcertHall.mk('1993/5/22', :浜松市民会館, list_a, 'うなぎ', 'Eels'),
    ConcertHall.mk('1993/5/29', :福井市文化会館, list_a, '越前そば', "Echizen's Buckwheat"),
    ConcertHall.mk('1993/5/30', :石川厚生年金会館, list_a, '????', '????'),
    ConcertHall.mk('1993/6/1', :富山県民会館, list_a, '鱒寿司', '{FUNAZUSHI}'),
    ConcertHall.mk('1993/6/3', :長野市民会館, list_a, '信州そば', "Shinsyu's Buckwheat"),
    ConcertHall.mk('1993/6/7', :中野サンプラザ, list_a, '馬刺し', 'Slices of raw horse', nil, nil, 'ウッチャンナンチャンが飛び入り？', 'Special Guest "Ucchan Nanchan"'),
    ConcertHall.mk('1993/6/8', :中野サンプラザ, list_a, 'とんこつラーメン', '{TONKOTSU-RAMEN}'),
    ConcertHall.mk('1993/6/15', :足利市民会館, list_a, 'ぱんぢゅう', '{PANJU}', nil, nil, 'ダブルアンコールは、渡良瀬橋', 'The double encore song is "Watarasebashi"'),
    ConcertHall.mk('1993/6/21', :豊橋勤労福祉会館, list_a, 'ちくわ', '{CHIKUWA}'),
    ConcertHall.mk('1993/6/22', :岐阜市民会館, list_a, '鮎', '{AYU}'),
    ConcertHall.mk('1993/6/24', :四日市市文化会館, list_a, 'あかふく', '{AKAFUKU}'),
    ConcertHall.mk('1993/6/30', :旭川市民文化会館, list_a, '旭川ラーメン', "Asahikawa's {RAMEN}"),
    ConcertHall.mk('1993/7/1', :北海道厚生年金会館, list_a, '札幌ラーメン', "Sapporo's {RAMEN}", nil, nil, 'MCで客に煽られて「氷雨」を歌う', %q(She sang "Hisame" by audience's request.)),
    ConcertHall.mk('1993/7/3', :群馬県民会館, list_a, '焼きまんじゅう', '{YAKI-MANJU} (a sweet bun)'),
    ConcertHall.mk('1993/7/9', :秋田市文化会館, list_a, 'きりたんぽ', '{KIRITANPO}'),
    ConcertHall.mk('1993/7/10', :青森市文化会館, list_a, '青森りんご', "Aomori's Apple"),
    ConcertHall.mk('1993/7/15', :新潟県民会館, list_a, 'コシヒカリ', '{KOSHIHIKARI}(rice)'),
    ConcertHall.mk('1993/7/21', :長崎市公会堂, list_a, '????', '????'),
    ConcertHall.mk('1993/7/22', :熊本市民会館, list_a, '????', '????'),
    ConcertHall.mk('1993/7/25', :舞鶴市総合文化会館, list_a, 'いわしちくわ', '{IWASHI-CHIKUWA}'),
    ConcertHall.mk('1993/7/26', :大阪厚生年金会館, list_a, 'たこ焼きやお好み焼き', '{TAKOYAKI} and {OKONOMIYAKI}'),
    ConcertHall.mk('1993/7/28', :福岡市民会館, list_a, '博多ラーメン', "Hakata's {RAMEN}"),
    ConcertHall.mk('1993/8/3', :静岡市民文化会館, list_a, 'うなぎ', 'Eels'),
    ConcertHall.mk('1993/8/7', :郡山市民文化センター, list_a, 'イカ人参', '{IKA-NINJIN}'),
    ConcertHall.mk('1993/8/14', :大宮ソニックシティ, list_a, '大宮ハム', "Omiya's ham"),
    ConcertHall.mk('1993/8/19', :神奈川県民ホール, list_a, '中華料理', 'Chinese cooking'),
    ConcertHall.mk('1993/8/23', :中野サンプラザ, list_b, '馬刺し', 'Slices of raw horse'),
    ConcertHall.mk('1993/8/24', :中野サンプラザ, list_b, 'とんこつラーメン', '{TONKOTSU-RAMEN}'),
    ConcertHall.mk('1993/8/30', :宮城県民会館, list_c, '笹かまぼこ', '{SASA-KAMABOKO}(boiled fish paste)'),
    ConcertHall.mk('1993/8/31', :岩手県民会館, list_c, 'れーめん', '{REMEN}'),
    ConcertHall.mk('1993/9/6', :岡山市民会館, list_c, 'きびだんご', '{KIBIDANGO}'),
    ConcertHall.mk('1993/9/7', :メルパルクホール広島, list_c, 'お好み焼き', '{OKONOMIYAKI}'),
    ConcertHall.mk('1993/9/9', :島根県民会館, list_c, '????', '????'),
    ConcertHall.mk('1993/9/11', :姫路市文化センター, list_c, '揖保の糸そうめん', '{ITO-SOMEN}', nil, nil, 'コンサートの夜 無し', %q(She didn't sing "{Concert-NO YORU}".)),
    ConcertHall.mk('1993/9/15', :佐賀市民会館, list_c, 'ムツゴロウと佐賀海苔', "{MUTUGORO} and Saga's laver"),
    ConcertHall.mk('1993/9/16', :鹿児島県文化センター, list_c, '薩摩揚げ', '{SATSUMAAGE}'),
    ConcertHall.mk('1993/9/18', :大分文化会館, list_c, 'だんご汁', '{DANGO-JIRU}', nil, nil, 'コンサートの夜 無し', %q(She didn't sing "{Concert-NO YORU}".)),
    ConcertHall.mk('1993/9/22', :愛媛県県民文化会館, list_c, 'サブ', 'Sub', '????', '????'),
    ConcertHall.mk('1993/9/24', :高松市民会館, list_c, '讃岐うどん', "Sanuki's noodles"),
    ConcertHall.mk('1993/9/26', :徳島市立文化センター, list_c, '鳴門わかめ', "Naruto's {WAKAME}"),
    ConcertHall.mk('1993/9/27', :高知県民文化ホール, list_c, 'カツオのたたき', '{KATSUO-NO TATAKI}'),
    ConcertHall.mk('1993/10/2', :奈良県文化会館, list_c, '奈良漬け', '{NARAZUKE}', nil, nil, 'コンサートの夜 無し', %q(She didn't sing "{Concert-NO YORU}".)),
    ConcertHall.mk('1993/10/3', :名古屋センチュリーホール, list_c, '味噌煮込みうどん', '{MISONIKOMI}-Noodles'),
    ConcertHall.mk('1993/10/12', :京都会館, list_d, '湯豆腐', '{YUDOFU}', '第１ホール', 'First hall', '電源が落ちて30分間中断', 'The concert was interrupted by a power failure.'),
    ConcertHall.mk('1993/10/15', :山形市民会館, list_d, '????', '????'),
    ConcertHall.mk('1993/10/18', :山梨県立県民文化ホール, list_d, 'ほうとう', '{HO-TO-}'),
    ConcertHall.mk('1993/10/20', :千葉県文化会館, list_d, '落花生', 'Peanuts'),
    ConcertHall.mk('1993/10/24', :松本文化会館, list_d, '馬刺し', 'Slices of raw horse'),
    ConcertHall.mk('1993/10/27', :宇都宮市文化会館, list_d, 'しもつかれ', '{SHIMOTSUKARE}'),
    ConcertHall.mk('1993/11/7', :和歌山市民会館, list_d, '梅干し', '{UMEBOSHI}'),
    ConcertHall.mk('1993/11/8', :神戸国際会館, list_d, '神戸コロッケ', "Kobe's croquette"),
    ConcertHall.mk('1993/11/11', :宮崎市民会館, list_d, 'ひや汁', '{HIYA-SHIRU}'),
    ConcertHall.mk('1993/11/15', :那覇市民会館, list_d, 'ソーキそば', '{SOKI-SOBA}'),
    ConcertHall.mk('1993/11/24', :徳山市文化会館, list_d, '????', '????'),
    ConcertHall.mk('1993/11/26', :鳥取県民会館, list_d, '????', '????'),
    ConcertHall.mk('1993/11/29', :東京厚生年金会館, list_d, '馬刺し', 'Slices of raw horse', nil, nil, 'MCで「ロックンオムレツ」を(1コーラスだけ)歌う', %q(She sang "Rock'n Omelette" in MC.)),
    ConcertHall.mk('1993/11/30', :東京厚生年金会館, list_d, 'とんこつらーめん', '{TONKOTSU-RAMEN}', nil, nil, 'MCで「ロックンオムレツ」を(1コーラスだけ)歌う|ビデオ収録', %q(She sang "Rock'n Omelette" in MC.|Video Recording)),
  ]
end

