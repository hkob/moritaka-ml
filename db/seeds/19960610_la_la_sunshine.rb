key = :single_la_la_sunshine
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :la_la_sunshine do
    {
      device_type: :single,
      event_date_id: '1996/6/10',
      sort_order: 199606100,
      number: '29th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4280.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDA-29', '1st', false, :one_up_music, '1,000'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/la-la-sunshine-ep/id278096606', '1st', true, :warner_music_japan, '750']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:la_la_sunshine, %i[VOCAL iChisatoMoritaka STRINGSARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka APIANO iShinHashimoto GUITAR iYukioSeto BASS iYukioSeto]],
    [:mukashino_hitowa, %i[VOCAL iChisatoMoritaka STRINGSARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka KEYBOARDS iYuichiTakahashi EPIANO iShinHashimoto GUITAR iYukioSeto]],
    [:la_la_sunshine, %i[VOCAL iKaraoke STRINGSARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka APIANO iShinHashimoto GUITAR iYukioSeto BASS iYukioSeto], '(オリジナル・カラオケ)', '(Original karaoke)']
  ]
  device_activity_factory single, :thema_song_mezamashi_tv
end
