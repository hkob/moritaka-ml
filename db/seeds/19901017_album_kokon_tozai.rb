key = :album_kokon_tozai
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :kokon_tozai do
    {
      device_type: :album,
      event_date_id: '1990/10/17',
      minutes: 62,
      seconds: 58,
      sort_order: 199010170,
      number: '6th',
      singer_id: :iChisatoMoritaka,
      j_comment: '初回限定 32ページカラー写真集',
      e_comment: '32 page photo book(1st lot only)',
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4042.html'
    }
  end

  album.media_from_array [
    ["#{key}_ct", :ct, 'WPTL-181', :first, false, :warner_pioneer, '2,900'],
    ["#{key}_cd1", :cd, 'WPCL-181', :first, false, :warner_pioneer, '2,900'],
    ["#{key}_cd2", :cd, 'WPCL-689', :second, true, :warner_music_japan, '2,400'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/古今東西/id528973426', :first, true, :warner_music_japan, '2,100'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  performer = %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito]
  performerWC = performer + %i[CHORUS iManahoSaito]
  list.list_contents_from_array [
    [nil, [], nil, nil, 'プロローグ', 'Prologue'],
    [:onitaiji, performer],
    [:busters_blues, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito PROGRAMS iHideoSaito PIANO iChisatoMoritaka GUITAR iYukioSeto]],
    [nil, [], nil, nil, 'Interlude No.5', 'Interlude No.5'],
    [:aru_olno_seishun, performer],
    [:oye_como_va, %i[VOCAL iChisatoMoritaka ALLINST iHideoSaito PROGRAMS iHideoSaito CHORUS iHideoSaito CHORUS iChisatoMoritaka CHORUS iSeijiMatsuura]],
    [:ame, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito CHORUS iSeijiMatsuura], '(アルバム・ヴァージョン)', '(Album version)'],
    [:daibouken, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito CHORUS iHideoSaito CHORUS iSeijiMatsuura]],
    [:hong_kong, performer],
    [:hare, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi ALLINST iYuichiTakahashi PROGRAMS iYuichiTakahashi CHORUS iYuichiTakahashi CHORUS iSeijiMatsuura], 'モノラル', 'monaural'],
    [:misaki, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito CHORUS iHideoSaito]],
    [:funkey_monkey_baby, performer],
    [:tsukiyono_kokai, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi PROGRAMS iYuichiTakahashi GUITAR iYuichiTakahashi GUITAR iYukioSeto GUITAR iSeijiMatsuura CHORUS iYuichiTakahashi CHORUS iSeijiMatsuura]],
    [:tomodachi, performer],
    [:konomachi, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito CHORUS iHideoSaito CHORUS iSeijiMatsuura]],
    [:teriyaki_burger, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito CHORUS iHideoSaito CHORUS iSeijiMatsuura]],
    [nil, [], 'エピローグ', 'Epilogue'],
    [:uchini_kagitte, %i[VOCAL iChisatoMoritaka ARRANGE iCarnation GUITAR iMasataroNaoe GUITAR iOsamuToba BASS iYujiMada KEYBOARDS iYuichiTanaya DRUMS iHiroshiYabe]],
  ]

  device_activity_factory album, :cm_pioneer
  device_activity_factory album, :cm_ezaki_guriko
end
