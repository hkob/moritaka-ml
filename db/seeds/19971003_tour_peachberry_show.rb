key = :tour_peachberry_show
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  band = band_factory(:the_purple_haze)
  band.band_members_from_array %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PIANICA iChisatoMoritaka ACCORDION iChisatoMoritaka CLARINET iChisatoMoritaka RECORDER iChisatoMoritaka GUITAR iChisatoMoritaka KEYBOARDS iChisatoMoritaka DRUMS iToshihiroTsuchiya GUITAR iToruHasegawa CHORUS iToruHasegawa BASS iMasafumiYokoyama CHORUS iMasafumiYokoyama KEYBOARDS iShinKohno CHORUS iShinKohno KEYBOARDS iYasuakiMaejima CHORUS iYasuakiMaejima BANDLEADER iYasuakiMaejima]
  concert = concert_factory key do
    {
      j_subtitle: "Chisato Moritaka Concert Tour '97",
      e_subtitle: "Chisato Moritaka Concert Tour '97",
      concert_type: :tour,
      from_id: '1997/10/3',
      to_id: '1997/11/18',
      has_song_list: true,
      has_product: true,
      sort_order: 199710030,
      num_of_performances: 14,
      num_of_halls: 10,
      band_id: :the_purple_haze
    }
  end

  list_a, list_b, list_c, list_d, list_e = %i[A B C D E].map.with_index { |k, i| list_factory("#{key}_#{k}") { {concert_id: concert, keyword: k, sort_order: i+1} } }

  list_a.list_contents_from_array %i[hoshini_negaio la_la_sunshine] + @mc + %i[tony_slavin shintou_mekkyaku ginirono_yume] + @mc + %i[kareha futariwa_koibito watarasebashi rockn_roll_kencho_shozaichi] + @mc + %i[my_anniversary sutekina_tanjoubi] + ListContent.title_only('ビデオ', 'Video') + %i[lets_go] + @mc + %i[snow_again miracle_light] + @mc + ListContent.title_only('オリジナルインストゥルメンタル', 'Original Instrumental') + %i[ame] + ListContent.medley_in('シングルメドレー', 'Single Medley') + %i[new_season seventeen gin_gin_gin stress michi gin_gin_gin watashino_natsu haeotoko] + @medley_out + @mc + %i[anatawa_ninkimono kibun_soukai] + @encore + %i[sweet_candy] + @mc_member + %i[konomachi] + @double_encore + @mc + %i[watashiga_obasanni_nattemo]
  list_b.list_contents_from_array %i[hoshini_negaio la_la_sunshine] + @mc + %i[tony_slavin shintou_mekkyaku ginirono_yume] + @mc + %i[kareha futariwa_koibito watarasebashi rockn_roll_kencho_shozaichi] + @mc + %i[my_anniversary sutekina_tanjoubi] + ListContent.title_only('ビデオ', 'Video') + %i[lets_go] + @mc + %i[snow_again] + [[:miracle_light, [], 'ショートヴァージョン', 'short version']] + @mc + ListContent.title_only('オリジナルインストゥルメンタル', 'Original Instrumental') + %i[ame] + ListContent.medley_in('シングルメドレー', 'Single Medley') + %i[new_season seventeen gin_gin_gin stress michi gin_gin_gin watashino_natsu haeotoko] + @medley_out + @mc + %i[anatawa_ninkimono kibun_soukai] + @encore + %i[sweet_candy] + @mc_member + %i[konomachi] + @double_encore + @mc + %i[watashiga_obasanni_nattemo]
  list_c.list_contents_from_array %i[hoshini_negaio la_la_sunshine] + @mc + %i[tony_slavin shintou_mekkyaku ginirono_yume] + @mc + %i[kareha futariwa_koibito watarasebashi rockn_roll_kencho_shozaichi] + @mc + %i[my_anniversary sutekina_tanjoubi] + ListContent.title_only('ビデオ', 'Video') + %i[lets_go] + @mc + %i[snow_again miracle_light] + @mc + ListContent.title_only('オリジナルインストゥルメンタル', 'Original Instrumental') + %i[ame] + ListContent.medley_in('シングルメドレー', 'Single Medley') + %i[new_season seventeen gin_gin_gin stress michi gin_gin_gin watashino_natsu haeotoko] + @medley_out + @mc + %i[anatawa_ninkimono kibun_soukai] + @encore + %i[sweet_candy] + @mc_member + [[:futsuno_shiawase, [], 'ショートヴァージョン', 'short version']] + %i[konomachi] + @double_encore + @mc + %i[watashiga_obasanni_nattemo]
  list_d.list_contents_from_array %i[hoshini_negaio la_la_sunshine] + @mc + %i[tony_slavin shintou_mekkyaku ginirono_yume] + @mc + %i[les_champs_elysees futariwa_koibito watarasebashi rockn_roll_kencho_shozaichi] + @mc + %i[my_anniversary sutekina_tanjoubi] + ListContent.title_only('ビデオ', 'Video') + %i[lets_go] + @mc + %i[snow_again miracle_light] + @mc + ListContent.title_only('オリジナルインストゥルメンタル', 'Original Instrumental') + %i[ame] + ListContent.medley_in('シングルメドレー', 'Single Medley') + %i[new_season seventeen gin_gin_gin stress michi gin_gin_gin watashino_natsu haeotoko] + @medley_out + @mc + %i[anatawa_ninkimono kibun_soukai] + @encore + %i[sweet_candy] + @mc_member + [[:futsuno_shiawase, [], 'ショートヴァージョン', 'short version']] + %i[konomachi] + @double_encore + @mc + %i[watashiga_obasanni_nattemo]
  list_e.list_contents_from_array %i[hoshini_negaio la_la_sunshine] + @mc + %i[tony_slavin shintou_mekkyaku ginirono_yume] + @mc + %i[les_champs_elysees kareha] + ListContent.title_only('MC', 'MC', '夏の扉', "`Door for summer'") + %i[futariwa_koibito watarasebashi rockn_roll_kencho_shozaichi] + @mc + %i[my_anniversary sutekina_tanjoubi] + ListContent.title_only('ビデオ', 'Video') + %i[lets_go] + @mc + %i[snow_again miracle_light] + @mc + %i[clarinet_fantasy] + ListContent.title_only('オリジナルインストゥルメンタル', 'Original Instrumental') + %i[ame] + ListContent.medley_in('シングルメドレー', 'Single Medley') + %i[new_season seventeen gin_gin_gin stress michi gin_gin_gin watashino_natsu haeotoko] + @medley_out + @mc + %i[anatawa_ninkimono kibun_soukai] + @encore + %i[sweet_candy] + ListContent.title_only('MC', 'MC', '私の「普通の幸せ」ベスト 5', "My best 5 about `ordinary hapiness'") + [[:futsuno_shiawase, [], 'ショートヴァージョン', 'short version']] + @mc_member + %i[konomachi] + @double_encore + @mc + %i[watashiga_obasanni_nattemo]

  concert.concert_halls_from_array [
    ConcertHall.mk('1997/10/3', :よこすか芸術劇場, list_a, 'とんこつラーメン', '{TONKOTSU-RAMEN}'),
    ConcertHall.mk('1997/10/9', :名古屋センチュリーホール, list_a, '味噌煮込みうどん', '{MISONIKOMI}-Noodles', nil, nil, 'ミラクルライトにてフリスビーを飛ばす', 'She threw discuses in Miracle Light.'),
    ConcertHall.mk('1997/10/10', :名古屋センチュリーホール, list_b, '手羽先', '{TEBASAKI}'),
    ConcertHall.mk('1997/10/14', :渋谷公会堂, list_a, 'とんこつらーめん', '{TONKOTSU-RAMEN}'),
    ConcertHall.mk('1997/10/15', :渋谷公会堂, list_c, 'とんこつらーめん', '{TONKOTSU-RAMEN}'),
    ConcertHall.mk('1997/10/19', :アクトシティ浜松, list_a, 'うなぎ', 'Eels'),
    ConcertHall.mk('1997/10/23', :フェスティバルホール, list_b, 'お好み焼き', '{OKONOMI-YAKI}'),
    ConcertHall.mk('1997/10/24', :フェスティバルホール, list_b, 'お好み焼き', '{OKONOMI-YAKI}'),
    ConcertHall.mk('1997/10/28', :福岡サンパレス, list_b, 'からし明太子', '{KARASHI-MENTAIKO}'),
    ConcertHall.mk('1997/10/31', :石川厚生年金会館, list_b, '甘海老', '{AMAEBI}'),
    ConcertHall.mk('1997/11/5', :仙台サンプラザホール, list_b, '牛タン', 'Beef tongue'),
    ConcertHall.mk('1997/11/11', :北海道厚生年金会館, list_b, '札幌ラーメン', '{SAPPORO-RAMEN}'),
    ConcertHall.mk('1997/11/17', :中野サンプラザ, list_d, '馬刺し', 'Slices of raw horse'),
    ConcertHall.mk('1997/11/18', :中野サンプラザ, list_e, 'とんこつラーメン', '{TONKOTSU-RAMEN}'),
  ]
end
