key = :album_my_favorites
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :my_favorites do
    {
      device_type: :album,
      event_date_id: '2004/11/26',
      minutes: 74,
      seconds: 00,
      sort_order: 200411260,
      number: '19th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4154.html',
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'EPCE-5330', :first, false, :up_front_works, '3,059'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/my-favorites/id278097518', :first, false, :up_front_works, '2,000'],
  ]

  vc = %i[VOCAL iChisatoMoritaka]
  list = list_factory("#{key}_common") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    [[:docchimo_docchi, vc, '(ミセス森高バージョン2004)', '(Mrs Moritaka version 2004)']] +
    %i[ame konomachi obasan rockn_roll_kencho_shozaichi aoi_umi watarasebashi ichido_asobini_kiteyo kyoukara gin_gin_gin dekirudesho toga_tatsu lets_go tony_slavin snow_again nagasarete watashinoyouni].map { |k| [k, vc] }
  )
end

