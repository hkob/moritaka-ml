key = :single_ame
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :ame do
    {
      device_type: :single,
      event_date_id: '1990/9/10',
      sort_order: 199009100,
      number: '11th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4191.html'
    }
  end

  single.media_from_array [
    ["#{key}_cds1", :cds, 'WPDL-4170', '1st', false, :warner_pioneer, '900'],
    ["#{key}_cds2", :cds, 'WPDL-4170', '2nd', false, :warner_music_japan, '900'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/雨-single/id528971976', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [ :ame, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito CHORUS iSeijiMatsuura]],
    [ :cup_myudol, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito CHORUS iSeijiMatsuura]]
  ]
end



