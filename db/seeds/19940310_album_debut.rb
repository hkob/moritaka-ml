key = :album_debut
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :debut do
    {
      device_type: 'cover_album|lyric_album',
      event_date_id: '1994/3/10',
      minutes: 42,
      seconds: 28,
      sort_order: 199403100,
      number: '1st',
      singer_id: :iNorikoKato
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'WPCL-814', :first, false, :warner_music_japan, '2,900'],
  ]

  vn = %i[VOCAL iNorikoKato]
  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    [[:kondo_watashi_dokoka, %i[ARRANGE iHideoSaito VOCAL iNorikoKato]]] +
    ListContent.title_with_performer(vn, 'この道を歩いてく', '{KONO MICHI-O ARUITEKU}') +
    ListContent.title_with_performer(vn, 'HISTORY BOOK') +
    ListContent.title_with_performer(vn, 'Melody') +
    ListContent.title_with_performer(vn, 'いつの日か', '{ITSU-NO HI-KA}') +
    ListContent.title_with_performer(vn, 'ＤＯ−して', '{DO-SHITE}') +
    ListContent.title_with_performer(vn, 'ちょっと言わせて！', '{CHOTO IWASETE!}') +
    [[:hikisakanaide_futario, %i[VOCAL iNorikoKato ARRANGE iHideoSaito]]] +
    ListContent.title_with_performer(vn, '思いきり泣いていい', '{OMOIKIRI NAITE-II}') +
    ListContent.title_with_performer(vn, 'ねえ？　だから　おねがい・・・', 'NE? DAKARA ONEGAI...')
  )
end
