key = :video_cdv_overheat_night
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key, :overheat_night do
    {
      device_type: :cdv,
      event_date_id: '1988/11/28',
      minutes: 23,
      seconds: 10,
      sort_order: 198811280,
      number: '2nd',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4313.html'
    }
  end

  video.media_from_array [
    ["#{key}_cdv", :cdv, '24VL-4', :first, false, :warner_pioneer, '2,400 2,328'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/music-video/obahito-naito/id534975726', :first, true, :warner_music_japan, '400'],
  ]

  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }

  list.list_contents_from_array [
    [:new_season, @vc, '[SLバージョン]', '[SL version]'],
    [:yumeno_owari, @vc],
    [:period, @vc, '[LPバージョン]', '[LP version]'],
    [:weekend_blue, @vc],
    [:overheat_night_2, @vc, '(VIDEO Part)', '(VIDEO Part)']
  ]
end
