key = :video_live_rock_alive
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key, :live_rock_alive do
    {
      device_type: :live_video,
      event_date_id: '1993/2/25',
      minutes: 95,
      sort_order: 199302250,
      number: '5th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4333.html'
    }
  end

  video.media_from_array [
    ["#{key}_vhs1", :vhs, 'WPVL-8152', :first, false, :warner_music_japan, '5,000'],
    ["#{key}_ld1", :ld, 'WPLL-8152', :first, false, :warner_music_japan, '5,000'],
  ]

  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }
  list.list_contents_from_array((%i[concert_no_yoru rhythm_to_bass fight natsuno_umi watashiga_obasanni_nattemo yowaseteyo_konyadake the_blue_blues wakarimashita mitsuketa_saifu rock_alive kusaimononiwa_futaoshiro].map { |k| [k, @vc] }) + [[:ame, @vc, '[アルバム・ヴァージョン]', '[Album version]']] + (%i[seventeen yacchimaina get_smile amenochi_hare guitar].map { |k| [k, @vc] }) + [[:konomachi, @vc, '(HOME MIX)', '(HOME MIX)'], [:seishun, @vc]])

  concert = concert_factory :tour_rock_alive
  concert_hall = concert_hall_factory concert, 53
  concert_list = list_factory(:tour_rock_alive_C)
  concert_list_contents = concert_list.list_contents.order_sort_order

  csos = { 0 => 0, 1=> 1, 2 => 3, 3 => 4, 4 => 5, 5 => 7, 6 => 8, 7 => 10, 8 => 11, 9 => 13, 10 => 14, 11 => 16, 12 => 17, 13 => 18, 14 => 20, 15 => 21, 16 => 23, 17 => 24, 18 => 26 }
  list.list_contents.order_sort_order.each_with_index do |vsls, i|
    concert_video_factory concert_list_contents[csos[i]], vsls, concert_hall if csos[i]
  end
end
