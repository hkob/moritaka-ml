key = :album_best_selection
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :best_selection do
    {
      device_type: 'cover_album|lyric_album',
      event_date_id: '1995/5/25',
      minutes: 54,
      seconds: 31,
      sort_order: 199505250,
      number: '6th',
      singer_id: :iSanaeJonouchi
    }
  end

  album.media_from_array [
    ["#{key}_cd", :cd, 'PSCF-5002', :first, false, :y_j_sounds, '3,000'],
  ]

  vs = %i[VOCAL iSanaeJonouchi]
  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    [
      [:shiawaseni_narimasu, %i[VOCAL iSanaeJonouchi ARRANGE iHideoSaito]],
      [:yowaseteyo_konyadake, %i[VOCAL iSanaeJonouchi ARRANGE iHideoSaito]],
    ] +
    ListContent.title_with_performer(vs, 'あじさい橋', '{AJISAI-BASHI}') +
    ListContent.title_with_performer(vs, '泣くだけ泣いたら', '{NAKU-DAKE NAITARA}') +
    ListContent.title_with_performer(vs, '氷河期', '{HYOGAKI}') +
    ListContent.title_with_performer(vs, 'くちびる', '{KUCHIBIRU}') +
    [[:watarasebashi, %i[VOCAL iSanaeJonouchi ARRANGE iHideoSaito]]] +
    ListContent.title_with_performer(vs, '止められない悲しみ', '{KOMERARENAI KANASHIMI}') +
    ListContent.title_with_performer(vs, '夢までTAXI', '{YUME-MADE Taxi}') +
    ListContent.title_with_performer(vs, 'ロマンティックに乾杯', '{Romantic NI KANPAI}') +
    ListContent.title_with_performer(vs, '音無橋', '{OTONASHI-BASHI}') +
    ListContent.title_with_performer(vs, '秋止符', '{SHUSHIFU}')
  )
end
