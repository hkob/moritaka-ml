key = :picture_book_watashiga_obasanni_nattemo
obj = Book.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  book = book_factory key, :watashiga_obasanni_nattemo do
    {
      book_type: :picture_book,
      title_id: :watashiga_obasanni_nattemo,
      publisher_id: :roman_shinsha,
      seller_id: :ss_communications,
      photographer_id: :iHaruKimura,
      isbn: '4-8275-1339-2 C0072 P2300E',
      price: '2,300',
      event_date_id: '1992/5/14',
      sort_order: 199205140,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4466.html'
    }
  end
end


