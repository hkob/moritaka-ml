key = :score_yasashiku_hikeru_piano_solo_album2
obj = Book.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  book = book_factory key do
    {
      book_type: :score_book,
      publisher_id: :kmp,
      isbn: '4-7732-0934-8 C0073 P1800E',
      price: '1,890',
      event_date_id: '1997/9/15',
      sort_order: 199709150,
    }
  end

  list = list_factory("#{key}_A") { {book_id: book, keyword: :A, sort_order: 1} }
  list.list_contents_from_array %i[new_season seventeen michi ame seishun kusaimononiwa_futaoshiro konomachi fight watashiga_obasanni_nattemo concert_no_yoru watarasebashi watashino_natsu haeotoko memories rockn_omelette watashino_daijina_hito kibun_soukai kazeni_fukarete hoshino_ojisama futariwa_koibito zuruyasumi natsuno_hi sutekina_tanjoubi yasumino_gogo mirai ginirono_yume taiyouto_aoi_tsuki lets_go botto_shitemiyou la_la_sunshine sweet_candy so_blue]
end
