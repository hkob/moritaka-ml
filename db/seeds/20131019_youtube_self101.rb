yt = :sc101

yt_hash = {
  '2013/10/19' => {
    link: '07OWQZHSo8E',
    number: '101',
    song_id: :cant_say_good_bye,
    comment: "作詞：伊秩弘将　作曲：斉藤英夫　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」101曲目は、\n1988年発表のアルバム「ミーハー」から『CAN'T SAY GOOD-BYE』！\nドラムは森高千里による新録音！\n\n衣装協力：3rd by VANQUISH／VICKY"
  },
  '2013/10/25' => {
    link: 'GS4HdGb_Ez0',
    number: '102',
    song_id: :ways,
    comment: "作詞：福永ひろみ　作曲：津垣博通　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」102曲目は、\n1987年発表のアルバム「NEW SEASON」から『WAYS』！\nドラムは森高千里による新録音！\n\n衣装協力：Smork／Immanoel"
  },
  '2013/11/01' => {
    link: 'xGuQg9YgQYk',
    number: '103',
    song_id: :konomachi,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」103曲目は\n1990年発表のアルバム「古今東西」から『この街』\n1991年には「勉強の歌」との両A面シングルとしてリカットされました\nドラムは森高千里による新録音のループドラム！\n\n衣装協力：BLACK BY MOUSSY"
  },
  '2013/11/13' => {
    link: 'lxBAAwEPrT4',
    number: '104',
    song_id: :forty_seven_hard_nights,
    comment: "作詞：伊秩弘将　作曲：斉藤英夫　編曲：dessert,desert\n\n公式チャンネル独占企画「200曲セルフカヴァー」104曲目は、\n1988年発表のアルバム「ミーハー」から『47 HARD NIGHTS』！\nドラムは森高千里による新録音！\n\n衣装協力：TWNROOM"
  },
  '2013/11/16' => {
    link: 'A-BB6Gil-lk',
    number: '105',
    song_id: :namida_good_bye,
    comment: "作詞：菅野真吾　作曲：山本拓巳　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」105曲目は、\n1987年発表のアルバム「NEW SEASON」から『涙 Good-bye』！\nドラムは森高千里による新録音！\n\n衣装協力：GROWZE"
  },
  '2013/11/23' => {
    link: '4l6GoqtmVgQ',
    number: '106',
    song_id: :mahiruno_hoshi,
    comment: "作詞：森高千里　作曲：スガシカオ　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」106曲目は、\n1999年発表のシングル『まひるの星』！\nドラムは森高千里によるループドラム！"
  },
  '2013/12/1' => {
    link: '1Lsk46RtttM',
    number: '107',
    song_id: :michi,
    comment: "作詞：森高千里　作曲：安田信二\n\n公式チャンネル独占企画「200曲セルフカヴァー」107曲目は、\n1990年発表の両A面シングル「道/青春」から『道』！\nドラムは森高千里による新録音！"
  },
  '2013/12/7' => {
    link: '9wu03WwWPA4',
    number: '108',
    song_id: :kiss_the_night,
    comment: "作詞：伊秩弘将　作曲：山本拓巳　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」108曲目は、\n1988年発表のアルバム「ミーハー」から『KISS THE NIGHT』！\nドラムは森高千里による新録音！\n\n衣装協力：GROWZE"
  },
  '2013/12/15' => {
    link: 'oI01XscPOoA',
    number: '109',
    song_id: :jin_jin_jinglebell,
    j_title: "森高千里 『ジン ジン ジングルベル2013』 【セルフカヴァー】",
    e_title: "Chisato Moritaka 『JIN JIN JINGLEBELL 2013』 【Self cover】",
    comment: "作詞・作曲：森高千里　編曲：S&T\n\n公式チャンネル独占企画「200曲セルフカヴァー」109曲目は、\n1995年発表のシングル「ジン ジン ジングルベル」！\nドラムは森高千里によるループドラム"
  },
  '2013/12/22' => {
    link: '1Iwf4Rjvp1I',
    number: '110',
    song_id: :wakasugita_koi,
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」110曲目は、\n1989年発表のアルバム「非実力派宣言」から『若すぎた恋』！\nドラムは森高千里による新録音です！\n\n衣装協力：TITE IN THE STORE／GROWZE"
  },
  '2013/12/29' => {
    link: 'EHV8ON6foXA',
    number: '111',
    song_id: :miss_lady,
    comment: "作詞・作曲：伊秩弘将　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」111曲目は、\n1987年発表のアルバム「NEW SEASON」から『Miss Lady』！\nドラムは森高千里による新録音です！\n\n衣装協力：GROWZE／rienda"
  },
  '2013/12/31' => {
    link: 'gdZTCyFH1cA',
    number: '112',
    song_id: :ichigatsu_ichijitsu,
    j_title: "森高千里 『一月一日2014』 【セルフカヴァー】",
    e_title: "Chisato Moritaka 『{ICHIGATSU ICHIJITSU} 2014』 【Self cover】",
    comment: "作詞：千家尊福　作曲：上真行　編曲：S&T\n\n公式チャンネル独占企画「200曲セルフカヴァー」112曲目は、\n1995年発表のシングル「ジンジン ジングルベル」のC/W「一月一日」！\nドラムは森高千里による新録音です！\n\n衣装協力：goa"
  },
  '2014/1/4' => {
    link: 'ia2nyHG1UcA',
    number: '113',
    song_id: :kyoukara,
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」113曲目は、\n1995年発表のベストアルバム「DO THE BEST」から「今日から」！\nこのベストアルバム用に書き下ろした曲です。\nドラムは森高千里による新録音です！\n\n衣装協力：BEAMS"
  },
  '2014/1/13' => {
    link: '8VX5a5jtDJc',
    number: '114',
    song_id: :rhythm_to_bass,
    comment: "作詞：森高千里　作曲：松尾弘良　編曲：dessert,desert\n\n公式チャンネル独占企画「200曲セルフカヴァー」114曲目は、\n1992年発表のアルバム「ROCK ALIVE」から「RHYTHMとBASS」！\nドラムは森高千里によるループドラムです！"
  },
  '2014/1/19' => {
    link: '90ACLap5358',
    number: '115',
    song_id: :taifu,
    comment: "作詞：森高千里　作曲：伊秩弘将\n\n公式チャンネル独占企画「200曲セルフカヴァー」115曲目は、\n1994年発表のアルバム「STEP BY STEP」から『台風』！\nドラムは森高千里による新録音です！\n\n衣装協力：CHIC MUSE"
  },
  '2014/1/25' => {
    link: 'IoNTfKNStTw',
    number: '116',
    song_id: :mi_ha_,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」116曲目は、\n1988年発表のアルバム「ミーハー」からタイトル曲『ミーハー』！後に「ザ・ミーハー」としてシングルカット。\nドラムは森高千里による新録音です！\n\n衣装協力：Million Carats"
  },
  '2014/2/1' => {
    link: 'E6xZoj_4oYM',
    number: '117',
    song_id: :onitaiji,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」117曲目は、\n1990年発表のアルバム「古今東西」から『鬼たいじ』！\nドラムは森高千里による新録音です！\n\n衣装協力：Discoat"
  },
  '2014/2/7' => {
    link: 'hI-24LqUZVY',
    number: '118',
    song_id: :ringoshuno_rule,
    comment: "作詞：高柳恋　作曲：島健　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」118曲目は、\n1987年発表のデビューアルバム「NEW SEASON」から『林檎酒のルール』！\nドラムは森高千里による新録音です！\n\n撮影協力：ジャリーヴ！\n衣装協力：Million Carats"
  },
  '2014/2/14' => {
    link: 'IctKslfT0gg',
    number: '119',
    song_id: :mite,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」119曲目は、\n1988年発表のアルバム「見て」からタイトル曲『見て』！\nもちろんボーカルとドラムは今回のカヴァー用に新録音しました！\n\n衣装協力：BOUNA GIORNATA／GROWZE"
  },
  '2014/2/25' => {
    link: 'SnS_wUm88J0',
    number: '120',
    song_id: :mirai,
    comment: "作詞・作曲：森高千里　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」120曲目は、\n1997年発表のシングル「SWEET CANDY」のカップリング曲『未来』！\n1999年に行われた『第54回くまもと未来国体』のテーマソングです。\nドラムは森高千里による新録音です！\n\n衣装協力：GROWZE"
  },
}
create_youtube_factory_from_hash(yt, yt_hash)
