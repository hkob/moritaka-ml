key = :single_watashino_natsu
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :watashino_natsu do
    {
      device_type: :single,
      event_date_id: '1993/4/10',
      sort_order: 199304100,
      number: '18th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4210.html'
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'WPDL-4343', '1st', false, :warner_music_japan, '900'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/sino-xia-single/id531608790', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:watashino_natsu, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka PIANO iChisatoMoritaka GUITAR iHideoSaito BASS iHideoSaito SYNTHESIZER iHideoSaito]],
    [:rockn_roll_kencho_shozaichi, %i[VOCAL iChisatoMoritaka ALLINST iChisatoMoritaka CHORUS iChisatoMoritaka], '(シングル・ヴァージョン)', '(Single version)'],
  ]
  device_activity_factory single, :cm_ana
end

