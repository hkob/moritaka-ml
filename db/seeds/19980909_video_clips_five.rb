key = :video_clips_five
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key, :five do
    {
      device_type: :video_clips,
      event_date_id: '1998/9/9',
      minutes: 60,
      sort_order: 199809090,
      number: '6th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4349.html'
    }
  end

  video.media_from_array [
    ["#{key}_vhs1", :vhs, 'EPVE-5001', :first, false, :zetima, '4,893' ],
    ["#{key}_ld1", :ld, 'EPLE-5001', :first, false, :zetima, '4,893' ],
  ]

  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }
  list.list_contents_from_array %i[futariwa_koibito yasumino_gogo so_blue la_la_sunshine ginirono_yume lets_go sweet_candy miracle_light snow_again telephone tokyo_rush umimade_5fun] + [[:futariwa_koibito, [], '[Color Version]', '[Color Version]']]
end



