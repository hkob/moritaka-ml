key = :thema_song_mezamashi_tv
obj = Activity.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity = activity_factory key do
    {
      activity_type: :thema_song,
      title_id: %q(めざましテレビ|{MEZAMASHI} TV|めざましてれび),
      j_comment: 'テーマソング',
      e_comment: 'Thema Song',
      company_id: :fuji_tv,
      from_id: '1996/4/1',
      to_id: '1997/3/28',
      song_id: :la_la_sunshine,
      sort_order: 199604010
    }
  end
end

