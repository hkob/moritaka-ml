key = :album_peachberry
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :peachberry do
    {
      device_type: :album,
      event_date_id: '1996/7/15',
      minutes: 54,
      seconds: 5,
      sort_order: 199607150,
      number: '14th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4137.html',
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'EPCA-7010', :first, false, :one_up_music, '3,000'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/peachberry/id278800108', :first, true, :up_front_works, '2,000'],
  ]

  list_common = list_factory("#{key}_common") { {device_id: album, keyword: :common, sort_order: 1} }
  performer = %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito]
  list_common.list_contents_from_array [
    [:sweet_candy, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka KEYBOARDS iYuichiTakahashi AGUITAR iYuichiTakahashi PIANO iShinHashimoto FRHODES iShinHashimoto BASS iYukioSeto EGUITAR iYukioSeto CONGA iYukioSeto SHAKER iYukioSeto JINGLE iYukioSeto], '(ALBUM VERSION)', '(ALBUM VERSION)'],
    [:my_anniversary, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka TAMBOURINE iChisatoMoritaka SHAKER iChisatoMoritaka KEYBOARDS iYuichiTakahashi EGUITAR iYuichiTakahashi AGUITAR iYuichiTakahashi PIANO iShinHashimoto SYNTHESIZER iShinHashimoto BASS iYukioSeto EGUITARSOLO iYukioSeto BGVOCAL iNobutakaOkubo BGVOCAL iDaisukeIto BGVOCAL iChihiroImai]],
    [:osharefu, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka KEYBOARDS iYuichiTakahashi PIANO iShinHashimoto FRHODES iShinHashimoto GUITAR iYukioSeto]],
    [:shintou_mekkyaku, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka GUITAR iYuichiTakahashi WURLITZER iShinHashimoto HANDBELL iShuheiMiyachi BASS iYukioSeto GUITARSOLO iYukioSeto]],
    [:lets_go, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka BGVOCAL iChisatoMoritaka KEYBOARDS iYuichiTakahashi AGUITAR iYuichiTakahashi PIANO iShinHashimoto FRHODES iShinHashimoto SYNTHESIZER iShinHashimoto BASS iYukioSeto EGUITAR iYukioSeto BARCHIMES iYukioSeto PERCUSSION iYukioSeto]],
    [:anatawa_ninkimono, %i[VOCAL iChisatoMoritaka ARRANGE iShinKohno DRUMS iChisatoMoritaka TIMBALES iChisatoMoritaka KEYBOARDS iShinKohno PIANO iShinKohno FRHODES iShinKohno PERCUSSION iShinKohno BASS iMasafumiYokoyama GUITAR iYukioSeto SHAKER iYukioSeto TRUMPET iToshioAraki TRUMPET iKojiNishihara ALTOSAX iMasakuniTakeno TENORSAX iTakuoYamamoto FLUTESOLO iTakuoYamamoto TROMBONE iMasanoriHirohara BTROMBONE iJunkoYamashiro]],
    [:futsuno_shiawase, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka RECORDER iChisatoMoritaka ACCORDION iChisatoMoritaka KEYBOARDS iYuichiTakahashi AGUITAR iYuichiTakahashi PIANO iShinHashimoto GUITAR iShinHashimoto BASS iYukioSeto GUITAR iYukioSeto]],
    [:mitatoriyo_watashi, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PIANO iChisatoMoritaka KALIMBA iChisatoMoritaka KEYBOARDS iYuichiTakahashi FRHODES iShinHashimoto SYNTHESIZER iShinHashimoto KALIMBA iShinHashimoto BASS iYukioSeto DJEMBE iYukioSeto DIDJERIDOO iYukioSeto]],
    [:hoshini_negaio, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka KEYBOARDS iYuichiTakahashi GUITAR iYuichiTakahashi PIANO iShinHashimoto SYNTHESIZER iShinHashimoto EGUITAR iYukioSeto DJEMBE iYukioSeto TIMBALES iYukioSeto BARCHIMES iYukioSeto]],
    [:kataomoi, %i[VOCAL iChisatoMoritaka ARRANGE iShinKohno DRUMS iChisatoMoritaka TAMBOURINE iChisatoMoritaka KEYBOARDS iShinKohno PIANO iShinKohno FRHODES iShinKohno AGUITAR iYuichiTakahashi BASS iMasafumiYokoyama EGUITAR iYukioSeto BARCHIMES iYukioSeto TENORSAX iTakuoYamamoto]],
    [:tony_slavin, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PIANICA iChisatoMoritaka KEYBOARDS iYuichiTakahashi EGUITAR iYuichiTakahashi AGUITAR iYuichiTakahashi PIANO iShinHashimoto EGUITARSOLO iShinHashimoto BASS iYukioSeto DJEMBE iYukioSeto]],
    [:clarinet_fantasy, %i[DRUMLOOPS iChisatoMoritaka CLARINET iChisatoMoritaka KEYBOARDS iYuichiTakahashi PERCUSSION iYuichiTakahashi FRHODES iShinHashimoto SYNTHESIZER iShinHashimoto PERCUSSION iShinHashimoto PERCUSSION iShuheiMiyachi GUITAR iYukioSeto PERCUSSION iYukioSeto], '(INSTRUMENTAL)', '(INSTRUMENTAL)'],
    [:ginirono_yume, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PERCUSSION iChisatoMoritaka BGVOCAL iChisatoMoritaka KEYBOARDS iYuichiTakahashi GUITAR iYuichiTakahashi PIANO iShinHashimoto TAISHOGOTO iShinHashimoto BASS iYukioSeto GUITARSOLO iYukioSeto PERCUSSION iYukioSeto]],
  ]
  device_activity_factory album, :cm_nihon_seimei
end

