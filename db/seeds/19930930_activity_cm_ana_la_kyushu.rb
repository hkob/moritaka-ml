key = :cm_ana_la_kyushu
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_ana,
      j_title: "全日空(ANA) 「ANA'S ラ・九州」",
      e_title: %q{All Nihon Airline(ANA) "ANA'S La. Kyushu"},
      from_id: '1993/9/30',
      to_id: '1994/3/31',
      j_comment: '屋台ラーメン + 手招き版 [15 秒]|卓球 + 振り返り版 [15 秒]|フル + 振り返り版 [30 秒]',
      e_comment: '{YATAI RAMEN} + {TEMANEKI} version [15 seconds]|Table Tennis + {FURIKAERI} version [15 seconds]|Full + {FURIKAERI} version [30 seconds]',
      sort_order: 199309300,
      song_id: :kazeni_fukarete
    }
  end
end
