key = :picture_book_opera
obj = Book.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  book = book_factory key, :opera do
    {
      book_type: :picture_book,
      title_id: :opera,
      publisher_id: :sony_magazines,
      photographer_id: :iNoriakiOsawa,
      isbn: '4-7897-0469-6 C0072 P1600E',
      price: '1,600',
      event_date_id: '1989/10/20',
      sort_order: 198910200,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4459.html'
    }
  end

  list = list_factory("#{key}_A") { {book_id: book, keyword: :A, sort_order: 1} }
  list.list_contents_from_array [
    [nil, [], nil, nil, 'ACT I. Sea Side Story', 'ACT I. Sea Side Story'],
    [nil, [], nil, nil, 'INTER-ACT INTERVIEW Part I', 'INTER-ACT INTERVIEW Part I'],
    [nil, [], nil, nil, 'ACT II. Midnight Call', 'ACT II. Midnight Call'],
    [nil, [], nil, nil, 'INTER-ACT INTERVIEW Part II', 'INTER-ACT INTERVIEW Part II'],
    [nil, [], nil, nil, 'ACT III. A Lady In Blue', 'ACT III. A Lady In Blue'],
    [nil, [], nil, nil, 'INTER-ACT INTERVIEW Part III', 'INTER-ACT INTERVIEW Part III'],
    [nil, [], nil, nil, 'Curtain Call', 'Curtain Call'],
    [nil, [], nil, nil, 'Finale', 'Finale']
  ]
end
