key = :album_kotoshino_natsuwa_more_better
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :kotoshino_natsuwa_more_better do
    {
      device_type: :album,
      event_date_id: '1998/5/21',
      minutes: 35,
      seconds: 0,
      sort_order: 199805210,
      number: '15th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4143.html',
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'EPCE-5001', :first, false, :zetima, '3,059'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/kotoshi-no-natsu-wa-more-better/id278608820', :first, true, :up_front_works, '1,800'],
  ]

  list_common = list_factory("#{key}_common") { {device_id: album, keyword: :common, sort_order: 1} }
  list_common.list_contents_from_array [
    [:tokyo_rush, %i[ARRANGE iHarryHosono VOCAL iChisatoMoritaka VOCAL iHarryHosono ADDDRUMS iChisatoMoritaka INSTRUMENTS iHarryHosono]],
    [:natsuno_umi, %i[ARRANGE iHarryHosono VOCAL iChisatoMoritaka CHORUS iMiharuKoshi INSTRUMENTS iHarryHosono]],
    [:hey_dog, %i[ARRANGE iHarryHosono VOCAL iChisatoMoritaka HOWDOG iHarryHosono INSTRUMENTS iHarryHosono]],
    [:a_bientot, %i[ARRANGE iMiharuKoshi VOCAL iChisatoMoritaka INSTRUMENTS iHarryHosono INSTRUMENTS iMiharuKoshi]],
    [:hooraibo, %i[ARRANGE iHarryHosono VOCAL iChisatoMoritaka VOCAL iHarryHosono INSTRUMENTS iHarryHosono]],
    [:beach_party, %i[ARRANGE iHarryHosono VOICE iChisatoMoritaka VOICE iHarryHosono INSTRUMENTS iHarryHosono]],
    [:calypsono_musume, %i[ARRANGE iHarryHosono VOCAL iChisatoMoritaka STEELDRUMS iChisatoMoritaka INSTRUMENTS iHarryHosono]],
    [:miracle_light, %i[ARRANGE iHarryHosono VOCAL iChisatoMoritaka GUITAR iHarryHosono BASS iHarryHosono INSTRUMENTS iHarryHosono], 'Twist version', 'Twist version'],
    [:miracle_woman, %i[ARRANGE iHarryHosono VOCAL iChisatoMoritaka ADDDRUMS iChisatoMoritaka INSTRUMENTS iHarryHosono], 'Vinyl version', 'Vinyl version']
  ]
end


