key = :poems_wakariyasui_koi
obj = Book.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  book = book_factory key, :wakariyasui_koi do
    {
      book_type: :poems,
      publisher_id: :kadokawa_shoten,
      author_id: :iNatsuoGiniro,
      photographer_id: :iNatsuoGiniro,
      isbn: '4-0416-7302-X',
      price: '596',
      event_date_id: '1987/12/18',
      sort_order: 198712180,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4475.html',
      j_comment: '写真のモデル',
      e_comment: 'Photographic model'
    }
  end
end
