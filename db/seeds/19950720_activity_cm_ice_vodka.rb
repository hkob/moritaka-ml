key = :cm_suntory_ice_vodka
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_suntory,
      j_title: 'サントリー 「アイスウォッカ」',
      e_title: 'Suntory "ICE VODKA"',
      from_id: '1995/7/20',
      to_id: '1995/11/21',
      sort_order: 199507200,
      song_id: :hey_vodka
    }
  end
end

