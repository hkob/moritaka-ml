key = :video_clips_rock_alive
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key, :rock_alive do
    {
      device_type: :video_clips,
      event_date_id: '1992/6/10',
      minutes: 30,
      sort_order: 199206100,
      number: '4th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4331.html'
    }
  end

  video.media_from_array [
    ["#{key}_vhs1", :vhs, 'WPVL-8138', :first, false, :warner_music_japan, '3,800' ],
    ["#{key}_ld1", :ld, 'WPLL-8138', :first, false, :warner_music_japan, '3,800' ],
  ]

  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:rock_alive, %i[VOCAL iChisatoMoritaka]],
    [:benkyono_uta, %i[VOCAL iChisatoMoritaka]],
    [:benkyono_uta, %i[VOCAL iChisatoMoritaka], '-LIVE-', '-LIVE-'],
    [:the_blue_blues, %i[VOCAL iChisatoMoritaka]],
    [:mitsuketa_saifu, %i[VOCAL iChisatoMoritaka]],
    [:hachigatsuno_koi, %i[VOCAL iChisatoMoritaka]],
    [:natsuno_umi, %i[VOCAL iChisatoMoritaka], 'メイキング / NG集', 'Making / NG collection'],
  ]
  concert = concert_factory :tour_the_moritaka
  concert_hall = concert_hall_factory concert, 10
  concert_list = list_factory :tour_the_moritaka_B
  concert_list_contents = concert_list.list_contents.order_sort_order

  concert_video_factory concert_list_contents[2], list.list_contents.order_sort_order[2], concert_hall
end

