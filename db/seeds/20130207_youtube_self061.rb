yt = :sc061

yt_hash = {
  '2013/2/7' => {
    link: '_LDqWlRIavI',
    number: '61',
    song_id: :anatawa_ninkimono,
    comment: "作詞：森高千里　作曲：河野伸　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」61曲目は、1997年発表のアルバム「PEACHBERRY」から「あなたは人気者」！\n\n衣装協力：アーカー"
  },
  '2013/2/12' => {
    link: 'KZdYBBnJHCo',
    number: '62',
    song_id: :kondo_watashi_dokoka,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」62曲目は、1989年発表のアルバム「非実力派宣言」から「今度私どこか連れていって下さいよ」！"
  },
  '2013/2/22' => {
    link: 'uz5xleKo0h4',
    number: '63',
    song_id: :kataomoi,
    comment: "作詞：森高千里　作曲：河野伸\n\n公式チャンネル独占企画「200曲セルフカヴァー」63曲目は、\n1997年発表のアルバム「PEACHBERRY」から「片思い」！\n気鋭のジャズ・ギタリスト・太田雄二さんをフィーチャー！\n\n衣装協力：スタイルミー"
  },
  '2013/3/8' => {
    link: '88VsnEYWjxQ',
    number: '64',
    song_id: :mukashino_hitowa,
    comment: "作詞・作曲：森高千里\n\n公式チャンネル独占企画「200曲セルフカヴァー」64曲目は、\n1996年発表のシングル「ララ サンシャイン」のC/W曲「むかしの人は・・・」！\n気鋭のジャズ・ギタリスト・太田雄二さんをフィーチャー！\n\n衣装協力：MET"
  },
  '2013/4/6' => {
    link: 'OMN9Wq2psRo',
    number: '65',
    song_id: :yacchimaina,
    comment: "作詞：森高千里　作曲：前嶋康明　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」65曲目は、\n1992年発表のアルバム「ROCK ALIVE」から「やっちまいな」！\nドラムは森高千里によるループドラム！\n\n衣装協力：BUONA GIORNATA／Danny＆Anne"
  },
  '2013/4/13' => {
    link: 'frnn2h73tW4',
    number: '66',
    song_id: :otokonara,
    comment: "作詞：森高千里　作曲：斉藤英夫　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」66曲目は、\n1994年発表のアルバム「STEP BY STEP」から「男なら」！\nドラムは森高千里による新録音！"
  },
  '2013/4/20' => {
    link: '84THqnXPTgo',
    number: '67',
    song_id: :aru_olno_seishun,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」67曲目は、\n1990年発表のアルバム「古今東西」から「あるOLの青春～A子の場合～」！\nドラムは森高千里によるループドラム！"
  },
  '2013/4/27' => {
    link: 'TcdxKsglqkc',
    number: '68',
    song_id: :ichido_asobini_kiteyo,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」68曲目は、\n1994年発表のアルバム「STEP BY STEP」から「一度遊びに来てよ」！\n1999年にシングルとしてもリカットしています。\nドラムは森高千里による新録音！\n\n衣装協力：「HUMAN WOMAN」「BUONA GIORNATA」"
  },
  '2013/5/1' => {
    link: '8dcLPTyvh_M',
    number: '69',
    song_id: :amenochi_hare,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」69曲目は、\n1992年発表のアルバム「ROCK ALIVE」から「雨のち晴れ」！\nドラムは森高千里による新録音！\n\n衣装協力：ルージュ・ヴィフ／ティテインザストア"
  },
  '2013/5/6' => {
    link: 'JLJ5dK0u8OA',
    number: '70',
    song_id: :daite,
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」70曲目は、\n1989年発表のアルバム「非実力派宣言」から「だいて」！\n同年、シングルとしても発売しています。\nドラムは森高千里による新録音！\n\n衣装協力：BEATRICE"
  },
  '2013/5/15' => {
    link: '40tP352yXYQ',
    number: '71',
    song_id: :guitar,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」71曲目は、\n1992年発表のアルバム「ROCK ALIVE」から「ギター」！\nドラムは森高千里による新録音！\n\n衣装協力：ドゥヴィネット"
  },
  '2013/5/20' => {
    link: 'fee8qgfUnbk',
    number: '72',
    song_id: :rock_alive,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」72曲目は、\n1992年発表のアルバム「ROCK ALIVE」からタイトルチューン「ROCK ALIVE」！\nドラムは森高千里による新録音！\n\n衣装協力：Jet Lag Drive"
  },
  '2013/5/22' => {
    link: 'K8dxkkNBFyQ',
    number: '73',
    song_id: :my_anniversary,
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」73曲目は、\n1997年発表のアルバム「PEACHBERRY」から「マイ・アニバーサリー」！\nドラムは森高千里による新録音！\n\n衣装協力：Jet Lag Drive"
  },
  '2013/5/24' => {
    link: 'h29n27O1RzQ',
    number: '74',
    song_id: :watashino_daijina_hito,
    comment: "作詞・作曲：森高千里　編曲：前嶋康明\n\n本日は5月25日、森高千里デビュー日です！\nデビュー26周年に突入しました！\n\nそんな日を記念して・・・\n公式チャンネル独占企画「200曲セルフカヴァー」74曲目は、\n1994年発表のアルバム「STEP BY STEP」から「私の大事な人」！\n同年、シングル「素敵な誕生日」の両A面としてリカットしました。\n\nモニターの中の映像はDVD「気分爽快」のMAKING映像を加工し合成しています。\n\n撮影場所：シダックス\n衣装協力：HALLYWOOD RANCH MARKET／アガット"
  },
  '2013/5/31' => {
    link: 'iT9b0lyVZSc',
    number: '75',
    song_id: :tsumetai_tsuki,
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」75曲目は、\n1998年発表のシングル「冷たい月」！\nドラムは森高千里による新録音！\n\n衣装協力：BLACK BY MOUSSY (バロックジャパンリミテット)／NOLLEY'S"
  },
  '2013/6/8' => {
    link: '6SZFreFMEDI',
    number: '76',
    song_id: :mitsuketa_saifu,
    comment: "作詞：森高千里　作曲：斉藤英夫\nremix：dessert,desert\n\n公式チャンネル独占企画「200曲セルフカヴァー」76曲目は、\n1992年発表のアルバム「ROCK ALIVE」から「見つけたサイフ」！\nドラムは森高千里によるループドラム！\n\n衣装協力：FREDY&GLOSTER／Moussy (バロックジャパンリミテット)"
  },
  '2013/6/15' => {
    link: 'nrXKXgQoDjA',
    number: '77',
    song_id: :shintou_mekkyaku,
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」77曲目は、\n1997年発表のアルバム「PEACHBERRY」から「心頭滅却すれば火もまた涼し」！\nドラムは森高千里による新録音！\n\n撮影協力：恵林寺（山梨県甲州市）"
  },
  '2013/6/19' => {
    link: 'lK-JbZ7qPhU',
    number: '78',
    song_id: :memories,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」78曲目は、\n1993年発表のアルバム「LUCKY 7」から「Memories」！\n後に「ハエ男」との両A面でシングルカットされました。\nドラムは森高千里による新録音！\n\n衣装協力：Westwood Outfitters(カイタックインターナショナル)"
  },
  '2013/6/22' => {
    link: '3PYsb0vgSb0',
    number: '79',
    song_id: :futsuno_shiawase,
    comment: "作詞：森高千里　作曲：今井千尋　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」79曲目は、\n1997年発表のアルバム「PEACHBERRY」から「普通の幸せ」！\nドラムは森高千里による新録音！\n\n衣装協力：DR DENIM(EIKOプレスルーム)"
  },
  '2013/6/28' => {
    link: 'Xa6GMs9j49A',
    number: '80',
    song_id: :ame,
    comment: "作詞：森高千里　作曲：松浦誠二\n\n公式チャンネル独占企画「200曲セルフカヴァー」80曲目は、\n1990年発表のシングル「雨」！\nドラムは森高千里による新録音！\n\n衣装協力：UNDER BAR RAW (バロックジャパンリミテット)\nmanics"
  },
}
create_youtube_factory_from_hash(yt, yt_hash)
