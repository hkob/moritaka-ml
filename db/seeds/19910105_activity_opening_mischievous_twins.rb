key = :thema_song_mischievous_twins_opening
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :thema_song_mischievous_twins,
      j_title: 'アニメ「おちゃめなふたご クレア学院物語」オープニングソング',
      e_title: "Opening song for animation program {OCHAME-NA FUTAGO KUREA GAKUIN MONOGATARI}: [Mischievous Twins: The Tales of St. Clare's]",
      from_id: '1991/1/5',
      to_id: '1991/11/2',
      sort_order: 199101050,
      song_id: :benkyono_uta,
    }
  end
end
