key = :single_yowaseteyo_konyadake
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :yowaseteyo_konyadake do
    {
      device_type: 'cover_single',
      event_date_id: '1993/8/25',
      sort_order: 199308250,
      number: '8th',
      singer_id: :iSanaeJonouchi,
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'PSDR-5023', '1st', false, :polystar, '1,000'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:yowaseteyo_konyadake, %i[VOCAL iSanaeJonouchi ARRANGE iHideoSaito]],
    [:watarasebashi, %i[VOCAL iSanaeJonouchi ARRANGE iHideoSaito]],
    [:yowaseteyo_konyadake, %i[VOCAL iKaraoke ARRANGE iHideoSaito], '(オリジナル・カラオケ)', '(Original karaoke)'],
    [:watarasebashi, %i[VOCAL iKaraoke ARRANGE iHideoSaito], '(オリジナル・カラオケ)', '(Original Karaoke)'],
  ]
end
