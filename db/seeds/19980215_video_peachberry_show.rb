key = :video_peachberry_show
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key do
    {
      device_type: :live_video,
      event_date_id: '1998/2/15',
      minutes: 110,
      seconds: 0,
      sort_order: 199802150,
      number: '8th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4347.html'
    }
  end

  video.media_from_array [
    ["#{key}_vhs1", :vhs, 'EPVA-8', :first, false, :one_up_music, '4,854'],
    ["#{key}_ld1", :ld, 'EPLA-8', :first, false, :one_up_music, '4,854'],
  ]

  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }
  vc = %i[VOCAL iChisatoMoritaka]
  list.list_contents_from_array ListContent.title_only('オープニング', 'Opening') +
    (%i[hoshini_negaio la_la_sunshine tony_slavin shintou_mekkyaku ginirono_yume les_champs_elysees futariwa_koibito watarasebashi my_anniversary].map { |k| [k, vc] }) +
    [[:my_anniversary, vc, '[INTERMISSION]', '[INTERMISSION]']] +
    (%i[lets_go snow_again miracle_light].map { |k| [k, vc] }) +
    ListContent.medley_in('シングルメドレー', 'Single medley') +
    (%i[new_season seventeen gin_gin_gin the_stress michi gin_gin_gin watashino_natsu haeotoko].map { |k| [k, vc] }) +
    @medley_out +
    (%i[anatawa_ninkimono kibun_soukai sweet_candy konomachi watashiga_obasanni_nattemo].map { |k| [k, vc] }) +
    ListContent.title_only('ENDING')

  concert = concert_factory :tour_peachberry_show
  concert_hall = concert_hall_factory concert, 14
  concert_list = list_factory(:tour_peachberry_show_E)
  concert_list_contents = concert_list.list_contents.order_sort_order
  csos = {1 => 0, 2 => 1, 3 => 3, 4 => 4, 5 => 5, 6 => 7, 7 => 10, 8 => 11, 9 => 14, 10 => 16, 11 => 17, 12 => 19, 13 => 20, 15 => 26, 16 => 27, 17 => 28, 18 => 29, 19 => 30, 20 => 31, 21 => 32, 22 => 33, 24 => 36, 25 => 37, 26 => 39, 27 => 43, 28 => 46}
  list.list_contents.order_sort_order.each_with_index do |vsls, i|
    concert_video_factory concert_list_contents[csos[i]], vsls, concert_hall if csos[i]
  end
end
