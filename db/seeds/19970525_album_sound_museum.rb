key = :album_sound_museum
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :sound_museum do
    {
      device_type: :part_album,
      event_date_id: '1997/5/25',
      minutes: 49,
      seconds: 50,
      sort_order: 199705250,
      number: '2nd',
      singer_id: :iTowaTei
    }
  end

  album.media_from_array [
    ["#{key}_cd", :cd, 'AMCY-2258,2259', :first, false, :east_west_japan, '2,854'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_only('the Sound Museum') +
    ListContent.title_only('Time After Time') +
    ListContent.title_only('Happy') +
    ListContent.title_only('BMT') +
    ListContent.title_only('Higher') +
    ListContent.title_only('Corridor') +
    ListContent.title_only('GBI (German Bold Italic)') +
    [[:tamilano, %i[SDRUMBREAKS iChisatoMoritaka]]] +
    ListContent.title_only('Private Eyes') +
    ListContent.title_only('Everything We Do Is Music')
  )
end
