key = :video_kokon_tozai
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key, :tour_kokon_tozai do
    {
      device_type: :live_video,
      event_date_id: '1991/6/17',
      minutes: 89,
      sort_order: 199106170,
      number: '4th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4329.html'
    }
  end

  video.media_from_array [
    ["#{key}_vhs1", :vhs, 'WPVL-8122', :first, false, :warner_pioneer, '5,709'],
    ["#{key}_ld1", :ld, 'WPLL-8122', :first, false, :warner_pioneer, '5,600'],
    ["#{key}_vhs2", :vhs, 'WPVL-8122', :second, false, :warner_music_japan, '5,709'],
    ["#{key}_ld2", :ld, 'WPLL-8122', :second, false, :warner_music_japan, '5,600'],
  ]

  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:onitaiji, @vc],
    [:kondo_watashi_dokoka, @vc],
    [:aru_olno_seishun, @vc],
    [nil, [], 'ロックンロール・メドレー(?)', "Rock'n Roll medley(?)", 'MEDLEY_IN', 'MEDLEY_IN'],
    [:kusaimononiwa_futaoshiro, @vc],
    [:rockn_roll_widow, @vc],
  ] + @medley_out + [
    [:hong_kong, @vc],
    [nil, [], 'シングル・メドレー', 'Single medley', 'MEDLEY_IN', 'MEDLEY_IN'],
    [:new_season, @vc],
    [:overheat_night_2, @vc],
    [:alone, @vc],
    [:michi, @vc],
    [:the_stress, @vc],
    [:get_smile, @vc]
  ] + @medley_out + [
    [:benkyono_uta, @vc],
    [:konomachi, @vc],
    [:seventeen, @vc],
    [:sonogono_watashi, @vc],
    [:funkey_monkey_baby, @vc],
    [:yoruno_entotsu, @vc],
    [:teriyaki_burger, @vc],
    [:busters_blues, @vc],
    [:ame, @vc, '(ロック・ヴァージョン)', '(Rock version)'],
    [:ame, [], '[インストゥルメンタル]', '[Instrumental]'],
  ]

  concert = concert_factory :tour_kokon_tozai
  concert_hall = concert_hall_factory concert, 27
  concert_list = list_factory(:tour_kokon_tozai_D)
  concert_list_contents = concert_list.list_contents.order_sort_order

  csos = { 0 => 0, 1 => 2, 2 =>3, 4 =>6, 5 => 7, 7 => 9, 9 => 11, 10 => 12, 11 => 13, 12 => 14, 13 => 15, 14 => 16, 16 => 18, 17 => 19, 18 =>20, 19 => 21, 20 => 22, 21 => 23, 22 => 24, 23 => 26, 24 => 28 }
  list.list_contents.order_sort_order.each_with_index do |vsls, i|
    concert_video_factory concert_list_contents[csos[i]], vsls, concert_hall if csos[i]
  end
end


