key = :live_wpb_valentine_etc
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  concert = concert_factory key, :wpb_valentine_etc do
    {
      concert_type: :live,
      from_id: '1992/2/11',
      to_id: '1992/3/28',
      has_song_list: true,
      sort_order: 199202110,
      num_of_performances: 4,
      num_of_halls: 4,
    }
  end

  list_a, list_b, list_c = %i[A B C].map.with_index { |k, i| list_factory("#{key}_#{k}") { {concert_id: concert, keyword: k, sort_order: i+1} } }
  list_a.list_contents_from_array %i[hitorigurashi good_bye_season kondo_watashi_dokoka aru_olno_seishun misaki ame seventeen benkyono_uta fight] + ListContent.medley_in('ロックンロール・メドレー(?)', "Rock'n Roll medley(?)") + %i[kusaimononiwa_futaoshiro rockn_roll_widow] + @medley_out + %i[funkey_monkey_baby yoruno_entotsu] + @konomachi_home_mix + @encore + %i[period teriyaki_burger]
  list_b.list_contents_from_array %i[concert_no_yoru good_bye_season] + @mc + %i[new_season kondo_watashi_dokoka aru_olno_seishun misaki] + @mc + %i[ame seventeen] + ListContent.title_only('つなぎ', 'fill up the time') + %i[benkyono_uta fight] + @mc + ListContent.medley_in('ロックンロール・メドレー(?)', "Rock'n Roll medley(?)") + %i[kusaimononiwa_futaoshiro rockn_roll_widow] + @medley_out + %i[funkey_monkey_baby yoruno_entotsu] + @konomachi_home_mix + @encore + %i[period mi_ha_] + @mc_member + %i[teriyaki_burger]
  list_c.list_contents_from_array %i[concert_no_yoru good_bye_season] + @mc + %i[new_season kondo_watashi_dokoka aru_olno_seishun misaki] + @mc + %i[ame seventeen] + ListContent.title_only('つなぎ', 'fill up the time') + %i[benkyono_uta fight] + @mc + ListContent.medley_in('ロックンロール・メドレー(?)', "Rock'n Roll medley(?)") + %i[kusaimononiwa_futaoshiro rockn_roll_widow] + @medley_out + %i[funkey_monkey_baby yoruno_entotsu] + @konomachi_home_mix + @encore + %i[period] + @mc_member + %i[teriyaki_burger]

  concert.concert_halls_from_array [
    ['1992/2/11', :大阪メルパルク, list_a, nil, nil, 'WPB バレンタイン', 'WPB Valentine'],
    ['1992/2/13', :芝メルパルク, list_a, nil, nil, 'WPB バレンタイン', 'WPB Valentine'],
    ['1992/3/22', :秦野文化会館, list_b],
    ['1992/3/28', :関内ホール, list_c, nil, nil, 'はまフレンド 横浜市勤労者福祉共済', 'HAMA-Friend Yokohama-shi KINRO FUKUSHI presents'],
  ]
end



