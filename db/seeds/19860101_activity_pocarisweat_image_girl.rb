key = :other_pocarisweat
obj = Activity.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_factory key do
    {
      activity_type: :other,
      title_id: %q(大塚製薬 「第1回ポカリスエット・イメージガール・コンテスト」グランプリ受賞|She received the Grand Prix at `The first Pocarisweat image girl contest' by Otsuka Pharmaceutical Company|おおつかせいやく　だいいっかいぽかりすえっといめーじがーるこんてすと　ぐらんぷりじゅしょう),
      company_id: :otsuka_seiyaku,
      from_id: '1986',
      sort_order: 198601010
    }
  end
end

