pkey = :cm_takeda_yakuhin
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  company_factory :takeda_yakuhin
  activity_factory pkey do
    {
      activity_type: :cm,
      title_id: 'exist|takeda_yakuhin',
      company_id: :takeda_yakuhin,
      sort_order: 200404010,
    }
  end
end

key = :cm_takeda_yakuhin_hicee_white_b_mate2
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_takeda_yakuhin,
      j_title: '武田薬品「ハイシーホワイト2」「ハイシーBメイト2」',
      e_title: 'Takeda Pharmaceutical Company "Hicee white2", "Hicee B mate 2"',
      from_id: '2004/4',
      to_id: '2005/5',
      sort_order: 200404010,
    }
  end
end

