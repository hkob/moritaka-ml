key = :video_live_get_smile
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key, :live_get_smile do
    {
      device_type: :live_video,
      event_date_id: '1988/3/25',
      minutes: 48,
      sort_order: 198803250,
      number: '1st',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/3998.html'
    }
  end

  video.media_from_array [
    ["#{key}_vhs", :vhs, '08PV-79', :first, false, :warner_pioneer, '7,800'],
    ["#{key}_beta", :beta, '08PX-79', :first, false, :warner_pioneer, '7,800'],
    ["#{key}_ld1", :ld, '08PL-40', :first, false, :warner_pioneer, '6,800 6,420'],
    ["#{key}_vhs", :vhs, 'WPVL-8103', :second, false, :warner_music_japan, '4,077'],
    ["#{key}_ld2", :ld, 'WPLL-8103', :second, false, :warner_music_japan, '4,000'],
  ]

  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(%i[good_bye_season namida_good_bye anohino_photograph ringoshuno_rule yumeno_owari weekend_blue get_smile overheat_night_2 new_season].map { |key| [key, @vc] })

  concert = concert_factory :concert_over_get
  concert_hall = concert_hall_factory concert, 2
  concert_list = list_factory(:concert_over_get_A)
  concert_list_contents = concert_list.list_contents.order_sort_order
  list.list_contents.order_sort_order.each_with_index do |vsls, i|
    concert_video_factory concert_list_contents[i], vsls, concert_hall
  end
end
