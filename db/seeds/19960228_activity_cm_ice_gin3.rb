key = :cm_suntory_ice_gin3
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_suntory,
      j_title: 'サントリー 「アイスジン」',
      e_title: 'Suntory "ICE GIN"',
      from_id: '1996/2/28',
      to_id: '1996/3',
      sort_order: 199602280,
      j_comment: '日本 GIN 編 (アニメーション / 歌のみ) [15 秒]',
      e_comment: 'Nippon GIN version (Animation / Song only) [15 seconds]',
      song_id: :gin_gin_gin
    }
  end
end



