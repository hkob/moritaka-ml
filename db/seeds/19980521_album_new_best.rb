key = :album_new_best_yurikamome
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :new_best_yurikamome do
    {
      device_type: 'cover_album|lyric_album',
      event_date_id: '1998/5/21',
      minutes: 68,
      seconds: 47,
      sort_order: 199805210,
      number: '7th',
      singer_id: :iSanaeJonouchi
    }
  end

  album.media_from_array [
    ["#{key}_cd", :cd, 'EPCE-4007', :first, false, :zetima, '3,059'],
  ]

  vs = %i[VOCAL iSanaeJonouchi]
  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_with_performer(vs, '都鳥 (ゆりかもめ)', '{MIYAKODORI (YURIKAMOME)}') +
    [[:yowaseteyo_konyadake, %i[VOCAL iSanaeJonouchi ARRANGE iHideoSaito]]] +
    ListContent.title_with_performer(vs, 'くちびる', '{KUCHIBIRU}') +
    ListContent.title_with_performer(vs, '香港', 'Hong Kong') +
    ListContent.title_with_performer(vs, '秋止符', '{SHUSHIFU}') + [
      [:watarasebashi, %i[VOCAL iSanaeJonouchi ARRANGE iHideoSaito]],
      [:shiawaseni_narimasu, %i[VOCAL iSanaeJonouchi ARRANGE iHideoSaito]]
    ] +
    ListContent.title_with_performer(vs, '都鳥 (ゆりかもめ) 北国編', '{MIYAKODORI (YURIKAMOME) KITAGUMI-HEN}') +
    ListContent.title_with_performer(vs, 'あじさい橋', '{AJISAI-BASHI}') +
    ListContent.title_with_performer(vs, '泣くだけ泣いたら', '{NAKU-DAKE NAITARA}') +
    ListContent.title_with_performer(vs, '音無橋', '{OTONASHI-BASHI}') +
    ListContent.title_with_performer(vs, '氷河期', '{HYOGAKI}') +
    ListContent.title_with_performer(vs, '雪降りやまず', '{YUKI FURIYAMAZU}') +
    ListContent.title_with_performer(vs, '立待岬', '{TACHIMACHI-MISAKI}')
  )
end
