key = :concert_do_the_best
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  band = band_factory(:shin_yokohama_arenas)
  band.band_members_from_array %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka RECORDER iChisatoMoritaka GUITAR iChisatoMoritaka KEYBOARDS iChisatoMoritaka DRUMS iMansakuKimura GUITAR iMasahiroInaba BASS iMasafumiYokoyama KEYBOARDS iShinKohno KEYBOARDS iYasuakiMaejima BANDLEADER iYasuakiMaejima]

  concert = concert_factory key, :do_the_best do
    {
      j_subtitle: "Chisato Moritaka Concert '96",
      e_subtitle: "Chisato Moritaka Concert '96",
      concert_type: :live,
      from_id: '1996/4/21',
      to_id: '1996/4/28',
      has_song_list: true,
      has_product: true,
      sort_order: 199604210,
      num_of_performances: 2,
      num_of_halls: 1,
      band_id: :shin_yokohama_arenas
    }
  end

  list_a = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }
  list_a.list_contents_from_array %i[kibun_soukai seventeen futariwa_koibito] + @mc + %i[watashino_natsu natsuno_hi the_stress the_benkyono_uta] + @mc + %i[ame kazeni_fukarete watashiga_obasanni_nattemo] + ListContent.title_only('衣裳替え', 'Costume change') + %i[haeotoko watarasebashi] + @mc + ListContent.medley_in('ジンジンジンメドレー', 'GIN GIN GIN Medley') + %i[gin_gin_gin jin_jin_jinglebell hey_vodka] + @medley_out + %i[so_blue] + @mc + %i[kusaimononiwa_futaoshiro rockn_roll_kencho_shozaichi yoruno_entotsu konomachi] + @encore + %i[sutekina_tanjoubi yasumino_gogo] + @mc_member + %i[get_smile] + @double_encore + [[:kibun_soukai, [], '(4/28のみ)', '(Only 4/28)']]

  concert.concert_halls_from_array [
    ConcertHall.mk('1996/4/21', :横浜アリーナ, list_a, 'とんこつらーめん', '{TONKOTSU-RAMEN}'),
    ConcertHall.mk('1996/4/28', :横浜アリーナ, list_a, 'とんこつらーめん', '{TONKOTSU-RAMEN}'),
  ]
end
