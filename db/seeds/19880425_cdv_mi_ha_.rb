key = :video_cdv_mi_ha_
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key, :mi_ha_ do
    {
      device_type: :cdv,
      event_date_id: '1988/4/25',
      minutes: 23,
      seconds: 59,
      sort_order: 198804250,
      number: '1st',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4048.html'
    }
  end

  video.media_from_array [
    ["#{key}_cdv", :cdv, '24VL-13', :first, false, :warner_pioneer, '2,400 2,328'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/music-video/get-smile/id534975913', :first, true, :warner_music_japan, '400'],
  ]

  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }

  list.list_contents_from_array [
    [:yokohama_one_night, @vc],
    [:good_bye_season, @vc],
    [:forty_seven_hard_nights, @vc],
    [:mi_ha_, @vc],
    [:get_smile, @vc, '(VIDEO Part)', '(VIDEO Part)']
  ]
end
