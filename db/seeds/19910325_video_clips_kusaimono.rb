key = :video_clips_kusaimono
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key, :kusaimononiwa_futaoshiro do
    {
      device_type: :video_clips,
      event_date_id: '1991/3/25',
      minutes: 25,
      sort_order: 199103250,
      number: '3rd',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4327.html'
    }
  end

  video.media_from_array [
    ["#{key}_vhs1", :vhs, 'WPVL-8077', :first, false, :warner_pioneer, '3,364'],
    ["#{key}_ld1", :ld, 'WPLL-8077', :first, false, :warner_pioneer, '3,300'],
    ["#{key}_vhs2", :vhs, 'WPVL-8077', :second, false, :warner_music_japan, '3,364'],
    ["#{key}_ld2", :ld, 'WPLL-8077', :second, false, :warner_music_japan, '3,300'],
  ]

  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:michi, %i[VOCAL iChisatoMoritaka]],
    [:michi, %i[VOCAL iChisatoMoritaka], '[千里姫の冒険]', '[Adventure of Princess Chisato]'],
    [:kusaimononiwa_futaoshiro, %i[VOCAL iChisatoMoritaka]],
    [:busters_blues, %i[VOCAL iChisatoMoritaka]],
    [:tsukiyono_kokai, %i[VOCAL iChisatoMoritaka], '[番外篇]', '[Extra version]'],
  ]
end
