key = :video_live_do_the_best
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key do
    {
      device_type: :live_video,
      event_date_id: '1996/7/25',
      minutes: 100,
      sort_order: 199607250,
      number: '7th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4341.html'
    }
  end

  video.media_from_array [
    ["#{key}_vhs1", :vhs, 'EPVA-5', :first, false, :warner_music_japan, '5,000'],
    ["#{key}_ld1", :ld, 'EPLA-5', :first, false, :warner_music_japan, '5,000'],
  ]

  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }
  vc = %i[VOCAL iChisatoMoritaka]
  list.list_contents_from_array ListContent.title_only('オープニング', 'Opening') +
    (%i[kibun_soukai seventeen futariwa_koibito watashino_natsu natsuno_hi the_stress ame watashiga_obasanni_nattemo haeotoko watarasebashi so_blue kusaimononiwa_futaoshiro rockn_roll_kencho_shozaichi yoruno_entotsu konomachi sutekina_tanjoubi yasumino_gogo get_smile].map { |k| [k, vc] }) +
    [[:kibun_soukai, vc, '[ダブル・アンコール]', '[Double encore]']]

  concert = concert_factory :concert_do_the_best
  concert_hall = concert_hall_factory concert, 2
  concert_list = list_factory(:concert_do_the_best_A)
  concert_list_contents = concert_list.list_contents.order_sort_order
  csos = {1 => 0, 2 => 1, 3 => 2, 4 => 4, 5 => 5, 6 => 6, 7 => 9, 8 => 11, 9 => 13, 10 => 14, 11 => 21, 12 => 23, 13 => 24, 14 => 25, 15 => 26, 16 => 28, 17 => 29, 18 => 31, 19 => 33}
  list.list_contents.order_sort_order.each_with_index do |vsls, i|
    concert_video_factory concert_list_contents[csos[i]], vsls, concert_hall if csos[i]
  end
end


