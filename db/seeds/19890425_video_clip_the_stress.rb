key = :video_clip_the_stress
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key, :the_stress do
    {
      device_type: :video_clip,
      event_date_id: '1989/4/25',
      minutes: 10,
      seconds: 20,
      sort_order: 198904250,
      number: '2nd',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4317.html'
    }
  end

  video.media_from_array [
    ["#{key}_vhs1", :vhs, '15L8-8024', :first, false, :warner_pioneer, '1,545'],
    ["#{key}_ld1", :ld, '24L6-8025', :first, false, :warner_pioneer, '2,472'],
    ["#{key}_vhs2", :vhs, '15L8-8024', :second, false, :warner_music_japan, '1,500'],
    ["#{key}_ld2", :ld, '24L6-8025', :second, false, :warner_music_japan, '2,472'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/music-video/za-sutoresu-sutoresu-zhong/id534978131', :first, true, :warner_music_japan, 400],
  ]

  list_vhs = list_factory("#{key}_vhs") { {device_id: video, keyword: :vhs, sort_order: 1} }
  list_ld = list_factory("#{key}_ld") { {device_id: video, keyword: :ld, sort_order: 2} }
  list_vhs.list_contents_from_array [
    [:the_stress, @vc, '[ストレス中近東ヴァージョン]', '[Stress {CHUUKINTOU} version]'],
    [:modorenai_natsu, @vc, "[メイキング・オブ・「ザ・ストレス」 BGM]", "[Making of `the stress' BGM]"],
  ]
  list_ld.list_contents_from_array [
    [:overheat_night, @vc],
    [:get_smile, @vc],
    [:the_stress, @vc, '[ストレス中近東ヴァージョン]', '[Stress {CHUUKINTOU} version]'],
    [:modorenai_natsu, @vc, "[メイキング・オブ・「ザ・ストレス」 BGM]", "[Making of `the stress' BGM]"],
  ]
end
