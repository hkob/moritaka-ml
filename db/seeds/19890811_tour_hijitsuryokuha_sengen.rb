key = :tour_hijitsuryokuha_sengen
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  band = band_factory(:animals_2)
  band.band_members_from_array %i[VOCAL iChisatoMoritaka GUITAR iHiroyukiKato SAXOPHONE iGinjiOgawa BASS iKazuyoshiYamauchi DRUMS iNaokiKimura KEYBOARDS iHitoshiKitamura]

  concert = concert_factory key do
    {
      j_subtitle: "森高千里サマーコンサートツアー'89",
      e_subtitle: "Chisato Moritaka summer concert tour '89",
      concert_type: :tour,
      from_id: '1989/8/11',
      to_id: '1989/8/31',
      has_song_list: true,
      sort_order: 198908110,
      num_of_performances: 5,
      num_of_halls: 4,
      band_id: :animals_2
    }
  end

  list = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }
  list.list_contents_from_array %i[korekkiri_byebye hijitsuryokuha_sengen hatachi new_season overheat_night alone the_stress shiritagari watashiwa_onchi kondo_watashi_dokoka sonogono_watashi seventeen hadakaniwa_naranai yoruno_entotsu let_me_go get_smile mite] + @encore + [[:daite, [], '[ラスベガス・ヴァージョン]', '[Las Vegas version]'], [:seventeen, [], '[オレンジ・ミックス]', '[Orange Mix]']]

  concert.concert_halls_from_array [
    ['1989/8/11', :新潟県民会館, list, '大ホール', 'Big hall'],
    ['1989/8/21', :中野サンプラザ, list, nil, nil, '<ビデオ収録>', '<Video recording>'],
    ['1989/8/22', :中野サンプラザ, list],
    ['1989/8/30', :愛知勤労会館, list],
    ['1989/8/31', :大阪厚生年金会館, list, '大ホール', 'Big hall'],
  ]
end

