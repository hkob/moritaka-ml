key = :single_sutekina_tanjoubi_watashino_daijina_hito
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :sutekina_tanjoubi_watashino_daijina_hito do
    {
      device_type: :single,
      event_date_id: '1994/10/10',
      sort_order: 199410100,
      number: '24th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4258.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDA-7', '1st', false, :one_up_music, '1,000'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:sutekina_tanjoubi, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka GUITAR iYuichiTakahashi CHORUS iYuichiTakahashi SYNTHESIZER iYuichiTakahashi BASS iYukioSeto]],
    [:watashino_daijina_hito, %i[VOCAL iChisatoMoritaka ARRANGE iYasuakiMaejima DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka APF iYasuakiMaejima EPF iYasuakiMaejima SYNTHESIZER iYasuakiMaejima], '(シングル・ヴァージョン)', '(Single version)'],
    [:sutekina_tanjoubi, %i[VOCAL iKaraoke ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka GUITAR iYuichiTakahashi CHORUS iYuichiTakahashi SYNTHESIZER iYuichiTakahashi BASS iYukioSeto], '[オリジナル・カラオケ]', '[Original karaoke]'],
    [:watashino_daijina_hito, %i[VOCAL iKaraoke ARRANGE iYasuakiMaejima DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka APF iYasuakiMaejima EPF iYasuakiMaejima SYNTHESIZER iYasuakiMaejima], '(シングル・ヴァージョン) [オリジナル・カラオケ]', '(Single version) [Original karaoke]']
  ]
  device_activity_factory single, :cm_asahi_beer
end

