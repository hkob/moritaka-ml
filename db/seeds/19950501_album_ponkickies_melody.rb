key = :album_ponkickies_melody
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :ponkickies_melody do
    {
      device_type: 'compilation_album',
      event_date_id: '1995/5/1',
      minutes: 40,
      seconds: 42,
      singer_id: :iVariousArtists,
      sort_order: 199505010,
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'ESCB-1589', :first, false, :epic_sony_records, '2,800'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_only('Welcome to Ponkickies (GET UP AND DANCE)', 'Welcome to Ponkickies (GET UP AND DANCE)') +
    ListContent.title_only('歩いて帰ろう / 斉藤和義', '{ARUITE KAERO} / Kazuyoshi Saito') +
    [[:rockn_omelette, %i[VOCAL iChisatoMoritaka]]] +
    ListContent.title_only('花子さんがきた!! / マユタン', '{HANAKO-SAN-GA KITA!!}') +
    ListContent.title_only('ポポ (Radio Edit) / 電気グルーヴ', 'POPO (Radio Edit) / Denki Groove') +
    ListContent.title_only('地球をくすぐっチャオ / ニキリナ WITH 渡辺貞夫', '{CHIKYU-WO KUSUGUCHAO} / Niki-Rina WITH Sadao Watanabe') +
    ListContent.title_only("パレード ('82リミックス・ヴァージョン) / 山下達郎", "Parade ('82 Remix version) / Tatsuro Yamashita") +
    ListContent.title_only('ゴー! ゴー! コニーちゃん!のテーマ', 'Go! Go! KONI-CHAN!-NO Thema') +
    ListContent.title_only('うちのパパとママとボク / 山田のぼる', '{UCHI-NO PAPA-TO MAMA-TO BOKU} / Noboru Yamada') +
    ListContent.title_only('夏の決心 / 大江千里', '{NATSU-NO KESSHIN} / Senri Oe') +
    ListContent.title_only('夢のヒヨコ / 矢野顕子', '{YUME-NO HIYOKO} / Akiko Yano') +
    ListContent.title_only("Child's days memory / 米米CLUB", "Child's days memory / Kome Kome CLUB")
  )
end
