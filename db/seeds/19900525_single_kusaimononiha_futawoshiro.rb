key = :single_kusaimononiwa_futaoshiro
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :kusaimononiwa_futaoshiro do
    {
      device_type: :single,
      event_date_id: '1990/5/25',
      sort_order: 199005250,
      number: '10th',
      singer_id: :iChisatoMoritaka,
      j_comment: 'おまけステッカー付き',
      e_comment: 'include a sticker',
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4183.html'
    }
  end

  single.media_from_array [
    ["#{key}_cds1", :cds, 'WPDL-4148', '1st', false, :warner_pioneer, '937'],
    ["#{key}_cds2", :cds, 'WPDL-4148', '2nd', false, :warner_music_japan, '937'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/chouimononihafutaoshiro!!/id528973648', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [ :kusaimononiwa_futaoshiro, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito]],
    [ :nozokanaide, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito]]
  ]
  device_activity_factory single, :cm_pioneer
end


