key = :album_junior_sweet
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :junior_sweet do
    {
      device_type: :part_album,
      event_date_id: '1997/9/21',
      minutes: 54,
      seconds: 0,
      sort_order: 199709210,
      number: '5th Original',
      singer_id: :iChara
    }
  end

  album.media_from_array [
    ["#{key}_cd", :cd, 'ESCB-1835', :first, false, :epic_sony_records, '2,854'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  vc = %i[VOCAL iChara]
  list.list_contents_from_array(
    ListContent.title_with_performer(vc, 'ミルク', 'Milk') +
    ListContent.title_with_performer(vc, 'やさしい気持ち', '{YASASHII KIMOCHI}', '(しあわせ version)', '({SHIAWASE} version)') +
    [[:shimashimano_banbi, %i[VOCAL iChara ROCKDRUMS iChisatoMoritaka]]] +
    ListContent.title_with_performer(vc, '私の名前はおバカさん', '{WATASHI-NO NAMAE-HA OBAKA-SAN}') +
    ListContent.title_with_performer(vc, 'タイムマシーン', 'Time Machine') +
    ListContent.title_with_performer(vc, '勝手にきた', '{KATTE-NI KITA}') +
    ListContent.title_with_performer(vc, 'どこに行ったんだろうあのバカは', '{DOKO-NI ITTANDAROU ANO BAKA-HA}') +
    ListContent.title_with_performer(vc, '私はかわいい人といわれたい', '{WATASHI-HA KAWAII HITO-TO IWARETAI}', '(Original version)', '(Original version)') +
    ListContent.title_with_performer(vc, 'Junior Sweet', 'Junior Sweet') +
    ListContent.title_with_performer(vc, '花の夢', '{HANA-NO YUME}') +
    ListContent.title_with_performer(vc, '愛の絆', '{AI-NO KIZUNA}') +
    ListContent.title_with_performer(vc, 'せつないもの', '{SETSUNAI MONO}')
  )
end

