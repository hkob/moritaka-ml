key = :single_tsumetai_tsuki
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :tsumetai_tsuki do
    {
      device_type: :single,
      event_date_id: '1998/10/1',
      sort_order: 199810010,
      number: '37th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4296.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDE-1011', '1st', false, :zetima, '1,020'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:tsumetai_tsuki, %i[ARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka AGUITAR iYuichiTakahashi PROGRAMMING iYuichiTakahashi PIANO iYasuakiMaejima FRHODES iYasuakiMaejima AGUITAR iYukioSeto GUTGUITAR iYukioSeto EGUITAR iYukioSeto BASS iYukioSeto PERCUSSION iYukioSeto]],
    [:kikenna_hodou, %i[ARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka GUITAR iYuichiTakahashi PROGRAMMING iYuichiTakahashi PIANO iYasuakiMaejima], '(5 version)', '(5 version)'],
    [:tsumetai_tsuki, %i[ARRANGE iYuichiTakahashi VOCAL iKaraoke DRUMS iChisatoMoritaka AGUITAR iYuichiTakahashi PROGRAMMING iYuichiTakahashi PIANO iYasuakiMaejima FRHODES iYasuakiMaejima AGUITAR iYukioSeto GUTGUITAR iYukioSeto EGUITAR iYukioSeto BASS iYukioSeto PERCUSSION iYukioSeto], '(ORIGINL KARAOKE)', '(ORIGINL KARAOKE)']
  ]
end




