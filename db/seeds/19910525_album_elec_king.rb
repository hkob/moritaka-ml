key = :album_elec_king
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :elec_king do
    {
      device_type: 'part_album|cover_album',
      event_date_id: '1991/5/25',
      minutes: 44,
      seconds: 7,
      sort_order: 199105250,
      number: '3rd',
      singer_id: :iCarnation
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'TKCA-30293', :first, false, :wax_records, '3,000'],
    ["#{key}_cd2", :cd, 'TKCA-70963', :first, false, :wax_records, '2,300'],
  ]

  vm = %i[VOCAL iMasataroNaoe]
  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [nil, vm, nil, nil, 'からまわる世界', '{KARAMAWARU SEKAI}'],
    [nil, vm, nil, nil, 'パーキング・メーター', 'Parking Meter'],
    [nil, vm, nil, nil, 'ビッグ', 'BIG'],
    [nil, vm, nil, nil, 'テレフォン・ガール', 'Telephone Girl'],
    [nil, vm, nil, nil, 'マイ・フェイヴァリット・ボート', 'My favorite boat'],
    [:hadakaniwa_sasenai, vm, nil, nil, nil, nil, '「はだかにはならない」のかえ歌', 'A parody song of {HADAKA-NIHA NARANAI}'],
    [nil, vm, nil, nil, 'ロック・ゾンビ', 'Rock Zombie'],
    [nil, vm, nil, nil, '悲しきめまい', '{KANASHIKI MEMAI}'],
    [nil, vm, nil, nil, 'ハレハレハレ', '{HARE HARE HARE}'],
    [nil, vm, nil, nil, '王様の庭', '{OSAMA-NO NIWA}'],
    [:moretsuna_hito_moretsuna_koi, %i[VOCAL iMasataroNaoe VOCAL iChisatoMoritaka], nil, nil, nil, nil, '「うちにかぎってそんなことはないはず」のかえ歌', 'A parody song of {UCHINI KAGITTE SONNA KOTOHA NAIHAZU}'],
  ]
end




