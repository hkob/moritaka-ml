key = :album_the_moritaka
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :the_moritaka do
    {
      device_type: :album,
      event_date_id: '1991/7/10',
      minutes: 70,
      seconds: 40,
      sort_order: 199107100,
      number: '7th',
      singer_id: :iChisatoMoritaka,
      j_comment: '2nd ベスト・アルバム (5th ANNIVERSARY 特別企画アルバム)|初回限定 32ページカラー写真集',
      e_comment: '2nd best album (5th anniversary special album)|32 page photo book(1st lot only)',
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4044.html'
    }
  end

  album.media_from_array [
    ["#{key}_ct", :ct, 'WPTL-403', :first, false, :warner_pioneer, '2,900'],
    ["#{key}_cd1", :cd, 'WPCL-403', :first, false, :warner_pioneer, '2,900'],
    ["#{key}_cd2", :cd, 'WPCL-403', :second, true, :warner_music_japan, '2,900'],
    ["#{key}_cd3", :cd, 'WPC6-8321', :third, true, :warner_music_japan, '2,447'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/ザ-森高/id528981543', :first, true, :warner_music_japan, '2,100'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  performer = %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito]
  performerWC = performer + %i[CHORUS iManahoSaito]
  list.list_contents_from_array [
    [:mijikai_natsu, performer],
    [:kusaimononiwa_futaoshiro, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito BGVOCAL iHideoSaito], '(もっと臭いものヴァージョン)', '({MOTTO-KUSAI-MONO} version)'],
    [:daite, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi SYNTHESIZER iYuichiTakahashi PROGRAMS iYuichiTakahashi AGUITAR iYuichiTakahashi CHORUS iYuichiTakahashi EGUITAR iHirokuniKorekata TENORSAX iTeruoGoto PIANO iKenShima], '(ラスベガス・ヴァージョン) (reproduction)', '(Las Vegas version) (reproduction)'],
    [:the_benkyono_uta, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito CHORUS iChisatoMoritaka CHORUS iYuichiTakahashi CHORUS iSeijiMatsuura]],
    [:hachigatsuno_koi, performer, '(アルバム・ヴァージョン)', '(Album version)'],
    [:get_smile, %i[VOCAL iChisatoMoritaka ARRANGE iYasuakiMaejima KEYBOARDS iYasuakiMaejima CHORUS iYasuakiMaejima KEYBOARDS iShinKohno CHORUS iShinKohno BASS iMasafumiYokoyama CHORUS iMasafumiYokoyama DRUMS iMakotoYoshihara GUITAR iHiroyoshiMatsuo], '(コンサート・アレンジ・ヴァージョン)', '(Concert arrange version)'],
    [:the_nozokanaide, performer],
    [:ame, performer, '(ロック・ヴァージョン)', '(Rock version)'],
    [:kanojo, performer],
    [:seishun, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito KEYBOARDS iYasuakiMaejima CHORUS iYasuakiMaejima KEYBOARDS iShinKohno CHORUS iShinKohno BASS iMasafumiYokoyama CHORUS iMasafumiYokoyama DRUMS iMakotoYoshihara CHORUS iMakotoYoshihara GUITAR iHiroyoshiMatsuo CHORUS iHiroyoshiMatsuo], '(ザ・森高テイク)', '(The Moritaka Take)'],
    [:the_mi_ha_, performer, '(ザ・森高ヴァージョン)', '(The Moritaka version)'],
    [:the_stress, performer, '(ザ・森高ヴァージョン)', '(The Moritaka version)'],
    [:kusaimononiwa_futaoshiro, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito APIANO iYasuakiMaejima BGVOCAL iYasuakiMaejima ORGAN iShinKohno BASS iMasafumiYokoyama DRUMS iMakotoYoshihara BGVOCAL iMakotoYoshihara GUITAR iHiroyoshiMatsuo GUITARSOLO iOJISAN], '(おじさんヴァージョン)', '({OJISAN} version)'],
    [:konomachi, performer, '(ザ・森高ヴァージョン)', '(The Moritaka version)']
  ]
end



