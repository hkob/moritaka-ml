key = :single_13gatsuno_ame1
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :juusangatsuno_ame do
    {
      device_type: :music_single,
      event_date_id: '1994/11/23',
      sort_order: 199411230,
      number: '??th',
      singer_id: :iAyakoShimizu,
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'VIDL-10575', '1st', false, :victor_entertainment, '1,000'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:juusangatsuno_ame, %i[VOCAL iAyakoShimizu ARRANGE iMegumiWakakusa]],
  ] + ListContent.title_with_performer(%i[VOCAL iAyakoShimizu], 'ガラスの雨', '{GARASU-NO AME}') + [
    [:juusangatsuno_ame, %i[VOCAL iKaraoke ARRANGE iMegumiWakakusa], '(オリジナル・カラオケ)', '(Original karaoke)'],
    [:juusangatsuno_ame, %i[VOCAL iKaraoke ARRANGE iMegumiWakakusa], '(男性用カラオケ)', '(Karaoke for men)'],
  ]
  device_activity_factory single, :thema_song_iitabi_yumekibun
end

