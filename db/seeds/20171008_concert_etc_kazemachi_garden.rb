key = :concert_etc_kazemachi_garden_2017
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  concert = concert_factory key do
    {
      j_subtitle: '～松本 隆 作詞活動47周年記念スペシャル・プロジェクト × 恵比寿ガーデンプレイス23周年記念WEEK～ サッポロ生ビール黒ラベルPresents',
      e_subtitle: '-- Takashi Matsumoto Lyrics Activity 47th Anniversary Special Project × Ebisu Garden Place 23rd Anniversary WEEK -- Sapporo Draft Beer Black Label Presents',
      concert_type: :etc,
      from_id: '2017/10/8',
      has_song_list: true,
      sort_order: 201710080,
      num_of_performances: 1,
      num_of_halls: 1,
    }
  end

  list_a = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }
  list_a.list_contents_from_array [
    [:tanabatano_yoru_kimini_aitai, %i[VOCAL iChisatoMoritaka]],
  ]

  concert.concert_halls_from_array [
    ['2017/10/8', :恵比寿ザ・ガーデンホール, list_a],
  ]
end


