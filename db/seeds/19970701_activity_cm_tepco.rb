pkey = :cm_tepco
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  company_factory :tepco
  activity_factory pkey do
    {
      activity_type: :cm,
      title_id: 'exist|tepco',
      company_id: :tepco,
      sort_order: 199709010,
    }
  end
end

key = :cm_tepco_futsuno_shiawase
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_tepco,
      j_title: '東京電力「‘97バヂャー家シリーズ　イメージソング」（楽曲のみ）',
      e_title: %q{Image song for TEPCO "'97 badger family series" (only song)},
      from_id: '1997/7',
      to_id: '1997/9',
      sort_order: 199709010,
      song_id: :futsuno_shiawase
    }
  end
end


