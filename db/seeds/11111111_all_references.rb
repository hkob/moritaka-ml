references = {
  official: {
    reference_type: :official_site,
    jtitle: '森高千里 オフィシャルウェブサイト',
    etitle: 'Chisato Moritaka Official Web Site',
    yomi: 'もりたかちさと　おふぃしゃるうぇぶさいと',
    link: 'http://www.moritaka-chisato.com/',
    sort_order: 1,
  },
  facebook: {
    reference_type: :official_site,
    jtitle: '森高千里 Facebook',
    etitle: 'Chisato Moritaka Facebook',
    yomi: 'もりたかちさと　ふぇーすぶっく',
    link: 'https://www.facebook.com/chisatomoritaka',
    sort_order: 2,
  },
  up_front_works: {
    reference_type: :official_site,
    jtitle: 'UP-FRONT WORKS',
    etitle: 'UP-FRONT WORKS',
    yomi: 'あっぷふろんとわーくす',
    link: 'http://www.up-front-works.jp/',
    sort_order: 3,
  },
  warner_music_japan: {
    reference_type: :official_site,
    jtitle: 'Warner Music Japan',
    etitle: 'Warner Music Japan',
    yomi: 'わーなーみゅーじっくじゃぱん',
    link: 'http://wmg.jp/artist/moritakachisato/',
    sort_order: 4,
  },
  youtube: {
    reference_type: :official_site,
    jtitle: '森高千里 Official Youtube Channel',
    etitle: 'Chisato Moritaka Official Youtube Channel',
    yomi: 'もりたかちさと　おふぃしゃるゆうちゅーぶちゃねる',
    link: 'https://www.youtube.com/user/moritakachannel',
    sort_order: 5,
  },
  cm_room: {
    reference_type: :fan_site,
    jtitle: '森高千里の部屋',
    etitle: "Chisato Moritaka's room",
    yomi: 'もりたかちさとのへや',
    link: 'http://moritaka-web.com/',
    sort_order: 1,
  },
  cm_memorandum: {
    reference_type: :fan_site,
    jtitle: '森高千里 覚え書き',
    etitle: 'Chisato Moritaka Memorandum',
    yomi: 'もりたかちさと　おぼえがき',
    link: 'http://www.moritaka.info/',
    sort_order: 2,
  },
}

references.each do |key, hash|
  reference_factory key do
    {
      reference_type: hash[:reference_type],
      title_id: hash.values_at(:jtitle, :etitle, :yomi).join('|'),
      link: hash[:link],
      sort_order: hash[:sort_order]
    }
  end
end
