key = :album_dear_yuming
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :dear_yuming do
    {
      device_type: 'compilation_album',
      event_date_id: '1999/9/22',
      minutes: 45,
      seconds: 59,
      singer_id: :iVariousArtists,
      sort_order: 199909220
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'SRCL-4649', :first, false, :sme_records, '3,059'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_only('COBALT HOUR / NOKKO') +
    [[:anohini_kaeritai, %i[ARRANGE iYasuakiMaejima VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka FRHODES iYasuakiMaejima PROGRAMMING iYasuakiMaejima GUITAR iYukioSeto]]] +
    ListContent.title_only('静かなまぼろし / m-flo', '{SHIZUKANA MOBOROSHI} / m-flo') +
    ListContent.title_only("情熱に届かない 〜 Don't Let Me Go 〜 / 松崎ナオ", "{JOUNETSU-NI TODOKANAI} -- Don't Let Me Go -- / Nao Matsuzaki") +
    ListContent.title_only('DOWNTOWN BOY / 露崎春女', 'DOWNTOWN BOY / Harumi Tsuyuzaki') +
    ListContent.title_only('スラバヤ通りの妹へ / 大江千里', '{SURABAYA-TOORI-NO IMOUTO-E} / Senri Oe') +
    ListContent.title_only('Hello, my friend / 井出麻理子', 'Hello, my friend / Mariko Ide') +
    ListContent.title_only('翳りゆく部屋 / 椎名林檎', '{KAGERIYUKU HEYA} / Ringo Shina') +
    ListContent.title_only('恋人がサンタクロース / 奥井香', '{KOIBITO-GA} SantaClaus / Kaoru Okui') +
    ListContent.title_only('A HAPPY NEW YEAR / ゴスペラーズ', 'A HAPPY NEW YEAR / Gospellers')
  )
end

