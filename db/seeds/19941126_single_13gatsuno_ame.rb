key = :single_13gatsuno_ame2
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :juusangatsuno_ame do
    {
      device_type: :music_single,
      event_date_id: '1994/11/26',
      sort_order: 199411260,
      number: '??th',
      singer_id: :iNafuna,
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'PODH-1233', '1st', false, :polydor, '1,000'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:juusangatsuno_ame, %i[VOCAL iNafuna ARRANGE iMegumiWakakusa]],
    [:juusangatsuno_ame, %i[VOCAL iNafuna ARRANGE iMegumiWakakusa], '(韓国語)', '(Korean)'],
    [:juusangatsuno_ame, %i[VOCAL iKaraoke ARRANGE iMegumiWakakusa], '(男性用／オリジナルカラオケ)', '(Karaoke for men / Original karaoke)'],
    [:juusangatsuno_ame, %i[VOCAL iKaraoke ARRANGE iMegumiWakakusa], '(女性用／オリジナルカラオケ)', '(Karaoke for women / Original karaoke)'],
  ]
  device_activity_factory single, :thema_song_iitabi_yumekibun
end

