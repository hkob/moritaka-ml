key = :album_mix_age
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :mix_age do
    {
      device_type: :album,
      event_date_id: '1999/11/3',
      minutes: 48,
      seconds: 0,
      sort_order: 199911030,
      number: '18th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4150.html',
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'EPCE-5029', :first, false, :zetima, '3,059'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/mix-age/id388390609', :first, false, :up_front_works, '2,000'],
  ]

  vc = %i[VOCAL iChisatoMoritaka]
  list = list_factory("#{key}_common") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:snow_again, %i[VOCAL iChisatoMoritaka LOOPDRUMS iChisatoMoritaka AGUITAR iYuichiTakahashi PROGRAMMING iYuichiTakahashi EGUITAR iYukioSeto BASS iYukioSeto], '(SNOW BLINK Re-Mix)', '(SNOW BLINK Re-Mix)'],
    [:watashinoyouni, %i[VOCAL iChisatoMoritaka], '(ナチュラルズ CM SLOW Version)', '(Naturals CM SLOW Version)'],
    [:watashinoyouni, %i[VOCAL iChisatoMoritaka LOOPDRUMS iChisatoMoritaka PROGRAMMING iYuichiTakahashi GUITAR iYukioSeto], '(NAZUNA Re-Mix)', '(NAZUNA Re-Mix)'],
    [:wasurekaketeta_yume, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka AGUITAR iYuichiTakahashi PROGRAMMING iYuichiTakahashi PIANO iYasuakiMaejima EGUITAR iYukioSeto BASS iYukioSeto WINDCHIME iYukioSeto COWBELL iYukioSeto], '(日本生命 CM Version)', '({NIHON-SEIMEI} CM version)'],
    [:ichido_asobini_kiteyo99, %i[VOCAL iChisatoMoritaka LOOPDRUMS iChisatoMoritaka GUITAR iDavidTWalker FRHODES iYasuakiMaejima KEYBOARDS iYasuakiMaejima PROGRAMMING iYasuakiMaejima]],
    [:sala_bossa_nova, %i[VOCAL iChisatoMoritaka LOOPDRUMS iChisatoMoritaka PROGRAMMING iYuichiTakahashi GUITAR iYukioSeto], '(むかしの人は… Re-Mix)', '({MUKASHINO-HITOWA --Re-Mix)'],
    [:umimade_5fun, %i[VOCAL iChisatoMoritaka LOOPDRUMS iChisatoMoritaka PROGRAMMING iYuichiTakahashi GUITAR iYukioSeto], '(Out Take)', '(Out Take)'],
    [:ame_1999, %i[VOCAL iChisatoMoritaka GUITAR iYuichiTakahashi PROGRAMMING iYuichiTakahashi PIANO iYasuakiMaejima GUITAR iYukioSeto WINDCHIME iYukioSeto]],
    [:itsumono_mise, %i[VOCAL iChisatoMoritaka LOOPDRUMS iChisatoMoritaka PROGRAMMING iYuichiTakahashi], '(CASAR Re-Mix)', '(CASAR Re-Mix)'],
    [:kibun_soukai, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka GUITAR iYuichiTakahashi CHORUS iYuichiTakahashi PROGRAMMING iYuichiTakahashi PIANO iShinKohno BASS iMasafumiYokoyama], '(Out Take)', '(Out Take)'],
    [:every_day, %i[VOCAL iChisatoMoritaka LOOPDRUMS iChisatoMoritaka AGUITAR iYuichiTakahashi PROGRAMMING iYuichiTakahashi EGUITAR iYukioSeto], '(ALBUM Version)', '(ALBUM Version)'],
  ]
end
