key = :concert_90_osaka_kousei_nenkin
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  concert = concert_factory key, '大阪厚生年金会館' do
    {
      concert_type: :live,
      from_id: '1990/4/2',
      has_song_list: true,
      sort_order: 199004020,
      num_of_performances: 1,
      num_of_halls: 1
    }
  end

  list = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }

  list.list_contents_from_array(%i[good_bye_season let_me_go uwasa michi seventeen kondo_watashi_dokoka hadakaniwa_naranai akunno_higeki yoruno_entotsu sonogono_watashi alone stress new_season overheat_night mi_ha_ get_smile] + @encore + %i[daite seishun])

  concert.concert_halls_from_array [
    ['1989/4/2', :大阪厚生年金会館, list],
  ]
end

