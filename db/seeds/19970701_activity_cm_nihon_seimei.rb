pkey = :cm_nihon_seimei
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  company_factory :nihon_seimei
  activity_factory pkey do
    {
      activity_type: :cm,
      title_id: 'exist|nihon_seimei',
      company_id: :nihon_seimei,
      sort_order: 199707010,
    }
  end
end

key = :cm_nihon_seimei_my_anniversary
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_nihon_seimei,
      j_title: '日本生命「My アニバーサリー」',
      e_title: 'Nihon Seimei "My Anniversary"',
      from_id: '1997/7',
      to_id: '1998/4',
      sort_order: 199707010,
      song_id: :my_anniversary
    }
  end
end

key = :cm_nihon_seimei_wasurekaketeta_yume
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_nihon_seimei,
      j_title: '日本生命「ふれあい家族ナイスケア」',
      e_title: 'Nihon Seimei "{FUREAI KAZOKU} Nice care"',
      from_id: '1998/5',
      to_id: '1998/9',
      sort_order: 199805010,
      song_id: :wasurekaketeta_yume
    }
  end
end

key = :cm_nihon_seimei_utopia
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_nihon_seimei,
      j_title: '日本生命「ふれあい家族ナイスケア」',
      e_title: 'Nihon Seimei "{FUREAI KAZOKU} Nice care"',
      from_id: '1998/10',
      to_id: '1999/8',
      sort_order: 199810010,
      song_id: :utopia
    }
  end
end

key = :cm_nihon_seimei_every_day
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_nihon_seimei,
      j_title: '日本生命「ふれあい家族ナイスケアEX」',
      e_title: 'Nihon Seimei "{FUREAI KAZOKU} Nice care EX"',
      from_id: '1999/9',
      to_id: '2000/8',
      sort_order: 199909010,
      song_id: :every_day
    }
  end
end
