key = :mini_album_romantic
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :romantic do
    {
      device_type: :mini_album,
      event_date_id: '1988/7/10',
      minutes: 20,
      seconds: 31,
      sort_order: 198807100,
      number: '1st',
      singer_id: :iChisatoMoritaka,
      j_comment: 'サマー特別企画ミニアルバム',
      e_comment: 'A Mini-Album as a summer special project',
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4034.html'
    }
  end

  album.media_from_array [
    ["#{key}_cds1", :cds, '18SL-1001', :first, false, :warner_pioneer, '1,800'],
    ["#{key}_ct", :ct, 'LKE-3001', :first, false, :warner_pioneer, '1,800'],
    ["#{key}_cds2", :cd, '18SL-1001', :first, false, :warner_music_japan, '1,689'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/romantic-romantikku-ep/id528977583', :first, true, :warner_music_japan, '1,250']
  ]

  list = list_factory("#{key}_common") { {device_id: album, keyword: :common, sort_order: 1} }

  list.list_contents_from_array [
    [:do_you_know_the_way_to_san_jose, %i[VOCAL iChisatoMoritaka VOCAL iTakumiYamamoto BASS iKeiichiIshibashi DRUMS iKeiichiIshibashi CHORUS iChisatoMoritaka CHORUS iTakumiYamamoto CHORUS iKenShima PIANO iKenShima]],
    [:romantic, %i[VOCAL iChisatoMoritaka KEYBOARDS iKenShima BASS iKenjiTakamizu DRUMS iMasahiroMiyazaki SYNPR iKeijiToriyama]],
    [:anohino_photograph, %i[VOCAL iChisatoMoritaka VOCAL iKenShima GUITAR iYukioSeto KEYBOARDS iKenShima BASS iKenjiTakamizu DRUMS iMasahiroMiyazaki SYNPR iKeijiToriyama PIANO iKenShima], '(ボサノバヴァージョン)', '(Bossa Nova version)'],
    [:kaigan, %i[VOCAL iChisatoMoritaka SYNPR iTomoakiArima CHORUS iNana]],
    [:shizukana_natsu, %i[VOCAL iChisatoMoritaka SYNPR iTomoakiArima]]
  ]
end
