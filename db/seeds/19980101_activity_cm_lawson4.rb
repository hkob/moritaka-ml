key = :cm_lawson_itsumono_mise
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_lawson,
      j_title: 'ローソン(LAWSON)',
      e_title: 'LAWSON',
      from_id: '1998/1',
      to_id: '1998/3',
      sort_order: 199801010,
      song_id: :itsumono_mise,
    }
  end
end

