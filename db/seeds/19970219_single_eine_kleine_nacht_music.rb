key = :single_eine_kleine_nacht_music
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :eine_kleine_nacht_music do
    {
      device_type: :part_single,
      event_date_id: '1997/2/19',
      sort_order: 199702190,
      number: '12th',
      singer_id: :iLR,
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'PCDA-00937', '1st', false, :poly_canyon, '1,000'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_with_performer(%i[VOCAL iKenichiKurosawa], 'アイネ・クライネ・ナハト・ミュージック', 'Eine Kleine Nacht Music') +
    [[:sonna_kibunja_nai, %i[VOCAL iKenichiKurosawa DRUMS iChisatoMoritaka], '(JAM TASTE Version)', '(JAM TASTE Version)']]
  )
end



