key = :album_hit_tunes1
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :hit_tunes1 do
    {
      device_type: 'compilation_album',
      event_date_id: '1996/10/2',
      minutes: 60,
      seconds: 37,
      singer_id: :iVariousArtists,
      sort_order: 199610020,
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'SRCL-3657', :first, false, :sony_records, '2,000'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_only('TOMORROW') +
    ListContent.title_only('I BELIEVE') +
    ListContent.title_only("Don't wanna cry") +
    ListContent.title_only('LIKE A HARD RAIN') +
    ListContent.title_only('ミッドナイト・シャッフル', 'Midnight Shuffle') +
    ListContent.title_only('KANSHA して', 'KANSHA-SHITE') +
    [[:so_blue, %i[VOCAL iChisatoMoritaka]]] +
    ListContent.title_only('FREEDOM') +
    ListContent.title_only('MAGIC CHANNEL') +
    ListContent.title_only('Love & Peace Forever') +
    ListContent.title_only('MUSIC FOR THE PEOPLE') +
    ListContent.title_only('ぼくらが旅に出る理由', '{BOKURA-GA TABI-NI DERU RIYUU}') +
    ListContent.title_only('あなたに逢いたくて 〜 Missing You 〜', '{ANATA-NI AITAKUTE} - Missing You -')
  )
end

key = :album_hit_tunes2
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :hit_tunes2 do
    {
      device_type: 'compilation_album',
      event_date_id: '1996/10/2',
      minutes: 57,
      seconds: 27,
      singer_id: :iVariousArtists,
      sort_order: 19961003,
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'SRCL-3658', :first, false, :sony_records, '2,000'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_only('はだかの王様 〜しぶとく強く〜', '{HADAKA-NO OUSAMA} - {SHIBUTOKU TSUYOKU} -') +
    ListContent.title_only('FRIENDSHIP') +
    ListContent.title_only('LA・LA・LA LOVE SONG', 'LA.LA.LA LOVE SONG') +
    ListContent.title_only('Just a real love night') +
    ListContent.title_only('BREAK OUT') +
    ListContent.title_only("You're my sunshine") +
    ListContent.title_only('平凡なハッピーじゃ物足りない', '{HEIBON-NA} Happy-{JA MONOTARINAI}') +
    ListContent.title_only('Hey! Ladies & Gentlemen') +
    ListContent.title_only('風になって', '{KAZE-NI NATTE}') +
    ListContent.title_only('FOREVER') +
    ListContent.title_only('BEAT YOUR HEART') +
    [[:la_la_sunshine, %i[VOCAL iChisatoMoritaka]]] +
    ListContent.title_only('イージ(○+ュ)ー★ライダー', '{I-Ju-} rider')
  )
end

