key = :picture_book_rikutsujanai
obj = Book.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  book = book_factory key, :rikutsuja_nai do
    {
      book_type: :picture_book,
      publisher_id: :kodansha,
      photographer_id: :iHaruKimura,
      isbn: '4-06-103105-8 C0072 P1550E',
      price: '1,550',
      event_date_id: '1989/6/12',
      sort_order: 198906120,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4462.html'
    }
  end

  list = list_factory("#{key}_A") { {book_id: book, keyword: :A, sort_order: 1} }
  list.list_contents_from_array [
    [nil, [], nil, nil, '新しい季節', '{ATARASHII KISETSU}'],
    [nil, [], nil, nil, '素敵だ…', '{SUTEKI-DA...}'],
    [nil, [], nil, nil, 'Talk Essay', 'Talk Essay'],
    [nil, [], nil, nil, '忘れない', '{WASURENAI}'],
    [nil, [], nil, nil, 'なんて不意なサヨナラ', '{NANTE FUI-NA SAYONARA}'],
    [nil, [], nil, nil, '夢の終わり', '{YUME-NO OWARI}'],
    [nil, [], nil, nil, 'いけない嘘', '{IKENAI USO}'],
    [nil, [], nil, nil, '戻れない夏', '{MODORENAI NATSU}'],
  ]
end
