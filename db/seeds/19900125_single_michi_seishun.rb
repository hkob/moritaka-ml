key = :single_michi_seishun
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :michi_seishun do
    {
      device_type: :single,
      event_date_id: '1990/1/25',
      sort_order: 199001250,
      number: '9th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4173.html'
    }
  end

  single.media_from_array [
    ["#{key}_cds1", :cds, 'WPDL-4139', '1st', false, :warner_pioneer, '937'],
    ["#{key}_cds2", :cds, 'WPDL-4139', '2nd', false, :warner_music_japan, '937'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/道-青春-single/id528967172', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [ :michi, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito]],
    [ :seishun, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito]]
  ]
  device_activity_factory single, :cm_ezaki_guriko
  device_activity_factory single, :thema_song_ikenai_joshikou
end

