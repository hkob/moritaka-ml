key = :movie_aitsuni_koishite
obj = Activity.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_factory key do
    {
      activity_type: :movie,
      title_id: 'あいつに恋して|{AITSU-NI KOISHITE}|あいつにこいして',
      company_id: :toho,
      from_id: '1987/5/30',
      j_comment: '主題歌',
      e_comment: 'Thema song',
      song_id: :new_season,
      sort_order: 198705300,
    }
  end
end


