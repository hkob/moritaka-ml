key = :single_rockn_omelette
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :rockn_omelette do
    {
      device_type: :single,
      event_date_id: '1994/1/25',
      sort_order: 199401250,
      number: '21st',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4230.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDA-2', '1st', false, :one_up_music, '800'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/rokkun-omuretsu-single/id531610344', '1st', true, :warner_music_japan, '750']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:rockn_omelette, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka GUITAR iYuichiTakahashi BASS iYuichiTakahashi PIANO iShinKohno CHORUS iChisatoMoritaka CHORUS iYuichiTakahashi]],
    [:rockn_omelette, %i[VOCAL iKaraoke DRUMS iChisatoMoritaka GUITAR iYuichiTakahashi BASS iYuichiTakahashi PIANO iShinKohno CHORUS iChisatoMoritaka CHORUS iYuichiTakahashi], '[オリジナル・カラオケ]', '[Original karaoke]'],
  ]
  device_activity_factory single, :thema_song_ponkickies
end

