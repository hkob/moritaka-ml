key = :album_kanashimiyo_hitotsubuno_namidamo
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :kanashimiyo_hitotsubuno_namidamo do
    {
      device_type: :music_album,
      event_date_id: '1994/8/1',
      sort_order: 199408010,
      number: '9th',
      singer_id: :iGenTakayama,
    }
  end

  album.media_from_array [
    ["#{key}_cd", :cd, 'PSCR-5317', :first, false, :polystar, '3,059'],
  ]

  vg = %i[VOCAL iGenTakayama]
  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_with_performer(vg, '悲しみよ一粒の涙も', '{KANASHIMI-YO HITOTSUBU-NO NAMIDA-MO}') +
    ListContent.title_with_performer(vg, '風船', '{FUSEN}') +
    ListContent.title_with_performer(vg, 'おしえて', '{OSHIETE}') +
    ListContent.title_with_performer(vg, '夢の道草', '{YUME-NO MICHIKUSA}') +
    ListContent.title_with_performer(vg, '夢を手探りで', '{YUME-WO TESAGURI-DE}') +
    ListContent.title_with_performer(vg, '忘れません', '{WASUREMASEN}') +
    ListContent.title_with_performer(vg, '忘却雨', '{WASURE-AME}') +
    ListContent.title_with_performer(vg, '抱きよせて', '{DAKIYOSETE}') +
    [[:nemurasete, vg]] +
    ListContent.title_with_performer(vg, '馬鹿やね', '{BAKA-YANE}')
  )
end

