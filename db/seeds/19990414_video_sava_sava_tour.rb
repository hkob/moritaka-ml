key = :video_sava_sava_tour
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key do
    {
      device_type: :live_video,
      event_date_id: '1999/4/14',
      minutes: 110,
      seconds: 0,
      sort_order: 199904140,
      number: '9th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4353.html'
    }
  end

  video.media_from_array [
    ["#{key}_vhs1", :vhs, 'EPVE-5003', :first, false, :zetima, '5,145'],
    ["#{key}_ld1", :ld, 'EPLE-5003', :first, false, :zetima, '5,145'],
  ]

  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }
  vc = %i[VOCAL iChisatoMoritaka]
  list.list_contents_from_array ListContent.title_only('オープニング', 'Opening') +
    %i[utopia telephone sweet_candy watarasebashi umimade_5fun zarude_mizukumu_koigokoro rockn_roll_kencho_shozaichi kikenna_hodou tsumetai_tsuki tanpopono_tane la_la_sunshine konomachi nagasarete anatawa_ninkimono watashiga_obasanni_nattemo snow_again] + [[:seventeen, [], '(カーネーション・ヴァージョン)', '(Carnation Version)']] + %i[kibun_soukai ame]

  concert = concert_factory :tour_sava_sava
  concert_hall = concert_hall_factory concert, 21
  concert_list = list_factory(:tour_sava_sava_B)
  concert_list_contents = concert_list.list_contents.order_sort_order
  csos = {1 => 0, 2 => 1, 3 => 3, 4 => 5, 5 => 7, 6 => 9, 7 => 10, 8 => 11, 9 => 13, 10 => 14, 11 => 15, 12 => 17, 13 => 19, 14 => 20, 15 => 21, 16 => 23, 17 => 24, 18 => 26, 19 => 28}
  list.list_contents.order_sort_order.each_with_index do |vsls, i|
    concert_video_factory concert_list_contents[csos[i]], vsls, concert_hall if csos[i]
  end
end
