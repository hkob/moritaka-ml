key = :concert_tour_get_smile
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  concert = concert_factory key, :tour_get_smile do
    {
      j_subtitle: "森高千里 LIVE TOUR '88",
      e_subtitle: "Chisato Moritaka LIVE TOUR '88",
      concert_type: :tour,
      from_id: '1988/3/28',
      to_id: '1988/4/14',
      num_of_performances: 10,
      num_of_halls: 8,
      has_song_list: true,
      sort_order: 198803280
    }
  end

  list = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }

  list.list_contents_from_array %i[weekend_blue yokohama_one_night yumeno_owari anohino_photograph pi_a_no otisreddingni_kanpai good_bye_season get_smile mi_ha_ cant_say_good_bye overheat_night forty_seven_hard_nights] + @encore + %i[new_season get_smile]

  concert.concert_halls_from_array [
    ['1988/3/28', :渋谷ライブイン],
    ['1988/3/29', :渋谷ライブイン],
    ['1988/3/30', :渋谷ライブイン, list],
    ['1988/4/5', :仙台モーニングムーン, list],
    ['1988/4/6', :新潟県民会館, list, '小ホール', 'Small hall', 'ライブ後腹痛に見舞われ救急病院へ', 'She was rushed to an emergency hospital because she had a bad stomachache after the live.'],
    ['1988/4/8', :大阪厚生年金会館, list, '中ホール', 'Medium hall'],
    ['1988/4/9', :名古屋ハートランド, list],
    ['1988/4/11', :札幌メッセホール, list],
    ['1988/4/13', :熊本郵便貯金ホール, list],
    ['1988/4/14', :福岡ビブレ, list]
  ]
end
