key = :live_the_seishun_special_live
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  concert = concert_factory key, :the_seishun_special_live do
    {
      concert_type: :live,
      from_id: '1991/3/22',
      has_song_list: true,
      sort_order: 199103220,
      num_of_performances: 1,
      num_of_halls: 1,
    }
  end

  list = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }
  list.list_contents_from_array %i[uchini_kagitte mi_ha_ aru_olno_seishun ame benkyono_uta] + @konomachi_home_mix + %i[funkey_monkey_baby get_smile teriyaki_burger] + @encore + %i[michi new_season]

  concert.concert_halls_from_array [
    ['1991/3/22', :Space_Zero, list],
  ]
end

