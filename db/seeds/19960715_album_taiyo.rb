key = :album_taiyo
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :taiyo do
    {
      device_type: :album,
      event_date_id: '1996/7/15',
      minutes: 57,
      seconds: 33,
      sort_order: 199607150,
      number: '13th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4135.html',
      j_comment: '初回限定 ステレオ・グラフ 12ページカラー写真集',
      e_comment: 'Stereo Graph & 12 page photo book(1st lot only)',
    }
  end

  album.media_from_array [
    ["#{key}_ct", :ct, 'EPTA-7006', :first, false, :one_up_music, '3,000'],
    ["#{key}_cd1", :cd, 'EPCA-7006', :first, false, :one_up_music, '3,000'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/taiyo/id278797857', :first, true, :up_front_works, '2,000'],
  ]

  list_common = list_factory("#{key}_common") { {device_id: album, keyword: :common, sort_order: 1} }
  performer = %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito]
  list_common.list_contents_from_array [
    [:natsuwa_pararaylon, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka FRHODES iYasuakiMaejima KEYBOARDS iYuichiTakahashi GUITAR iYuichiTakahashi APIANO iShinHashimoto BASS iYukioSeto]],
    [:dekirudesho, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka APIANO iYasuakiMaejima KEYBOARDS iYuichiTakahashi GUITAR iYuichiTakahashi APIANO iShinHashimoto BASS iYukioSeto GUITAR iYukioSeto]],
    [:gin_gin_gin, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka FRHODES iYasuakiMaejima BASS iMasafumiYokoyama KEYBOARDS iYuichiTakahashi GUITAR iYuichiTakahashi GUITAR iYukioSeto]],
    [:akino_sora, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka APIANO iYasuakiMaejima KEYBOARDS iYuichiTakahashi GUITAR iYuichiTakahashi KEYBOARDS iShinHashimoto GUITAR iYukioSeto BASS iYukioSeto]],
    [:chounanto_inakamon, %i[VOCAL iChisatoMoritaka VOCAL iHenderson DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka KEYBOARDS iYuichiTakahashi GUITAR iYuichiTakahashi APIANO iShinHashimoto BASS iYukioSeto]],
    [:so_blue, %i[ARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka APIANO iYasuakiMaejima KEYBOARDS iYuichiTakahashi GUITAR iYuichiTakahashi BASS iYukioSeto]],
    [:taiyouto_aoi_tsuki, %i[ARRANGE iYasuakiMaejima VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka KEYBOARDS iYasuakiMaejima PERCUSSION iYasuakiMaejima GUITAR iYuichiTakahashi GUITAR iYukioSeto]],
    [:yasumino_gogo, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka GUITARS iHideoSaito BASS iHideoSaito HORN iHideoSaito TAMBOURINE iHideoSaito CHORUS iHideoSaito PROGRAMMING iHideoSaito APIANO iYasuakiMaejima ORGAN iYasuakiMaejima]],
    [:la_la_sunshine, %i[STRINGSARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka KEYBOARDS iYuichiTakahashi GUITAR iYuichiTakahashi APIANO iShinHashimoto BASS iYukioSeto GUITAR iYukioSeto]],
    [:toga_tatsu, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka KEYBOARDS iYuichiTakahashi GUITAR iYuichiTakahashi BGVOCAL iYuichiTakahashi APIANO iShinHashimoto TAISHOGOTO iShinHashimoto BGVOCAL iTakahisaYuzawa BASS iYukioSeto]],
    [:yoruno_umi, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka BASS iTeruoMoritaka FRHODES iYasuakiMaejima KEYBOARDS iYuichiTakahashi GUITAR iYuichiTakahashi GUITAR iYukioSeto WINDBELL iYukioSeto]],
    [:hey_vodka, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka APIANO iYasuakiMaejima FRHODES iYasuakiMaejima BASS iMasafumiYokoyama KEYBOARDS iYuichiTakahashi GUITAR iYuichiTakahashi GUITAR iYukioSeto]],
    [:tereya, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka RECORDER iChisatoMoritaka GUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi APIANO iShinHashimoto KEYBOARDS iShinHashimoto GUITAR iShinHashimoto BASS iYukioSeto]],
    [:botto_shitemiyou, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka KEYBOARDS iYuichiTakahashi GUITAR iYuichiTakahashi APIANO iShinHashimoto KEYBOARDS iShinHashimoto GUITAR iShinHashimoto TAISHOGOTO iShinHashimoto BASS iYukioSeto]],
    [:here_comes_the_sun, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka RECORDER iChisatoMoritaka GUITAR iYuichiTakahashi CHORUS iYuichiTakahashi KEYBOARDS iShinHashimoto CHORUS iShinHashimoto BASS iYukioSeto]],
  ]
end
