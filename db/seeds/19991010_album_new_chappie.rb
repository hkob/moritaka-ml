key = :album_new_chappie
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :new_chappie do
    {
      device_type: :part_album,
      event_date_id: '1999/10/10',
      minutes: 41,
      seconds: 56,
      singer_id: :iChappie,
      sort_order: 199909220
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'AICT-1126', :first, false, :sony_music_entertainment, '3,059'],
  ]

  vc = %i[VOCAL iChappie]
  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_only('COBALT HOUR / NOKKO') +

    ListContent.title_with_performer(vc, 'Welcome Morning', 'Welcome Morning', '(album edit)') +
    ListContent.title_with_performer(vc, 'Everyday') +
    ListContent.title_with_performer(vc, 'DOCU-MENTARY KISS') +
    ListContent.title_with_performer(vc, 'Good Day Afternoon') +
    ListContent.title_with_performer(vc, '水中メガネ', '{SUICHUU MEGANE}', '(album version)') +
    ListContent.title_with_performer(vc, "The International Chappie's Cheer-leading Term") +
    ListContent.title_with_performer(vc, 'Space Latin Age') +
    ListContent.title_with_performer(vc, 'デリカシーのかけら', 'Delicacy {NO KAKERA}') +
    ListContent.title_with_performer(vc, "Happyending Soulwriter's Council Band") +
    [[:tanabatano_yoru_kimini_aitai, %i[VOCAL iChappie TVOCAL iChisatoMoritaka]]] +
    ListContent.title_with_performer(vc, "Chappie's Attack")
  )
end


