key = :score_yasashiku_hikeru_piano_solo_album1
obj = Book.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  book = book_factory key do
    {
      book_type: :score_book,
      publisher_id: :kmp,
      isbn: '4-7732-0934-8 C0073 P1648E',
      price: '1,648',
      event_date_id: '1994/9/15',
      sort_order: 199409150,
    }
  end

  list = list_factory("#{key}_A") { {book_id: book, keyword: :A, sort_order: 1} }
  list.list_contents_from_array %i[new_season seventeen michi ame seishun kusaimononiwa_futaoshiro konomachi fight watashiga_obasanni_nattemo concert_no_yoru watarasebashi watashino_natsu haeotoko memories rockn_omelette watashino_daijina_hito kibun_soukai kazeni_fukarete hoshino_ojisama ichido_asobini_kiteyo zuruyasumi natsuno_hi torikago office_gaino_koi step_by_step_kareno_jinsei]
end



