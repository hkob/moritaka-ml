key = :live_document_and_clip_taiyo_on_and_off
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key, :taiyo_on_and_off do
    {
      device_type: :live_document_and_clip,
      event_date_id: '1997/3/10',
      minutes: 80,
      seconds: 0,
      sort_order: 199703100,
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4343.html'
    }
  end

  video.media_from_array [
    ["#{key}_vhs1", :vhs, 'EPVA-7', :first, false, :one_up_music, '5,000' ],
    ["#{key}_ld1", :ld, 'EPLA-7', :first, false, :one_up_music, '5,000' ],
  ]

  vc = %i[VOCAL iChisatoMoritaka]
  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_only('名古屋レインボーホール楽屋風景，武道館バックステージ', 'dressing room scene in Nagoya rainbow hall, back stage in Budo-kan', '[OFF-1]') +
    [[:natsuwa_pararaylon, vc, '(LIVE) [ON-1]', '(LIVE) [ON-1]']] +
    ListContent.title_only('インタビュー 1, 新幹線移動風景', 'interview 1, scene in Shinkansen', '[OFF-2]', '[OFF-2]') +
    [[:taiyouto_aoi_tsuki, vc, '(LIVE) [ON-2]', '(LIVE) [ON-2]']] +
    ListContent.title_only('リハーサルシーン 1', 'rehearsal scene 1', '[OFF-3]') +
    [[:futariwa_koibito, vc, '(CLIP) [ON-3]', '(CLIP) [ON-3]']] +
    ListContent.title_only('インタビュー 2', 'interview 2', '[OFF-4]') +
    [[:yasumino_gogo, vc, '(CLIP & LIVE) [ON-4]', '(CLIP & LIVE) [ON-4]']] +
    ListContent.title_only('インタビュー 3, リハーサル風景 2', 'interview 3, rehearsal scene 2', '[OFF-5]') + [
      [:jin_jin_jinglebell, vc, '(LIVE) [ON-5]', '(LIVE) [ON-5]'],
      [:kibun_soukai, vc, '(CLIP & LIVE) [ON-6]', '(CLIP & LIVE) [ON-6]'],
      [:teo_tatako, vc, '(LIVE) [ON-7]', '(LIVE) [ON-7]'],
    ] +
    ListContent.title_only('インタビュー 4', 'interview 4', '[OFF-6]') +
    [[:tereya, vc, '(LIVE) [ON-8]', '(LIVE) [ON-8]']] +
    ListContent.title_only('東京→大阪移動ショット，大阪城ホール楽屋風景', 'Scene in moving to Osaka, dressing room scene in Osaka-Jo hall', '[OFF-7]') +
    [[:toga_tatsu, vc, '(LIVE) [ON-9]', '(LIVE) [ON-9]']] +
    ListContent.title_only('インタビュー 5', 'interview 5', '[OFF-8]') + [
      [:so_blue, vc, '(CLIP) [ON-10]', '(CLIP) [ON-10]'],
      [:la_la_sunshine, vc, '(CLIP & LIVE) [ON-11]', '(CLIP & LIVE) [ON-11]'],
    ] +
    ListContent.title_only('インタビュー 6, 武道館楽屋風景', 'interview 6, dressing room scene in Budo-kan', '[OFF-9]') +
    [[:ginirono_yume, vc, '(CLIP & LIVE) [ON-12]', '(CLIP & LIVE) [ON-12]']] +
    ListContent.title_only('インタビュー 7', 'interview 7', '[OFF-10]') +
    [[:kyoukara, vc, '(LIVE) [ON-13]', '(LIVE) [ON-13]']] +
    ListContent.title_only('武道館終演後 楽屋風景', 'dressing room scene in Budo-kan after the concert', '[OFF-11]') +
    [[:ame, vc, '(LIVE) [ON-14]', '(LIVE) [ON-14]']]
  )
end
