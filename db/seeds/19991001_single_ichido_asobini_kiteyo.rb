key = :single_ichido_asobini_kiteyo
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :ichido_asobini_kiteyo99 do
    {
      device_type: :single,
      event_date_id: '1999/10/1',
      sort_order: 199910010,
      number: '40th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4302.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDE-1054', '1st', false, :zetima, '1,050'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:ichido_asobini_kiteyo99, %i[VOCAL iChisatoMoritaka ARRANGE iYasuakiMaejima LOOPDRUMS iChisatoMoritaka GUITAR iDavidTWalker FRHODES iYasuakiMaejima KEYBOARDS iYasuakiMaejima PROGRAMMING iYasuakiMaejima]],
    [:every_day, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi LOOPDRUMS iChisatoMoritaka GUITAR iDavidTWalker FRHODES iYasuakiMaejima KEYBOARDS iYuichiTakahashi PROGRAMMING iYuichiTakahashi BASS iYukioSeto]],
    [:ichido_asobini_kiteyo99, %i[VOCAL iKaraoke ARRANGE iYasuakiMaejima LOOPDRUMS iChisatoMoritaka GUITAR iDavidTWalker FRHODES iYasuakiMaejima KEYBOARDS iYasuakiMaejima PROGRAMMING iYasuakiMaejima], '(ORIGINL KARAOKE)', '(ORIGINL KARAOKE)'],
    [:every_day, %i[VOCAL iKaraoke ARRANGE iYuichiTakahashi LOOPDRUMS iChisatoMoritaka GUITAR iDavidTWalker FRHODES iYasuakiMaejima KEYBOARDS iYuichiTakahashi PROGRAMMING iYuichiTakahashi BASS iYukioSeto], '(ORIGINL KARAOKE)', '(ORIGINL KARAOKE)']
  ]
end


