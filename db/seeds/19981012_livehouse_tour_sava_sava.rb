key = :livehouse_tour_sava_sava
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  concert = concert_factory key do
    {
      j_subtitle: "Chisato Moritaka Live House Tour '98",
      e_subtitle: "Chisato Moritaka Live House Tour '98",
      concert_type: :tour,
      from_id: '1998/10/12',
      to_id: '1998/11/13',
      has_song_list: true,
      has_product: true,
      sort_order: 199810120,
      num_of_performances: 3,
      num_of_halls: 19,
    }
  end

  list_a, list_b = %i[A B].map.with_index { |k, i| list_factory("#{key}_#{k}") { {concert_id: concert, keyword: k, sort_order: i+1} } }

  list_a.list_contents_from_array %i[the_stress seventeen] + @mc + [[:telephone, [], '(Album Version)', '(Album Version)']] + %i[sweet_candy] + [[:ame, [], '(ロング・ヴァージョン)', '(Long Version)']] + @mc + %i[tokyo_rush kikenna_hodou kibun_soukai] + @mc + %i[new_season watarasebashi tanpopono_tane umimade_5fun] + @mc + %i[snow_again funkey_monkey_baby yoruno_entotsu get_smile] + @encore + %i[tsumetai_tsuki] + @mc + %i[watashiga_obasanni_nattemo] + @double_encore + @mc_member + %i[konomachi]
  list_b.list_contents_from_array %i[the_stress seventeen] + @mc + [[:tokyo_rush, [], '(大阪ラッシュや天神ラッシュ)', '(Osaka Rush or Tenjin Rush)']] + %i[sweet_candy watarasebashi] + @mc + %i[utopia kikenna_hodou kibun_soukai] + @mc + %i[new_season la_la_sunshine tanpopono_tane umimade_5fun] + @mc + %i[snow_again funkey_monkey_baby yoruno_entotsu get_smile] + @encore + %i[tsumetai_tsuki] + @mc + %i[watashiga_obasanni_nattemo] + @double_encore + @mc_member + %i[konomachi]

  concert.concert_halls_from_array [
    ConcertHall.mk('1998/10/12', :赤坂BLITZ, list_a, 'とんこつラーメン', '{TONKOTSU-RAMEN}'),
    ConcertHall.mk('1998/10/18', :大阪IMPホール, list_b, 'たこやき', '{TAKOYAKI}'),
    ConcertHall.mk('1998/11/13', :福岡ドラムロゴス, list_b, 'からし明太子', '{KARASHI-MENTAIKO}'),
  ]
end


