key = :single_miracle_light
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :miracle_light do
    {
      device_type: :single,
      event_date_id: '1997/10/15',
      sort_order: 199710150,
      number: '33rd',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4288.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'EPDA-50', '1st', false, :one_up_music, '1,020'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:miracle_light, %i[ARRANGE iHaruomiHosono VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PROGRAMMING iHarryHosono KEYBOARDS iHarryHosono GUITAR iHarryHosono BASS iHarryHosono]],
    [:miracle_woman, %i[ARRANGE iHaruomiHosono VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PROGRAMMING iHarryHosono KEYBOARDS iHarryHosono]],
    [:miracle_light, %i[ARRANGE iHaruomiHosono VOCAL iKaraoke DRUMS iChisatoMoritaka PROGRAMMING iHarryHosono KEYBOARDS iHarryHosono GUITAR iHarryHosono BASS iHarryHosono], '(ORIGINL KARAOKE)', '(ORIGINL KARAOKE)'],
  ]
  device_activity_factory single, :cm_lawson
end
