key = :album_harvest_time
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :harvest_time do
    {
      device_type: :compilation_album,
      event_date_id: '1999/11/25',
      minutes: 57,
      seconds: 51,
      sort_order: 199911250,
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4152.html',
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'WPC7-10039', :first, false, :zetima, '2,800'],
  ]

  vc = %i[VOCAL iChisatoMoritaka]
  list = list_factory("#{key}_common") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:rockn_roll_kencho_shozaichi, %i[VOCAL iChisatoMoritaka ALLINST iChisatoMoritaka CHORUS iChisatoMoritaka]],
    [:teriyaki_burger, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka ALLINST iHideoSaito PROGRAMMING iHideoSaito CHORUS iHideoSaito CHORUS iSeijiMatsuura]],
    [:mitsuketa_saifu, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka ALLINST iHideoSaito PROGRAMMING iHideoSaito]],
    [:daibouken, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka ALLINST iHideoSaito PROGRAMMING iHideoSaito CHORUS iHideoSaito CHORUS iSeijiMatsuura]],
    [:aru_olno_seishun, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka ALLINST iHideoSaito PROGRAMMING iHideoSaito]],
    [:nozokanaide, %i[ARRANGE iHideoSaito VOCAL iChisatoMoritaka ALLINST iHideoSaito PROGRAMMING iHideoSaito]],
    [:watashiwa_onchi, %i[ARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka GUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi CHORUS iYuichiTakahashi]],
    [:shiritagari, %i[VOCAL iChisatoMoritaka ARRANGE iCarlosKanno ARRANGE iYasuakiMaejima SPERCUSSION iChisatoMoritaka PERCUSSION iCarlosKanno PIANO iYasuakiMaejima GUITAR iAkiraWada DRUMS iMansakuKimura BASS iHiroshiSawada SYNTHESIZER iHiromichiTsugaki TRUMPET iKenjiYoshida TRUMPET iMasahiroKobayashi TROMBONE iOsamuMatsuki ALTOSAX iHisashiYoshinaga TENORSAX iTakeruMuraoka]],
    [:mijikai_natsu, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMMING iHideoSaito]],
    [:yowaseteyo_konyadake, %i[VOCAL iChisatoMoritaka ARRANGE iShinKohno KEYBOARDS iShinKohno GUITAR iYukioSeto]],
    [:docchimo_docchi, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PIANO iChisatoMoritaka RGUITAR iChisatoMoritaka GUITAR iYuichiTakahashi BASS iYuichiTakahashi CHORUS iYuichiTakahashi]],
    [:yoruno_entotsu, %i[VOCAL iChisatoMoritaka ARRANGE iCarnation AGUITAR iMasataroNaoe EGUITAR iMasataroNaoe CHORUS iMasataroNaoe EGUITAR iGiroBando BASS iYujiMada CHORUS iYujiMada KEYBOARDS iYuichiTanaya CHORUS iYuichiTanaya DRUMS iHiroshiYabe], '(ビデオ・ミックス)', '(Video mix)'],
    [:hikisakanaide_futario, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMMING iHideoSaito]],
    [:konomachi, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMMING iHideoSaito CHORUS iHideoSaito]],
  ]
end
