key = :album_hits1_japan
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :hits1_japan do
    {
      device_type: 'compilation_album',
      event_date_id: '1995/4/25',
      minutes: 72,
      seconds: 17,
      singer_id: :iVariousArtists,
      sort_order: 199504250,
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'WPC6-8125', :first, false, :wea_japan, '2,800'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_only('純愛ラプソディ / 竹内まりや', '{JUN-AI RAPUSODI} / Maria Takeuchi') +
    ListContent.title_only('Rusty Nail / X JAPAN', 'Rusty Nail / X JAPAN') +
    ListContent.title_only('SPY / 槇原敬之', 'SPY / Noriyuki Makihara') +
    ListContent.title_only('WARNING / いしだ壱成', 'WARNING / Issei Ishida') +
    ListContent.title_only('BRIDGE 〜あの橋をわたるとき〜 / HOUND DOG', 'BRIDGE ~{ANO HASHI-WO WATARUTOKI}~ / HOUND DOG') +
    ListContent.title_only('天使の微笑 / 衛藤利恵', '{TENSHI-NO HOHOEMI} / Rie Eto') +
    ListContent.title_only('木蘭の涙 / スターダスト・レビュー', '{MOKUREN-NO NAMIDA} / Stardust Revue') +
    ListContent.title_only("パレード ('82リミックス・ヴァージョン) / 山下達郎", "Parade ('82 Remix Version) / Tatsuro Yamashita") +
    [[:sutekina_tanjoubi, %i[ARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka]]] +
    ListContent.title_only('だまってないで / 松田樹利亜', '{DAMATTE NAIDE} / Juria Matsuda') +
    ListContent.title_only('関白失脚 / さだまさし', '{KANPAKU SHIKKYAKU} / Masashi Sada') +
    ListContent.title_only('Wind Climbing 〜風にあそばれて〜 / 奥井亜紀', 'Wind Climbing ~{KAZE-NI ASOBARETE}~ / Aki Okui') +
    ListContent.title_only('夢 / THE BLUE HEARTS', 'Dream / THE BLUE HEARTS') +
    ListContent.title_only("LIVIN'IN A PARADISE / 杉山清貴", "LIVIN'IN A PARADISE / Kiyotaka Sugiyama") +
    ListContent.title_only('愛を今夜 届けに来たよ / STAGGER', '{AI-WO KONYA KODOKE-NI KITAYO} / STAGGER') +
    ListContent.title_only('今を抱きしめて / NOA', '{IMA-WO DAKISHIMETE} / NAO')
  )
end
