pkey = :cm_pocarisweat
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  company_factory :otsuka_seiyaku
  activity_factory pkey do
    {
      activity_type: :cm,
      title_id: 'exist|otsuka_seiyaku',
      j_comment: 'ポカリスエット',
      e_comment: 'Pocarisweat',
      company_id: :otsuka_seiyaku,
      sort_order: 198610010
    }
  end
end

key = :cm_pocarisweat_1
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_pocarisweat,
      j_title: '大塚製薬「ポカリスエット」',
      e_title: "Otsuka Pharmaceutical Company `Pocarisweat'",
      from_id: '1986/10/1',
      to_id: '1988/9/30',
      j_comment: '和服編|寝室編|電車編|自動販売機編|骨董店編|レストラン編|温泉編',
      e_comment: 'Japanese custume version|Bed room version|Train version|{JIDOHANBAIKI} version|{KOTTOHIN-TEN} version|Restaurant version|{ONSEN} version',
      sort_order: 198610010
    }
  end
end
