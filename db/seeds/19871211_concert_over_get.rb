key = :concert_over_get
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  band = band_factory(:ms_band)
  band.band_members_from_array %i[VOCAL iChisatoMoritaka SAXOPHONE iGinjiOgawa KEYBOARDS iHisanoriMizuno DRUMS iMasayukiMuraishi PERCUSSION iYouichiOkabe GUITAR iHiroyukiKato CHORUS iYukariFujio]

  concert = concert_factory key, :overheat_night_get_smile do
    {
      concert_type: :live,
      from_id: '1987/12/11',
      to_id: '1988/3/11',
      has_song_list: true,
      sort_order: 198712110,
      num_of_performances: 3,
      num_of_halls: 3,
      band_id: :ms_band
    }
  end

  list = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }

  list.list_contents_from_array %i[good_bye_season namida_good_bye anohino_photograph ringoshuno_rule yumeno_owari weekend_blue get_smile overheat_night new_season]

  concert.concert_halls_from_array [
    ['1987/12/11', :大阪厚生年金会館, nil, '中ホール', 'Medium hall', '「OVERHEAT. NIGHT」', '"OVERHEAT. NIGHT"'],
    ['1987/12/17', :日本青年館, list, nil, nil, '「GET SMILE」|<ビデオ収録>', '"GET SMILE"|<Video Recording>'],
    ['1988/3/11', :徳島ベガホール, nil, nil, nil, '「GET SMILE」', '"GET SMILE"']
  ]
end
