key = :single_benkyono_uta_konomachi
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :benkyono_uta_konomachi do
    {
      device_type: :single,
      event_date_id: '1991/2/10',
      sort_order: 199102100,
      number: '12th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4197.html'
    }
  end

  single.media_from_array [
    ["#{key}_cds1", :cds, 'WPDL-4204', '1st', false, :warner_pioneer, '900'],
    ["#{key}_cds2", :cds, 'WPDL-4204', '2nd', false, :warner_music_japan, '900'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/勉強の歌-この街-home-mix-single/id528977693', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:benkyono_uta, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito]],
    [:konomachi, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito], '[HOME MIX]', '[HOME MIX]'],
  ]
  device_activity_factory single, :thema_song_mischievous_twins
end
