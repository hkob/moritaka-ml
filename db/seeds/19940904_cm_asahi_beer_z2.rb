key = :cm_asahi_beer_z2
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity = activity_sub_factory key do
    {
      activity_id: :cm_asahi_beer,
      j_title: 'アサヒビール 「アサヒ生ビールＺ」(秋冬編)',
      e_title: 'Asahi beer "Asahi draft beer Z" (Fall & Winter version)',
      from_id: '1994/9/4',
      to_id: '1994/12',
      j_comment: 'キャンプ編 [15 秒]|雪山編 [15秒]',
      e_comment: "Camp version [15 seconds]|`on the snow mountain' version [15 seconds]",
      song_id: :sutekina_tanjoubi,
      sort_order: 199409040
    }
  end
end
