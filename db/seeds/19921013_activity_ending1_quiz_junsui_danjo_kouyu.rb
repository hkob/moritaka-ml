pkey = :thema_song_quiz_junsui_danjo_kouyuu
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_factory pkey do
    {
      activity_type: :thema_song,
      title_id: %q(クイズ番組 「クイズ! 純粋男女交遊」|TV program (Quiz) "Quiz! {JUNSUI DANJO KOUYU}(First part)"|くいず　じゅんすいだんじょこうゆう),
      company_id: :tv_asahi,
      sort_order: 199210130,
      song_id: :docchimo_docchi,
    }
  end
end

key = :thema_song_quiz_junsui_danjo_kouyuu_ending1
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :thema_song_quiz_junsui_danjo_kouyuu,
      j_title: 'クイズ番組 「クイズ! 純粋男女交遊」エンディングテーマ(前期)',
      e_title: 'Ending thema song of TV program (Quiz) "Quiz! {JUNSUI DANJO KOUYU}(First part)"',
      from_id: '1992/10/13',
      to_id: '1992/12',
      sort_order: 1,
      song_id: :docchimo_docchi,
    }
  end
end

