key = :single_alone
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :alone do
    {
      device_type: 'ep_single|single',
      event_date_id: '1988/10/25',
      sort_order: 198810250,
      number: '5th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4028.html'
    }
  end

  single.media_from_array [
    ["#{key}_ep", :ep, '07L7-4018', '1st', false, :warner_pioneer, '700'],
    ["#{key}_ct", :ct, '10L5-4018', '1st', false, :warner_pioneer, '1,000'],
    ["#{key}_cds1", :cds, '10L3-4018', '1st', false, :warner_pioneer, '937'],
    ["#{key}_cds2", :cds, '10L3-4018', '2nd', false, :warner_music_japan, '937'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/alone-single/id528969806', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:alone, %i[VOCAL iChisatoMoritaka ARRANGE iShinjiYasuda]],
    [:chase, %i[VOCAL iChisatoMoritaka ARRANGE iTakumiYamamoto]]
  ]
end
