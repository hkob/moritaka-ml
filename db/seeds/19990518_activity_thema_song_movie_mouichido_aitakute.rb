pkey = :movie_mouichido_aitakute
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_factory pkey do
    {
      activity_type: :thema_song,
      title_id: '映画「もういちど逢いたくて 星月童話」|Movie "Moonlight Express"|もういちどあいたくて',
      company_id: :sony_pictures_entertainment,
      sort_order: 199905290,
    }
  end
end

key = :thema_song_movie_mouichido_aitakute
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :movie_mouichido_aitakute,
      j_title: '映画「もういちど逢いたくて 星月童話」主題歌',
      e_title: 'Thema song for Movie "Moonlight Express"',
      from_id: '1999/5/29',
      to_id: '1999/7',
      song_id: :mahiruno_hoshi,
      sort_order: 199905290,
    }
  end
end



