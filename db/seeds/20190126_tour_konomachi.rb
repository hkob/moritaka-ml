key = :tour_konomachi
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  band = band_factory(:white_queens)
  band.band_members_from_array %i[VOCAL iChisatoMoritaka GUITAR iChisatoMoritaka BASS iMasafumiYokoyama BANDLEADER iYuichiTakahashi GUITAR iYuichiTakahashi GUITAR iMariaSuziki]

  concert = concert_factory key do
    {
      concert_type: :tour,
      from_id: '2019/1/28',
      to_id: '2019/2/9',
      has_song_list: true,
      has_product: true,
      sort_order: 201901280,
      num_of_performances: 5,
      num_of_halls: 5,
      band_id: :white_queens
    }
  end

  list_a, list_b = %i[A].map.with_index { |k, i| list_factory("#{key}_#{k}") { {concert_id: concert, keyword: k, sort_order: i+1} } }

  list_a.list_contents_from_array %i[new_season mi_ha_] + @mc + %i[rockn_roll_kencho_shozaichi benkyono_uta snow_again] + @mc + %i[kazeni_fukarete stress seventeen] + @mc + %i[watashiga_obasanni_nattemo ame la_la_sunshine] + @mc + %i[watarasebashi futariwa_koibito kusaimononiwa_futaoshiro] + @mc_member + %i[kibun_soukai get_smile teriyaki_burger] + @encore + @mc + %i[konomachi] + @double_encore + %i[concert_no_yoru]

  concert.concert_halls_from_array [
    ConcertHall.mk('2019/1/28', '狭山市市民会館', list_a, 'とんこつラーメン', '{TONKOTSU-RAMEN}'),
    ConcertHall.mk('2019/2/8', 'ホクト文化ホール', list_a, '不明', 'Unknown'),
    ConcertHall.mk('2019/2/9', '本多の森ホール', list_a, '不明', 'Unknown'),
    ConcertHall.mk('2019/2/23', '北九州ソレイユホール', list_a, 'とんこつラーメン', '{TONKOTSU-RAMEN}'),
    ConcertHall.mk('2019/2/24', 'パトリア日田', list_a, '日田やきそば、団子汁ととり天', '{HITA YAKISOBA}, {DANGO-JIRU} and {TORI-TEN}', '大ホール', 'Big hall'),
  ]
end
