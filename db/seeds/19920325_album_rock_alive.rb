key = :album_rock_alive
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :rock_alive do
    {
      device_type: :album,
      event_date_id: '1992/3/25',
      minutes: 74,
      seconds: 16,
      sort_order: 199203250,
      number: '8th',
      singer_id: :iChisatoMoritaka,
      j_comment: '初回限定 32ページカラー写真集',
      e_comment: '32 page photo book(1st lot only)',
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4046.html'
    }
  end

  album.media_from_array [
    ["#{key}_ct", :ct, 'WPTL-639', :first, false, :warner_music_japan, '2,900'],
    ["#{key}_cd1", :cd, 'WPCL-639', :first, false, :warner_music_japan, '2,900'],
    ["#{key}_cd2", :cd, 'WPC6-8322', :second, true, :warner_music_japan, '2,447'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/rock-alive/id528982379', :first, true, :warner_music_japan, '2,100'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  performer = %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito]
  list.list_contents_from_array [
    [:concert_no_yoru, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito CHORUS iHideoSaito CHORUS iSeijiMatsuura], '[アルバム・ヴァージョン]', '[Album version]'],
    [:yacchimaina, %i[VOCAL iChisatoMoritaka ARRANGE iYasuakiMaejima KEYBOARDS iYasuakiMaejima PROGRAMS iYasuakiMaejima CHORUS iChisatoMoritaka]],
    [:watashiga_obasanni_nattemo, performer],
    [:obasan, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi PIANO iHiromasaIjichi KEYBOARDS iYuichiTakahashi PROGRAMS iYuichiTakahashi CHORUS iYuichiTakahashi BASS iMasafumiYokoyama GUITAR iYukioSeto]],
    [:guitar, performer],
    [:the_blue_blues, %i[VOCAL iChisatoMoritaka ARRANGE iShinKohno CHORUS iChisatoMoritaka FRHODES iShinKohno PROGRAMS iShinKohno GUITAR iYukioSeto]],
    [:fight, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi AGUITAR iYuichiTakahashi PROGRAMS iYuichiTakahashi CHORUS iYuichiTakahashi CHORUS iChisatoMoritaka APIANO iYasuakiMaejima GUITAR iHiroyoshiMatsuo], '[アルバム・ヴァージョン]', '[Album version]'],
    [:furusatono_sora, %i[VOCAL iChisatoMoritaka ARRANGE iTakayukiHattori APIANO iNobuoKurata STRINGS iGAVYNStrings STRINGSCM iGAVYNWRIGHT]],
    [:rock_alive, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito JGUITAR iChisatoMoritaka OTHERINST iHideoSaito PROGRAMS iHideoSaito]],
    [:yowaseteyo_konyadake, %i[VOCAL iChisatoMoritaka ARRANGE iShinKohno KEYBOARDS iShinKohno GUITAR iYukioSeto]],
    [:mitsuketa_saifu, performer],
    [:rhythm_to_bass, %i[VOCAL iChisatoMoritaka ARRANGE iHiroyoshiMatsuo GUITAR iHiroyoshiMatsuo PROGRAMS iHiroyoshiMatsuo KEYBOARDS iHiroyoshiMatsuo CHORUS iGoodLookingMen]],
    [:wakarimashita, %i[VOCAL iChisatoMoritaka ARRANGE iMasafumiYokoyama BASS iMasafumiYokoyama GUITAR iMasafumiYokoyama PROGRAMS iMasafumiYokoyama GUITAR iHiroyoshiMatsuo AGUITAR iYuichiTakahashi CHORUS iYuichiTakahashi]],
    [:bossa_marina, %i[VOCAL iChisatoMoritaka ARRANGE iTakayukiHattori APIANO iNobuoKurata BASS iKenjiTakamizu GUITAR iYukioSeto FRSOLO iYukioSeto ALTOFLUTES iJeffDaly ALTOFLUTES iRonAsprey FHORN iSteveSidwell STRINGS iGAVYNStrings STRINGSCM iGAVYNWRIGHT]],
    [:natsuno_umi, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi GUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi PROGRAMS iYuichiTakahashi CHORUS iYuichiTakahashi BASS iMasafumiYokoyama]],
    [:amenochi_hare, performer],
  ]
end
