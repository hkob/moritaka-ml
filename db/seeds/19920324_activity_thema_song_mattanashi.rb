key = :thema_song_mattanashi
obj = Activity.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity = activity_factory key do
    {
      activity_type: :thema_song,
      title_id: %q(ドラマ「まったナシ!」|TV drama "{MATTANASHI!}" (NTV)|どらま　まったなししゅだいか),
      j_comment: '主題歌',
      e_comment: 'Thema song',
      company_id: :nihon_tv,
      from_id: '1992/7/4',
      to_id: '1992/9/26',
      song_id: :watashiga_obasanni_nattemo,
      sort_order: 199207040
    }
  end
end

