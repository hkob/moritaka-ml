key = :concert_act_against_aids
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  concert = concert_factory key, :act_against_aids97 do
    {
      concert_type: :etc,
      from_id: '1997/12/1',
      has_song_list: true,
      sort_order: 199712010,
      num_of_performances: 1,
      num_of_halls: 1,
    }
  end

  list_a = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }
  list_a.list_contents_from_array %i[snow_again] + @mc + [[:m, [], '(バック・コーラス)', '(Back Chorus)']]

  concert.concert_halls_from_array [
    ['1997/12/1', :日本武道館, list_a],
  ]
end


