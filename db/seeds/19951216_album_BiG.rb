key = :album_BiG
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :BiG do
    {
      device_type: :cover_album,
      event_date_id: '1995/12/16',
      minutes: 48,
      seconds: 01,
      sort_order: 199512160,
      number: '2nd',
      singer_id: :iYumiAdachi
    }
  end

  album.media_from_array [
    ["#{key}_cd", :cd, 'VICL-716', :first, false, :victor_entertainment, '3,000'],
  ]

  vy = %i[VOCAL iYumiAdachi]
  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_with_performer(vy, 'ろっくんろーる中学生', "Rock'n roll {CHUGAKUSEI}") +
    ListContent.title_with_performer(vy, '逃げたいときは', '{NIGETAITOKIHA}') +
    ListContent.title_with_performer(vy, '風の中のダンス', '{KAZU-NO NAKA-NO Dance}') +
    ListContent.title_with_performer(vy, '散歩', '{SANPO}') +
    ListContent.title_with_performer(vy, '胸のリボンを結ぼう', '{MUNE-NO RIBON-O MUSUBO}') +
    ListContent.title_with_performer(vy, '革命', '{KAKUMEI}') +
    ListContent.title_with_performer(vy, "帰ってきたどーした!安達'95", "{KAETTEKITA DO-SHITA! Adachi'95}") +
    ListContent.title_with_performer(vy, '泣いてもいいよ', '{NAITEMO IIYO}') +
    [[:kibun_soukai, %i[VOCAL iYumiAdachi ARRANGE iAtsutoshiYamaji]]] +
    ListContent.title_with_performer(vy, '誓いのブリッジ', '{CHIKAI-NO Bridge}')
  )
end
