key = :picture_book_peachberry
obj = Book.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  book = book_factory key, :peachberry do
    {
      book_type: :picture_book,
      title_id: :peachberry,
      publisher_id: :up_front_books,
      seller_id: :wani_books,
      photographer_id: :iKenjiMiura,
      isbn: '4-8470-2474-5 C0076 P3000E',
      price: '3,150',
      event_date_id: '1997/11/4',
      sort_order: 199711040,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4473.html'
    }
  end
end

