key = :concert_festival_1993
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  concert = concert_factory key, :festival_1993 do
    {
      concert_type: :festival,
      from_id: '1993/11/3',
      has_song_list: true,
      has_product: true,
      sort_order: 199311030,
      num_of_performances: 1,
      num_of_halls: 1
    }
  end

  list = list_factory("#{key}_A") { {concert_id: concert, keyword: :A, sort_order: 1} }

  list.list_contents_from_array [
    :teo_tatako,
    :rockn_roll_kencho_shozaichi,
    [:fight, [], '(ショートヴァージョン)', '(Short version)'],
    :memories,
    [nil, [], nil, nil, 'MC((学園祭の食べ物)(ツアー行ったことある?) (生で見てどう?)', 'MC(about food, tour, and her impression)'],
    [nil, [], nil, nil, 'メンバー紹介 (ボーカルは?)(バンド名…学生のボケ→ブーイング(^_^;))', 'Introduction of band members'],
    :benkyono_uta,
    :alone,
    [nil, [], nil, nil, 'MC(楽器やってる) (大学生の楽しみって?)(業界人になりたい?)', 'about musical instruments, university students'],
    [:ame, [], '(エレピ付)', '(With E-piano)'],
    :i_love_you,
    :jimina_onna,
    [nil, [], nil, nil, 'MC(いつから知ってる？),(いくつ？→３つ(^_^;)),(東海大や湘南校舎について → rich(^_^;))', 'about Chisato.,about Tokai university'],
    [:watarasebashi, [], '(リコーダー付)', '(with Recorder)'],
    [nil, [], nil, nil, 'MC(どの曲が好き？)', "``What songs do the audience like?''"],
    [:haeotoko, [], '(♪ハ〜エおとこっ♪の大合唱付フルヴァージョン(^_^;))', "sing in chorus as ``HAEOTOKO''"],
    :watashiga_obasanni_nattemo,
    :teriyaki_burger
  ] + @encore + [
    :kazeni_fukarete,
    [nil, [], nil, nil, 'MC(ツアーにきてネ),(九州に行ったことある?),(学生なんで金欠 → ここの学生はリッチ(^_^;))', "about tour, kyusyu, about Tokai University's students"],
    :watashino_natsu
  ]

  concert.concert_halls_from_array [
    ['1993/11/3', :東海大学_湘南校舎, list],
  ]
end
