key = :live_the_new_season_do_my_best
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  band = band_factory(:lost_in_space)
  band.band_members_from_array %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka RECORDER iChisatoMoritaka DRUMS iShinjiYasuda GUITAR iYuichiTakahashi BASS iMasafumiYokoyama KEYBOARDS iYasuakiMaejima BANDLEADER iYuichiTakahashi]

  concert = concert_factory key do
    {
      concert_type: :live,
      from_id: '2013/3/30',
      to_id: '2013/5/11',
      has_song_list: true,
      has_product: true,
      sort_order: 201303300,
      num_of_performances: 6,
      num_of_halls: 4,
      band_id: :lost_in_space
    }
  end

  list_a, list_b, list_c, list_d = %i[A B C D].map.with_index { |k, i| list_factory("#{key}_#{k}") { {concert_id: concert, keyword: k, sort_order: i+1} } }
  list_a.list_contents_from_array %i[rockn_roll_kencho_shozaichi zarude_mizukumu_koigokoro] + @mc + %i[new_season mi_ha_] + @mc_member + %i[michi watashiga_obasanni_nattemo] + @mc + %i[the_stress sweet_candy futariwa_koibito] + ListContent.title_only('衣装替え(足利散策ビデオ: 2012年5月収録)', 'Costume change(Video for Ashikaga walking: recorded at May 2012)') + %i[watarasebashi ichido_asobini_kiteyo] + @mc + %i[la_la_sunshine kibun_soukai konomachi] + @mc + %i[anatawa_ninkimono get_smile] + @mc + %i[teriyaki_burger] + @encore + %i[ame] + @mc + %i[concert_no_yoru] + @double_encore + %i[kyoukara]
  list_b.list_contents_from_array %i[rockn_roll_kencho_shozaichi my_anniversary] + @mc + %i[new_season mi_ha_] + @mc_member + %i[michi watashiga_obasanni_nattemo] + @mc + %i[the_stress sweet_candy futariwa_koibito] + ListContent.title_only('衣装替え(足利散策ビデオ: 2012年5月収録)', 'Costume change(Video for Ashikaga walking: recorded at May 2012)') + %i[watarasebashi ichido_asobini_kiteyo] + @mc + %i[seventeen kibun_soukai konomachi] + @mc + %i[anatawa_ninkimono get_smile] + @mc + %i[teriyaki_burger] + @encore + %i[ame] + @mc + %i[concert_no_yoru] + @double_encore + %i[kyoukara]
  list_c.list_contents_from_array %i[rockn_roll_kencho_shozaichi zarude_mizukumu_koigokoro] + @mc + %i[new_season mi_ha_] + @mc_member + %i[michi watashiga_obasanni_nattemo] + @mc + %i[the_stress sweet_candy futariwa_koibito] + ListContent.title_only('衣装替え(足利散策ビデオ: 2012年5月収録)', 'Costume change(Video for Ashikaga walking: recorded at May 2012)') + %i[watarasebashi ichido_asobini_kiteyo] + @mc + %i[kibun_soukai seventeen konomachi] + @mc + %i[anatawa_ninkimono get_smile] + @mc + %i[teriyaki_burger] + @encore + %i[ame] + @mc + %i[concert_no_yoru] + @double_encore + %i[mite]
  list_d.list_contents_from_array %i[rockn_roll_kencho_shozaichi my_anniversary] + @mc + %i[new_season mi_ha_] + @mc_member + %i[michi watashiga_obasanni_nattemo] + @mc + %i[the_stress sweet_candy futariwa_koibito] + ListContent.title_only('衣装替え(足利散策ビデオ: 2012年5月収録)', 'Costume change(Video for Ashikaga walking: recorded at May 2012)') + %i[watarasebashi ichido_asobini_kiteyo] + @mc + %i[kibun_soukai seventeen konomachi] + @mc + %i[anatawa_ninkimono get_smile] + @mc + %i[teriyaki_burger] + @encore + %i[ame] + @mc + %i[concert_no_yoru] + @double_encore + %i[mite]

  concert.concert_halls_from_array [
    ConcertHall.mk('2013/3/30', :足利市民会館, list_a, '?', '?'),
    ConcertHall.mk('2013/3/31', :足利市民会館, list_b, '?', '?'),
    ConcertHall.mk('2013/4/20', :オリックス劇場, list_c, '?', '?'),
    ConcertHall.mk('2013/4/22', :中野サンプラザ, list_d, '?', '?'),
    ConcertHall.mk('2013/4/23', :中野サンプラザ, list_d, '?', '?'),
    ConcertHall.mk('2013/5/11', :日本特殊陶業市民会館, list_c, '金のシャチホコ/味噌煮込みうどん', 'Gold {SHACHIHOKO}/{MISO-NIKOMI UDON}', 'フォレストホール', 'Forest hall'),
  ]
end

