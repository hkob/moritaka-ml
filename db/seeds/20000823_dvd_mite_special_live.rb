key = :dvd_mite_special_live
obj = Video.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  video = video_factory key do
    {
      device_type: :live_video,
      event_date_id: '2000/8/23',
      minutes: 55,
      sort_order: 200008231,
      number: '2nd',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4497.html'
    }
  end

  video.media_from_array [
    ["#{key}_dvd", :dvd, 'WPB6-90003', :first, true, :warner_vision_japan, '4,104'],
  ]

  list = list_factory("#{key}_common") { {device_id: video, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:the_stress, @vc, '[ストレス中近東ヴァージョン]', '[Stress {CHUUKINTOU} version]'],
    [:watashiga_hen, @vc],
    [:yurusenai, @vc],
    [:seventeen, @vc],
    [:the_loco_motion, @vc],
    [:overheat_night_2, @vc],
    [:let_me_go, @vc],
    [:wakareta_onna, @vc],
    [:new_season, @vc],
    [:mi_ha_, @vc],
    [:mite, @vc],
    [:alone, @vc],
  ]

  concert = concert_factory :live_mite_special
  concert_hall = concert_hall_factory concert, 1
  concert_list = list_factory(:live_mite_special_A)
  concert_list_contents = concert_list.list_contents.order_sort_order
  csos = { 0 => 0, 1 => 1, 2 => 3, 3 => 7, 4 => 8, 5 => 9, 6 => 10, 7 => 13, 8 => 15, 9 => 17, 10 => 18, 11 => 19 }

  list.list_contents.order_sort_order.each_with_index do |vsls, i|
    concert_video_factory concert_list_contents[csos[i]], vsls, concert_hall
  end
end

