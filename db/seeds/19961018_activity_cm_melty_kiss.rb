key = :cm_meiji_seika_melty_kiss
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_meiji_seika,
      j_title: '明治製菓「Melty Kiss」',
      e_title: 'Meiji-Seika "Melty Kiss"',
      from_id: '1996/10/18',
      to_id: '1997/2',
      j_comment: '15 秒版 / 30 秒版|ニュージーランドにて撮影',
      e_comment: '15 seconds version / 30 seconds version|Make a film in New Zealand',
      sort_order: 199610180,
      song_id: :ginirono_yume
    }
  end
end
