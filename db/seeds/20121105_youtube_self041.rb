yt = :sc041

yt_hash = {
  '2012/11/5' => {
    link: 'ZM2ksjNtDBY',
    number: '41',
    song_id: :futariwa_koibito,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」41曲目は、\n1995年発表の25thシングル「二人は恋人」！\nドラムは森高千里による新録音！"
  },
  '2012/11/11' => {
    link: 'vTpFMBzp1zI',
    number: '42',
    song_id: :every_day,
    comment: "作詞：森高千里  作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」42曲目は、\n1999年発表の40thシングル「一度遊びに来てよ'99」のカップリング曲！\nドラムは森高千里による新録音！"
  },
  '2012/11/26' => {
    link: 'EBWE-sC3tgY',
    number: '43',
    song_id: :itsumono_mise,
    comment: "作詞：森高千里　作曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」43曲目は、\n1998年発表の13thアルバム「Sava Sava」より「いつもの店」！\n気鋭のジャズ・ギタリスト・太田雄二さんをフィーチャー！\n\n衣装協力　スタイルミー・MET"
  },
  '2012/11/29' => {
    link: 'r4AUczQcGoo',
    number: '44',
    song_id: :office_gaino_koi,
    comment: "作詞：森高千里　作曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」44曲目は、\n1994年発表の9thアルバム「STEP BY STEP」より「オフィス街の恋」！\n気鋭のジャズ・ギタリスト・太田雄二さんをフィーチャー！\n\n衣装協力：アガット"
  },
  '2012/12/1' => {
    link: '9tlia4Es7AQ',
    number: '45',
    song_id: :jin_jin_jinglebell,
    comment: "作詞・作曲：森高千里　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」45曲目は、\n1995年発表の「ジンジンジングルベル」！\nドラムは森高千里によるループドラム！"
  },
  '2012/12/8' => {
    link: '6oM8TmPypac',
    number: '46',
    song_id: :the_blue_blues,
    comment: "作詞：森高千里　作曲：河野伸\n\n公式チャンネル独占企画「200曲セルフカヴァー」46曲目は、\n1992年発表のアルバム「ROCK ALIVE」から「THE BLUE BLUES」！\n気鋭のジャズ・ギタリスト・太田雄二さんをフィーチャー！\n\n衣装協力：AHKAH"
  },
  '2012/12/10' => {
    link: 'lLNm886nss4',
    number: '47',
    song_id: :snow_again,
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」47曲目は、\n1997年発表のシングル「SNOW AGAIN」！\nドラムは森高千里によるループドラム！\n\n衣装協力：ヴィヴィアンタム"
  },
  '2012/12/14' => {
    link: 'Wedz3CwCRCI',
    number: '48',
    song_id: :rockn_omelette,
    comment: "作詞：森高千里　作曲：伊秩弘将　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」48曲目は、\n1994年発表のシングル「ロックン・オムレツ」！\nドラムは森高千里による新録音！"
  },
  '2012/12/17' => {
    link: 'iX7MtgtfIpM',
    number: '49',
    song_id: :benkyono_uta,
    comment: "作詞：森高千里　作曲：斉藤英夫　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」49曲目は、\n1991年発表のシングル「勉強の歌」！\nドラムは森高千里によるループドラム！\n\n衣装協力：HOLLYWOOD RANCH MARKET・HUMAN WOMAN"
  },
  '2012/12/22' => {
    link: 'PTqNNasYj_4',
    number: '50',
    song_id: :new_season,
    comment: "作詞：HIRO　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」50曲目は、2012年11月24日に横浜BLITZにて行われたYouTube公開収録から、1987年発表のデビューシングル「NEW SEASON」です！"
  },
  '2012/12/26' => {
    link: 'A3CgE4pP1oc',
    number: '51',
    song_id: :yowaseteyo_konyadake,
    comment: "作詞・作曲：森高千里　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」51曲目は、\n1992年発表のアルバム「ROCK ALIVE」から「酔わせてよ今夜だけ」！\n\n衣装協力：ツールフェイス"
  },
  '2012/12/28' => {
    link: 'beXwKZiyBlY',
    number: '52',
    song_id: :rockn_roll_kencho_shozaichi,
    comment: "作詞・作曲：森高千里　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」52曲目は、2012年11月24日に横浜BLITZにて行われたYouTube公開収録から、1992年発表の「ロックンロール県庁所在地」です！"
  },
  '2012/12/31' => {
    link: 'GZ9m6-Q-ddo',
    number: '53',
    song_id: :ichigatsu_ichijitsu,
    comment: "作詞：千家尊福　作曲：上真行　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」53曲目は、\n1995年発表のシングル「ジンジンジングルベル」のC/W「一月一日」！"
  },
  '2013/1/2' => {
    link: 'Aq7XdSIPTUQ',
    number: '54',
    song_id: :get_smile,
    comment: "作詞・伊秩弘将　作曲：島健　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」54曲目は、2012年11月24日に横浜BLITZにて行われたYouTube公開収録から、1988年発表の3rdシングル「GET SMILE」です！\n\nYouTube収録用として、3曲目に歌った「GET SMILE」。\nテイク1収録後、スタッフから「振りが中途半端」というダメ出しを受け、\n森高自らがヒールを脱いでテイク2を歌うことを志願、\n会場は大いに盛り上がりました。\nその模様も含めてご覧ください！"
  },
  '2013/1/5' => {
    link: 'QvwOs4J6Ojs',
    number: '55',
    song_id: :seventeen,
    comment: "作詞:有馬三恵子　作曲：筒美京平\n\n公式チャンネル独占企画「200曲セルフカヴァー」55曲目は、2012年11月24日に横浜BLITZにて行われたYouTube公開収録から、1989年発表の7thシングル「17才」です！\n\nYouTube収録用として、最後（4曲目）に歌った「17才」。\nテイク１収録後、盛り上がった客席から「もう１回！」の熱いアンコールが起こりました。\n森高自身もそれに応えるかたちで（笑）、テイク２を歌うことに！\n会場の盛り上がりも最高潮に！\nその当日のやりとりも含めてご覧ください！"
  },
  '2013/1/9' => {
    link: 'xtIZl8muvdU',
    number: '56',
    song_id: :ginirono_yume,
    comment: "作詞：森高千里　作曲：伊秩弘将　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」56曲目は、1996年発表のシングル「銀色の夢」！\nドラムは森高千里によるループドラム！"
  },
  '2013/1/15' => {
    link: '54yIMQYwQlY',
    number: '57',
    song_id: :hoshino_ojisama,
    comment: "作詞：森高千里　作曲：前嶋康明\n\n公式チャンネル独占企画「200曲セルフカヴァー」57曲目は、\n1994年発表のアルバム「STEP BY STEP」から「星の王子様」！\n気鋭のジャズ・ギタリスト・太田雄二さんをフィーチャー！\n\n衣装協力：MET/メット& STAR JEWELRY"
  },
  '2013/1/19' => {
    link: 'Yqe7lV6NQNs',
    number: '58',
    song_id: :busters_blues,
    comment: "作詞：森高千里　作曲：斉藤英夫\n\n公式チャンネル独占企画「200曲セルフカヴァー」58曲目は、\n1990年発表のアルバム「古今東西」から「ザ・バスターズ・ブルース」！\n気鋭のジャズ・ギタリスト・太田雄二さんをフィーチャー！"
  },
  '2013/1/26' => {
    link: 'Ep6Orwoh6T8',
    number: '59',
    song_id: :misaki,
    comment: "作詞：森高千里　作曲：斉藤英夫　編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」59曲目は、1990年発表のアルバム「古今東西」から「岬」！\n\n衣装協力：アーカー"
  },
  '2013/1/30' => {
    link: '_IiBjrYgips',
    number: '60',
    song_id: :sutekina_tanjoubi,
    comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」60曲目は、1994年発表のシングル「素敵な誕生日」！\nドラムは森高千里によるループドラム！\n\n衣装協力：HUMAN WOMAN"
  },
}
create_youtube_factory_from_hash(yt, yt_hash)
