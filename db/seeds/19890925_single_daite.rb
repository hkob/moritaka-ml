key = :single_daite
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :daite do
    {
      device_type: :single,
      event_date_id: '1989/9/25',
      sort_order: 198909250,
      number: '8th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4169.html'
    }
  end

  single.media_from_array [
    ["#{key}_ct", :ct, '09L5-4105', '1st', false, :warner_pioneer, '???'],
    ["#{key}_cds1", :cds, '09L3-4105', '1st', false, :warner_pioneer, '937'],
    ["#{key}_cds2", :cds, '09L3-4105', '2nd', false, :warner_music_japan, '937'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/daite-rasubegasu-vu-ajon-single/id528968805', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [ :daite, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi], '[ラスベガス・ヴァージョン]', '[Las Vegas version]'],
    [ :wakasugita_koi, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi]]
  ]
end
