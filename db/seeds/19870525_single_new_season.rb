key = :single_new_season
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :new_season do
    {
      device_type: :ep_single,
      event_date_id: '1987/5/25',
      sort_order: 198705250,
      number: '1st',
      singer_id: :iChisatoMoritaka,
      j_comment: 'デビューシングル',
      e_comment: 'Debut Single',
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4005.html'
    }
  end
  single.media_from_array [
    ["#{key}_lp", :ep, 'K-1564', '1st', false, :warner_pioneer, '700'],
    ["#{key}_ct", :ct, 'LKC-2025', '1st', false, :warner_pioneer, '1,000'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/new-season-single/id528968883', '1st', true, :warner_music_japan, '500']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }

  list.list_contents_from_array [
    [:new_season, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito KEYBOARD_SOLO iChisatoMoritaka]],
    [:period, @vcah]
  ]
end
