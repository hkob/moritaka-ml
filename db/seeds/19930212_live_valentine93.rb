key = :live_valentine_93
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  concert = concert_factory key do
    {
      concert_type: :live,
      from_id: '1993/2/12',
      has_song_list: true,
      sort_order: 199302120,
      num_of_performances: 1,
      num_of_halls: 1,
    }
  end

  concert.concert_halls_from_array [
    ['1993/2/12', :名古屋センチュリーホール, nil, nil, 'サークルK 東海テレビ 主催', 'Circle K, Tokai TV presents'],
  ]
end


