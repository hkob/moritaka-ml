key = :album_kaze
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :kaze do
    {
      device_type: 'lyric_album|part_album',
      event_date_id: '1990/10/25',
      minutes: 40,
      seconds: 23,
      sort_order: 199010250,
      number: '1st',
      singer_id: :iYuichiTakahashi,
    }
  end

  album.media_from_array [
    ["#{key}_ct", :ct, 'WPTL-185', :first, false, :warner_pioneer, '2,900'],
    ["#{key}_cd", :cd, 'WPCL-185', :first, false, :warner_pioneer, '2,900'],
  ]

  vy = %i[VOCAL iYuichiTakahashi]
  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [nil, vy, nil, nil, 'あの夏のコンパニオン', '{ANO NATSU-NO Companion}: [Companion in that summer]'],
    [nil, vy, nil, nil, 'タヒチの夜', '{TAHICHI-NO YORU}: [Night in Tahichi]'],
    [nil, vy, nil, nil, '夢', '{YUME}: [Dream]'],
    [nil, vy, nil, nil, '十月の雨', '{JUGATSU-NO AME}: [October rain]'],
    [:koiwa_norikonaseinaimono, %i[VOCAL iYuichiTakahashi ARRANGE iYuichiTakahashi CHORUS iChisatoMoritaka]],
    [:kumorizora, %i[VOCAL iYuichiTakahashi ARRANGE iYuichiTakahashi]],
    [nil, vy, nil, nil, '牧場の息子', '{BOKUJO-NO MUSUKO}: [Son of a ranch]'],
    [nil, vy, nil, nil, 'インディアン', 'Indian'],
    [nil, vy, nil, nil, '由比ヶ浜', 'Yuigahama'],
    [nil, vy, nil, nil, '宣教者', '{SENKYOSHA}: [A Missionary]'],
    [nil, vy, nil, nil,' 風', '{KAZE}: [Wind]'],
  ]
end



