key = :essay_step_by_step
obj = Book.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  book = book_factory key, :step_by_step do
    {
      book_type: :essay,
      publisher_id: :nippon_housou_project,
      seller_id: :fusousha,
      author_id: :iChisatoMoritaka,
      isbn: '4-594-02034-8 C0095 P1300E',
      price: '1,300',
      event_date_id: '1996/9/30',
      sort_order: 199609300,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4470.html'
    }
  end

  list = list_factory("#{key}_A") { {book_id: book, keyword: :A, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_only('上京', 'Go up to Tokyo') +
    ListContent.title_only('学級委員長', 'Chairperson of the class') +
    ListContent.title_only('ナマリ', 'Dialect') +
    ListContent.title_only('幼稚園の先生', 'Teacher of kindergarten') +
    ListContent.title_only('無愛想', 'Unamiableness') +
    ListContent.title_only('アイドル', 'idol') +
    ListContent.title_only('ル・パラディ', 'lu. Parady') +
    ListContent.title_only('役者', 'An actress') +
    ListContent.title_only('一人暮らし', 'Live alone') +
    ListContent.title_only('ノンジャンル', 'Non category') +
    ListContent.title_only('夏休み', 'Summer vacation') +
    ListContent.title_only('作詞', 'Lyric') +
    ListContent.title_only('変化', 'Change') +
    ListContent.title_only('太っていた頃', 'When I was a plump girl,...') +
    ListContent.title_only('実家', 'Parental home') +
    ListContent.title_only('ミニスカート，ハイヒール，ロングヘア', 'Mini skirt, high heels, long hair') +
    ListContent.title_only('ウワサ', 'Rumor') +
    ListContent.title_only('秋', 'Autumn') +
    ListContent.title_only('旅行', 'Travel') +
    ListContent.title_only('渡良瀬橋', 'Watarase-bashi') +
    ListContent.title_only('お出掛け', 'Go out shopping(?)') +
    ListContent.title_only('CM') +
    ListContent.title_only('白いブーツ', 'White boots') +
    ListContent.title_only('長いお休み', 'Long interval') +
    ListContent.title_only('エピローグ', 'Epilog') +
    ListContent.title_only('In Other Words') +
    ListContent.title_only('Discography')
  )
end

