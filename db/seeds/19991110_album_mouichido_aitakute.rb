key = :album_mouichido_aitakute
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :mouichido_aitakute_sound_track do
    {
      device_type: 'compilation_album',
      event_date_id: '1999/5/29',
      minutes: 47,
      seconds: 12,
      singer_id: :iVariousArtists,
      sort_order: 199905290,
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'ZMCZ-552', :first, false, :media_factory, '3,150'],
  ]

  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_only('Opening Thema') +
    ListContent.title_only('Car Accident') +
    ListContent.title_only('My God') +
    ListContent.title_only('家賃', '{YACHIN}') +
    ListContent.title_only('Hitomi') +
    ListContent.title_only('そこにある受話器', '{SOKO-NI ARU JUWAKI}') +
    ListContent.title_only('謎', '{NAZO}') +
    ListContent.title_only('ムーンライト・ララバイ', 'Moonlight {RARABAI}') +
    ListContent.title_only('もういちど逢いたくてメインテーマ', 'Main thema of {MOUICHIDO AITAKUTE}', '(ピアノバージョン)', '(Piano version)') +
    ListContent.title_only('Just When I Needed You Must', 'Just When I Needed You Must', '(アコースティックバージョン)', '(Acoustic version)') +
    ListContent.title_only('戦闘', '{SENTOU}') +
    ListContent.title_only('Floppy Disk') +
    ListContent.title_only('Rain') +
    ListContent.title_only('Orgel') +
    ListContent.title_only('もういちど逢いたくてメインテーマ', 'Main thema of {MOUICHIDO AITAKUTE}') +
    [[:mahiruno_hoshi, %i[ARRANGE iAkihikoMatsumoto VOCAL iChisatoMoritaka KEYBOARDS iAkihikoMatsumoto SYNTHESIZER iAkiraMorishita PROGRAMMING iAkiraMorishita GUITAR iMasayoshiFurukawa BGVOCAL iKumiSasaki BGVOCAL iYurieKokubu STRINGS iKoikeHiroyukiStrings]]] +
    ListContent.title_only('もういちど逢いたくてメインテーマ', 'Main thema of {MOUICHIDO AITAKUTE}', '(フルスコアバージョン)', '(Full score version)')
  )
end
