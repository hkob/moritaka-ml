key = :picture_book_shuka
obj = Book.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  book = book_factory key, :shuka do
    {
      book_type: :picture_book,
      publisher_id: :kindai_eigasha,
      photographer_id: :iYutakaNishimura,
      isbn: '4-7648-1448-X C0076 P1854E',
      price: '1,854',
      event_date_id: '1987/8/5',
      sort_order: 198708050,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4457.html'
    }
  end

  list = list_factory("#{key}_A") { {book_id: book, keyword: :A, sort_order: 1} }

  list.list_contents_from_array [
    [nil, [], nil, nil, '哀楽・都', 'Airaku - Miyako'],
    [nil, [], nil, nil, '春華・章', 'Shunka - Sho'],
    [nil, [], nil, nil, '夜楽・調', 'Yaraku - Shirabe'],
    [nil, [], nil, nil, '眩耀・女', 'Genyo - Onnna'],
    [nil, [], nil, nil, '失楽・園', 'Shitsuraku - En'],
  ]
end
