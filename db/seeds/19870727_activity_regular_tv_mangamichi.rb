key = :regular_tv_mangamichi
obj = Activity.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_factory key do
    {
      activity_type: :regular_tv,
      title_id: %q(銀河テレビ小説 まんが道・青春編|MANGA MICHI -- SEISHUN-HEN|ぎんがてれびしょうせつ　まんがまち　せいしゅんへん),
      company_id: :nhk,
      from_id: '1987/7/27',
      to_id: '1987/8/14',
      sort_order: 198707270,
    }
  end
end
