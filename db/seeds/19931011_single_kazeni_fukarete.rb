key = :single_kazeni_fukarete
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :kazeni_fukarete do
    {
      device_type: :single,
      event_date_id: '1993/10/11',
      sort_order: 199310110,
      number: '20th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4222.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'WPDL-4365', '1st', false, :warner_music_japan, '1,000'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/風に吹かれて-single/id531609165', '1st', true, :warner_music_japan, '750']
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:kazeni_fukarete, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka BASS iHideoSaito SYNTHESIZER iHideoSaito TAMBOURINE iHideoSaito AGUITAR iHideoSaito AGUITAR iHiroyoshiMatsuo AGUITAR iYuichiTakahashi AGUITAR iJunTakahashi]],
    [:kyushu_sodachi, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito DRUMS iChisatoMoritaka BASS iHideoSaito GUITAR iHideoSaito SYNTHESIZER iHideoSaito TAMBOURINE iHideoSaito CHORUS iHideoSaito AGUITAR iHideoSaito AGUITAR iHiroyoshiMatsuo AGUITAR iYuichiTakahashi AGUITAR iJunTakahashi]],
    [:kazeni_fukarete, %i[VOCAL iKaraoke ARRANGE iHideoSaito DRUMS iChisatoMoritaka BASS iHideoSaito SYNTHESIZER iHideoSaito TAMBOURINE iHideoSaito AGUITAR iHideoSaito AGUITAR iHiroyoshiMatsuo AGUITAR iYuichiTakahashi AGUITAR iJunTakahashi], '[オリジナル・カラオケ]', '[Original karaoke]'],
]
  device_activity_factory single, :cm_ana
end
