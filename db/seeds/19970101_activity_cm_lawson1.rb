pkey = :cm_lawson
obj = Activity.find_by(key: pkey)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  company_factory :lawson
  activity_factory pkey do
    {
      activity_type: :cm,
      title_id: 'exist|lawson',
      company_id: :lawson,
      sort_order: 199701010,
    }
  end
end

key = :cm_lawson_intro
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_lawson,
      j_title: 'ローソン(LAWSON)',
      e_title: 'LAWSON',
      from_id: '1997/1/1',
      to_id: '1997/1/4',
      j_comment: '共演: 細野晴臣, 高嶋政伸, 中山エミリ, つぶやきシロー, 野村宏伸, 篠原ともえ|ご挨拶編 (一瞬映るだけ) [15 秒]|ご挨拶編 (細野さんがTV を叩く) [30 秒]',
      e_comment: 'Coactor: Haruomi Hosono, Masanobu Takashima, Emiri Nakayama, Shiro Tsubuyaki, Hironobu Nomura, Tomoe Shinohara|Introduction version [15 seconds]|Introduction version [30 seconds]',
      sort_order: 199701010,
      song_id: :lets_go
    }
  end
end

key = :cm_lawson_skip
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_lawson,
      j_title: 'ローソン(LAWSON)',
      e_title: 'LAWSON',
      from_id: '1997/1/1',
      to_id: '1997/1/4',
      j_comment: '共演: 細野晴臣|スキップ編 [15 秒]',
      e_comment: 'Coactor: Haruomi Hosono|Skip version [15 seconds]',
      sort_order: 19970102,
      song_id: :lets_go
    }
  end
end

key = :cm_lawson_milk
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_lawson,
      j_title: 'ローソン 「ローソン牛乳」',
      e_title: 'LAWSON "LAWSON milk"',
      from_id: '1997/1/4',
      to_id: '1997/1/24',
      j_comment: '共演: 細野晴臣, 高嶋政伸|短縮版 [15 秒]|「一緒にか?」編[30 秒]',
      e_comment: 'Coactor: Haruomi Hosono, Masanobu Takashima|Short version [15 seconds]|"ISSHONIKA?" version[30 seconds]',
      sort_order: 19970104,
      song_id: :lets_go
    }
  end
end

key = :cm_lawson_osushi
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_lawson,
      j_title: 'ローソン 「お寿司」',
      e_title: 'LAWSON "{SUSHI}"',
      from_id: '1997/3/7',
      to_id: '1997/3/27',
      j_comment: '共演: 細野晴臣, 高嶋政伸|「ローソンに行かなくちゃ」編 [30 秒]|「食べたじゃない」編 [30 秒]',
      e_comment: %q{Coactor: Haruomi Hosono, Masanobu Takashima|"I've got to go to LAWSON" version [30 seconds]|"You ate them, don't you?" version [30 seconds]},
      sort_order: 19970307,
      song_id: :lets_go
    }
  end
end

key = :cm_lawson_natural_bakery
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_lawson,
      j_title: 'ローソン 「ナチュラルベーカリー」',
      e_title: 'LAWSON "Natural Bakery"',
      from_id: '1997/3/28',
      to_id: '1997/5/1',
      j_comment: '共演: 細野晴臣, 高嶋政伸|[15 秒]|[30 秒]',
      e_comment: 'Coactor: Haruomi Hosono, Masanobu Takashima|[15 seconds]|[30 seconds]',
      sort_order: 19970328,
      song_id: :lets_go
    }
  end
end

key = :cm_lawson_card
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_lawson,
      j_title: 'ローソン 「ローソンカード」',
      e_title: 'LAWSON "LAWSON Card"',
      from_id: '1997/5/2',
      to_id: '1997/6/27',
      j_comment: '共演: 細野晴臣, 高嶋政伸|細野晴臣ナレーション版 [15 秒]|森高千里ナレーション版 [15 秒]',
      e_comment: "Coactor: Haruomi Hosono, Masanobu Takashima|Hosono's talk version [15 seconds]|Chisato's talk version [15 seconds]",
      sort_order: 19970502,
      song_id: :lets_go
    }
  end
end

