key = :album_sava_sava
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :sava_sava do
    {
      device_type: :album,
      event_date_id: '1998/9/9',
      minutes: 48,
      seconds: 0,
      sort_order: 199809090,
      number: '16th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4145.html',
    }
  end

  album.media_from_array [
    ["#{key}_cd1", :cd, 'EPCE-5004', :first, false, :zetima, '3,059'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/sava-sava/id278709039', :first, true, :up_front_works, '2,000'],
  ]

  list_common = list_factory("#{key}_common") { {device_id: album, keyword: :common, sort_order: 1} }
  list_common.list_contents_from_array [
    [:utopia, %i[ARRANGE iShinKohno VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka KEYBOARDS iShinKohno PROGRAMMING iShinKohno EGUITAR iShunsukeSuzuki AGUITAR iYuichiTakahashi BASS iNaoyukiIrie CELLO iMasaakiShigematsu CELLO iYasuoMaruyama]],
    [:telephone, %i[ARRANGE iShunsukeSuzuki VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka GUITAR iShunsukeSuzuki UKULELE iShunsukeSuzuki SITAR iShunsukeSuzuki PROGRAMMING iShunsukeSuzuki BASS iHideyukiKomatsu PROGRAMMING iYuichiTakahashi], '[Album Version]', '[Album Version]'],
    [:tanpopono_tane, %i[ARRANGE iShunsukeSuzuki VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka GUITAR iShikaoSuga BASS iShikaoSuga CHORUS iShikaoSuga MANIPULATE iFumitoshiNakamura]],
    [:itsumono_mise, %i[ARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka AGUITAR iYuichiTakahashi PROGRAMMING iYuichiTakahashi PIANO iShinHashimoto FRHODES iShinHashimoto EGUITAR iYukioSeto]],
    [:nagasarete, %i[ARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PROGRAMMING iYuichiTakahashi PIANO iYasuakiMaejima FRHODES iYasuakiMaejima GUITAR iYukioSeto]],
    [:two_of_me, %i[ARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka GUITAR iYuichiTakahashi PROGRAMMING iYuichiTakahashi BASS iYukioSeto GUITARSOLO iYukioSeto]],
    [:umimade_5fun, %i[STRINGSARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka PROGRAMMING iYuichiTakahashi FRHODES iShinKohno GUITAR iYukioSeto PERCUSSION iYukioSeto], '(Album Version)', '(Album Version)'],
    [:wasurekaketeta_yume, %i[ARRANGE iShunsukeSuzuki VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka GUITAR iShunsukeSuzuki PEDALSTEEL iShunsukeSuzuki MANDOLIN iShunsukeSuzuki BASS iYukioSeto WINDCHIME iYukioSeto]],
    [:zarude_mizukumu_koigokoro, %i[ARRANGE iCoil VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka ALLINSTEXCEPTDRUMS iCoil CHORUS iCoil]],
    [:kikenna_hodou, %i[ARRANGE iYuichiTakahashi VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka EGUITAR iChisatoMoritaka AGUITAR iYuichiTakahashi EGUITAR iYuichiTakahashi PROGRAMMING iYuichiTakahashi PIANO iShinHashimoto]],
    [:snow_again, %i[VOCAL iChisatoMoritaka DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka AGUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi PIANO iShinHashimoto KEYBOARDS iShinHashimoto BASS iYukioSeto EGUITAR iYukioSeto]],
  ]
end
