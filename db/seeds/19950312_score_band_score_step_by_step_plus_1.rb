key = :score_band_score_step_by_step_plus_1
obj = Book.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  book = book_factory key do
    {
      book_type: :score_book,
      publisher_id: :shinko_music,
      isbn: '4-401-34719-6 C0073 P2987E',
      price: '2,987',
      event_date_id: '1995/3/12',
      sort_order: 199503120,
    }
  end

  list = list_factory("#{key}_A") { {book_id: book, keyword: :A, sort_order: 1} }
  list.list_contents_from_array %i[kibun_soukai wakasano_hiketsu zuruyasumi natsuno_hi everybodys_got_something torikago hoshino_ojisama watashino_daijina_hito ichido_asobini_kiteyo office_gaino_koi taifu kazeni_fukarete step_by_step_kareno_jinsei sutekina_tanjoubi]
end
