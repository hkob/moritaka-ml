key = :album_mi_ha_
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :mi_ha_ do
    {
      device_type: :album,
      event_date_id: '1988/3/25',
      minutes: 48,
      seconds: 32,
      sort_order: 198803250,
      number: '2nd',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4032.html'
    }
  end

  album.media_from_array [
    ["#{key}_lp", :lp, 'K-12540', :first, false, :warner_pioneer, '2,800'],
    ["#{key}_ct", :ct, 'LKF-8801', :first, false, :warner_pioneer, '2,800'],
    ["#{key}_cd1", :cd, '32XL-271', :first, false, :warner_pioneer, '3,200 3,008'],
    ["#{key}_cd2", :cd, 'WPCL-503', :second, true, :warner_music_japan, '2,400'],
    ["#{key}_itunes", :itunes, 'https://itunes.apple.com/jp/album/miha/id528967994', :first, true, :warner_music_japan, '2,400']
  ]

  list = list_factory("#{key}_common") { {device_id: album, keyword: :common, sort_order: 1} }

  list.list_contents_from_array [
    [:overheat_night_2, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito GUITARS iHideoSaito DRUMPR iHideoSaito SYNPR iHideoSaito KEYBOARDS iNobitaTsukada SYNPR iNobitaTsukada HIHAT iRyubenTsujino CRASH iRyubenTsujino CONGAS iShingoKanno CHORUS iHideoSaito BGVOCAL iMisaNakayama]],
    [:yokohama_one_night, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito GUITARS iHideoSaito CHORUS iHideoSaito DRUMPR iHideoSaito SYNPR iHideoSaito KEYBOARDS iNobitaTsukada SYNPR iNobitaTsukada GUIRO iShingoKanno TENORSAX iJakeHConception BGVOCAL iMisaNakayama]],
    [:good_bye_season, %i[VOCAL iChisatoMoritaka ARRANGE iTakumiYamamoto GUITARS iJunroSato CHORUS iNana BGVOCAL iYukariFujio TENORSAX iJakeHConception SYNPR iTomoakiArima]],
    [:cant_say_good_bye, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito GUITARS iHideoSaito CHORUS iHideoSaito DRUMPR iHideoSaito SYNPR iHideoSaito KEYBOARDS iNobitaTsukada SYNPR iNobitaTsukada]],
    [:pi_a_no, %i[VOCAL iChisatoMoritaka VOCAL iKenShima ARRANGE iKenShima FRSOLO iChisatoMoritaka APIANO iKenShima OTHERKB iKenShima SYNPR iTakayukiNegishi]],
    [:forty_seven_hard_nights, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito GUITARS iHideoSaito CHORUS iHideoSaito DRUMPR iHideoSaito SYNPR iHideoSaito KEYBOARDS iNobitaTsukada SYNPR iNobitaTsukada]],
    [:weekend_blue, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito GUITARS iHideoSaito CHORUS iHideoSaito DRUMPR iHideoSaito SYNPR iHideoSaito KEYBOARDS iNobitaTsukada SYNPR iNobitaTsukada CRASH iRyubenTsujino TROMBONE iShingoKanno]],
    [:kiss_the_night, %i[VOCAL iChisatoMoritaka ARRANGE iTakumiYamamoto GUITARS iJunroSato CHORUS iNana CHORUS iTakumiYamamoto BGVOCAL iYukariFujio BASS iChiharuMikuzuki EPIANO iHatsuhoFurukawa SYNPR iTomoakiArima]],
    [:mi_ha_, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito TIMSOLO iChisatoMoritaka GUITARS iHideoSaito CHORUS iHideoSaito DRUMPR iHideoSaito SYNPR iHideoSaito KEYBOARDS iNobitaTsukada SYNPR iNobitaTsukada BGVOCAL iMisaNakayama]],
    [:get_smile, %i[VOCAL iChisatoMoritaka ARRANGE iKenShima CARRANGE iHideoSaito GARRANGE iHideoSaito KEYBOARDS iKenShima GUITAR iHideoSaito CHORUS iHideoSaito BASS iChiharuMikuzuki TENORSAX iJakeHConception BGVOCAL iYukariFujio SYNPR iTakayukiNegishi]]
  ]
end
