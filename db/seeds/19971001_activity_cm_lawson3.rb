key = :cm_lawson_miracle_light
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :cm_lawson,
      j_title: 'ローソン(LAWSON)',
      e_title: 'LAWSON',
      from_id: '1997/10',
      to_id: '1997/12',
      sort_order: 199710010,
      song_id: :miracle_light,
    }
  end
end
