key = :album_kokoro_korasete
obj = Album.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  album = album_factory key, :kokoro_korasete do
    {
      device_type: :cover_album,
      event_date_id: '1993/6/25',
      sort_order: 199306250,
      number: '8th',
      singer_id: :iGenTakayama
    }
  end

  album.media_from_array [
    ["#{key}_cd", :cd, 'PSCR-5017', :first, false, :polystar, '3,059'],
  ]

  vg = %i[VOCAL iGenTakayama]
  list = list_factory("#{key}_commmon") { {device_id: album, keyword: :common, sort_order: 1} }
  list.list_contents_from_array(
    ListContent.title_with_performer(vg, '心凍らせて', '{KOKORO KORASETE}') +
    ListContent.title_with_performer(vg, '握りこぶしのブルース', '{NIGIRIKOBUSHI-NO} Blues') +
    ListContent.title_with_performer(vg, '泣くなよ', '{NAKUNAYO}') +
    ListContent.title_with_performer(vg, '泣かされたって', '{NAKASARETATTE}') +
    ListContent.title_with_performer(vg, '化粧', '{KESHO}') +
    ListContent.title_with_performer(vg, '希笛', '{KITEKI}') +
    ListContent.title_with_performer(vg, '三陸海岸', '{SANRIKU KAIGAN}') +
    [[:watarasebashi, vg], [:yowaseteyo_konyadake, vg]] +
    ListContent.title_with_performer(vg, 'ふぞろいの人生', '{FUZOROI-NO JINSEI}') +
    ListContent.title_with_performer(vg, '祈るだけの恋', '{INORUDAKENO KOI}') +
    ListContent.title_with_performer(vg, '貴方のことだけ', '{ANATA-NO KOTODAKE}')
  )
end
