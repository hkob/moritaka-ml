key = :thema_song_quiz_junsui_danjo_kouyuu_ending2
obj = ActivitySub.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity_sub_factory key do
    {
      activity_id: :thema_song_quiz_junsui_danjo_kouyuu,
      j_title: 'クイズ番組 「クイズ! 純粋男女交遊」エンディングテーマ(後期)',
      e_title: 'Ending thema song of TV program (Quiz) "Quiz! {JUNSUI DANJO KOUYU}(Second part)"',
      from_id: '1993/1',
      to_id: '1993/3/16',
      sort_order: 199301010,
      song_id: :writer_shibo,
    }
  end
end

