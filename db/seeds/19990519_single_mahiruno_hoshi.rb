key = :single_mahiruno_hoshi
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :mahiruno_hoshi do
    {
      device_type: :single,
      event_date_id: '1999/5/19',
      sort_order: 199905190,
      number: '39th',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4300.html',
    }
  end

  single.media_from_array [
    ["#{key}_cd", :cds, 'ZMDZ-161', '1st', false, :media_factory, '1,050'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }
  list.list_contents_from_array [
    [:mahiruno_hoshi, %i[VOCAL iChisatoMoritaka ARRANGE iShinKohno DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka GUITARSOLO iMotoakiFukanuma PIANO iYasuakiMaejima AGUITAR iYuichiTakahashi PROGRAMMING iYuichiTakahashi BASS iYukioSeto]],
    [:ame_1999, %i[VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi PIANO iYasuakiMaejima GUITAR iYuichiTakahashi GUITAR iYukioSeto WINDCHIME iYukioSeto]],
    [:mahiruno_hoshi, %i[VOCAL iKaraoke ARRANGE iShinKohno DRUMS iChisatoMoritaka CHORUS iChisatoMoritaka GUITARSOLO iMotoakiFukanuma PIANO iYasuakiMaejima AGUITAR iYuichiTakahashi PROGRAMMING iYuichiTakahashi BASS iYukioSeto], '(ORIGINL KARAOKE)', '(ORIGINL KARAOKE)']
]
  device_activity_factory single, :movie_mouichido_aitakute
end

