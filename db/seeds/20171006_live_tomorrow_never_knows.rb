key = :live_tomorrow_never_knows
obj = Concert.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)

  concert = concert_factory key do
    {
      concert_type: :live,
      from_id: '2017/10/6',
      to_id: '2017/10/12',
      has_song_list: true,
      has_product: true,
      sort_order: 201710060,
      num_of_performances: 3,
      num_of_halls: 3,
    }
  end

  list_a, list_b, list_c = %i[A B C].map.with_index { |k, i| list_factory("#{key}_#{k}") { {concert_id: concert, keyword: k, sort_order: i+1} } }
  list_a.list_contents_from_array %i[kanojo mijikai_natsu] + @mc + %i[the_benkyono_uta seishun aru_olno_seishun the_mi_ha_] + @mc + %i[ame the_nozokanaide] + ListContent.title_only('衣装替え', 'Costume change') + %i[the_stress] + ListContent.title_only('衣装早替え', 'Fast custume change') + %i[new_season] + @mc + %i[kusaimononiwa_futaoshiro sonogono_watashi yoruno_entotsu get_smile] + @encore + ListContent.title_only('衣装替え') + %i[hachigatsuno_koi funkey_monkey_baby] + @mc_member + %i[teriyaki_burger] + @double_encore + @mc + %i[konomachi]
  list_b.list_contents_from_array %i[concert_no_yoru rhythm_to_bass] + @mc + %i[fight natsuno_umi watashiga_obasanni_nattemo] + @mc + %i[yowaseteyo_konyadake the_blue_blues] + @mc + %i[the_benkyono_uta] + ListContent.title_only('～衣装替え(上着脱)～', 'Costume change') + %i[wakarimashita mitsuketa_saifu rock_alive] + @mc + %i[kusaimononiwa_futaoshiro] + @mc + %i[ame seventeen] + @mc + %i[yacchimaina sonogono_watashi get_smile amenochi_hare] + @encore + %i[guitar] + @mc_member + %i[konomachi] + @double_encore + %i[seishun]
  list_c.list_contents_from_array %i[lucky7_blues teo_tatako rockn_roll_kencho_shozaichi fight] + @mc + %i[jimina_onna tomodachino_kare writer_shibo] + @mc + %i[michi stress watashino_natsu] + @mc + %i[kazeni_fukarete rockn_omelette watarasebashi sayonara_watashino_koi i_love_you] + @mc + %i[haeotoko watashiga_obasanni_nattemo teriyaki_burger] + @encore + %i[ame memories] + @mc + %i[konomachi] + @double_encore + %i[concert_no_yoru]


  concert.concert_halls_from_array [
    ConcertHall.mk('2017/10/6', :昭和女子大学人見記念講堂, list_a, 'とんこつラーメン→東京ラーメン', '{TONKOTSU RAMEN}→Tokyo {RAMEN}', nil, nil, '「ザ・森高」', '[The Moritaka]'),
    ConcertHall.mk('2017/10/11', :ZeppDiverCity, list_b, '馬刺し', 'Slices of raw horse', nil, nil, '「ROCK ALIVE」', '[ROCK ALIVE]'),
    ConcertHall.mk('2017/10/12', :ZeppDiverCity, list_c, 'とんこつラーメン', '{TONKOTSU RAMEN}', nil, nil, '「LUCKY SEVEN」', '[LUCKY SEVEN]'),
  ]
end

