key = :single_new_season2
obj = Single.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  single = single_factory key, :new_season_overheat_night do
    {
      device_type: :single,
      event_date_id: '1988/3/25',
      sort_order: 198803250,
      number: 'Re-released',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4020.html'
    }
  end

  single.media_from_array [
    ["#{key}_cds1", :cds, '10SL-110', '1st', false, :warner_pioneer, '937'],
    ["#{key}_cds2", :cds, '10SL-110', '2nd', false, :warner_music_japan, '937'],
  ]

  list = list_factory("#{key}_common") { {device_id: single, keyword: :common, sort_order: 1} }

  list.list_contents_from_array [
    [:new_season, %i[VOCAL iChisatoMoritaka ARRANGE iHideoSaito KEYBOARD_SOLO iChisatoMoritaka]],
    [:overheat_night_2, @vcah]
  ]
end
