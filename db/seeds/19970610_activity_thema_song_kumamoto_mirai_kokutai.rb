key = :thema_song_kumamoto_mirai_kokutai
obj = Activity.find_by(key: key)
if already_created?(obj, __FILE__)
  print "... skip"
else
  obj.try(:destroy)
  activity = activity_factory key do
    {
      activity_type: :thema_song,
      title_id: %q(「熊本未来国体」|"KUMAMOTO MIRAI KOKUTAI"|くまもとみらいこくたい),
      j_comment: 'イメージソング',
      e_comment: 'Image Song',
      from_id: '1999/1/27',
      to_id: '1999/10/28',
      song_id: :mirai,
      sort_order: 199901270
    }
  end
end
