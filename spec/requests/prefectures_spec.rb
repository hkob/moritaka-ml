require 'rails_helper'

RSpec.describe :Prefectures, type: :request do
  let!(:prefecture) { prefecture_factory :Tokyo }
  let!(:another) { prefecture_factory :Hokkaido }

  describe 'GET index' do
    subject { -> { get prefectures_path } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[東京 北海道]
  end

  describe 'GET show' do
    subject { -> { get prefecture_path(key: prefecture.key) } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[東京 北海道]
  end
end
