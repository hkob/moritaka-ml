require 'rails_helper'

RSpec.describe :Companies, type: :request do
  let!(:company) { company_factory :warner_pioneer }
  let!(:another) { company_factory :warner_music_japan }

  describe 'GET index' do
    subject { -> { get companies_path } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[Warner Pioneer Music Japan]
  end

  describe 'GET show' do
    subject { -> { get company_path(key: company.key) } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[Warner Pioneer Music Japan]
  end
end
