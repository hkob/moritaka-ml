require 'rails_helper'

RSpec.describe :People, type: :request do
  let!(:person) { person_factory :iChisatoMoritaka }
  let!(:another) { person_factory :iHideoSaito }

  describe 'GET #index' do
    subject { -> { get people_path, params: {division: '6'} } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[森高千里]
    it_behaves_like :response_body_not_includes, %w[斉藤英夫]
  end

  describe 'GET #show' do
    subject { -> { get person_path(key: person.key) } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[森高千里]
    it_behaves_like :response_body_not_includes, %w[斉藤英夫]
  end
end
