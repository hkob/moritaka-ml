require 'rails_helper'

RSpec.describe :Home, type: :request do

  describe "GET #index" do
    subject { -> { get home_index_path } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[アルバム一覧]
  end

  describe 'GET #about' do
    subject { -> { get home_about_path } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[小林弘幸]
  end

end
