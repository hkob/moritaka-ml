require 'rails_helper'

RSpec.describe :Books, type: :request do
  let!(:book) { book_test_factory }
  let!(:another) { book_test2_factory }

  describe 'GET index' do
    subject { -> { get books_path, params: {division: :poems} } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[わかりやすい恋]
    it_behaves_like :response_body_not_includes, %w[朱夏]
  end

  describe 'GET show' do
    subject { -> { get book_path(key: book.key) } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[わかりやすい恋]
    it_behaves_like :response_body_not_includes, %w[朱夏]
  end
end
