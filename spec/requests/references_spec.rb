require 'rails_helper'

RSpec.describe :References, type: :request do
  let!(:reference) { reference_test_factory }

  describe 'GET index' do
    subject { -> { get references_path, params: {key: reference.reference_type} } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[オフィシャルウェブサイト]
  end
end
