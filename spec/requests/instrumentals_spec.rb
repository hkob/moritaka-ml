require 'rails_helper'

RSpec.describe :Instrumentals, type: :request do
  let!(:instrumental) { instrumental_factory :VOCAL }
  let!(:another) { instrumental_factory :ARRANGE }

  describe 'get #index' do
    subject { -> { get instrumentals_path } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[Vocal 編曲]
  end

  describe 'get #show' do
    subject { -> { get instrumental_path(key: instrumental.key) } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[Vocal]
    it_behaves_like :response_body_not_includes, %w[編曲]
  end
end
