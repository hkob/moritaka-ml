require 'rails_helper'

RSpec.describe :Videos, type: :request do
  let!(:video) { video_test_factory }
  let!(:another) { video_test2_factory }

  describe 'GET index' do
    subject { -> { get videos_path } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[GET SMILE]
    it_behaves_like :response_body_not_includes, %w[ミーハー]
  end

  describe 'GET show' do
    subject { -> { get video_path(key: video.key) } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[GET SMILE]
    it_behaves_like :response_body_not_includes, %w[夢の終わり]
  end
end
