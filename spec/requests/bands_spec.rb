require 'rails_helper'

RSpec.describe :Bands, type: :request do
  let!(:band) { band_factory :animals_1 }
  let!(:another) { band_factory :the_london }

  describe 'GET index' do
    subject { -> { get bands_path } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[ANIMALS The London]
  end

  describe 'GET :show' do
    subject { -> { get band_path(key: band.key) } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[ANIMALS The London]
  end
end
