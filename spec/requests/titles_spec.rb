require 'rails_helper'

RSpec.describe :Titles, type: :request do
  let!(:title) { title_factories %i[new_season yumeno_owari]}

  describe 'GET #index' do
    subject { -> { get titles_path, params: {division: '41'} } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[NEW SEASON]
    it_behaves_like :response_body_not_includes, %w[夢の終わり]
  end
end
