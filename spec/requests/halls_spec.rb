require 'rails_helper'

RSpec.describe :Halls, type: :request do
  let!(:hall) { hall_factory :日本青年館 }

  describe 'GET show' do
    subject { -> { get hall_path(key: hall.key) } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[日本青年館]
  end
end
