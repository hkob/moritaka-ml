require 'rails_helper'

RSpec.describe :Albums, type: :request do
  let!(:album) { album_test_factory }
  let!(:another) { album_test2_factory }

  describe 'GET index' do
    subject { -> { get albums_path } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[NEW SEASON ミーハー]
    it_behaves_like :response_body_not_includes, %w[夢の終わり]
  end

  describe 'GET show' do
    subject { -> { get album_path(key: album.key) } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[NEW SEASON]
  end
end
