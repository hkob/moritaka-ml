require 'rails_helper'

RSpec.describe :Songs, type: :request do
  let!(:song) { song_factory :new_season }
  let!(:another) { song_factory :yomeno_owari }

  describe 'GET index' do
    subject { -> { get songs_path, params: {division: '4'} } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[NEW SEASON]
    it_behaves_like :response_body_not_includes, %w[夢の終わり]
  end

  describe 'GET :list_by_date' do
    subject { -> { get list_by_date_songs_path, params: {year_id: song.event_date.year} } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[NEW SEASON]
    it_behaves_like :response_body_not_includes, %w[夢の終わり]
  end

  describe 'GET :show' do
    subject { -> { get song_path(key: song.key) } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[NEW SEASON]
    it_behaves_like :response_body_not_includes, %w[夢の終わり]
  end
end
