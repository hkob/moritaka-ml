require 'rails_helper'

RSpec.describe :Concerts, type: :request do
  let!(:concert) { concert_test_factory }
  let!(:another) { concert_test2_factory }

  describe 'GET index' do
    subject { -> { get concerts_path, params: {division: :live} } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[OVERHEAT. NIGHT 渋谷ライブイン]
  end

  describe 'GET show' do
    subject { -> { get concert_path(key: concert.key) } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[OVERHEAT. NIGHT 渋谷ライブイン]
  end
end
