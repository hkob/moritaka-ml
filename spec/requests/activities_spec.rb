require 'rails_helper'

RSpec.describe :Activities, type: :request do
  let!(:activity) { activity_test_factory }
  let!(:another) { activity_test2_factory }

  describe 'GET index' do
    subject { -> { get activities_path, params: {division: activity.activity_type} } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, 'TVハッカー'
    it_behaves_like :response_body_not_includes, '大塚製薬'
  end

  describe 'GET show' do
    subject { -> { get activity_path(activity), params: {key: activity.key} } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, 'TVハッカー'
    it_behaves_like :response_body_not_includes, '大塚製薬'
  end
end
