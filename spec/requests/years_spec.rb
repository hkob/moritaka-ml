require 'rails_helper'

RSpec.describe :Years, type: :request do
  let!(:year) { year_factory 1987 }
  let!(:another) { year_factory 1988 }

  describe 'GET index' do
    subject { -> { get years_path } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[1987年 1988年]
  end

  describe 'GET show' do
    subject { -> { get year_path(key: year.key) } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[1987年 1988年]
  end
end
