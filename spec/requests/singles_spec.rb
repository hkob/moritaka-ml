require 'rails_helper'

RSpec.describe :Singles, type: :request do
  let!(:single) { single_test_factory }

  describe 'GET index' do
    subject { -> { get singles_path, params: {division: single.device_type} } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[NEW SEASON]
    it_behaves_like :response_body_not_includes, %w[夢の終わり]
  end

  describe 'GET show' do
    subject { -> { get single_path(key: single.key) } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[NEW SEASON]
    it_behaves_like :response_body_not_includes, %w[夢の終わり]
  end
end
