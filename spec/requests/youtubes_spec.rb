require 'rails_helper'

RSpec.describe :Youtubes, type: :request do
  let!(:youtube) { youtube_test_factory }

  describe 'GET index' do
    subject { -> { get youtubes_path, params: {division: 'sc001'} } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[セルフカヴァー(1〜20)]
  end

  describe 'GET show' do
    subject { -> { get youtube_path(key: youtube.key) } }
    it_behaves_like :response_status_check, 200
    it_behaves_like :response_body_includes, %w[SWEET CANDY]
  end

end
