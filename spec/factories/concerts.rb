FactoryBot.define do
  factory :concert do
    has_product {false}
    num_of_performances {1}
    num_of_halls {1}
  end
end
