# @param [Symbol] fg_key user の FactoryBot のキー
# @return [User] ログインしたユーザ
def login_teacher_as(fg_key)
  @one = teacher_factory(fg_key)
  sign_in @one
  @one
end

# @note サインアウト
def sign_out_one
  sign_out @one
end

# @param [Symbol] fg_key user の FactoryBot のキー
# @note before と after を自動設置する．
def teacher_login(fg_key)
  before { login_teacher_as fg_key }
  after { sign_out_one }
end
