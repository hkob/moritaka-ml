shared_examples_for :presence_validates do |keys|
  # keys を指定しない場合にも，valid だけは確認
  it { is_expected.to be_valid }
  # keys が指定された時には keys を一つずつ外しながら invalid を確認
  if keys
    keys.each do |key|
      context "when #{key} is nil" do
        it do
          subject[key] = nil
          is_expected.to_not be_valid
        end
      end
    end
  end
end

shared_examples_for :unique_validates do |keys, block|
  keys.each do |key|
    context "when another object has same #{key}" do
      it do
        another_object = block.call
        subject[key] = another_object[key]
        is_expected.to_not be_valid
      end
    end
  end
end

shared_examples_for :plural_unique_validates do |keys, block|
  context "when another object has same #{keys.join ', '}" do
    it do
      another_object = block.call
      keys.each do |key|
        subject[key] = another_object[key]
      end
      is_expected.to_not be_valid
    end
  end

  context "when another object has at least one different keys(#{keys.join ', '})" do
    it do
      keys.each do |except_key|
        another_object = block.call
        compare = true
        keys.each do |key|
          if key == except_key
            compare = false if subject[key] == another_object[key]
          else
            another_object[key] = subject[key]
          end
        end
        expect(another_object).to be_valid if compare
        another_object.destroy
      end
    end
  end
end

shared_examples_for :destroy_validates do
  context 'should destroy' do
    it do
      klass = subject.class
      expect { subject.destroy }.to change(klass, :count).by -1
    end
  end
end

shared_examples_for :reject_destroy_validates do
  context 'should not destroy' do
    it do
      klass = subject.class
      expect { subject.destroy }.not_to change(klass, :count)
    end
  end
end

shared_examples_for :belongs_to do |model, hash|
  has_many_relations = hash[:has_many]
  has_one_relations = hash[:has_one]
  children = hash[:children] || model.to_s.pluralize
  child = hash[:child] || model
  context "when destroying #{model}" do
    if has_many_relations
      has_many_relations.each do |relation|
        it "#{relation}.#{children}.count should decrease" do
          parent = subject.send(relation)
          expect { subject.destroy }.to change(parent.send(children), :count).by(-1)
        end
      end
    end

    if has_one_relations
      has_one_relations.each do |relation|
        it "#{relation}.#{child} should be nil" do
          parent = subject.send(relation)
          subject.destroy
          expect(parent.reload.send(child)).to be_nil
        end
      end
    end
  end
end

shared_examples_for :dependent_destroy do |model, relations|
  relations.each do |relation|
    context "when destroying #{relation}" do
      it 'should be destroyed by dependency' do
        parent = subject.send(relation)
        expect { parent.destroy }.to change(model.to_s.pluralize.classify.constantize, :count).by(-1)
      end
    end
  end
end

shared_examples_for :destroy_nullify_for_relations do |model, relations|
  relations.each do |relation|
    context "when destroying #{model}.#{relation}" do
      it "#{model} should set null to #{relation}" do
        parent = subject.send(relation)
        parent.destroy
        expect(subject.reload.send(relation)).to be_nil
      end
    end
  end
end

shared_examples_for :reject_destroy_for_relations do |model, relations|
  relations.each do |relation|
    context "when destroying #{model}.#{relation}" do
      it "#{model} should reject destroying #{relation}" do
        parent = subject.send(relation)
        parent.destroy
        expect(parent.errors[:base].size).to eq(1)
      end
    end
  end
end

shared_examples_for :mst_block do |block|
  it "should receive above methods(mst)" do
    block.call(targets).each do |method, array|
      print "mst: #{method}\n"
      array.each_slice(2) do |(v, a)|
        expect(subject.send(method, *v)).to eq a
      end
    end
  end
end

shared_examples_for :msta_block do |block|
  it "should receive above methods(msta)" do
    block.call(targets).each do |method, array|
      print "msta: #{method}\n"
      array.each_slice(2) do |(v, a)|
        expect(subject.send(method, *v)).to match_array a
      end
    end
  end
end

shared_examples_for :amst_block do |block|
  it "should receive above methods(amst)" do
    block.call(targets).each do |method, array|
      print "amst: #{method}\n"
      array.each_slice(2) do |(v, a)|
        expect(subject.map { |o| o.send(method, *v) }).to eq a
      end
    end
  end
end

shared_examples_for :amsta_block do |block|
  it "should receive above methods(amsta)" do
    block.call(targets).each do |method, array|
      print "amsta: #{method}\n"
      array.each_slice(2) do |(v, a)|
        answers = subject.map { |o| o.send(method, *v) }
        answers.zip(a).each do |ans, a_a|
          expect(ans).to match_array a_a
        end
      end
    end
  end
end

shared_examples_for :mst do |method, block|
  it "should receive #{method}" do
    array = block.call
    array.each_slice(2) do |(v, a)|
      expect(subject.send(method, *v)).to eq a
    end
  end
end

shared_examples_for :msta do |method, block|
  it "should receive #{method}" do
    array = block.call
    array.each_slice(2) do |(v, a)|
      expect(subject.send(method, *v)).to match_array a
    end
  end
end

shared_examples_for :amst do |method, block|
  it "should receive #{method}" do
    array = block.call
    array.each_slice(2) do |(v, a)|
      expect(subject.map { |o| o.send(method, *v) }).to eq a
    end
  end
end

shared_examples_for :amsta do |method, block|
  it "should receive #{method}" do
    array = block.call
    array.each_slice(2) do |(v, a)|
      answers = subject.map { |o| o.send(method, *v) }
      answers.zip(a).each do |ans, a_a|
        expect(ans).to match_array a_a
      end
    end
  end
end

shared_context :prepare_factories do |symbols, block|
  symbols.each do |symbol|
    let!(symbol) { block.call(symbol) }
  end
end

shared_examples_for :gp do |klass|
  subject { klass } if klass
  it 'should receive gp' do
    expect(subject.gp).to eq [subject, "#{ subject.name.underscore }_id".to_sym]
    expect(subject.gp(false)).to eq [subject, "#{ subject.name.underscore }_id".to_sym]
    expect(subject.gp(true)).to eq [subject, :id]
  end
end

shared_examples_for :response_status_check do |value|
  it "response should be #{value}" do
    subject.call; expect(response.status).to eq value
  end
end

shared_examples_for :response_body_includes do |strs|
  it "response body includes #{strs}" do
    subject.call
    Array(strs).each { |str| expect(response.body).to include str }
  end
end

shared_examples_for :response_body_not_includes do |strs|
  it "response body does not include #{strs}" do
    subject.call
    Array(strs).each { |str| expect(response.body).not_to include str }
  end
end

shared_examples_for :redirect_to do
  it { subject.call; expect(response).to redirect_to(return_path) }
end

shared_examples_for :increment_object_by_create do |klass|
  it { expect { subject.call }.to change(klass, :count).by 1 }
end

shared_examples_for :not_increment_object_by_create do |klass|
  it { expect { subject.call }.not_to change(klass, :count) }
end

shared_examples_for :decrement_object_by_destroy do |klass|
  it { expect { subject.call }.to change(klass, :count).by -1 }
end

shared_examples_for :change_object_count_by_create_or_destroy do |klass, n|
  it { expect { subject.call }.to change(klass, :count).by n }
end

shared_examples_for :change_object_value_by_update do |klass, key, value|
  it do
    pre_value = object.send(key)
    expect { subject.call }.to change { klass.find(object.id).send(key) }.from(pre_value).to(value)
  end
end

shared_examples_for :not_change_object_value_by_update do |klass, key|
  it { expect { subject.call }.not_to change { klass.find(object.id).send(key) } }
end

shared_examples_for :flash_notice_message do |str|
  it { subject.call; expect(flash.now[:notice]).to eq str }
end

shared_examples_for :flash_alert_message do |str|
  it { subject.call; expect(flash.now[:alert]).to eq str }
end

