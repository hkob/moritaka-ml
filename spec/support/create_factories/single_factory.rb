# @param [Symbol] key
# @option hash [String] device_type デバイスタイプ
# @option hash [Symbol] title_key タイトルの key
# @option hash [String] date 発売日
# @option hash [String] minutes 分
# @option Hash [String] seconds 秒
# @option Hash [Fixnum] sort_order 並び順
# @option Hash [String] number 通番
# @option Hash [Singer] singer_key 歌手の key
# @option Hash [Person] singer 歌手
# @option Hash [String] j_comment 日本語コメント
# @option Hash [String] e_comment 英語コメント
# @option Hash [String] link 外部リンク
# @return [Single] Single FactoryGirl オブジェクト
def single_factory(key, title_key = nil)
  title_key ||= key
  FB.find_or_create(:single, key) do
    hash = yield.merge({key: key, title_id: title_factory(title_key).id})
    FB.replace_attr(hash, :device_type) do |k|
      k.to_s.split('|').inject(0) { |dtv, key| dtv + Single::SingleStr2Num[key.to_sym] }
    end
    FB.replace_attr(hash, :event_date_id) { |k| event_date_factory(k).id }
    FB.replace_attr(hash, :singer_id) { |k| person_factory(k).id }
    hash
  end
end

# @return [Single] Single FactoryGirl オブジェクト
def single_test_factory
  single_factory :new_season do
    {
      device_type: :ep_single,
      event_date_id: '1987/5/25',
      sort_order: 1,
      number: '1st',
      singer_id: :iChisatoMoritaka,
      j_comment: 'デビューシングル',
      e_comment: 'Debut Single'
    }
  end
end
