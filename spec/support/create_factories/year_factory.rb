# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Year] Year FactoryGirl オブジェクト
def year_factory(year)
  FB.find_or_create(:year, nil) { {year: year.to_i} }
end

# @param [Array<Symbol, String>] keys オブジェクトを一意に決定するキーの配列
# @return [Array<Year>] Year FactoryGirl オブジェクトの配列
def year_factories(keys)
  keys.map { |k| year_factory(k) }
end

