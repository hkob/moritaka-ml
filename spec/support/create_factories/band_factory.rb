BandFactoryHash = {
  ms_band: [ "M's BAND", "M's BAND", 'えむずばんど', '(at 日本青年館)', '(at Nihon SEINEN-KAN' ],
  animals_1: [ 'ANIMALS (1)', 'ANIMALS (1)', 'あにまるず', '-', '-' ],
  animals_2: [ 'ANIMALS (2)', 'ANIMALS (2)', 'あにまるずに', '-', '-' ],
  janet_jacksons: ["Janet Jackson's", "Janet Jackson's", 'じゃねっとじゃくそんず', '-', '-'],
  the_london: ['The London', 'The London', 'ざ　ろんどん', '-', '-'],
  smilyMaejimaHelloGoodBye: ['Smily Maejima & Hello Good-bye', 'Smily Maejima & Hello Good-bye', 'すまいりーまえじまとはろーぐっばい', '-', '-'],
  shin_yokohama_arenas: ['新・横浜アリーナーズ', 'Shin Yokohama Arenas', 'しんよこはまありーなーず', '-', '-'],
  smilyMaejimaHelloGoodBye2: ['Smily Maejima & Hello Good-bye, 名古屋レインボーズ', 'Smily Maejima & Hello Good-bye, Nagoya Rainbows', 'すまいりーまえじまとはろーぐっばいなごやれいんぼうず', 'Smily Maejima & Hello Good-bye [in 大阪・東京]|名古屋レインボーズ [in 名古屋]', 'Smily Maejima & Hello Good-bye [in Osaka, Tokyo]|Nagoya Rainbows [in Nagoya]'],
  the_purple_haze: ['The Purple Haze', 'The Purple Haze', 'ざぱーぷるへいず'],
  utopia: ["ザ・ユートピア・モーニング息子・The Rock'n Roll Circus", "The Utopia, Morning {MUSUKO}, The Rock'n Roll Circus", "ざゆーとぴあもーにんぐむすこざろっくんろーるさーかす", "The Rock'n Roll Circus [in 川口]|モーニング息子 [in 仙台]|ザ・ユートピア [in その他]", "The Rock'n Roll Circus [in Kawaguchi]|Morning {MUSUKO} [in Sendai]|The Utopia: [in other place]"],
  lost_in_space: ["ロストインスペース", "Lost in space", "ろすといんすぺーす"],
  nazono_enban_ufos: ['謎の円盤ユーエフオーズ', '{NAZO-NO ENBAN} UFOs', 'なぞのえんばんゆーえふおーず'],
  white_queens: ['ホワイトクイーンズ', 'White Queens', 'ほわいとくいーんず'],
}

# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Band] Band FactoryGirl オブジェクト
def band_factory(key)
  jt, et, yo, jc, ec = BandFactoryHash[key.to_sym]
  FB.find_or_create(:band, key) do
    title = title_factory_with_values key, jt, et, yo
    {
      key: key,
      title_id: title.id,
      j_comment: jc == '-' ? nil : jc,
      e_comment: ec == '-' ? nil : ec
    }
  end if jt
end

# @param [Array<Symbol, String>] keys オブジェクトを一意に決定するキーの配列
# @return [Array<Band>] Band FactoryGirl オブジェクトの配列
def band_factories(keys)
  keys.map { |k| band_factory(k) }
end

