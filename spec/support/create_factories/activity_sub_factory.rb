# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Activity] Activity FactoryGirl オブジェクト
def activity_sub_factory(key)
  FB.find_or_create(:activity_sub, key) do
    hash = yield.merge(key: key)
    FB.replace_attr(hash, :activity_id) { |k| activity_factory(k).id }
    FB.replace_attr(hash, :from_id) { |k| event_date_factory(k).id }
    FB.replace_attr(hash, :to_id) { |k| event_date_factory(k).id }
    FB.replace_attr(hash, :song_id) { |k| song_factory(k).id }
    hash
  end
end

def activity_sub_test_factory
  activity_test_factory
  activity_sub_factory :TVhacker1 do
    {
      j_title: 'TVハッカー',
      e_title: 'TV Hacker',
      from_id: '1987/4/19',
      to_id: '1987/9/27',
      j_comment: '毎週日曜 20:00 - 20:54',
      e_comment: 'Every Sunday, 20:00 - 20:54',
      sort_order: 1,
      activity_id: :TVhacker,
      song_id: :new_season
    }
  end
end

def activity_sub_test2_factory
  activity_test2_factory
  activity_sub_factory :pocarisweat1 do
    {
      j_title: %Q(大塚製薬 「ポカリスエット」),
      e_title: %Q(Otsuka Pharmaceutical Company `Pocarisweat'"),
      activity_id: :pocarisweat,
      from_id: '1986/10/1',
      to_id: '1988/9/30',
      j_comment: '和服編|寝室編|電車編|自動販売機編|骨董店編|レストラン編|温泉編',
      e_comment: 'Japanese custume version|Bed room version|Train version|{JIDOHANBAIKI} version|{KOTTOHIN-TEN} version|Restaurant version|{ONSEN} version',
      sort_order: 2
    }
  end
end

def activity_sub_factories_for_music_tv(activity_key, hash, file = __FILE__)
  hash.each do |k, array|
    date, song_list, jt, et, jc, ec = array
    so = k.to_i
    key = "#{activity_key}_#{k}"
    obj = ActivitySub.find_by(key: key)
    if already_created?(obj, file)
      print "... skip"
    else
      obj.try(:destroy)
      as = activity_sub_factory key do
        {
          activity_id: activity_key,
          j_title: jt,
          e_title: et,
          j_comment: jc,
          e_comment: ec,
          from_id: date,
          sort_order: so,
        }
      end
      list = list_factory(key) { {activity_sub_id: as, keyword: k, sort_order: so } }
      list.list_contents_from_array song_list
    end
  end
end
