InstrumentalFactoryHash = {
  # Band leader
  BANDLEADER: ['バンドリーダー', 'Band leader', 'ばんどりーだー', 0],

  # Vocal(main)
  VOCAL: ['Vocal', 'Vocal', 'ぼーかる', 500],

  # Arrange
  ARRANGE: ['編曲', 'Arrangement', 'へんきょく', 1000],
  CARRANGE: ['Chorus Arrangement', 'Chorus Arrangement', 'こーらすあれんじ', 1010],
  GARRANGE: ['Guitar Arrangement', 'Guitar Arrangement', 'ぎたーあれんじ', 1020],
  STRINGSARRANGE: ['Strings Arrangement', 'Strings Arrangement', 'すとりんぐすあれんじ', 1030],
  HORNARRANGEMENT: ['Horn Arrangement', 'Horn Arrangement', 'ほるんあれんじめんと', 1040],

  # Vocal(other)
  BGVOCAL: ['Background Vocal', 'Background Vocal', 'ばっくぐらうんとぼーかる', 2010],
  TVOCAL: ['Vocal(?)', 'Vocal(?)', 'ぼーかる', 2020],
  CHACHACHA: ['Background Vocal (Omocha-no Cha Cha Cha)', 'Background Vocal (Omocha-no Cha Cha Cha)', 'ばっくぐらうんとぼーかるおもちゃのちゃちゃちゃ', 2030],
  DAISUKI: ['Background Vocal (Daisuki)', 'Background Vocal (Daisuki)', 'ばっくぐらうんとぼーかるだいすき', 2040],
  CHORUS: ['Chorus', 'Chorus', 'こーらす', 2050],
  VOICE: ['Voice', 'Voice', 'ぼいす', 2060],

  # All instrument
  ALLINST: ['All Instruments', 'All Instruments', 'おーるいんすつるめんつ', 3000],
  ALLINSTEXCEPTDRUMS: ['All Instruments (except Drums)', 'All Instruments (except Drums)', 'おーるいんすつるめんつえくすぺくとどらむす', 3010],
  INSTRUMENTS: ['Instruments', 'Instruments', 'いんすつるめんつ', 3020],
  MANIPULATE: ['Manipulate', 'Manipulate', 'まにぴゅれーと', 3030],
  OTHERINST: ['Other Instruments', 'Other Instruments', 'あざーいんすつるめんつ', 3040],

  # DRUMS
  DRUMS: ['Drums', 'Drums', 'どらむす', 4000],
  ADDDRUMS: ['Additional Drums', 'Additional Drums', 'あどどらむす', 4010],
  LOOPDRUMS: ['Loop Drums', 'Loop Drums', 'るーぷどらむす', 4020],
  STEELDRUMS: ['Steel Drums', 'Steel Drums', 'すちーるどらむす', 4030],
  TWINDRUMS: ['Twin Drums', 'Twin Drums', 'ついんどらむす', 4040],
  DRUMLOOPS: ['Drum Loops', 'Drum Loops', 'どらむるーぷす', 4050],
  DRUMPR: ['Drum Programs', 'Drum Programs', 'どらむぷろぐらむす', 4060],
  SDRUMBREAKS: ['Sampling Drum Breaks', 'Sampling Drum Breaks', 'さんぷりんぐどらむぶれーくす', 4070],
  SIMMONS: ['Simmons', 'Simmons', 'しもんず', 4080],
  EDRUMS: ['Electric Drums', 'Electric Drums', 'えれくとりっくどらむす', 4090],
  ROCKDRUMS: ['Rock Drums', 'Rock Drums', 'ろっくどらむす', 4100],

  # GUITAR
  GUITAR: ['Guitar', 'Guitar', 'ぎたー', 5000],
  GUITARS: ['Guitars', 'Guitars', 'ぎたーず', 5010],
  AGUITAR: ['Acoustic Guitar', 'Acoustic Guitar', 'あこーすてぃっくぎたー', 5020],
  EGUITAR: ['E.Guitar', 'E.Guitar', 'いーぎたー', 5030],
  EGUITARSOLO: ['E.Guitar Solo', 'E.Guitar Solo', 'いーぎたーそろ', 5040],
  FGUITAR: ['Guzz Guitar', 'Guzz Guitar', 'ぐずぎたー', 5050],
  GUITARSOLO: ['Guitar Solo', 'Guitar Solo', 'ぎたーそろ', 5060],
  GUTGUITAR: ['Gut Guitar', 'Gut Guitar', 'がっとぎたー', 5070],
  JGUITAR: ['Jaka Jaka Guitar', 'Jaka Jaka Guitar', 'じゃかじゃかぎたー', 5080],
  RGUITAR: ['Rhythm Guitar', 'Rhythm Guitar', 'りずむぎたー', 5090],
  WWGUITAR: ['Wah Wah Guitar', 'Wah Wah Guitar', 'わうわうぎたー', 5100],
  SPICE: ['Spice', 'Spice', 'すぱいす', 5110],

  # BASS
  BASS: ['Bass', 'Bass', 'べーす', 6000],

  # Keyboard
  KEYBOARDS: ['Keyboards', 'Keyboards', 'きーぼーど', 7000],
  EPIANO: ['Electric Piano', 'Electric Piano', 'えれくとりっくぴあの', 7010],
  PIANO: ['Piano', 'Piano', 'ぴあの', 7020],
  APIANO: ['Acoustic Piano', 'Acoustic Piano', 'あこーすてぃっくぴあの', 7030],
  ACCORDION: ['Accordion', 'Accordion', 'あこーでぃおん', 7040],
  APF: ['Apf', 'Apf', 'えーぴーえふ', 7050],
  BTHREEORGAN: ['B3 organ', 'B3 organ', 'びーさんおるがん', 7060],
  EPF: ['Epf', 'Epf', 'いーぴーえふ', 7070],
  FRHODES: ['Fender Rhodes', 'Fender Rhodes', 'ふぇんだーろーず', 7080],
  FRSOLO: ['Fender Rhodes Solo', 'Fender Rhodes Solo', 'ふぇんだーろーずそろ', 7090],
  HARPSICHORD: ['Harpsichord', 'Harpsichord', 'はーぷしこーど', 7100],
  ORGAN: ['Organ', 'Organ', 'おるがん', 7110],
  OTHERKB: ['Other Keyboards', 'Other Keyboards', 'あざーきーぼーど', 7120],
  PIANICA: ['Pianica', 'Pianica', 'ぴあにか', 7130],
  PIANOLC: ['Piano (left Channel)', 'Piano (left Channel)', 'ぴあのれふとちゃねる', 7140],
  PIANORC: ['Piano (right Channel)', 'Piano (right Channel)', 'ぴあのらいとちゃねる', 7150],
  RHODES: ['Rhodes', 'Rhodes', 'ろーず', 7160],
  SKEYSOLO: ['Sampling Keyboard Solo', 'Sampling Keyboard Solo', 'さんぷりんぐきーぼーどそろ', 7170],
  SYNPR: ['Synthesizer Programs', 'Synthesizer Programs', 'しんせさいざーぷろぐらむす', 7180],
  SYNTHESIZER: ['Synthesizer', 'Synthesizer', 'しんせさいざー', 7190],
  SYNTHOP: ['Synth.Operator', 'Synth.Operator', 'しんすおぺれーた', 7200],
  WURLITZER: ['WURLITZER', 'WURLITZER', 'わーりっつぁー', 7210],
  KEYBOARD_SOLO: ['Keyboard Solo', 'Keyboard Solo', 'きーぼーど　そろ', 7220],
  CLAVINET: ['CLAVINET', 'CLAVINET', 'くらびねっと', 7230],

  # Strings
  NANCHATTEOKOTO: ['Nanchatte OKOTO', 'Nanchatte OKOTO', 'なんちゃっておこと', 8000],
  SITAR: ['Sitar', 'Sitar', 'したーる', 8010],
  STRINGS: ['Strings', 'Strings', 'すとりんぐす', 8020],
  STRINGSCM: ['Strings Concert Mastre', 'Strings Concert Mastre', 'すとりんぐすこんさーとますたー', 8030],
  TAISHOGOTO: ['Taisho-goto', 'Taisho-goto', 'たいしょうごと', 8040],
  UKULELE: ['Ukulele', 'Ukulele', 'うくれれ', 8050],
  CELLO: ['Cello', 'Cello', 'ちぇろ', 8060],

  # Winds
  ALTOFLUTES: ['Alto Flutes', 'Alto Flutes', 'あるとふるーと', 9000],
  ALTOSAX: ['Alto Saxophone', 'Alto Saxophone', 'あるとさきそふぉん', 9010],
  BTROMBONE: ['Bass Trombone', 'Bass Trombone', 'べーすとろんぼーん', 9020],
  CLARINET: ['Clarinet', 'Clarinet', 'くらりねっと', 9030],
  DIDJERIDOO: ['Didjeridoo', 'Didjeridoo', 'でぃじゅりどぅ', 9040],
  FHORN: ['Flugel Horn', 'Flugel Horn', 'ふりゅーげるほるん', 9050],
  FLUTESOLO: ['Flute Solo', 'Flute Solo', 'ふるーとそろ', 9060],
  HORN: ['Horns', 'Horns', 'ほるん', 9070],
  RECORDER: ['Recorder', 'Recorder', 'りこーだー', 9080],
  SAXOPHONE: ['Saxophone', 'Saxophone', 'さきそふぉん', 9090],
  TENORSAX: ['Tenor Saxophone', 'Tenor Saxophone', 'てなーさきそふぉん', 9100],
  TROMBONE: ['Trombone', 'Trombone', 'とろんぼーん', 9110],
  TRUMPET: ['Trumpet', 'Trumpet', 'とらんぺっと', 9120],
  TENORBARITONSAX: ['Tenor Bariton Sax', 'Tenor Bariton Sax', 'てなーばりとんさっくす', 9130],

  # Percussion
  BELL: ['Bell', 'Bell', 'べる', 10000],
  BARCHIMES: ['Bar Chimes', 'Bar Chimes', 'ばーちゃいむす', 10010],
  CONGA: ['Conga', 'Conga', 'こんが', 10020],
  CONGAS: ['Congas', 'Congas', 'こんがす', 10030],
  COWBELL: ['Cowbell', 'Cowbell', 'かうべる', 10040],
  CRASH: ['Crash', 'Crash', 'くらっしゅ', 10050],
  CYMBALS: ['Cymbals', 'Cymbals', 'しんばる', 10060],
  DJEMBE: ['Djembe', 'Djembe', 'じゃんべ', 10070],
  GUIRO: ['Guiro', 'Guiro', 'ぎろ', 10080],
  HANDBELL: ['Handbell', 'Handbell', 'はんどべる', 10090],
  HIHAT: ['Hi Hat', 'Hi Hat', 'はいはっと', 10100],
  HOWDOG: ['Howling Dog', 'Howling Dog', 'はうりんぐどっぐ', 10110],
  JINGLE: ['JINGLE', 'JINGLE', 'じんぐる', 10120],
  KALIMBA: ['Kalimba', 'Kalimba', 'かりんば', 10130],
  MANDOLIN: ['Mandolin', 'Mandolin', 'まんどりん', 10140],
  PERCUSSION: ['Percussion', 'Percussion', 'ぱーかっしょん', 10150],
  PEDALSTEEL: ['Pedal Steel', 'Pedal Steel', 'ぺだるすちーる', 10160],
  SHAKER: ['Shaker', 'Shaker', 'しぇいかー', 10170],
  TAMBOURINE: ['Tambourine', 'Tambourine', 'たんばりん', 10180],
  TIMBALES: ['Timbales', 'Timbales', 'てぃんばれす', 10190],
  TIMSOLO: ['Timbales Solo', 'Timbales Solo', 'てぃんばれすそろ', 10200],
  TOMTOMSOLO: ['Tom Tom Solo', 'Tom Tom Solo', 'とむとむそろ', 10210],
  TOMS: ['Toms', 'Toms', 'とむす', 10220],
  WINDBELL: ['Wind Bell', 'Wind Bell', 'ういんどべる', 10230],
  WINDCHIME: ['Wind Chime', 'Wind Chime', 'ういんどちゃいむ', 10240],
  SPERCUSSION: ['Synthesizer Percussion', 'Synthesizer Percussion', 'しんせさいざー　ぱーかっしょん', 10250],

  # Engineer
  ENGINEER: ['Engineer', 'Engineer', 'えんじにあ', 11000],
  MIXENGINEER: ['Mixing Engineer', 'Mixing Engineer', 'みきしんぐえんじにあ', 11010],
  PROGRAMING: ['Programing', 'Programing', 'ぷろぐらみんぐ', 11020],
  PROGRAMMING: ['Programming', 'Programming', 'ぷろぐらみんぐ', 11030],
  PROGRAMS: ['Programs', 'Programs', 'ぷろぐらむす', 11040],
  RECENGINEER: ['Recording Engineer', 'Recording Engineer', 'れこーでぃんぐえんじにあ', 11050],
  REMIX: ['Remix', 'Remix', 'りみっくす', 11060],
}

# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Instrumental] Instrumental FactoryGirl オブジェクト
def instrumental_factory(key)
  jt, et, yo, so = InstrumentalFactoryHash[key.to_sym]
  FB.find_or_create(:instrumental, key) do
    {
      key: key,
      title_id: title_factory_with_values(key, jt, et, yo).id,
      sort_order: so.to_i,
    }
  end if jt
end

# @param [Array<Symbol, String>] keys オブジェクトを一意に決定するキーの配列
# @return [Array<Instrumental>] Instrumental FactoryGirl オブジェクトの配列
def instrumental_factories(keys)
  keys.map { |k| instrumental_factory(k) }
end

