# @param [String] key
# @option hash [String] j_title 日本語タイトル
# @option hash [String] e_title 英語タイトル
# @option hash [String] yomi 読み
# @option hash [String] j_subtitle 日本語サブタイトル
# @option hash [String] e_subtitle 英語サブタイトル
# @option hash [String] j_comment 日本語コメント
# @option hash [String] e_comment 英語コメント
# @option hash [String] concert_type コンサートタイプ
# @option hash [Date] from 公演開始日
# @option hash [Date] to 公演終了日
# @option hash [Boolean] has_song_list song_list があれば true
# @option Hash [Boolean] has_product product があれば true
# @option Hash [Symbol] band_key バンドを一意に決定する key
# @option Hash [Fixnum] num_of_performances 公演数
# @option Hash [Fixnum] num_of_halls 公演ホール数
# @option Hash [Fixnum] sort_order 並び順
# @return [Concert] Concert FactoryGirl オブジェクト
def concert_factory(key, title_key = nil)
  title_key ||= key
  FB.find_or_create(:concert, key) do
    ans = yield.merge({key: key, title_id: title_factory(title_key).id})
    FB.replace_attr(ans, :from_id) { |k| event_date_factory(k).id }
    FB.replace_attr(ans, :to_id) { |k| event_date_factory(k).id }
    FB.replace_attr(ans, :band_id) { |k| band_factory(k).id }
    ans
  end
end

def concert_test_factory
  key = :overheat_night_get_smile
  concert_factory key do
    {
      concert_type: :live,
      from_id: '1987/12/11',
      to_id: '1988/3/11',
      num_of_performances: 3,
      num_of_halls: 2,
      has_song_list: true,
      band_id: :ms_band,
      sort_order: 2,
    }
  end
end

def concert_test2_factory
  key = :shibuya_live_in_first_live
  concert_factory key do
    {
      concert_type: :live,
      from_id: '1987/9/7',
      has_song_list: true,
      num_of_halls: 1,
      num_of_performances: 1,
      sort_order: 1
    }
  end
end
