# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Book] Book FactoryGirl オブジェクト
def book_factory(key, title_key = nil)
  FB.find_or_create(:book, key) do
    title_key ||= key
    ans = yield.merge({key: key, title_id: title_factory(title_key).id})
    FB.replace_attr(ans, :event_date_id) { |k| event_date_factory(k).id }
    FB.replace_attr(ans, :book_type) { |k| Book.book_types[k.to_s] }
    FB.replace_attr(ans, :publisher_id) { |k| company_factory(k).id }
    FB.replace_attr(ans, :seller_id) { |k| company_factory(k).id }
    FB.replace_attr(ans, :author_id) { |k| person_factory(k).id }
    FB.replace_attr(ans, :photographer_id) { |k| person_factory(k).id }
    ans
  end
end

def book_test_factory
  book_factory :wakariyasui_koi do
    {
      book_type: :poems,
      publisher_id: :kadokawa_shoten,
      seller_id: :kadokawa_shoten,
      author_id: :iNatsuoGiniro,
      photographer_id: :iNatsuoGiniro,
      isbn: '4-0416-7302-X',
      price: '596',
      event_date_id: '1987/12/18',
      sort_order: 2,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4475.html',
      j_comment: '写真のモデル',
      e_comment: 'Photographic model'
    }
  end
end

def book_test2_factory
  book_factory :shuka do
    {
      book_type: :picture_book,
      publisher_id: :kindai_eigasha,
      seller_id: :kindai_eigasha,
      photographer_id: :iYutakaNishimura,
      isbn: '4-7648-1448-X C0076 P1854E',
      price: '1,854',
      event_date_id: '1987/8/5',
      sort_order: 1
    }
  end
end

