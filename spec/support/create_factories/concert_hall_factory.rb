# @param [Concert] concert コンサート
# @param [Fixnum] sort_order 並び順
# @option hash [String] concert_key concert を一意に決定するキー
# @option hash [Concert] concert concert
# @option hash [Fixnum] sort_order 並び順
# @option hash [String] hall_key hall を一意に決定するキー
# @option hash [Hall] hall hall
# @option hash [String] date 日付文字列
# @option hash [String] j_comment 日本語コメント
# @option hash [String] e_comment 英語コメント
# @option hash [String] j_product 日本語名産品
# @option hash [String] e_product 英語名産品
# @option hash [SongList] song_list ソングリスト
# @option hash [Device] device デバイス
# @return [ConcertHall] ConcertHall FactoryGirl オブジェクト
def concert_hall_factory(concert, sort_order)
  ans = ConcertHall.get_object(concert, sort_order)
  unless ans
    ans = FB.find_or_create(:concert_hall, nil) do
      hash = yield.merge({concert_id: concert.id, sort_order: sort_order})
      FB.replace_attr(hash, :hall_id) { |k| hall_factory(k).id }
      FB.replace_attr(hash, :event_date_id) { |k| event_date_factory(k).id }
      hash
    end
  end
  ans
end

def concert_hall_test_factory
  concert_hall_factory(concert_test_factory, 1) do
    {
      hall_id: :渋谷ライブイン,
      event_date_id: '1987/9/7',
      list_id: list_test_factory.id,
    }
  end
end

def concert_hall_test2_factory
  concert_hall_factory(concert_test2_factory, 2) do
    {
      hall_id: :渋谷ライブイン,
      event_date_id: '1987/9/7',
      list_id: list_test2_factory.id,
    }
  end
end

