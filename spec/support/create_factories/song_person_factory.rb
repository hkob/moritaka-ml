# @param [Hash] hash
# @option hash [ListContent] list_content ソングリスト曲
# @option hash [Fixnum] person_key 関係者を一意に確定する key
# @option hash [Person] person 関係者
# @option hash [Fixnum] instrumental_key 楽器を一意に確定する key
# @option hash [Instrumental] instrumental 楽器
# @return [SongPerson] SongPerson FactoryGirl オブジェクト
def song_person_factory(list_content, person, instrumental)
  if person.nil?
    print instrumental.name(true)
  elsif instrumental.nil?
    print person.name(true)
  end
  FB.find_or_create(:song_person, nil) do
    {
      list_content_id: list_content.id,
      person_id: person.id,
      instrumental_id: instrumental.id
    }
  end
end

# @return [SongPerson] SongPerson FactoryGirl オブジェクト
def song_person_test_factory
  song_person_factory list_content_test_factory, person_factory(:iChisatoMoritaka), instrumental_factory(:VOCAL)
end

