# @param [Hash] hash
# @option hash [Song] song 曲
# @option hash [Symbol, String] person_key 関係者を一意に決定するキー
# @option hash [Person] person 関係者
# @option hash [Symbol, String, Fixnum] sort_order 並び順
# @return [Lyric] Lyric FactoryGirl オブジェクト
def lyric_factory(key)
  FB.find_or_create(:lyric, key) do
    song_key, person_key, sort_order = key.split(':')
    person = person_factory person_key
    song = song_factory song_key
    {
      key: key,
      song_id: song.id,
      person_id: person.id,
      sort_order: sort_order.to_i
    }
  end
end

# @param [Symbol] song_key 関係者を一意に決定するキー
# @param [Array<Symbol, String>] people_keys 関係者を一意に決定するキーの配列
# @return [Array<Lyric>] Lyric FactoryGirl オブジェクトの配列
def lyric_factories(song_key, people_keys)
  people_keys.map.with_index { |p, i| lyric_factory("#{song_key}:#{p}:#{i}") }
end

