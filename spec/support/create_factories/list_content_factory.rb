# @param [Hash] hash
# @option hash [List] list ソングリスト
# @option hash [String] song_key 曲を一意に確定する key
# @option hash [Song] song 曲
# @option hash [String] j_title 曲でないときの日本語名
# @option hash [String] e_title 曲でないときの英語名
# @option hash [String] j_ver 日本語バージョン名
# @option hash [String] e_ver 英語バージョン名
# @option hash [String,Fixnum] sort_order 並び順
# @return [ListContent] ListContent FactoryGirl オブジェクト
def list_content_factory(list, sort_order)
  ans = list.list_contents.sort_order_value_is(sort_order).take
  unless ans
    ans = FB.find_or_create(:list_content, nil) do
      hash = yield.merge({list_id: list.id, sort_order: sort_order})
      FB.replace_attr(hash, :song_id) { |k| song_factory(k).id }
      hash
    end
  end
  ans
end

# @return [ListContent] ListContent FactoryGirl オブジェクト
def list_content_test_factory
  list = list_test_factory
  list_content_factory(list, 9) do
    {song_id: :new_season}
  end
end

# @return [ListContent] ListContent FactoryGirl オブジェクト
def list_content_test2_factory
  list = list_test2_factory
  list_content_factory(list, 2) do
    {song_id: :yumeno_owari}
  end
end

# @return [ListContent] ListContent FactoryGirl オブジェクト
def list_content_test3_factory
  list = list_test3_factory
  list_content_factory(list, 1) do
    {j_title: '哀楽・都', e_title: 'Airaku - Miyako'}
  end
end
