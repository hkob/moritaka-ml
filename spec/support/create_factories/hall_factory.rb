HallFactoryHash = {
  #### 北海道
  札幌メッセホール: [:Hokkaidou, 1, '札幌メッセホール', 'Sapporo Messe Hall', 'さっぽろめっせほーる'],
  札幌道新ホール: [:Hokkaidou, 2, '札幌道新ホール', 'Sapporo DOSHIN Hall', 'さっぽろどうしんほーる'],
  札幌市教育文化会館: [:Hokkaidou, 3, '札幌市教育文化会館', 'Sapporo Education and Culture Hall', 'さっぽろしきょういくぶんかかいかん'],
  北海道厚生年金会館: [:Hokkaidou, 4, '北海道厚生年金会館', 'Hokkaido KOSEI NENKIN KAIKAN', 'ほっかいどうこうせいねんきんかいかん'],
  旭川市民文化会館: [:Hokkaidou, 5, '旭川市民文化会館', 'Asahikawa Civic Culture Hall', 'あさひかわしみんぶんかかいかん'],
  釧路市民文化会館: [:Hokkaidou, 6, '釧路市民文化会館', 'Kushiro City Cultural Hall', 'くしろしみんぶんかかいかん'],
  #### 青森
  青森市文化会館: [:Aomori, 1, '青森市文化会館', 'Aomori City Cultural Hall', 'あおもりしぶんかかいかん'],
  #### 秋田
  秋田経済法科大学: [:Akita, 1, '秋田経済法科大学', 'Akita Economics and Low University', 'あきたけいざいほうかだいがく'],
  秋田市文化会館: [:Akita, 2, '秋田市文化会館', 'Akita City Cultural Hall', 'あきたしぶんかかいかん'],
  #### 岩手
  岩手県民会館: [:Iwate, 1, '岩手県民会館', 'Iwate Prefectural Hall', 'いわてけんみんかいかん'],
  #### 福島
  郡山市民文化センター: [:Fukushima, 1, '郡山市民文化センター', 'Koriyama City Cultural Center', 'こおりやましみんぶんかせんたー'],
  学法福島高校: [:Fukushima, 2, '学法福島高校', 'GAKUHO Fukushima High School', 'がくほうふくしまこうこう'],
  日本大学郡山: [:Fukushima, 3, '日本大学郡山', 'Nihon university Koriyama', 'にほんだいがくこうりやま'],
  #### 山形
  山形市民会館: [:Yamagata, 1, '山形市民会館', "Yamagata Citizens' Hall", 'やまがたしみんかいかん'],
  #### 宮城
  仙台モーニングムーン: [:Miyagi, 1, '仙台モーニングムーン', 'Sendai Morning Moon', 'せんだいもーにんぐむーん'],
  仙台市民会館: [:Miyagi, 2, '仙台市民会館', 'Sendai City Cultural Hall', 'せんだいしみんかいかん'],
  東北学院大学: [:Miyagi, 3, '東北学院大学', 'Tokoku Gakuin University', 'とうほくがくいんだいがく'],
  宮城県民会館: [:Miyagi, 4, '宮城県民会館', 'Miyagi Prefectural Auditorium', 'みやぎけんみんかいかん'],
  仙台市青年文化センター: [:Miyagi, 5, '仙台市青年文化センター', 'Sendai-shi SEINEN BUNKA center', 'せんだいしせいねんぶんかせんたー'],
  栗原文化会館: [:Miyagi, 6, '栗原文化会館', 'Kurihara BUNKA KAIKAN', 'くりはらぶんかかいかん'],
  石巻大学: [:Miyagi, 7, '石巻大学', 'Ishinomaki University', 'いしのまきだいがく'],
  仙台サンプラザホール: [:Miyagi, 8, '仙台サンプラザホール', 'Sendai Sunplaza Hall', 'せんだいさんぷらざほーる'],
  #### 茨城
  流通経済大学: [:Ibaraki, 1, '流通経済大学', 'Ryutsu Keizai University', 'りゅうつうけいざいだいがく'],
  茨城大学: [:Ibaraki, 2, '茨城大学', 'Ibaraki University', 'いばらきだいがく'],
  水戸市民会館: [:Ibaraki, 3, '水戸市民会館', 'Mito SHIMIN KAIKAN', 'みとしみんかいかん'],
  筑波大学: [:Ibaraki, 4, '筑波大学', 'Tsukuba University', 'つくばだいがく'],
  鹿島勤労文化会館: [:Ibaraki, 5, '鹿島勤労文化会館', 'Kashima KINROU BUNKA KAIKAN', 'かしまきんろうぶんかかいかん'],
  茨城県立県民文化センター: [:Ibaraki, 6, '茨城県立県民文化センター', 'Ibaraki KENMIN BUNKA center', 'いばらきけんりつけんみんぶんかせんたー'],
  いわき明星大学: [:Ibaraki, 7, 'いわき明星大学', 'Iwaki Meisei University', 'いわきめいせいだいがく'],
  #### 栃木
  足利工業大学: [:Tochigi, 1, '足利工業大学', 'Ashikaga Institute of Technology', 'あしかがこうぎょうだいがく'],
  宇都宮市文化会館: [:Tochigi, 2, '宇都宮市文化会館', 'Utsunomiya City Cultural Hall', 'うつのみやしぶんかかいかん'],
  足利市民会館: [:Tochigi, 3, '足利市民会館', 'Ashikaga SHIMIN KAIKAN', 'あしかがしみんかいかん'],
  #### 埼玉
  大宮ソニックシティ: [:Saitama, 1, '大宮ソニックシティ', 'Omiya Sonic City', 'おおみやそにっくしてぃ'],
  東洋大学: [:Saitama, 2, '東洋大学', 'Toyo university', 'とうようだいがく'],
  埼玉工業大学: [:Saitama, 3, '埼玉工業大学', 'Saitama Institute of Technology', 'さいたまこうぎょうだいがく'],
  久喜総合文化会館: [:Saitama, 4, '久喜総合文化会館', 'Kuki SOGO BUNKA KAIKAN', 'くきそうごうぶんかかいかん'],
  立正大学: [:Saitama, 5, '立正大学', 'Rissho University', 'りっしょうだいがく'],
  城西大学: [:Saitama, 6, '城西大学', 'Josai University', 'じょうさいだいがく'],
  川口リリア・メインホール: [:Saitama, 7, '川口リリア・メインホール', 'Kawaguchi Lilia Main Hall', 'かわぐちりりあめいんほーる'],
  狭山市市民会館: [:Saitama, 8, '狭山市市民会館', 'Sayama City Center', 'さやましみんかいかん'],
  #### 山梨
  山梨学院大学: [:Yamanashi, 1, '山梨学院大学', 'Yamanashi Gakuin University', 'やまなしがくいんだいがく'],
  山梨県立県民文化ホール: [:Yamanashi, 2, '山梨県立県民文化ホール', "Yamanashi Prefectural Citizen's Culture Hall", 'やまなしけんりつけんみんぶんかほーる'],
  #### 群馬
  桐生市産業文化会館: [:Gunma, 1, '桐生市産業文化会館', 'Kiryu-shi SANGYO BUNKA KAIKAN', 'きりゅうしさんぎょうぶんかかいかん'],
  群馬音楽センター: [:Gunma, 2, '群馬音楽センター', 'Gunma Ongaku Center', 'ぐんまおんがくせんたー'],
  伊勢崎市文化会館: [:Gunma, 3, '伊勢崎市文化会館', 'Isesaki-shi BUNKA KAIKAN', 'いせさきしぶんかかいかん'],
  群馬県民会館: [:Gunma, 4, '群馬県民会館', 'Gunma KENMIN KAIKAN', 'ぐんまけんみんかいかん'],
  #### 神奈川
  江ノ島シーサイド: [:Kanagawa, 1, '江ノ島シーサイド', 'Enoshima sea side', 'えのしましーさいど'],
  向ヶ丘遊園: [:Kanagawa, 2, '向ヶ丘遊園', 'Mukogaoka-Yuen', 'むこうがおかゆうえん'],
  クラブチッタ川崎: [:Kanagawa, 3, 'クラブチッタ川崎', "CLUB CITTA' Kawasaki", 'くらぶちったかわさき'],
  三浦海岸: [:Kanagawa, 4, '三浦海岸', 'Miura Kaigan', 'みうらかいがん'],
  文教大学: [:Kanagawa, 5, '文教大学', 'Bunkyo university', 'ぶんきょうだいがく'],
  北里大学: [:Kanagawa, 6, '北里大学', 'Kitasato University', 'きたさとだいがく'],
  神奈川県民ホール: [:Kanagawa, 7, '神奈川県民ホール', 'Kanagawa Kenmin Hall', 'かながわけんみんほーる'],
  川崎教育文化会館: [:Kanagawa, 8, '川崎教育文化会館', 'Kawasaki Civic Auditorium of Culture and Education', 'かわさききょういくぶんかかいかん'],
  江ノ島サーフ: [:Kanagawa, 9, '江ノ島サーフ', 'Enoshima Surf', 'えのしまさーふ'],
  大磯: [:Kanagawa, 10, '大磯', 'Oiso', 'おおいそ'],
  横浜文化体育館: [:Kanagawa, 11, '横浜文化体育館', 'Yokohama Cultural Gymnasium', 'よこはまぶんかたいいくかん'],
  神奈川大学: [:Kanagawa, 12, '神奈川大学', 'Kanagawa University', 'かながわだいがく'],
  秦野文化会館: [:Kanagawa, 13, '秦野文化会館', 'Hadano-shi culture hall', 'はだのぶんかかいかん'],
  関内ホール: [:Kanagawa, 14, '関内ホール', 'Kannai Hall', 'かんないほーる'],
  綾瀬市文化会館: [:Kanagawa, 15, '綾瀬市文化会館', 'Ayase-shi BUNKA KAIKAN', 'あやせしぶんかかいかん'],
  東海大学_湘南校舎: [:Kanagawa, 16, '東海大学 (湘南校舎)', 'Tokai university (Shonan schoolhouse)', 'とうかいだいがくしょうなんこうしゃ'],
  横浜アリーナ: [:Kanagawa, 17, '横浜アリーナ', 'Yokohama Arena', 'よこはまありーな'],
  よこすか芸術劇場: [:Kanagawa, 18, 'よこすか芸術劇場', 'Yokosuka Arts Theatre', 'よこすかげいじゅつげきじょう'],
  #### 千葉
  帝京技術大学: [:Chiba, 1, '帝京技術大学', 'Teikyo technical college', 'ていきょうぎじゅつだいがく'],
  日本大学松戸歯学部: [:Chiba, 2, '日本大学松戸歯学部', 'Nihon University School of Dentistry at Matsudo', 'にほんだいがくまつどしがくぶ'],
  船橋札幌ビール工場: [:Chiba, 3, '船橋札幌ビール工場', 'Funabashi Sapporo-beer factory', 'ふなばしさっぽろびーるこうじょう'],
  千葉日大第一高等学校: [:Chiba, 4, '千葉日大第一高等学校', 'Chiba Nihon University Daiichi Senior High School', 'ちばにちだいだいいちこうとうがっこう'],
  千葉県文化会館: [:Chiba, 5, '千葉県文化会館', 'Chiba-ken BUNKA KAIKAN', 'ちばけんぶんかかいかん'],
  #### 東京
  渋谷ライブイン: [:Tokyo, 1, '渋谷ライブイン', 'Shibuya Live-in', 'しぶやらいぶいん'],
  日本青年館: [:Tokyo, 2, '日本青年館', 'Nippon-seinenkan', 'にほんせいねんかん'],
  inkstick_芝浦_factory: [:Tokyo, 3, 'INKSTICK 芝浦 FACTORY', 'INKSTICK Shibaura FACTORY', 'いんすてぃっく　しばうら　ふぁくとりー'],
  日比谷野外音楽堂: [:Tokyo, 4, '日比谷野外音楽堂', 'Hibiya YAGAI ONGAKU-DO(bandstand)', 'ひびややがいおんがくどう'],
  有明MZAコンベンション: [:Tokyo, 5, '有明MZAコンベンション', 'Ariake MZA Convention', 'ありあけえむざこんべんしょん'],
  渋谷公会堂: [:Tokyo, 6, '渋谷公会堂', 'Shibuya KOKAIDO', 'しぶやこうかいどう'],
  立教女学院: [:Tokyo, 7, '立教女学院', "St Margaret's School", 'りっきょうじょがくえん'],
  東京都立科学技術大学: [:Tokyo, 8, '東京都立科学技術大学', 'Tokyo Metropolitan Institute of Technology', 'とうきょうとりつかがくぎじゅつだいがく'],
  汐留_PIT_II: [:Tokyo, 9, '汐留 PIT II', 'Shiodome PIT II', 'しおどめぴっとつう'],
  中野サンプラザ: [:Tokyo, 10, '中野サンプラザ', 'Nakano Sunplaza', 'なかのさんぷらざ'],
  東京経済大学: [:Tokyo, 11, '東京経済大学', 'Tokyo Keizai University', 'とうきょうけいざいだいがく'],
  拓殖大学: [:Tokyo, 12, '拓殖大学', 'Takushoku university', 'たくしょくだいがく'],
  武蔵野女子大学: [:Tokyo, 13, '武蔵野女子大学', "Musashino Women's University", 'むさしのじょしだいがく'],
  日本大学経済学部: [:Tokyo, 14, '日本大学経済学部', 'Nihon University College of Economics', 'にほんだいがくけいざいがくぶ'],
  明治薬科大学: [:Tokyo, 15, '明治薬科大学', 'Meiji Pharmaceutical University', 'めいじやっかだいがく'],
  武蔵工業大学: [:Tokyo, 16, '武蔵工業大学', 'Musashi Institute of Technology', 'むさしこうぎょうだいがく'],
  NHKホール: [:Tokyo, 17, 'NHKホール', 'NHK Hall', 'えぬえいちけいほーる'],
  東京厚生年金会館: [:Tokyo, 18, '東京厚生年金会館', 'Tokyo KOSEI NENKIN KAIKAN', 'とうきょうこうせいねんきんかいきん'],
  五反田ゆうぽうと: [:Tokyo, 19, '五反田ゆうぽうと', 'Gotanda YUPOTO', 'ごたんだゆうぽうと'],
  中央大学: [:Tokyo, 20, '中央大学', 'Chuo University', 'ちゅうおうだいがく'],
  日本武道館: [:Tokyo, 21, '日本武道館', 'Nippon Budokan', 'にっぽんぶどうかん'],
  東京歯科大学: [:Tokyo, 22, '東京歯科大学', 'Tokyo Dental College', 'とうきょうしかだいがく'],
  大妻女子短期大学: [:Tokyo, 23, '大妻女子短期大学', "Otsuma Women's University Junior College Division", 'おおつまじょしたんきだいがく'],
  駒沢大学: [:Tokyo, 24, '駒沢大学', 'Komazawa University', 'こまざわだいがく'],
  成徳女子短期大学: [:Tokyo, 25, '成徳女子短期大学', "Seitoku women's junior college", 'せいとくじょしたんきだいがく'],
  福生市民会館: [:Tokyo, 26, '福生市民会館', 'Fussa Shimin Kaikan', 'ふっさしみんかいかん'],
  日野市民会館: [:Tokyo, 27, '日野市民会館', 'Hino SHIMIN KAIKAN', 'ひのしみんかいかん'],
  Space_Zero: [:Tokyo, 28, 'Space Zero', 'Space Zero', 'すぺーすぜろ'],
  白百合女子大学: [:Tokyo, 29, '白百合女子大学', 'Shirayuri University', 'しらゆりじょしだいがく'],
  清泉女子大学: [:Tokyo, 30, '清泉女子大学', 'Seisen University', 'せいせんじょしだいがく'],
  芝メルパルク: [:Tokyo, 31, '芝メルパルク', 'Shiba Mielparque', 'しばめるぱるく'],
  北とぴあさくらホール: [:Tokyo, 32, '北とぴあさくらホール', 'HOKUTOPIA SAKURA Hall', 'ほくとぴあさくらほーる'],
  慶応大学_日吉校舎: [:Tokyo, 33, '慶応大学 (日吉校舎)', 'Keio university (Hiyoshi schoolhouse)', 'けいおうだいがくひよしこうしゃ'],
  日清パワーステーション: [:Tokyo, 34, '日清パワーステーション', 'Nisshin Power Station', 'にっしんぱわーすてーしょん'],
  東京国際フォーラム: [:Tokyo, 35, '東京国際フォーラム', 'Tokyo International Forum', 'とうきょうこくさいふぉーらむ'],
  赤坂BLITZ: [:Tokyo, 36, '赤坂 BLITZ', 'Akasaka BLITZ', 'あかさかぶりっつ'],
  昭和女子大学人見記念講堂: [:Tokyo, 37, '昭和女子大学人見記念講堂', "Showa Women's University Hitomi Memorial Hall", 'しょうわじょしだいがく　ひとみきねんこうどう'],
  恵比寿ザ・ガーデンホール: [:Tokyo, 38, '恵比寿ザ・ガーデンホール', 'Ebisu The Garden Hall', 'えびす　ざ　がーでんほーる'],
  ZeppTokyo: [:Tokyo, 39, 'Zepp Tokyo', 'Zepp Tokyo', 'ぜっぷ　とうきょう'],
  ZeppDiverCity: [:Tokyo, 40, 'Zepp DiverCity', 'Zepp DiverCity', 'ぜっぷ　だいばーしてぃ'],
  国技館: [:Tokyo, 41, '国技館', '{KOKUGIKAN}', 'こくぎかん'],
  #### 長野
  長野市内ライブハウス: [:Nagano, 1, '長野市内ライブハウス', 'Live House in the Nagano city', 'ながのしないらいぶはうす'],
  信州大学: [:Nagano, 2, '信州大学', 'Shinsyu university', 'しんしゅうだいがく'],
  長野県民文化会館: [:Nagano, 3, '長野県民文化会館', 'Nagano KENMIN BUNKA KAIKAN', 'ながのけんみんぶんかかいかん'],
  白馬: [:Nagano, 4, '白馬', 'Hakuba', 'はくば'],
  長野県県民文化会館: [:Nagano, 5, '長野県県民文化会館', 'Nagano-ken Kenmin Bunka Kaikan', 'ながのけんけんみんぶんかかいかん'],
  松本文化会館: [:Nagano, 6, '松本文化会館', 'Matsumoto BUNKA KAIKAN', 'まつもとぶんかかいかん'],
  長野市民会館: [:Nagano, 7, '長野市民会館', 'Nagano Civic Hall', 'ながのしみんかいかん'],
  ホクト文化ホール: [:Nagano, 8, 'ホクト文化ホール(長野県民文化会館)', 'Naganoken Kenmin Bunka kaikan', 'ほくとぶんかほーる'],
  #### 静岡
  静岡市民文化会館: [:Shizuoka, 1, '静岡市民文化会館', 'Shizuoka City Culture Hall', 'しずおかしみんぶんかかいかん'],
  浜松市民会館: [:Shizuoka, 2, '浜松市民会館', 'Hamamatsu SHIMIN KAIKAN', 'はままつしみんかいかん'],
  つま恋: [:Shizuoka, 3, 'つま恋', 'Tsumagoi', 'つまごい'],
  磐田市民文化会館: [:Shizuoka, 4, '磐田市民文化会館', 'Iwata Shimin Bunka Kaikan', 'いわたしみんぶんかかいかん'],
  静岡大学: [:Shizuoka, 5, '静岡大学', 'Shizuoka University', 'しずおかだいがく'],
  沼津市民文化センター: [:Shizuoka, 6, '沼津市民文化センター', 'Numazu SHIMIN BUNKA Center', 'ぬまづしみんぶんかせんたー'],
  アクトシティ浜松: [:Shizuoka, 7, 'アクトシティ浜松', 'Act City Hamamatsu', 'あくとしてぃはままつ'],
  #### 新潟
  新潟県民会館: [:Niigata, 1, '新潟県民会館', 'Niigata Prefectural Civic Center', 'にいがたけんみんかいかん'],
  新潟薬科大学: [:Niigata, 2, '新潟薬科大学', 'Nigata University of Phermacy and Applied Life Sciences', 'にいがたやっかだいがく'],
  新潟産業大学: [:Niigata, 3, '新潟産業大学', 'Niigata Sangyo University', 'にいがたさんぎょうだいがく'],
  六日町文化会館: [:Niigata, 4, '六日町文化会館', 'Muikamachi BUNKA KAIKAN', 'むいかまちぶんかかいかん'],
  柏崎市民会館: [:Niigata, 5, '柏崎市民会館', 'Kashiwazaki SHIMIN KAIKAN', 'かしわざきしみんかいかん'],
  #### 富山
  高岡短期大学: [:Toyama, 1, '高岡短期大学', 'Takaoka National College', 'たかおかたんきだいがく'],
  北アルプス文化センター: [:Toyama, 2, '北アルプス文化センター', 'North-Alps BUNKA center', 'きたあるぷすぶんかせんたー'],
  富山県民会館: [:Toyama, 3, '富山県民会館', 'Toyama KENMIN KAIKAN', 'とやまけんみんかいかん'],
  #### 石川
  金沢大学: [:Ishikawa, 1, '金沢大学', 'Kanazawa university', 'かなざわだいがく'],
  金沢医科大学: [:Ishikawa, 2, '金沢医科大学', 'Kanazawa Medical University', 'かなざわいかだいがく'],
  金沢市文化ホール: [:Ishikawa, 3, '金沢市文化ホール', 'Kanazawa Bunka Hall', 'かなざわしぶんかほーる'],
  石川厚生年金会館: [:Ishikawa, 4, '石川厚生年金会館', 'Ishikawa KOSEI NENKIN KAIKAN', 'いしかわこうせいねんきんかいかん'],
  金沢市観光会館: [:Ishikawa, 5, '金沢市観光会館', 'Kanazawa-shi KANKO KAIKAN', 'かなざわしかんこうかいかん'],
  本多の森ホール: [:Ishikawa, 6, '本多の森ホール', 'HONDANOMORI Hall', 'ほんだのもりほーる'],
  #### 愛知
  名古屋ハートランド: [:Aichi, 1, '名古屋ハートランド', 'Nagaya Heart Land', 'なごやはーとらんど'],
  豊橋科学技術大学: [:Aichi, 2, '豊橋科学技術大学', 'Toyohashi University of Technology', 'とよはしかがくぎじゅつだいがく'],
  名古屋勤労会館: [:Aichi, 3, '名古屋勤労会館', 'Nagoya KINRO KAIKAN', 'なごやきんろうかいかん'],
  名古屋大学: [:Aichi, 4, '名古屋大学', 'Nagoya University', 'なごやだいがく'],
  愛知勤労会館: [:Aichi, 5, '愛知勤労会館', 'Aichi KINRO KAIKAN', 'あいちきんろうかいかん'],
  愛知県立大学: [:Aichi, 6, '愛知県立大学', 'Aichi Prefectural University', 'あいちけんりつだいがく'],
  名古屋市民会館: [:Aichi, 7, '名古屋市民会館', 'Nagoya SHIMIN KAIKAN', 'なごやしみんかいかん'],
  名古屋市総合体育館: [:Aichi, 8, '名古屋市総合体育館', 'Nagoya Civic General Gymnasium', 'なごやしそうごうたいいくかん'],
  豊橋勤労福祉会館: [:Aichi, 9, '豊橋勤労福祉会館', 'Toyohashi KINROU FUKUSHI KAIKAN', 'とよはしきんろうふくしかいかん'],
  名古屋センチュリーホール: [:Aichi, 10, '名古屋センチュリーホール', 'Nagaya Century Hall', 'なごやせんちゅりーほーる'],
  名古屋レインボーホール: [:Aichi, 11, '名古屋レインボーホール', 'Nagoya Rainbow Hall', 'なごやれいんぼーほーる'],
  日本特殊陶業市民会館: [:Aichi, 12, '日本特殊陶業市民会館', 'NTK Hall', 'にほんとくしゅこうぎょうしみんかいかん'],
  ZeppNagoya: [:Aichi, 13, 'Zepp Nagoya', 'Zepp Nagoya', 'ぜっぷ　なごや'],
  #### 岐阜
  岐阜経済大学: [:Gifu, 1, '岐阜経済大学', 'Gifu Keizai University', 'ぎふけいざいだいがく'],
  岐阜市文化センター: [:Gifu, 2, '岐阜市文化センター', 'Gifu City Culture Center', 'ぎふしぶんかせんたー'],
  岐阜市民会館: [:Gifu, 3, '岐阜市民会館', 'Gifu Civic Auditorium', 'ぎふしみんかいかん'],
  #### 福井
  福井市文化会館: [:Fukui, 1, '福井市文化会館', 'Fukui-shi BUNKA KAIKAN', 'ふくいしぶんかかいかん'],
  #### 三重
  皇學館大学: [:Mie, 1, '皇學館大学', 'Kogakkan University', 'こうがっかんだいがく'],
  四日市市民会館: [:Mie, 2, '四日市市民会館?', 'Yokkaichi SHIMIN KAIKAN?', 'よっかいちししみんかいかん'],
  四日市市文化会館: [:Mie, 3, '四日市市文化会館', 'Yokkaichi City Cultural Center', 'よっかいちしぶんかかいかん'],
  #### 滋賀
  滋賀大学: [:Shiga, 1, '滋賀大学', 'Shiga university', 'しがだいがく'],
  野洲文化ホール: [:Shiga, 2, '野洲文化ホール', 'Yasu BUNKA HALL', 'やすぶんかほーる'],
  守山市民ホール: [:Shiga, 3, '守山市民ホール', 'Moriyama Citizen Hall', 'もりやましみんほーる'],
  #### 和歌山
  和歌山県民文化会館: [:Wakayama, 1, '和歌山県民文化会館', 'Wakayama Prefectural Cultural Hall', 'わかやまけんみんぶんかかいかん'],
  和歌山市民会館: [:Wakayama, 2, '和歌山市民会館', 'Wakayama Municipal Auditorium', 'わかやましみんかいかん'],
  #### 京都
  京都府立医科大学: [:Kyoto, 1, '京都府立医科大学', 'Kyoto Prefectural University of Medicine', 'きょうとふりついかだいがく'],
  京都会館: [:Kyoto, 2, '京都会館', 'Kyoto Kaikan', 'きょうとかいかん'],
  京都産業大学: [:Kyoto, 3, '京都産業大学', 'Kyoto Sangyou University', 'きょうとさんぎょうだいがく'],
  久美浜町公園: [:Kyoto, 4, '久美浜町公園', 'Kumihama-cho park', 'くみはまちょうこうえん'],
  舞鶴市総合文化会館: [:Kyoto, 5, '舞鶴市総合文化会館', 'Maizuru-shi SOGO BUNKA KAIKAN', 'まいづるしそうごうぶんかかいかん'],
  #### 奈良
  奈良県文化会館: [:Nara, 1, '奈良県文化会館', 'Nara-KEN BUNKA KAIKAN', 'ならけんぶんかかいかん'],
  #### 大阪
  大阪厚生年金会館: [:Osaka, 1, '大阪厚生年金会館', 'Osaka KOSEI NENKIN KAIKAN', 'おおさかこうせいねんきんかいかん'],
  大阪あじかわ: [:Osaka, 2, '大阪あじかわ', 'Osaka AJIKAWA', 'おおさかあじかわ'],
  阪南町文化センター: [:Osaka, 3, '阪南町文化センター', 'Hannan-Machi BUNKA Center', 'はんなんまちぶんかせんたー'],
  大阪医科大学: [:Osaka, 4, '大阪医科大学', 'Osaka Medical college', 'おおさかいかだいがく'],
  大阪メルパルク: [:Osaka, 5, '大阪メルパルク', 'Osaka Mielparque', 'おおさかめるぱるく'],
  大阪城ホール: [:Osaka, 6, '大阪城ホール', 'Osaka-Jo Hall', 'おおさかじょうほーる'],
  フェスティバルホール: [:Osaka, 7, 'フェスティバルホール', 'Festival Hall', 'ふぇすてぃばるほーる'],
  大阪IMPホール: [:Osaka, 8, '大阪 IMP ホール', 'Osaka IMP Hall', 'おおさかあいえむぴーほーる'],
  オリックス劇場: [:Osaka, 9, 'オリックス劇場', 'ORIX THEATER', 'おりっくすげきじょう'],
  ZeppNamba: [:Osaka, 10, 'Zepp Namba', 'Zepp Namba', 'ぜっぷ　なんば'],
  #### 兵庫
  神戸文化ホール: [:Hyogo, 1, '神戸文化ホール', 'Kobe Bunka Hall', 'こうべぶんかほーる'],
  岡山商科大学: [:Hyogo, 2, '岡山商科大学', 'Okayama Shoka University', 'おかやましょうかだいがく'],
  神戸国際会館: [:Hyogo, 3, '神戸国際会館', 'Kobe International House', 'こうべこくさいかいかん'],
  姫路市文化センター: [:Hyogo, 4, '姫路市文化センター', 'Himeji-shi BUNKA Center', 'ひめじしぶんかせんたー'],
  メリケンパーク: [:Hyogo, 5, 'メリケンパーク', 'MERIKEN Park', 'めりけんぱーく'],
  神戸国際会館ハーバーランドプラザ: [:Hyogo, 6, '神戸国際会館ハーバーランドプラザ', 'Kobe International House, Harborland Plaza', 'こうべこくさいかいかん　はーばーらんどぷらざ'],
  #### 岡山
  岡山市民会館: [:Okayama, 1, '岡山市民会館', 'Okayama Civic Hall', 'おかやましみんかいかん'],
  #### 広島
  広島蒲苅: [:Hiroshima, 1, '広島蒲苅', 'Hiroshima Kamagari', 'ひろしまかまがり'],
  広島郵便貯金会館: [:Hiroshima, 2, '広島郵便貯金会館', 'Hiroshima YUBINCHOKIN KAIKAN', 'ひろしまゆうびんちょきんかいかん'],
  福山大学: [:Hiroshima, 3, '福山大学', 'Fukuyama University', 'ふくやまだいがく'],
  メルパルクホール広島: [:Hiroshima, 4, 'メルパルクホール広島', 'Mielparque Hall Hiroshima', 'めるぱるくほーるひろしま'],
  広島厚生年金会館: [:Hiroshima, 5, '広島厚生年金会館', 'Hiroshima KOSEI NENKIN KAIKAN', 'ひろしまこうせいねんきんかいかん'],
  #### 島根
  出雲市民会館: [:Shimane, 1, '出雲市民会館', 'Izumo SHIMIN KAIKAN', 'いずもしみんかいかん'],
  島根県民会館: [:Shimane, 2, '島根県民会館', 'Shimane Civic Center', 'しまねけんみんかいかん'],
  #### 鳥取
  米子市公会堂: [:Tottori, 1, '米子市公会堂', 'Yonago Public Hall', 'よなごしこうかいどう'],
  鳥取県民会館: [:Tottori, 2, '鳥取県民会館', "Tottori Prefecture Citizens' Culture Hall", 'とっとりけんみんかいかん'],
  鳥取県立県民文化会館: [:Tottori, 3, '鳥取県立県民文化会館', 'Tottori KENRITSU KENMIN KAIKAN', 'とっとりけんりつけんみんぶんかかいかん'],
  #### 山口
  阿武町: [:Yamaguchi, 1, '阿武町', 'Abu-cho', 'あぶちょう'],
  徳山市文化会館: [:Yamaguchi, 2, '徳山市文化会館', 'Tokuyama-shi BUNKA KAIKAN', 'とくやましぶんかかいかん'],
  #### 香川
  高松市民会館: [:Kagawa, 1, '高松市民会館', 'Takamatsu SHIMIN KAIKAN', 'たかまつしみんかいかん'],
  香川県県民ホール: [:Kagawa, 2, '香川県県民ホール', 'Kagawa-ken KENMIN Hall', 'かがわけんけんみんほーる'],
  #### 愛媛
  愛媛文化講堂: [:Ehime, 1, '愛媛文化講堂', 'Ehime BUNKA KODO', 'えひめぶんかこうどう'],
  愛媛県県民文化会館: [:Ehime, 2, '愛媛県県民文化会館', 'Ehime KENMIN BUNKA KAIKAN', 'えひめけんけんみんぶんかかいかん'],
  松山市民会館: [:Ehime, 3, '松山市民会館', 'Matsuyama SHIMIN KAIKAN', 'まつやましみんかいかん'],
  #### 徳島
  徳島ベガホール: [:Tokushima, 1,  '徳島ベガホール', 'Tokushima Vega hall', 'とくしまべがほーる'],
  鳴門市文化会館: [:Tokushima, 2, '鳴門市文化会館', 'Naruto Bunka Kaikan', 'なるとぶんかかいかん'],
  徳島市立文化センター: [:Tokushima, 3, '徳島市立文化センター', 'Tokushima SHIRITSU BUNKA Center', 'とくしましりつぶんかせんたー'],
  #### 高知
  高知県民文化ホール: [:Kouchi, 1, '高知県民文化ホール', 'Kochi Prefectural Culture Hall', 'こうちけんみんぶんかほーる'],
  #### 福岡
  福岡ビブレ: [:Fukuoka, 1, '福岡ビブレ', 'Fukuoka VIVRE', 'ふくおかびぶれ'],
  九州工業大学: [:Fukuoka, 2, '九州工業大学', 'Kyusyu institute of technology', 'きゅうしゅうこうぎょうだいがく'],
  福岡都久志会館: [:Fukuoka, 3, '福岡都久志会館', 'Fukuoka Tsukushi KAIKAN', 'ふくおかつくしかいかん'],
  九州大学: [:Fukuoka, 4, '九州大学', 'Kyusyu University', 'きゅうしゅうだいがく'],
  福岡郵便貯金会館: [:Fukuoka, 5, '福岡郵便貯金会館', 'Fukuoka YUBINCHOKIN KAIKAN', 'ふくおかゆうびんちょきんかいかん'],
  平和台球場: [:Fukuoka, 6, '平和台球場', 'Heiwadai Stadium', 'へいわだいきゅうじょう'],
  メルパルクホール福岡: [:Fukuoka, 7, 'メルパルクホール福岡', 'Fukuoka Mielparque Hall', 'ふくおかめるぱるくほーる'],
  九州産業大学: [:Fukuoka, 8, '九州産業大学', 'Kyusyu Sangyou University', 'きゅうしゅうさんぎょうだいがく'],
  福岡市民会館: [:Fukuoka, 9, '福岡市民会館', 'Fukuoka Civic Hall', 'ふくおかしみんかいかん'],
  福岡サンパレス: [:Fukuoka, 10, '福岡サンパレス', 'Fukuoka Sunpalace', 'ふくおかさんぱれす'],
  福岡ドラムロゴス: [:Fukuoka, 11, '福岡ドラムロゴス', 'Fukuoka Drum Logos', 'ふくおかどらむろごす'],
  北九州ソレイユホール: [:Fukuoka, 12, '北九州ソレイユホール', 'Kita-kyuushuu soleil hall', 'きたきゅうしゅうそれいゆほーる'],
  #### 大分
  大分文化会館: [:Oita, 1, '大分文化会館', 'Oita BUNKA KAIKAN', 'おおいたぶんかかいかん'],
  大分県立総合文化センター: [:Oita, 2, '大分県立総合文化センター', 'Oita KENRITSU SOUGOU BUNKA Center', 'おおいたけんりつそうごうぶんかせんたー'],
  パトリア日田: [:Oita, 3, '日田市民文化会館｢パトリア日田｣ ', 'Hita {SHIMIN-BUNKA-KAIKAN} "Patria Hita"', 'ひたしみんぶんかかいかん　ぱとりあひた'],
  #### 佐賀
  嬉野町: [:Saga, 1, '嬉野町', 'Ureshino-cho', 'うれしのちょう'],
  佐賀市民会館: [:Saga, 2, '佐賀市民会館', 'Saga SHIMIN KAIKAN', 'さがしみんかいかん'],
  #### 長崎
  長崎NBCホール: [:Nagasaki, 1, '長崎NBCホール', 'Nagasaki NBC Hall', 'ながさきえぬびーしーほーる'],
  長崎平和会館: [:Nagasaki, 2, '長崎平和会館', 'Nagasaki Peace Hall', 'ながさきへいわかいかん'],
  長崎大学: [:Nagasaki, 3, '長崎大学', 'Nagasaki University', 'ながさきだいがく'],
  長崎市公会堂: [:Nagasaki, 4, '長崎市公会堂', 'Nagasaki Civic Auditorium', 'ながさきしこうかいどう'],
  #### 熊本
  熊本郵便貯金ホール: [:Kumamoto, 1, '熊本郵便貯金ホール', 'Kumamoto YUBINCHOKIN Hall', 'くまもとゆうびんちょきんほーる'],
  八代厚生年金会館: [:Kumamoto, 2, '八代厚生年金会館', 'Yatsushiro {KOSEI NENKIN KAIKAN}', 'やつしろこうせんねんきんかいかん'],
  熊本テクノリサーチパーク: [:Kumamoto, 3, '熊本テクノリサーチパーク', 'Kumamoto techno research park', 'くまもとてくのりさーちぱーく'],
  熊本市民会館: [:Kumamoto, 4, '熊本市民会館', 'Kumamoto SHIMIN KAIKAN', 'くまもとしみんかいかん'],
  #### 宮崎
  宮崎市民会館: [:Miyazaki, 1, '宮崎市民会館', 'Miyazaki SHIMIN KAIKAN', 'みやざきしみんかいかん'],
  #### 鹿児島
  鹿児島市文化センター: [:Kagoshima, 1, '鹿児島市文化センター', 'Kagoshima-shi BUNKA Center', 'かごしましぶんかせんたー'],
  鹿児島経済大学: [:Kagoshima, 2, '鹿児島経済大学', 'Kagoshima Economics University', 'かごしまけいざいだいがく'],
  鹿児島県文化センター: [:Kagoshima, 3, '鹿児島県文化センター', 'Kagoshima Prefectural Culture Center', 'かごしまけんぶんかせんたー'],
  鹿児島市民文化ホール: [:Kagoshima, 4, '鹿児島市民文化ホール', "Kagoshima Citizens' Culture Hall", 'かごしましみんぶんかほーる'],
  #### 沖縄
  沖縄市民会館: [:Okinawa, 1, '沖縄市民会館', 'Okinawa SHIMIN KAIKAN', 'おきなわしみんかいかん'],
  那覇市民会館: [:Okinawa, 2, '那覇市民会館', 'Naha Civic Hall', 'なはしみんかいかん'],
}

# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Hall] Hall FactoryGirl オブジェクト
def hall_factory(key)
  pr, so, jt, et, yo = HallFactoryHash[key.to_sym]
  FB.find_or_create(:hall, key) do
    title = title_factory_with_values key, jt, et, yo
    prefecture = prefecture_factory pr
    {
      key: key,
      title_id: title.id,
      prefecture_id: prefecture.id,
      sort_order: so.to_i
    }
  end if jt
end

# @param [Array<Symbol, String>] keys オブジェクトを一意に決定するキーの配列
# @return [Array<Hall>] Hall FactoryGirl オブジェクトの配列
def hall_factories(keys)
  keys.map { |k| hall_factory(k) }
end

