# @param [Symbol, String] key 一意に決定するキー
# @return [Music] Music FactoryGirl オブジェクト
def music_factory(key)
  FB.find_or_create(:music, key) do
    song_key, person_key, sort_order = key.split(':')
    person = person_factory person_key
    song = song_factory song_key
    {
      key: key,
      song_id: song.id,
      person_id: person.id,
      sort_order: sort_order.to_i
    }
  end
end

# @param [Symbol] song_key 関係者を一意に決定するキー
# @param [Array<Symbol, String>] people_keys 関係者を一意に決定するキーの配列
# @return [Array<Music>] Music FactoryGirl オブジェクトの配列
def music_factories(song_key, people_keys)
  people_keys.map.with_index { |p, i| music_factory("#{song_key}:#{p}:#{i}") }
end
