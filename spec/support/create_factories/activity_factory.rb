# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Activity] Activity FactoryGirl オブジェクト
def activity_factory(key)
  FB.find_or_create(:activity, key) do
    hash = yield.merge(key: key)
    FB.replace_attr(hash, :title_id) do |k|
      jt, et, yo = k.split('|')
      if jt == 'exist'
        title_factory(et.to_sym).id
      else
        title_factory_with_values(nil, jt, et, yo).id
      end
    end
    FB.replace_attr(hash, :from_id) { |k| event_date_factory(k).id }
    FB.replace_attr(hash, :to_id) { |k| event_date_factory(k).id }
    FB.replace_attr(hash, :activity_type) { |k| Activity.activity_types[k] }
    FB.replace_attr(hash, :company_id) { |k| company_factory(k).id }
    FB.replace_attr(hash, :song_id) { |k| song_factory(k).id }
    hash
  end
end

def activity_test_factory
  activity_factory :TVhacker do
    {
      activity_type: :regular_tv,
      title_id: 'TVハッカー|TV Hacker|てれびはっかー',
      company_id: :fuji_tv,
      from_id: '1987/4/19',
      to_id: '1987/9/27',
      j_comment: '毎週日曜 20:00 - 20:54',
      e_comment: 'Every Sunday, 20:00 - 20:54',
      sort_order: 1,
      song_id: :new_season
    }
  end
end

def activity_test2_factory
  activity_factory :pocarisweat do
    {
      activity_type: :cm,
      title_id: %Q(大塚製薬 「ポカリスエット」|Otsuka Pharmaceutical Company `Pocarisweat'"|おおつかせいやく　ぽかりすえっと),
      company_id: :otsuka_seiyaku,
      from_id: '1986/10/1',
      to_id: '1988/9/30',
      j_comment: '和服編|寝室編|電車編|自動販売機編|骨董店編|レストラン編|温泉編',
      e_comment: 'Japanese custume version|Bed room version|Train version|{JIDOHANBAIKI} version|{KOTTOHIN-TEN} version|Restaurant version|{ONSEN} version',
      sort_order: 2
    }
  end
end
