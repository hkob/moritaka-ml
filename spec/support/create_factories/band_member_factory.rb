# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [BandMember] BandMember FactoryGirl オブジェクト
def band_member_factory(band, person_key, instrumental_key)
  FB.find_or_create(:band_member, nil) do
    {
      band_id: band.id,
      instrumental_id: instrumental_factory(instrumental_key).id,
      person_id: person_factory(person_key).id
    }
  end
end

def band_member_test_factory
  band_member_factory band_factory(:ms_band), :iChisatoMoritaka, :VOCAL
end

def band_member_test2_factory
  band_member_factory band_factory(:ms_band), :iYukariFujio, :CHORUS
end
