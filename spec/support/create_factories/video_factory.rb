# @param [Symbol] key
# @option hash [String] device_type デバイスタイプ
# @option hash [Symbol] title_key タイトルのkey
# @option hash [String] date 発売日
# @option hash [String] minutes 分
# @option Hash [String] seconds 秒
# @option Hash [Fixnum] sort_order 並び順
# @option Hash [String] number 通番
# @option Hash [Singer] singer_key 歌手の key
# @option Hash [Person] singer 歌手
# @option Hash [String] j_comment 日本語コメント
# @option Hash [String] e_comment 英語コメント
# @option Hash [String] link リンク
# @return [Video] Video FactoryGirl オブジェクト
def video_factory(key, title_key = nil)
  title_key ||= key
  FB.find_or_create(:video, key) do
    hash = yield.merge({key: key, title_id: title_factory(title_key).id})
    FB.replace_attr(hash, :device_type) do |k|
      k.to_s.split('|').inject(0) { |dtv, key| dtv + Video::VideoStr2Num[key.to_sym] }
    end
    FB.replace_attr(hash, :event_date_id) { |k| event_date_factory(k).id }
    FB.replace_attr(hash, :singer_id) { |k| person_factory(k).id }
    hash
  end
end

# @return [Video] Video FactoryGirl オブジェクト
def video_test_factory
  video_factory :video_live_get_smile, :live_get_smile do
    {
      device_type: :live_video,
      event_date_id: '1988/3/25',
      minutes: 48,
      seconds: 00,
      sort_order: 1,
      number: '1st',
      singer_id: :iChisatoMoritaka
    }
  end
end

# @return [Video] Video FactoryGirl オブジェクト
def video_test2_factory
  video_factory :video_the_mi_ha_special_mi_ha_mix, :the_mi_ha_special_mi_ha_mix do
    {
      device_type: :video_clip,
      event_date_id: '1988/9/10',
      minutes: 7,
      sort_order: 1,
      number: '1st',
      singer_id: :iChisatoMoritaka
    }
  end
end

