# @param [Sting, Symbol] key
# @option hash [String] medium_device メディアデバイス
# @option hash [String] code コード
# @option hash [String] release リリース
# @option hash [Boolean] now_sale 現在発売中なら true
# @option hash [String] company_key 会社を一意に確定する key
# @option hash [Company] company 会社
# @option hash [String] price 価格
# @option hash [Fixnum] 並び順
# @option hash [Device] デバイス
# @return [Medium] Medium FactoryGirl オブジェクト
def medium_factory(key)
  FB.find_or_create(:medium, key) do
    ans = yield.merge({key: key})
    FB.replace_attr(ans, :company_id) { |k| company_factory(k).id }
    ans
  end
end

# @return [Medium] Medium FactoryGirl オブジェクト
def medium_test_factory
  medium_factory 'album:new_season:lp:K-12533' do
    {
      medium_device: :lp,
      code: 'K-12533',
      release: :first,
      now_sale: false,
      company_id: :warner_pioneer,
      price: '2,800',
      sort_order: 1,
      device_id: album_test_factory.id
    }
  end
end

# @return [Medium] Medium FactoryGirl オブジェクト
def medium_test2_factory
  medium_factory 'album:mi_ha_:lp:K-12540'do
    {
      medium_device: :lp,
      code: 'K-12540',
      release: :first,
      now_sale: false,
      company_id: :warner_pioneer,
      price: '2,800',
      sort_order: 1,
      device_id: album_test2_factory.id
    }
  end
end
