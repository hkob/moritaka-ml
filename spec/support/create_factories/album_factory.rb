# @param [Symbol] key
# @option hash [String] device_type デバイスタイプ
# @option hash [Symbol] title_key タイトルのkey
# @option hash [String] date 発売日
# @option hash [String] minutes 分
# @option Hash [String] seconds 秒
# @option Hash [Fixnum] sort_order 並び順
# @option Hash [String] number 通番
# @option Hash [Singer] singer_key 歌手の key
# @option Hash [Person] singer 歌手
# @option Hash [String] j_comment 日本語コメント
# @option Hash [String] e_comment 英語コメント
# @option Hash [String] link 外部リンク
# @return [Album] Album FactoryGirl オブジェクト
def album_factory(key, title_key = nil)
  title_key ||= key
  FactoryBot.find_or_create(:album, key) do
    hash = yield.merge({key: key, title_id: title_factory(title_key).id})
    FactoryBot.replace_attr(hash, :device_type) do |k|
      k.to_s.split('|').inject(0) { |dtv, key| dtv + Album::AlbumStr2Num[key.to_sym] }
    end
    FactoryBot.replace_attr(hash, :event_date_id) { |k| event_date_factory(k).id }
    FactoryBot.replace_attr(hash, :singer_id) { |k| person_factory(k).id }
    hash
  end
end

# @return [Album] Album FactoryGirl オブジェクト
def album_test_factory
  album_factory(:new_season) do
    {
      device_type: :album,
      event_date_id: '1987/7/25',
      minutes: 42,
      seconds: 25,
      sort_order: 1,
      number: '1st',
      singer_id: :iChisatoMoritaka,
      j_comment: 'デビューアルバム',
      e_comment: 'Debut Album',
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4101.html'
    }
  end
end

# @return [Album] Album FactoryGirl オブジェクト
def album_test2_factory
  album_factory(:mi_ha_) do
    {
      device_type: :album,
      event_date_id: '1988/3/25',
      minutes: 48,
      seconds: 32,
      sort_order: 2,
      number: '2nd',
      singer_id: :iChisatoMoritaka,
      link: 'http://www.moritaka-chisato.com/discogprahy/product/4032.html'
    }
  end
end
