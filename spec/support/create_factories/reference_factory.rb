# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Activity] Activity FactoryGirl オブジェクト
def reference_factory(key)
  FB.find_or_create(:reference, key) do
    hash = yield.merge(key: key)
    FB.replace_attr(hash, :title_id) do |k|
      jt, et, yo = k.split('|')
      title_factory_with_values(key, jt, et, yo).id
    end
    FB.replace_attr(hash, :reference_type) { |k| Reference.reference_types[k.to_s] }
    hash
  end
end

def reference_test_factory
  reference_factory :official do
    {
      title_id: '森高千里 オフィシャルウェブサイト|Chisato Moritaka Official Web Site|もりたかちさと　おふぃしゃるうぇぶさいと',
      link: 'http://www.moritaka-chisato.com',
      reference_type: :official_site,
      sort_order: 1
    }
  end
end

def reference_test2_factory
  reference_factory :room do
    {
      title_id: %Q(森高千里の部屋|Chisato Moritaka's room|もりたかちさとのへや),
      link: 'http://moritaka-web.com',
      reference_type: :fan_site,
      sort_order: 1
    }
  end
end

