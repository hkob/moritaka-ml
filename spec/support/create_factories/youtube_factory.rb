# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Activity] Activity FactoryGirl オブジェクト
def youtube_factory(key)
  FB.find_or_create(:youtube, key) do
    hash = yield.merge(key: key)
    FB.replace_attr(hash, :event_date_id) { |k| event_date_factory(k).id }
    FB.replace_attr(hash, :youtube_type) { |k| Youtube.youtube_types[k] }
    FB.replace_attr(hash, :song_id) { |k| song_factory(k).id }
    hash
  end
end

def youtube_test_factory
  youtube_factory :sc1_sweet_candy do
    {
      youtube_type: :sc001,
      link: 'http://www.youtube.com/watch?v=2qiPIg707_E',
      event_date_id: '2012/7/23',
      sort_order: 20120723,
      number: '1',
      song_id: :sweet_candy,
      comment: "作詞：森高千里　作曲・編曲：高橋諭一\n\n公式チャンネル独占企画「200曲セルフカヴァー」がついにスタート！記念すべき1曲目は1997年発表の32thシングル「SWEET CANDY」から！\n\nオリジナルは2012年8月8日発売のコンプリート・シングル・コレクション「ザ・シングルス」に収録されています！",
    }
  end
end

def youtube_test2_factory
  youtube_factory :pocarisweat do
    {
      youtube_type: :pv,
      link: 'https://www.youtube.com/watch?v=wTdQCfYCLzA',
      event_date_id: '2012/7/13',
      sort_order: 20120713,
      song_id: :tsumetai_tsuki,
      comment: "1998年10月1日発売の37thシングル。\n作詞：森高千里　作曲・編曲：高橋諭一",
    }
  end
end

def create_youtube_factory_from_hash(yt, yt_hash)
  yt_hash.each do |k, hash|
    k =~ /(\d+)\/(\d+)\/(\d+)/
    so = hash[:sort_order] || $1.to_i * 100000 + $2.to_i * 1000 + $3.to_i * 10
    hash[:youtube_type] = yt
    hash[:sort_order] = so
    hash[:event_date_id] ||= k
    if sid = hash[:song_id]
      song = song_factory(sid)
      hash[:j_title] ||= "森高千里 『#{song.name(true)}』 【セルフカヴァー】"
      hash[:e_title] ||= "Chisato Moritaka 『#{song.name(false)}』 【Self cover】"
    end
    youtube_factory "#{yt}_#{so}" do
      hash
    end
  end
end

