# @param [String,Symbol] key
# @option hash [String] keyword キーワード
# @option hash [String] device デバイス
# @option hash [String] concert コンサート
# @option hash [String, Fixnum] sort_order 並び順
# @return [List] List FactoryGirl オブジェクト
def list_factory(key)
  FB.find_or_create(:list, key) do
    ans = yield.merge(key: key)
    %i[device_id concert_id book_id activity_sub_id].each do |k|
      FB.replace_attr(ans, k) { |v| v.id }
    end
    ans
  end
end

# @return [List] List FactoryGirl オブジェクト
def list_test_factory
  list_factory :album_new_season_common do
    {
      keyword: :common,
      device_id: album_test_factory,
      sort_order: 1
    }
  end
end

# @return [List] List FactoryGirl オブジェクト
def list_test2_factory
  list_factory :overheat_night_get_smile_A do
    {
      keyword: :A,
      concert_id: concert_test_factory,
      sort_order: 2
    }
  end
end

# @return [List] List FactoryGirl オブジェクト
def list_test3_factory
  list_factory :book_shuka do
    {
      keyword: :A,
      book: book_test2_factory,
      sort_order: 1
    }
  end
end

# @return [List] List FactoryGirl オブジェクト
def list_test4_factory
  list_factory :shibuya_live_in_A do
    {
      keyword: :A,
      concert_id: concert_test2_factory,
      sort_order: 1
    }
  end
end
