PrefectureFactoryHash = {
  Hokkaidou: %w[北海道 Hokkaidou ほっかいどう 札幌 Sapporo Hokkaidou 1],
  Aomori: %w[青森 Aomori あおもり 青森 Aomori Touhoku 2],
  Akita: %w[秋田 Akita あきた 秋田 Akita Touhoku 3],
  Iwate: %w[岩手 Iwate いわて 盛岡 Morioka Touhoku 4],
  Fukushima: %w[福島 Fukushima ふくしま 福島 Fukushima Touhoku 5],
  Yamagata: %w[山形 Yamagata やまがた 山形 Yamagata Touhoku 6],
  Miyagi: %w[宮城 Miyagi みやぎ 仙台 Sendai Touhoku 7],
  Ibaraki: %w[茨城 Ibaraki いばらき 水戸 Mito Kantou 8],
  Tochigi: %w[栃木 Tochigi とちぎ 宇都宮 Utsunomiya Kantou 9],
  Saitama: %w[埼玉 Saitama さいたま さいたま Saitama Kantou 10],
  Yamanashi: %w[山梨 Yamanashi やまなし 甲府 こうふ Chuubu 11],
  Gunma: %w[群馬 Gunma ぐんま 前橋 Maebashi Kantou 12],
  Kanagawa: %w[神奈川 Kanagawa かながわ 横浜 Yokohama Kantou 13],
  Chiba: %w[千葉 Chiba ちば 千葉 Chiba Kantou 14],
  Tokyo: %w[東京 Tokyo とうきょう 東京 Tokyo Kantou 15],
  Nagano: %w[長野 Nagano ながの 長野 Nagano Chuubu 16],
  Shizuoka: %w[静岡 Shizuoka しずおか 静岡 Shizuoka Chuubu 17],
  Niigata: %w[新潟 Niigata にいがた 新潟 Niigata Chuubu 18],
  Toyama: %w[富山 Toyama とやま 富山 Toyama Chuubu 19],
  Ishikawa: %w[石川 Ishikawa いしかわ 金沢 Kanazawa Chuubu 20],
  Aichi: %w[愛知 Aichi あいち 名古屋 Nagoya Chuubu 21],
  Gifu: %w[岐阜 Gifu ぎふ 岐阜 Gifu Chuubu 22],
  Fukui: %w[福井 Fukui ふくい 福井 Fukui Kinki 23],
  Mie: %w[三重 Mie みえ 津 Tsu Kinki 24],
  Shiga: %w[滋賀 Shiga しが 大津 Otsu Kinki 25],
  Wakayama: %w[和歌山 Wakayama わかやま 和歌山 Wakayama Kinki 26],
  Kyoto: %w[京都 Kyoto きょうと 京都 Kyoto Kinki 27],
  Nara: %w[奈良 Nara なら 奈良 Nara Kinki 28],
  Osaka: %w[大阪 Osaka おおさか 大阪 Osaka Kinki 29],
  Hyogo: %w[兵庫 Hyogo ひょうご 神戸 Kobe Kinki 30],
  Okayama: %w[岡山 Okayama おかやま 岡山 Okayama Chuugoku 31],
  Hiroshima: %w[広島 Hiroshima ひろしま 広島 Hiroshima Chuugoku 32],
  Shimane: %w[島根 Shimane しまね 松江 Matsue Chuugoku 33],
  Tottori: %w[鳥取 Tottori とっとり 鳥取 Tottori Chuugoku 34],
  Yamaguchi: %w[山口 Yamaguchi やまぐち 山口 Yamaguchi Chuugoku 35],
  Kagawa: %w[香川 Kagawa かがわ 高松 Takamatsu Shikoku 36],
  Ehime: %w[愛媛 Ehime えひめ 松山 Matsuyama Shikoku 37],
  Tokushima: %w[徳島 Tokushima とくしま 徳島 Tokushima Shikoku 38],
  Kouchi: %w[高知 Kouchi こうち 高知 Kouchi Shikoku 39],
  Fukuoka: %w[福岡 Fukuoka ふくおか 福岡 Fukuoka Kyuushuu 40],
  Oita: %w[大分 Oita おおいた 大分 Oita Kyuushuu 41],
  Saga: %w[佐賀 Saga さが 佐賀 Saga Kyuushuu 42],
  Nagasaki: %w[長崎 Nagasaki ながさき 長崎 Nagasaki Kyuushuu 43],
  Kumamoto: %w[熊本 Kumamoto くまもと 熊本 Kumamoto Kyuushuu 44],
  Miyazaki: %w[宮崎 Miyazaki みやざき 宮崎 Miyazaki Kyuushuu 45],
  Kagoshima: %w[鹿児島 Kagoshima かごしま 鹿児島 Kagoshima Kyuushuu 46],
  Okinawa: %w[沖縄 Okinawa おきなわ 那覇 Naha Kyuushuu 47],
}

# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Prefecture] Prefecture FactoryGirl オブジェクト
def prefecture_factory(key)
  jt, et, yo, jc, ec, rg, so = PrefectureFactoryHash[key.to_sym]
  FB.find_or_create(:prefecture, key) do
    title = title_factory_with_values key, jt, et, yo
    {
      key: key,
      title_id: title.id,
      j_capital: jc,
      e_capital: ec,
      region: rg,
      sort_order: so.to_i
    }
  end if jt
end

# @param [Array<Symbol, String>] keys オブジェクトを一意に決定するキーの配列
# @return [Array<Prefecture>] Prefecture FactoryGirl オブジェクトの配列
def prefecture_factories(keys)
  keys.map { |k| prefecture_factory(k) }
end

