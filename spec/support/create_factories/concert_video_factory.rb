# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [ConcertVideo] ConcertVideo FactoryGirl オブジェクト
def concert_video_factory(clc, vlc, ch)
  FB.find_or_create(:concert_video, nil) do
    {
      concert_list_content_id: clc.id,
      video_list_content_id: vlc.id,
      concert_hall_id: ch.id
    }
  end
end

def concert_video_test_factory
  concert_video_factory list_content_test_factory, list_content_test2_factory, concert_hall_test_factory
end

