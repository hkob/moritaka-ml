def device_activity_factory(device, activity_key)
  FB.find_or_create(:device_activity, nil) do
    {
      device_id: device.id,
      activity_id: activity_factory(activity_key).id
    }
  end
end

# @return [Album] Album FactoryGirl オブジェクト
def device_activity_test_factory
  activity_test_factory
  device_activity_factory album_test2_factory, :TVhacker
end

# @return [Album] Album FactoryGirl オブジェクト
def device_activity_test2_factory
  activity_test2_factory
  device_activity_factory single_test_factory, :pocarisweat
end
