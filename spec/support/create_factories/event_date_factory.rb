EventDateFactoryHash = {
  '1986' => %w[1986/1/1 t t],
  '1986/10' => %w[1986/10/1 f t],
  '1988/11' => %w[1988/11/1 f t],
  '1990/10' => %w[1990/10/1 f t],
  '1992/9' => %w[1992/9/1 f t],
  '1992/12' => %w[1992/12/1 f t],
  '1993' => %w[1993/1/1 t t],
  '1993/1' => %w[1993/1/1 f t],
  '1993/10' => %w[1993/10/1 f t],
  '1994' => %w[1994/1/1 t t],
  '1994/1' => %w[1994/1/1 f t],
  '1994/3' => %w[1994/3/1 f t],
  '1994/12' => %w[1994/12/1 f t],
  '1995' => %w[1995/1/1 t t],
  '1995_2' => %w[1995/1/2 t t],
  '1996/2' => %w[1996/2/1 f t],
  '1996/3' => %w[1996/3/1 f t],
  '1997/2' => %w[1997/2/1 f t],
  '1997/7' => %w[1997/7/1 f t],
  '1997/10' => %w[1997/10/1 f t],
  '1998/1' => %w[1998/1/1 f t],
  '1998/3' => %w[1998/3/1 f t],
  '1998/4' => %w[1998/4/1 f t],
  '1998/5' => %w[1998/5/1 f t],
  '1998/7' => %w[1998/7/1 f t],
  '1998/8' => %w[1998/8/1 f t],
  '1998/9' => %w[1998/9/1 f t],
  '1998/10' => %w[1998/10/1 f t],
  '1999/1' => %w[1999/1/1 f t],
  '1999/3' => %w[1999/3/1 f t],
  '1999/8' => %w[1999/8/1 f t],
  '1999/9' => %w[1999/9/1 f t],
  '2000/8' => %w[2000/8/1 f t],
  '2001/3' => %w[2001/3/1 f t],
  '2001/4' => %w[2001/4/1 f t],
  '2001/5' => %w[2001/5/1 f t],
  '2001/6' => %w[2001/6/1 f t],
  '2002/5' => %w[2002/5/1 f t],
  '2004/3' => %w[2004/3/1 f t],
  '2004/4' => %w[2004/4/1 f t],
  '2004/5' => %w[2004/5/1 f t],
  '2005/5' => %w[2005/5/1 f t],
  '2009/4' => %w[2009/4/1 f t],
}

# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [EventDate] EventDate FactoryGirl オブジェクト
def event_date_factory(key)
  date, no_month, no_day = EventDateFactoryHash[key]
  if date
    date_obj = Date.parse(date)
  else
    date_obj = Date.parse(key)
    no_month = 'f'
    no_day = 'f'
  end
  FB.find_or_create(:event_date, key) do
    {
      key: key,
      date: date_obj,
      year_id: year_factory(date_obj.year).id,
      month: no_month == 't' ? nil : date_obj.month,
      day: no_day == 't' ? nil : date_obj.day
    }
  end
end

# @return [EventDate] EventDate FactoryGirl オブジェクト
def event_date_factories(keys)
  keys.map { |k| event_date_factory k }
end
