# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Company] Company FactoryGirl オブジェクト
def company_factory(key)
  FB.find_or_create(:company, key) do
    title = title_factory key
    {
      key: key,
      title_id: title.id
    }
  end
end

# @param [Array<Symbol, String>] keys オブジェクトを一意に決定するキーの配列
# @return [Array<Company>] Company FactoryGirl オブジェクトの配列
def company_factories(keys)
  keys.map { |k| company_factory(k) }
end

