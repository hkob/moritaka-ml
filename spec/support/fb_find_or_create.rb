FB = FactoryBot

# @see https://gist.github.com/thewatts/dcc91ef40b144aff42ae
module FactoryBot::Syntax::Methods
  def find_or_create(name, key)
    factory = FB.factory_by_name(name)
    klass   = factory.build_class

    result = key && klass.find_by(key: key)
    unless result
      attributes = yield

      factory_attributes = FB.attributes_for(name)
      attributes = factory_attributes.merge(attributes)

      result = klass.find_by(attributes)
      result ||= FB.create(name, attributes)
      print "  Create #{name}:#{key}\n" if key && !Rails.env.test?
    end
    result
  end

  def replace_attr(hash, key)
    if hash[key]
      hash[key] = yield hash[key]
    end
  end
end

