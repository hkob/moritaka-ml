require 'rails_helper'

RSpec.describe Book, type: :model do
  context 'common validation check' do
    subject { book_test_factory }

    it_behaves_like :presence_validates, %i[key book_type title_id sort_order]
    it_behaves_like :unique_validates, %i[key], -> { book_test2_factory }
    it_behaves_like :plural_unique_validates, %i[book_type sort_order], -> { book_test2_factory }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :book, has_many: %i[event_date]
    it_behaves_like :belongs_to, :book, has_many: %i[photographer], children: :photographed_books
    it_behaves_like :belongs_to, :book, has_many: %i[author], children: :written_books
    it_behaves_like :belongs_to, :book, has_many: %i[publisher], children: :published_books
    it_behaves_like :belongs_to, :book, has_many: %i[seller], children: :selled_books
    it_behaves_like :dependent_destroy, :book, %i[publisher seller photographer author]
  end

  context 'after some books are registrered' do
    let!(:targets) { [book_test_factory, book_test2_factory] }

    context 'for Book class' do
      subject { Book }
      wk, sk = Array(0..1)

      it_behaves_like :msta_block, -> t do
        {
          book_type_str_is: [:poems, t.values_at(wk)],
          order_sort_order: [nil, t.values_at(sk, wk)],
          year_is: [year_factory(1987), t],
        }
      end
    end

    context 'for Book instances' do
      subject { targets }

      it_behaves_like :amst_block, -> t do
        {
          event_title: [nil, %w[詩集 写真集]],
        }
      end
    end
  end
end

