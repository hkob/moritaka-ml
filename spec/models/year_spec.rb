require 'rails_helper'

RSpec.describe Year, type: :model do
  context 'common validation check' do
    subject { year_factory 1987 }

    it_behaves_like :presence_validates, %i[year]
    it_behaves_like :unique_validates, %i[year], -> { year_factory 1988 }
    it_behaves_like :destroy_validates
  end

  context 'after some years are registrered' do
    let!(:targets) { year_factories [1987, 1988] }

    context 'Year class' do
      subject { Year }
      y87, y88 = Array(0..1)

      it_behaves_like :mst_block, -> t do
        {
          order_year: [nil, t],
          order_year_desc: [nil, t.reverse],
          year_value_is: [1987, t.values_at(y87), 1988, t.values_at(y88)],
        }
      end
    end

    context 'Year instances' do
      subject { targets }

      it_behaves_like :amst_block, -> t do
        {
          name: [true, %w[1987年 1988年], false, ['In 1987', 'In 1988']],
        }
      end
    end
  end
end
