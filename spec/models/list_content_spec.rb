require 'rails_helper'

RSpec.describe ListContent, type: :model do
  context 'common validation check' do
    subject { list_content_test_factory }

    it_behaves_like :presence_validates, %i[list_id sort_order]
    it_behaves_like :plural_unique_validates, %i[list_id sort_order], -> { list_content_test2_factory }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :list_content, has_many: %i[list song]
    it_behaves_like :dependent_destroy, :list_content, %i[list song]
  end

  context 'after some list_contents are registrered' do
    let!(:targets) { [list_content_test_factory, list_content_test2_factory, list_content_test3_factory] }

    context 'ListContent class' do
      subject { ListContent }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'ListContent instances' do
      subject { targets }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end

