require 'rails_helper'

RSpec.describe ActivitySub, type: :model do
  context 'common validation check' do
    subject { activity_sub_test_factory }

    it_behaves_like :presence_validates, %i[key sort_order]
    it_behaves_like :unique_validates, %i[key], -> { activity_sub_test2_factory }
    it_behaves_like :plural_unique_validates, %i[activity_id sort_order], -> { activity_sub_test2_factory }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :activity_sub, has_many: %i[activity song]
    it_behaves_like :belongs_to, :activity_sub, has_many: %i[from], children: :activity_sub_froms
    it_behaves_like :belongs_to, :activity_sub, has_many: %i[to], children: :activity_sub_tos
    it_behaves_like :dependent_destroy, :activity_sub, %i[activity song from to]
  end

  context 'after some activity_sub_subs are registrered' do
    before(:all) { @activity_sub_subs = [activity_sub_test_factory, activity_sub_test2_factory] }

    context 'for Activity class' do
      subject { Activity }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Activity instances' do
      subject { @activity_sub_subs }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end

