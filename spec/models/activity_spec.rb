require 'rails_helper'

RSpec.describe Activity, type: :model do
  context 'common validation check' do
    subject { activity_test_factory }

    it_behaves_like :presence_validates, %i[key activity_type title_id sort_order]
    it_behaves_like :unique_validates, %i[key], -> { activity_test2_factory }
    it_behaves_like :plural_unique_validates, %i[activity_type sort_order], -> { activity_test2_factory }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :activity, has_many: %i[title company song]
    it_behaves_like :belongs_to, :activity, has_many: %i[from], children: :activity_froms
    it_behaves_like :belongs_to, :activity, has_many: %i[to], children: :activity_tos
    it_behaves_like :dependent_destroy, :activity, %i[title company song from to]
  end

  context 'after some activities are registrered' do
    let!(:targets) { [activity_test_factory, activity_test2_factory] }

    context 'for Activity class' do
      subject { Activity }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Activity instances' do
      subject { targets }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end

