require 'rails_helper'

RSpec.describe ConcertVideo, type: :model do
  context 'common validation check' do
    subject { concert_video_test_factory }

    it_behaves_like :presence_validates, %i[concert_list_content_id video_list_content_id concert_hall_id]
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :concert_video, has_many: %i[concert_list_content], children: :concert_to_concert_videos
    it_behaves_like :belongs_to, :concert_video, has_many: %i[video_list_content], children: :video_to_concert_videos
    it_behaves_like :belongs_to, :concert_video, has_many: %i[concert_hall]
    it_behaves_like :dependent_destroy, :concert_video, %i[concert_list_content video_list_content concert_hall]
  end

  context 'after some concert_videos are registrered' do
    let!(:targets) { [concert_video_test_factories] }

    context 'ConcertVideo class' do
      subject { ConcertVideo }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'ConcertVideo instances' do
      subject { targets }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end

