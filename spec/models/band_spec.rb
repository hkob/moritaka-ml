require 'rails_helper'

RSpec.describe Band, type: :model do
  context 'common validation check' do
    subject { band_factory :animals_1 }

    it_behaves_like :presence_validates, %i[key title_id]
    it_behaves_like :unique_validates, %i[key title_id], -> { band_factory :animals_2 }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :band, has_one: %i[title]
    it_behaves_like :dependent_destroy, :band, %i[title]
  end

  context 'after some bands are registrered' do
    let!(:targets) { band_factories %i[animals_1 animals_2] }

    context 'for Band class' do
      subject { Band }

      it_behaves_like :mst_block, -> t do
        {
          order_yomi: [nil, t],
        }
      end
    end

    context 'for Band instances' do
      subject { targets }

      it_behaves_like :amst_block, -> t do
        {
          name: [true, ['ANIMALS (1)', 'ANIMALS (2)'], false, ['ANIMALS (1)', 'ANIMALS (2)']],
        }
      end
    end
  end
end
