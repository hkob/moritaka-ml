require 'rails_helper'

RSpec.describe DeviceActivity, type: :model do
  context 'common validation check' do
    subject { device_activity_test_factory }

    it_behaves_like :presence_validates, %i[device_id activity_id]
    it_behaves_like :plural_unique_validates, %i[device_id activity_id], -> { device_activity_test2_factory }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :device_activity, has_many: %i[device activity]
    it_behaves_like :dependent_destroy, :device_activity, %i[device activity]
  end

  context 'after some activities are registrered' do
    let!(:targets) { [activity_test_factory, activity_test2_factory] }
  end
end
