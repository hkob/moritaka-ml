require 'rails_helper'

RSpec.describe Person, type: :model do
  context 'common validation check' do
    subject { person_factory :iChisatoMoritaka }

    it_behaves_like :presence_validates, %i[title_id]
    it_behaves_like :unique_validates, %i[key title_id], -> { person_factory :iHideoSaito }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :person, has_one: %i[title]
    it_behaves_like :dependent_destroy, :person, %i[title]
  end

  context 'after some people are registrered' do
    let!(:targets) { person_factories %i[iChisatoMoritaka iHideoSaito iHiro] }

    context 'Person class' do
      subject { Person }

    end

    context 'Person instances' do
      subject { targets }

      it_behaves_like :amst_block, -> t do
        {
          name: [true, %w[森高千里 斉藤英夫 HIRO], false, ['Chisato Moritaka', 'Hideo Saito', 'HIRO']],
          names: [true, ['森高千里', '斉藤英夫', '伊秩弘将, HIRO'], false, ['Chisato Moritaka', 'Hideo Saito', 'Hiromasa Ijichi, HIRO']],
        }
      end
    end
  end
end
