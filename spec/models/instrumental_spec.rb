require 'rails_helper'

RSpec.describe Instrumental, type: :model do
  context 'common validation check' do
    subject { instrumental_factory :VOCAL }

    it_behaves_like :presence_validates, %i[key sort_order title_id]
    it_behaves_like :unique_validates, %i[key sort_order title_id], -> { instrumental_factory :GUITAR }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :instrumental, has_one: %i[title]
    it_behaves_like :dependent_destroy, :instrumental, %i[title]
  end

  context 'after some instrumentals are registrered' do
    let!(:targets) { instrumental_factories %i[VOCAL ARRANGE] }

    context 'Instrumental class' do
      subject { Instrumental }
      vocal, arrange = Array(0..1)

      it_behaves_like :mst_block, -> t do
        {
          order_sort_order: [nil, t.values_at(vocal, arrange)],
        }
      end

      it_behaves_like :msta_block, -> t do
        {
          except_arrange: [nil, t.values_at(vocal)],
          is_arrange: [nil, t.values_at(arrange)],
        }
      end
    end

    context 'Instrumental instances' do
      subject { targets }

      it_behaves_like :amst_block, -> t do
        {
          name: [true, %w[Vocal 編曲], false, %w[Vocal Arrangement]],
        }
      end
    end
  end
end
