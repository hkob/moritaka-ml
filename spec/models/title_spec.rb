require 'rails_helper'

RSpec.describe Title, type: :model do
  context 'common validation check' do
    subject { title_factory :new_season }

    it_behaves_like :presence_validates, %i[japanese english yomi]
    it_behaves_like :destroy_validates
  end

  context 'after some titles are registrered' do
    let!(:targets) { title_factories %i[new_season yumeno_owari] }

    context 'Title class' do
      subject { Title }
      ns, yo = Array(0..1)

      it_behaves_like :mst_block, -> t do
        {
          order_yomi: [nil, t],
        }
      end

      it_behaves_like :msta_block, -> t do
        {
          head_value_is: ['41', t.values_at(ns), '72', t.values_at(yo), '11', []],
        }
      end
    end

    context 'Title instances' do
      subject { targets }

      it_behaves_like :amst_block, -> t do
        {
          yomi_suuji: [nil, %w[4172212295.00010 726344049081.000000]],
          name: [true, ['NEW SEASON', '夢の終わり'], false, ['NEW SEASON', '{YUME-NO OWARI}: [End of dream]']],
        }
      end
    end
  end
end
