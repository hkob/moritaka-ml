require 'rails_helper'

RSpec.describe BandMember, type: :model do
  context 'common validation check' do
    subject { band_member_test_factory }

    it_behaves_like :presence_validates, %i[band_id person_id instrumental_id]
    it_behaves_like :plural_unique_validates, %i[band_id person_id instrumental_id], -> { band_member_test2_factory }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :band_member, has_many: %i[band person instrumental]
    it_behaves_like :dependent_destroy, :band_member, %i[band person instrumental]
  end

  context 'after some band_members are registrered' do
    let!(:targets) { [band_member_test_factories, band_member_test2_factory] }

    context 'for BandMember class' do
      subject { BandMember }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for BandMember instances' do
      subject { targets }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end
