require 'rails_helper'

RSpec.describe Song, type: :model do
  context 'common validation check' do
    subject { song_factory :new_season }

    it_behaves_like :presence_validates, %i[key title_id event_date_id]
    it_behaves_like :unique_validates, %i[key title_id], -> { song_factory :yumeno_owari }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :song, has_many: %i[event_date], has_one: %i[title]
    it_behaves_like :dependent_destroy, :song, %i[title event_date]
  end

  context 'after some songs are registrered' do
    let!(:targets) { song_factories %i[yumeno_owari overheat_night] }

    context 'Song class' do
      subject { Song }
      yo, oh = Array(0..1)

      it_behaves_like :mst_block, -> t do
        {
          order_date: [nil, t],
          order_yomi: [nil, t.reverse],
          order_yomi_desc: [nil, t],
        }
      end

      it_behaves_like :msta_block, -> t do
        {
          head_value_is: ['72', t.values_at(yo)],
          year_is: [year_factory(1987), t],
        }
      end

      context 'with child' do
        let!(:oh2) { song_factory(:overheat_night_2) }
        it_behaves_like :msta_block, -> t do
          {
            only_main: [nil, t],
          }
        end
      end
    end

    context 'for Song instances' do
      subject { targets }

      it_behaves_like :amst_block, -> t do
        {
          name: [true, ['夢の終わり', 'オーバーヒート・ナイト'], false, ['{YUME-NO OWARI}: [End of dream]', 'OVERHEAT. NIGHT']],
        }
      end
    end
  end
end
