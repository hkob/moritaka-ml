require 'rails_helper'

RSpec.describe Lyric, type: :model do
  context 'common validation check' do
    subject { lyric_factory('new_season:iHiro:0') }

    it_behaves_like :presence_validates, %i[key song_id person_id sort_order]
    it_behaves_like :unique_validates, %i[key], -> { lyric_factory 'namida_good_bye:iShingoKanno:1' }
    it_behaves_like :plural_unique_validates, %i[song_id sort_order], -> { lyric_factory 'namida_good_bye:iShingoKanno:1' }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :lyric, has_many: %i[song person]
    it_behaves_like :dependent_destroy, :lyric, %i[song person]
  end

  context 'after some lyrics are registrered' do
    let!(:targets) { %w[new_season:iHiro:0 namida_good_bye:iShingoKanno:1].map { |k| lyric_factory k } }

    context 'Lyric class' do
      subject { Lyric }
      ns, ng = Array(0..1)

      it_behaves_like :mst_block, -> t do
        {
          order_sort_order: [nil, t],
          order_song_date: [nil, t],
          people_are: [[person_factories(%i[iHiro])], t.values_at(ns)],
        }
      end

    end

    context 'Lyric instances' do
      subject { targets }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end
