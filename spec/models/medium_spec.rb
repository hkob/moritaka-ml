require 'rails_helper'

RSpec.describe Medium, type: :model do
  context 'common validation check' do
    subject { medium_test_factory }

    it_behaves_like :presence_validates, %i[key medium_device sort_order device_id]
    it_behaves_like :unique_validates, %i[key], -> { medium_test2_factory }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :medium, has_many: %i[device company]
  end

  context 'after some media are registrered' do
    let!(:targets) { [medium_test_factory, medium_test2_factory] }

    context 'for Medium class' do
      subject { Medium }
      ns, mh = Array(0..1)

      it_behaves_like :msta_block, -> t do
        {
          device_is: [album_test_factory, t.values_at(ns)],
        }
      end
    end

    context 'for Medium instances' do
      subject { targets }
    end
  end
end
