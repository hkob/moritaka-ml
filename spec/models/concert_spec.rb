require 'rails_helper'

RSpec.describe Concert, type: :model do
  context 'common validation check' do
    subject { concert_test_factory }

    it_behaves_like :presence_validates, %i[key title_id concert_type from_id has_song_list has_product num_of_performances num_of_halls sort_order]
    it_behaves_like :unique_validates, %i[key title_id], -> { concert_test2_factory }
    it_behaves_like :plural_unique_validates, %i[concert_type sort_order], -> { concert_test2_factory }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :concert, has_many: %i[band], has_one: %i[title]
    it_behaves_like :belongs_to, :concert, has_many: %i[from], children: :concert_froms
    it_behaves_like :belongs_to, :concert, has_many: %i[to], children: :concert_tos
    it_behaves_like :dependent_destroy, :concert, %i[band title from to]
  end

  context 'after all concerts are registrered' do
    let!(:targets) { [concert_test_factory, concert_test2_factory] }

    context 'for Concert class' do
      subject { Concert }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Concert instances' do
      subject { targets }

      it_behaves_like :amst_block, -> t do
        {
          name: [true, ['「OVERHEAT. NIGHT」, 「GET SMILE」', '渋谷ライブイン ファーストライブ'], false, ['"OVERHEAT. NIGHT",  "GET SMILE"', 'First Live at Shibuya Live-in']],
          num_of_concerts: [nil, %w[3(2) 1(1)]],
        }
      end
    end
  end
end

