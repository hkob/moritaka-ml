require 'rails_helper'

RSpec.describe Prefecture, type: :model do
  context 'common validation check' do
    subject { prefecture_factory :Tokyo }

    it_behaves_like :presence_validates, %i[key title_id j_capital e_capital region sort_order]
    it_behaves_like :unique_validates, %i[key sort_order title_id], -> { prefecture_factory :Chiba }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :prefecture, has_one: %i[title]
  end

  context 'after all prefectures are registrered' do
    let!(:targets) { prefecture_factories %i[Hyogo Tokyo] }

    context 'for Prefecture class' do
      subject { Prefecture }
      hyogo, tokyo = Array(0..1)

      it_behaves_like :mst_block, -> t do
        {
          order_sort_order: [nil, t.values_at(tokyo, hyogo)],
          sort_order_value_is: [15, t.values_at(tokyo)],
          region_value_is: [:Kinki, t.values_at(hyogo)]
        }
      end
    end

    context 'for Prefecture instances' do
      subject { targets }

      it_behaves_like :amst_block, -> t do
        {
          capital: [true, %w[神戸 東京], false, %w[Kobe Tokyo]]
        }
      end
    end
  end
end

