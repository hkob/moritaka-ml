require 'rails_helper'

RSpec.describe Single, type: :model do
  context 'common validation check' do
    subject { single_test_factory }

    it_behaves_like :presence_validates, %i[type device_type title_id sort_order event_date_id]
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :single, has_many: %i[title event_date singer]
    it_behaves_like :dependent_destroy, :single, %i[title event_date singer]
  end

  context 'after some singles are registrered' do
    let!(:targets) { [single_test_factory] }

    context 'Single class' do
      subject { Single }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'Single instances' do
      subject { targets }

      it_behaves_like :amst_block, -> t do
        {
          name: [true, ['NEW SEASON'], false, ['NEW SEASON']],
        }
      end
    end
  end
end
