require 'rails_helper'

RSpec.describe Youtube, type: :model do
  context 'common validation check' do
    subject { youtube_test_factory }

    it_behaves_like :presence_validates, %i[key youtube_type sort_order]
    it_behaves_like :unique_validates, %i[key], -> { youtube_test2_factory }
    it_behaves_like :plural_unique_validates, %i[youtube_type sort_order], -> { youtube_test2_factory }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :youtube, has_many: %i[song event_date]
    it_behaves_like :dependent_destroy, :youtube, %i[song event_date]
  end

  context 'after all youtubes are registrered' do
    let!(:targets) { [youtube_test_factory, youtube_test2_factory] }

    context 'for Concert class' do
      subject { Youtube }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Concert instances' do
      subject { targets }

    end
  end
end

