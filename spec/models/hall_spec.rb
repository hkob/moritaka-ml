require 'rails_helper'

RSpec.describe Hall, type: :model do
  context 'common validation check' do
    subject { hall_factory :日本青年館 }

    it_behaves_like :presence_validates, %i[title_id prefecture_id sort_order]
    it_behaves_like :unique_validates, %i[title_id], -> { hall_factory :大阪厚生年金会館 }
    it_behaves_like :plural_unique_validates, %i[prefecture_id sort_order], -> { hall_factory :大阪厚生年金会館 }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :hall, has_many: %i[prefecture], has_one: %i[title]
    it_behaves_like :dependent_destroy, :hall, %i[title prefecture]
  end

  context 'after some halls are registrered' do
    let!(:targets) { hall_factories %i[日本青年館 大阪厚生年金会館] }

    context 'Hall class' do
      subject { Hall }
      ns, oknk = Array(0..1)

      it_behaves_like :mst_block, -> t do
        {
          order_sort_order: [nil, t.values_at(oknk, ns)],
        }
      end
    end

    context 'for Hall instances' do
      subject { targets }

      it_behaves_like :amst_block, -> t do
        {
          name: [true, %w[日本青年館 大阪厚生年金会館], false, ['Nippon-seinenkan', 'Osaka KOSEI NENKIN KAIKAN']],
        }
      end
    end
  end
end

