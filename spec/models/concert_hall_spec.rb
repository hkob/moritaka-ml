require 'rails_helper'

RSpec.describe ConcertHall, type: :model do
  context 'common validation check' do
    subject { concert_hall_test_factory }

    it_behaves_like :presence_validates, %i[concert_id sort_order hall_id event_date_id]
    it_behaves_like :plural_unique_validates, %i[concert_id sort_order hall_id], -> { concert_hall_test2_factory }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :concert_hall, has_many: %i[concert hall list event_date]
    it_behaves_like :dependent_destroy, :concert_hall, %i[concert hall list event_date]
  end

  context 'after some concert_halls are registrered' do
    let(:targets) { [concert_hall_test_factory, concert_hall_test2_factory] }

    context 'ConcertHall class' do
      subject { ConcertHall }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'ConcertHall instances' do
      subject { targets }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end
