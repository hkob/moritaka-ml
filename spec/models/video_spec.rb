require 'rails_helper'

RSpec.describe Video, type: :model do
  context 'common validation check' do
    subject { video_test_factory }

    it_behaves_like :presence_validates, %i[type device_type title_id sort_order event_date_id]
    it_behaves_like :unique_validates, %i[key], -> { video_test2_factory }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :video, has_many: %i[title event_date singer]
    it_behaves_like :dependent_destroy, :video, %i[title event_date singer]
  end

  context 'after some videos are registrered' do
    let!(:targets) { [video_test_factory, video_test2_factory] }

    context 'Video class' do
      subject { Video }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'Video instances' do
      subject { targets }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end
