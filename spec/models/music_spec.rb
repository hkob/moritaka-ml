require 'rails_helper'

RSpec.describe Music, type: :model do
  context 'common validation check' do
    subject { music_factory 'new_season:iHideoSaito:0' }

    it_behaves_like :presence_validates, %i[song_id person_id sort_order]
    it_behaves_like :unique_validates, %i[key], -> { music_factory 'namida_good_bye:iTakumiYamamoto:1' }
    it_behaves_like :plural_unique_validates, %i[song_id sort_order], -> { music_factory 'namida_good_bye:iTakumiYamamoto:1' }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :music, has_many: %i[song person]
    it_behaves_like :dependent_destroy, :music, %i[song person]
  end

  context 'after some musics are registrered' do
    let!(:targets) { %w[new_season:iHideoSaito:0 namida_good_bye:iTakumiYamamoto:1].map { |k| music_factory k } }

    context 'Music class' do
      subject { Music }
      ns, ng = Array(0..1)

      it_behaves_like :mst_block, -> t do
        {
          order_sort_order: [nil, t],
          order_song_date: [nil, t],
          people_are: [[person_factories(%i[iHideoSaito])], t.values_at(ns)],
        }
      end
    end

    context 'for Music instances' do
      subject { targets }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end
