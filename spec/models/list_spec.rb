require 'rails_helper'

RSpec.describe List, type: :model do
  context 'common validation check' do
    subject { list_test_factory }

    it_behaves_like :presence_validates, %i[key keyword]
    it_behaves_like :unique_validates, %i[key], -> { list_test2_factory }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :list, has_many: %i[album]
    it_behaves_like :dependent_destroy, :list, %i[album]
  end

  context 'after all lists are registrered' do
    let!(:targets) { [list_test_factory, list_test2_factory] }

    context 'List class' do
      subject { List }
      ans, cohgs = Array(0..1)

      it_behaves_like :mst_block, -> t do
        {
          order_sort_order: [nil, t],
          only_concert: [nil, t.values_at(cohgs)],
        }
      end
    end

    context 'List instances' do
      subject { targets }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end

