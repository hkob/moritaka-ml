require 'rails_helper'

RSpec.describe Company, type: :model do
  context 'common validation check' do
    subject { company_factory :warner_music_japan }

    it_behaves_like :presence_validates, %i[key title_id]
    it_behaves_like :unique_validates, %i[key], -> { company_factory :warner_pioneer }
    it_behaves_like :destroy_validates
  end

  context 'after some companies are registrered' do
    let!(:targets) { company_factories %i[warner_pioneer pioneer] }

    context 'Company class' do
      subject { Company }
      wmj, pioneer = Array(0..1)

      it_behaves_like :mst_block, -> t do
        {
          order_yomi: [nil, t.values_at(pioneer, wmj)],
        }
      end
    end

    context 'Company instances' do
      subject { targets }

      it_behaves_like :amst_block, -> t do
        {
          name: [true, ['Warner Pioneer', 'パイオニア'], false, ['Warner Pioneer', 'PIONEER']],
        }
      end
    end
  end
end
