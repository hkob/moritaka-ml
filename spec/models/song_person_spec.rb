require 'rails_helper'

RSpec.describe SongPerson, type: :model do
  context 'common validation check' do
    subject { song_person_test_factory }

    it_behaves_like :presence_validates, %i[list_content_id person_id instrumental_id]
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :song_person, has_many: %i[list_content person instrumental]
    it_behaves_like :dependent_destroy, :song_person, %i[list_content person instrumental]
  end

  context 'after some song_people are registrered' do
    let!(:targets) { [song_person_factory] }

    context 'SongPerson class' do
      subject { SongPerson }

      #      it 'should receive METHOD1, METHOD2' do
      #        target_method_send_test subject,
      #          :METHOD1, :ARG1, :ANS1,
      #          :METHOD2, :ARG2, :ANS2
      #      end
    end

    context 'SongPerson instances' do
      subject { @song_people }

      #      it 'should receive METHOD3, METHOD4' do
      #        target_array_method_send_test subject,
      #          :METHOD3?, :ARG3, :ANS3,
      #          :METHOD4?, :ARG4, :ANS4
      #      end
    end
  end
end
