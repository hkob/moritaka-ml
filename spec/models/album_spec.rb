require 'rails_helper'

RSpec.describe Album, type: :model do
  context 'common validation check' do
    subject { album_test_factory }

    it_behaves_like :presence_validates, %i[key type device_type title_id sort_order event_date_id]
    it_behaves_like :unique_validates, %i[key], -> { album_test2_factory }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :album, has_many: %i[title event_date singer]
    it_behaves_like :dependent_destroy, :album, %i[title event_date singer]
  end

  context 'after some albums are registrered' do
    let!(:targets) { [album_test_factory] }

    context 'Album class' do
      subject { Album }
      at = Album::AlbumStr2Num

      it_behaves_like :mst_block, -> t do
        {
          device_type_value_has: [1, t],
          only_album: [nil, t],
        }
      end
    end

    context 'Album instances' do
      subject { @albums }

    end
  end
end
