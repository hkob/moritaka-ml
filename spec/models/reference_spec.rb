require 'rails_helper'

RSpec.describe Reference, type: :model do
  context 'common validation check' do
    subject { reference_test_factory }

    it_behaves_like :presence_validates, %i[key link title_id sort_order]
    it_behaves_like :unique_validates, %i[key title_id], -> { reference_test2_factory }
    it_behaves_like :plural_unique_validates, %i[reference_type sort_order], -> { reference_test2_factory }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :reference, has_many: %i[title]
    it_behaves_like :dependent_destroy, :reference, %i[title]
  end

  context 'after some references are registrered' do
    let!(:targets) { [reference_test_factory, reference_test2_factory] }

    context 'for Activity class' do
      subject { Activity }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Activity instances' do
      subject { targets }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end

