require 'rails_helper'

RSpec.describe EventDate, type: :model do
  context 'common validation check' do
    subject { event_date_factory '1987/5/25' }

    it_behaves_like :presence_validates, %i[key date year_id]
    it_behaves_like :unique_validates, %i[key], -> { event_date_factory '1986/10' }
    it_behaves_like :destroy_validates
    it_behaves_like :belongs_to, :event_date, has_many: %i[year]
    it_behaves_like :dependent_destroy, :event_date, %i[year]
  end

  context 'after some event_dates are registrered' do
    let!(:targets) { event_date_factories %w[1986/10 1987/5/25] }
    e198610, e19870525 = Array(0..1)

    context 'EventDate class' do
      subject { EventDate }

      it_behaves_like :mst_block, -> t do
        {
          order_date: [nil, t],
          order_date_desc: [nil, t.values_at(e19870525, e198610)],
        }
      end
    end

    context 'EventDate instances' do
      subject { targets }

      it_behaves_like :amst_block, -> t do
        {
          name: [nil, %w[1986年10月 1987年5月25日(月)]],
        }
      end
    end
  end
end
