Rails.application.routes.draw do
  root to: 'home#index'

  get 'home/index'
  get 'home/about'

  resources :titles, only: %i[index], param: :key
  resources :years, only: %i[index show], param: :key
  resources :instrumentals, only: %i[index show], param: :key
  resources :people, only: %i[index show], param: :key
  resources :songs, only: %i[index show], param: :key do
    get :list_by_date, on: :collection
  end
  resources :albums, only: %i[index show], param: :key
  resources :singles, only: %i[index show], param: :key
  resources :videos, only: %i[index show], param: :key
  resources :companies, only: %i[index show], param: :key
  resources :prefectures, only: %i[index show], param: :key
  resources :halls, only: %i[show], param: :key
  resources :bands, only: %i[index show], param: :key
  resources :concerts, only: %i[index show], param: :key
  resources :books, only: %i[index show], param: :key
  resources :activities, only: %i[index show], param: :key
  resources :references, only: %i[index], param: :key
  resources :youtubes, only: %i[index show], param: :key
  resources :stats, only: %i[index] do
    get :songs_in_concerts, on: :collection
    get :prefectures_concerts_performed, on: :collection
    get :lyrics, on: :collection
    get :musics, on: :collection
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
