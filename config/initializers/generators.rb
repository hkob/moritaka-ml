Rails.application.config.generators do |g|
  g.fixture_replacement :factory_bot, dir: 'spec/factories'
  g.factory_bot     true
  g.test_framework  :rspec,
    fixtures: true,
    view_specs: false,
    routing_specs: false,
    controller_specs: false,
    request_specs: true
  g.stylesheets     false
  g.javascripts     false
  g.helper          false
  g.channel         assets: false
end
