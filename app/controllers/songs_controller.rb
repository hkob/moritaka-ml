class SongsController < ApplicationController
  include Head

  def index
    @head, @heads, @lhead_hash, @subtitle, @ids = get_heads(params[:division], @is_ja)
    @songs = Song.head_value_is(@head).include_title.order_yomi
    song_head_arel = Song.head_value_is(@head)
    @lyrics = {}
    @musics = {}
    @lyrics = hash_array_from_array(Lyric.joins(:song).merge(song_head_arel).include_person) { |l| [ l.song_id, l ] }
    @musics = hash_array_from_array(Music.joins(:song).merge(song_head_arel).include_person) { |d| [ d.song_id, d ] }
    list_content_arel = ListContent.joins(:song).merge(song_head_arel).group(:song_id)
    @num_of_albums = list_content_arel.only_album.count(:id)
    @num_of_singles = list_content_arel.only_single.count(:id)
    @num_of_videos = list_content_arel.only_video.count(:id)
    @num_of_concerts = list_content_arel.only_concert.joins(list: :concert_halls).count(:hall_id)
    @num_of_scores = list_content_arel.only_book.count(:id)
    @num_of_tv_programs = list_content_arel.only_activity_sub.count(:id)
  end

  def list_by_date
    if params[:year_key]
      @year = Year.find_by!(year: params[:year_key])
    else
      @year = Song.order_date.first.event_date.year
    end
    @year_hash = Song.joins(:event_date).merge(EventDate.group(EventDate.arel_table[:year_id])).count
    @years = Year.ids_are(@year_hash.keys).order_year
    @year ||= @years.first
    @songs = Song.year_is(@year).include_event_date.include_title.order_date_desc.order_yomi
    song_year_arel = Song.year_is(@year)
    @lyrics = hash_array_from_array(Lyric.joins(:song).merge(song_year_arel).include_person) { |l| [ l.song_id, l ] }
    @musics = hash_array_from_array(Music.joins(:song).merge(song_year_arel).include_person) { |d| [ d.song_id, d ] }
    list_content_arel = ListContent.joins(:song).merge(song_year_arel).group(:song_id)
    @num_of_albums = list_content_arel.only_album.count(:id)
    @num_of_singles = list_content_arel.only_single.count(:id)
    @num_of_videos = list_content_arel.only_video.count(:id)
    @num_of_concerts = list_content_arel.only_concert.joins(list: :concert_halls).count(:hall_id)
    @num_of_scores = list_content_arel.only_book.count(:id)
    @num_of_tv_programs = list_content_arel.only_activity_sub.count(:id)
  end

  def show
    @song = object_from_params_key Song
    root = @song.root
    redirect_to song_path(@song.root) unless @song == root
    @head, @heads, @lhead_hash, @subtitle, @ids = get_heads(@song.head1, @is_ja)
    @year = @song.event_date.year
    @all_songs = @song.all_objects
    @only_one = @all_songs.count == 1
    @devices = {
      'album' => ListContent.songs_are(@all_songs).include_device.include_song.only_album.order_device_date_desc,
      'single' => ListContent.songs_are(@all_songs).include_device.include_song.only_single.order_device_date_desc,
      'video' => ListContent.songs_are(@all_songs).include_device.include_song.only_video.order_device_date_desc,
    }
    @concerts = ListContent.songs_are(@all_songs).joins(list: :concert).merge(Concert.order_date_desc).merge(List.order_sort_order_desc)
    @scores = ListContent.songs_are(@all_songs).joins(list: :book).merge(Book.order_sort_order_desc)
    @tv_programs = ListContent.songs_are(@all_songs).joins(list: :activity_sub).merge(ActivitySub.order_sort_order_desc)

    @division = params[:division] || 'base_information'
    @division = 'base_information' if @devices[@division] && @devices[@division].count == 0
    if @division == 'base_information'
      @activities = @song.activities.order_sort_order
      @activity_subs = @song.activity_subs.order_sort_order
    end
  end
end
