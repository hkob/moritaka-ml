class ApplicationController < ActionController::Base
  prepend_view_path Rails.root.join("frontend")

  before_action :set_locale
  AVAILABLE_LOCALES = %i(en ja)

  JHeads = [%w[あ い う え お], %w[か き く け こ], %w[さ し す せ そ], %w[た ち つ て と], %w[な に ぬ ね の], %w[は ひ ふ へ ほ], %w[ま み む め も], %w[や ゆ よ], %w[ら り る れ ろ], %w[わ を]]
  EHeads = [%w[A I U E O], %w[KA KI KU KE KO], %w[SA SHI SU SE SO], %w[TA CHI TSU TE TO], %w[NA NI NU NE NO], %w[HA HI FU HE HO], %w[MA MI MU ME MO], %w[YA YU YO], %w[RA RI RU RE RO], %w[WA WO]]
  LHeads = %w[00 01 02 03 04 10 11 12 13 14 20 21 22 23 24 30 31 32 33 34 40 41 42 43 44 50 51 52 53 54 60 61 62 63 64 70 72 74 80 81 82 83 84 90 94]
  LHeads2 = %w[00 10 20 30 40 50 60 70 80 90]
  LHeadJHash = Hash[JHeads.flatten.zip(LHeads)]
  LHeadEHash = Hash[EHeads.flatten.zip(LHeads)]

  def get_links
    [
      [[albums_path], Album],
      [[singles_path], Single],
      [[videos_path], Video],
      [[concerts_path], [Concert, ConcertHall]],
      [[youtubes_path], [Youtube]],
      [[songs_path, list_by_date_songs_path], [Song, List, ListContent]],
      [[people_path], Person],
      [[books_path], Book],
      [[activities_path], [Activity, ActivitySub]],
      [[companies_path], Company],
      [[bands_path], Band],
      [[years_path], [Year, EventDate]],
      [[instrumentals_path], Instrumental],
      [[prefectures_path], [Prefecture, Hall]],
      [[titles_path], Title],
      [[references_path], Reference],
    ]
  end
  helper_method :get_links

  # @return [Boolean] I18n.locale が :ja なら true
  # @note params[:locale] から I18n.locale をセット
  def set_locale
    I18n.locale = params[:locale].try(:to_sym) || I18n.default_locale
    @is_ja = I18n.locale == :ja
  end

  # @params [Hash] options 追加オプション
  # @return [Hash] I18n.locale を locale キーにセットした hash
  def default_url_options(options = {})
    options.merge({locale: I18n.locale})
  end

  # @params [Class] model
  # @return [Object] オブジェクト
  def object_from_params_id(model)
    mid = params[:id]
    mid && model.find(mid)
  end

  # @params [Class] model
  # @return [Object] オブジェクト
  def object_from_params_key(model)
    mkey = params[:key]
    mkey && model.find_by!(key: mkey)
  end

  # @params [Class] models
  # @return [Array<Object>] 複数の場合、オブジェクトの配列
  # @return [Object] 一つの場合、オブジェクト
  # モデルクラスの配列を渡し、params に入っているデータを取得する
  def objects_from_params(*models)
    ans = models.map do |m|
      mkey = params["#{m.to_s.underscore}_key"]
      mkey && m.find_by!(key: mkey)
    end
    ans.count == 1 ? ans.first : ans
  end

  # @params [Object] obj オブジェクト
  # @return [Object] obj が nil でない時、yield(obj)、nil の時 alternative
  def ncm(obj)
    obj ? yield(obj) : nil
  end
  helper_method :ncm

  # @params [Fixnum] v 数値
  # @return [String] v が 0 なら空白、それ以外なら v
  def zero_null(v)
    v == 0 ? "" : v
  end
  helper_method :zero_null

  # @params [Array] array 配列
  # return [Hash<Array<Object>>] yield で取得した key-value を元に key の hash-array を返却
  def hash_array_from_array(array)
    ans = {}
    array.each do |o|
      k, v = yield(o)
      ans[k] = [] unless ans.key? k
      ans[k] << v
    end
    ans
  end
  helper_method :hash_array_from_array
end
