class StatsController < ApplicationController
  def index
  end

  def songs_in_concerts
    # select song_id, sum(number.count_all) from (select count(*) as count_all, concert_halls.list_id as concert_halls_list_id from concert_halls group by concert_halls.list_id) as number, list_contents where list_contents.list_id = concert_halls_list_id group by song_id;
    ch = ConcertHall.arel_table
    hall_count = ch.project(ch[:id].count.as('count'), ch[:list_id]).group(ch[:list_id]).as('hall_count')
    lc = ListContent.arel_table
    sn = lc.project(lc[:song_id], hall_count[:count].sum.as('sum')).join(hall_count).on(lc[:list_id].eq(hall_count[:list_id])).order('sum desc').group(lc[:song_id])
    con = ActiveRecord::Base.connection
    @song_nums = con.select_all(sn.to_sql).to_hash
    @song_nums.shift
    @songs = Song.include_title.all.map { |s| [s.id, s] }.to_h
    @labels = @song_nums.map { |v| @songs[v['song_id']].name(@is_ja) }
  end

  def prefectures_concerts_performed
    hall = Hall.arel_table
    @prefecture_count = ConcertHall.joins(:hall).group(hall[:prefecture_id]).order(hall[:count].desc).count(:id)
    @prefectures = Prefecture.include_title.map { |p| [p.id, p] }.to_h
    @labels = @prefecture_count.keys.map { |v| @prefectures[v].name(@is_ja) }
  end

  def lyrics
    lyric = Lyric.arel_table
    @lyric_count = Lyric.group(:person_id).order(lyric[:count].desc).count(:id)
    @people = Person.include_title.map { |p| [p.id, p] }.to_h
    @labels = @lyric_count.keys.map { |v| @people[v].name(@is_ja) }
  end

  def musics
    music = Music.arel_table
    @music_count = Music.group(:person_id).order(music[:count].desc).count(:id)
    @people = Person.include_title.map { |p| [p.id, p] }.to_h
    @labels = @music_count.keys.map { |v| @people[v].name(@is_ja) }
  end
end
