module Head
  def get_heads(phead, is_ja)
    heads = is_ja ? ApplicationController::JHeads : ApplicationController::EHeads
    lhead_hash = is_ja ? ApplicationController::LHeadJHash : ApplicationController::LHeadEHash
    phead ||= '0'
    selected = heads[phead.to_i]
    head_str = "#{selected.first} - #{selected.last}"
    ids = {head: phead}
    [phead, heads, lhead_hash, head_str, ids]
  end
end
