class HallsController < ApplicationController
  def show
    @hall = object_from_params_key Hall
    @prefecture = @hall.prefecture
    @concert_halls = @hall.concert_halls.include_concert.include_hall.include_event_date.order_date
    @halls = @prefecture.halls.merge(Hall.include_title).order_sort_order
  end
end
