class ActivitiesController < ApplicationController

  def index
    @activity_type = params[:division] || 'cm'
    @activities = Activity.activity_type_str_is(@activity_type).include_title.order_sort_order_desc
  end

  def show
    @activity = object_from_params_key Activity
    @activity_type = @activity.activity_type
    @division = params[:division] || 'cm'
    @activities = Activity.activity_type_str_is(@activity.activity_type).include_title.order_sort_order
  end
end
