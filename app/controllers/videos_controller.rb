class VideosController < ApplicationController
  def index
    @device_class = Video
    @n2s = @device_class.num2str
    @device_type = params[:division].try(:to_i) || @device_class.str2num[:live_video]
    @devices = Video.device_type_value_has(@device_type).include_title.include_singer.include_event_date.order_sort_order_desc
  end

  def show
    @device_class = Video
    @device = object_from_params_key @device_class
    @media = @device.media.include_company.order_sort_order
    @lists = @device.lists
    @division = params[:division] || 'base_information'
    @device_type = @device.device_types.first
    @recorded_at = {}
    case @division
    when 'base_information'
      @singer = @device.singer
      @whole_time = @device.whole_time(@is_ja)
      @lists.each do |sl|
        sl.list_contents.include_video_to_concert_videos.each do |sls|
          sls.video_to_concert_videos.each do |vcv|
            @recorded_at[vcv.concert_hall] = true
          end
        end
      end
    end
  end
end
