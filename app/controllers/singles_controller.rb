class SinglesController < ApplicationController
  def index
    @device_class = Single
    @n2s = @device_class.num2str
    @device_type = params[:division].try(:to_i) || @device_class.str2num[:single]
    @devices = Single.device_type_value_has(@device_type).include_title.include_singer.include_event_date.order_date_desc
  end

  def show
    @device_class = Single
    @device = object_from_params_key @device_class
    @media = @device.media.include_company.order_sort_order
    @lists = @device.lists
    @division = params[:division] || 'base_information'
    @device_type = @device.device_types.first
    case @division
    when 'base_information'
      @singer = @device.singer
      @whole_time = @device.whole_time(@is_ja)
    end
  end
end
