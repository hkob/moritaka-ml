class ReferencesController < ApplicationController
  def index
    @references_hash = hash_array_from_array(Reference.include_title.order_reference_type.order_sort_order) { |r| [r.reference_type, r] }
  end
end
