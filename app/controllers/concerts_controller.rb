class ConcertsController < ApplicationController
  def index
    @concert_type = params[:division] || 'tour'
    @concert_type_count = Concert.group(:concert_type).count(:id)
    @concerts = Concert.concert_type_value_is(Concert.concert_types[@concert_type]).include_from.include_to.include_title.include_band.order_sort_order_desc
  end

  def show
    @concert = object_from_params_key Concert
    @concert_type = @concert.concert_type
    @lists = @concert.lists
    @division = params[:division] || 'base_information'
    @concert_halls = @concert.concert_halls.order_sort_order_desc
    @concerts = Concert.concert_type_value_is(Concert.concert_types[@concert.concert_type]).include_title.order_sort_order_desc
  end
end
