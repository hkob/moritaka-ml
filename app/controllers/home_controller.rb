class HomeController < ApplicationController
  def index
    @links = get_links
    today = Date.today
    @devices = Device.include_title.include_event_date.merge(EventDate.date_is(today)).order_date
    @concert_halls = ConcertHall.include_hall.include_concert.include_event_date.merge(EventDate.date_is(today)).order_date
    @books = Book.include_title.include_event_date.merge(EventDate.date_is(today)).order_date
    @youtubes = Youtube.include_event_date.merge(EventDate.date_is(today)).order_date
    @hash = {}
    dcb = (@devices + @concert_halls + @books + @youtubes).map { |obj| [ obj.event_date, obj ] }
    af = Activity.include_from.merge(EventDate.date_is(today)).order_from.map { |obj| [ obj.from, obj ] }
    at = Activity.include_to.merge(EventDate.date_is(today)).order_to.map { |obj| [ obj.to, obj ] }
    asf = ActivitySub.include_from.merge(EventDate.date_is(today)).order_from.map { |obj| [ obj.from, obj ] }
    ast = ActivitySub.include_to.merge(EventDate.date_is(today)).order_to.map { |obj| [ obj.to, obj ] }
    (dcb + af + at + asf + ast).each do |(date, obj)|
      @hash[date] ||= []
      @hash[date] << obj
    end
  end

  def about
  end
end
