class YoutubesController < ApplicationController
  def index
    @youtube_type = params[:division] || 'sc001'
    @youtube_type_str = Youtube.youtube_types
    @youtubes = Youtube.youtube_type_value_is(@youtube_type).include_song.include_event_date.order_date_desc
  end

  def show
    @youtube = object_from_params_key Youtube
    @song = @youtube.song
    @youtube_type = @youtube.youtube_type
    @youtubes = Youtube.youtube_type_value_is(@youtube_type).include_song.include_event_date.order_date_desc.order_sort_order_desc
  end
end
