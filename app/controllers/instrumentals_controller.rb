class InstrumentalsController < ApplicationController
  def index
    @instrumentals = Instrumental.include_title.order_sort_order
    @num_of_performances = SongPerson.group(:instrumental_id).count(:id)
  end

  def show
    @instrumental = object_from_params_key Instrumental
    @song_people_hash = hash_array_from_array(@instrumental.song_people.include_list.include_device.include_person.order_person_yomi.order_device_date_id ) { |sp| [ sp.person, sp ]}
    @band_members_hash = hash_array_from_array(@instrumental.band_members.order_person_yomi) { |bp| [ bp.person, bp ] }
  end
end
