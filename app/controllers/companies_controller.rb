class CompaniesController < ApplicationController
  def index
    @companies = Company.include_title.order_yomi
    @num_of_media = Medium.group(:company_id).count(:id)
    @num_of_published_books = Book.group(:publisher_id).count(:id)
    @num_of_selled_books = Book.group(:seller_id).count(:id)
    @num_of_activities = Activity.group(:company_id).count(:id)
  end

  def show
    @company = object_from_params_key Company
    @devices = {}
    albums = @company.media.only_album.order_code
    @devices['album'] = albums if albums.count > 0
    singles = @company.media.only_single.order_code
    @devices['single'] = singles if singles.count > 0
    videos = @company.media.only_video.order_code
    @devices['video'] = videos if videos.count > 0
    books = [ @company.published_books, @company.selled_books ]
    @devices['book'] = books if books.map(&:count).inject(&:+) > 0
    activities = @company.activities
    @devices['activity'] = activities if activities.count > 0
    @division = params[:division] || @devices.keys.first
    @division = @devices.keys.first unless @devices[@division]
    @companies = Company.include_title.order_yomi
  end
end
