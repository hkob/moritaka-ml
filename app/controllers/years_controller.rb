class YearsController < ApplicationController
  def index
    @years = Year.order_year_desc
    @num_of_albums = Album.joins(:event_date).group(:year_id).count
    @num_of_singles = Single.joins(:event_date).group(:year_id).count
    @num_of_videos = Video.joins(:event_date).group(:year_id).count
    @num_of_concert_halls = ConcertHall.joins(:event_date).group(:year_id).count(:id)
    @num_of_books = Book.joins(:event_date).group(:year_id).count(:id)
    @num_of_from_activities = Activity.joins(:from).group(:year_id).count(:id)
    @num_of_from_activity_subs = ActivitySub.joins(:from).group(:year_id).count(:id)
    @num_of_to_activities = Activity.joins(:to).group(:year_id).count(:id)
    @num_of_to_activity_subs = ActivitySub.joins(:to).group(:year_id).count(:id)
    @num_of_youtubes = Youtube.joins(:event_date).group(:year_id).count(:id)
  end

  def show
    @year = Year.find_by!(year: params[:key])
    @month_hash = @year.event_dates.merge(EventDate.group_month).count
    @month_hash[13] = @month_hash[nil] if @month_hash[nil]
    @month_hash.delete(nil)
    @division = params[:division].try(:to_i) || @month_hash.keys.first
    @division = @month_hash.keys.first unless @month_hash[@division]
    @devices = Device.year_is(@year).include_title.include_event_date.merge(EventDate.month_is(@division)).order_date
    @concert_halls = ConcertHall.year_is(@year).include_hall.include_concert.include_event_date.merge(EventDate.month_is(@division).order_date)
    @books = Book.year_is(@year).include_title.include_event_date.merge(EventDate.month_is(@division)).order_date
    @youtubes = Youtube.year_is(@year).include_event_date.merge(EventDate.month_is(@division)).order_date
    @hash = {}
    dcb = (@devices + @concert_halls + @books + @youtubes).map { |obj| [ obj.event_date, obj ] }
    af = Activity.from_year_is(@year).include_from.merge(EventDate.month_is(@division)).order_from.map { |obj| [ obj.from, obj ] }
    at = Activity.to_year_is(@year).include_to.merge(EventDate.month_is(@division)).order_to.map { |obj| [ obj.to, obj ] }
    asf = ActivitySub.from_year_is(@year).include_from.merge(EventDate.month_is(@division)).order_from.map { |obj| [ obj.from, obj ] }
    ast = ActivitySub.to_year_is(@year).include_to.merge(EventDate.month_is(@division)).order_to.map { |obj| [ obj.to, obj ] }
    a_date = nil
    (dcb + af + at + asf + ast).each do |(date, obj)|
      @hash[date] ||= []
      @hash[date] << obj
      a_date = date
    end
    @years = Year.order_year_desc
    if @division != 13
      d = a_date.date.beginning_of_month
      @concert_within = Concert.from_date_before(d) & Concert.to_date_after(d)
      @activity_sub_within = ActivitySub.from_date_before(d) & ActivitySub.to_date_after(d)
    end
  end
end
