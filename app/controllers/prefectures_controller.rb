class PrefecturesController < ApplicationController
  def index
   @region = params[:division] || 'Kantou'
   @prefectures = Prefecture.region_value_is(@region).include_title.order_sort_order
   @num_of_halls = Hall.group(:prefecture_id).count(:id)
   @num_of_concerts = ConcertHall.include_hall.group(:prefecture_id).count(:id)
  end

  def show
    @prefecture = object_from_params_key Prefecture
    @region = @prefecture.region
    @halls = @prefecture.halls.include_title.order_sort_order
    @num_of_concerts = ConcertHall.prefecture_is(@prefecture).group(:hall_id).count(:id)
    @prefectures = Prefecture.region_value_is(@prefecture.region).include_title.order_sort_order
  end
end
