class BooksController < ApplicationController

  def index
    @book_type = params[:division] || 'picture_book'
    @books = Book.book_type_str_is(@book_type).include_title.include_event_date.order_sort_order_desc
  end

  def show
    @book = object_from_params_key Book
    @book_type = @book.book_type
    @list = @book.list
    @books = Book.book_type_str_is(@book.book_type).include_title.order_sort_order
  end
end
