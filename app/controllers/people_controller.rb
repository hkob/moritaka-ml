class PeopleController < ApplicationController
  include Head

  def index
    @head, @heads, @lhead_hash, @subtitle, @ids = get_heads(params[:division], @is_ja)
    @people = Person.head_value_is(@head).include_title.order_yomi
    @num_of_albums = Album.singer_head_value_is(@head).group(:singer_id).count(:id)
    @num_of_singles = Single.singer_head_value_is(@head).group(:singer_id).count(:id)
    @num_of_lyrics = Lyric.person_head_value_is(@head).group(:person_id).count(:id)
    @num_of_musics = Music.person_head_value_is(@head).group(:person_id).count(:id)
    @num_of_song_people = SongPerson.person_head_value_is(@head).group(:person_id).count(:id)
    @num_of_bands = BandMember.person_head_value_is(@head).group(:person_id).count(:id)
    @num_of_books = Book.photographer_head_value_is(@head).group(:photographer_id).count(:id).merge(Book.author_head_value_is(@head).group(:photographer_id).count(:id))
  end

  def show
    @person = object_from_params_key Person
    people = @person.all_objects
    @head, @heads, @lhead_hash, @subtitle, @ids = get_heads(@person.head1, @is_ja)
    @hash = {}
    @divisions = %w[lyrics musics albums singles videos bands photographer_books author_books]
    @divisions.each do |k|
      value = case k
              when 'lyrics'
                Lyric.people_are(people).count
              when 'musics'
                Music.people_are(people).count
              when 'albums'
                SongPerson.people_are(people).only_album.group(:device_id).count.keys.count
              when 'singles'
                SongPerson.people_are(people).only_single.group(:device_id).count.keys.count
              when 'videos'
                SongPerson.people_are(people).only_video.group(:device_id).count.keys.count
              when 'bands'
                BandMember.people_are(people).order_band_yomi.count
              when 'photographer_books'
                Book.photographer_are(people).order_sort_order.count
              when 'author_books'
                Book.author_are(people).order_sort_order.count
              end
      @hash[k] = value unless value == 0
    end
    @division = params[:division] || @hash.keys.first
    @division = @hash.keys.first unless @hash[@division]
    @array = case @division
             when 'lyrics'
               Lyric.people_are(people).order_song_date_desc
             when 'musics'
               Music.people_are(people).order_song_date_desc
             when 'albums'
               SongPerson.people_are(people).only_album
             when 'singles'
               SongPerson.people_are(people).only_single
             when 'videos'
               SongPerson.people_are(people).only_video
             when 'bands'
               BandMember.people_are(people).order_band_yomi
             when 'photographer_books'
               Book.photographer_are(people).order_sort_order_desc
             when 'author_books'
               Book.author_are(people).order_sort_order_desc
             end
    @people = Person.head_value_is(@person.head1).include_title.order_yomi
  end
end
