class BandsController < ApplicationController
  def index
   @bands = Band.include_title.order_yomi
  end

  def show
    @band = object_from_params_key Band
    @division = params[:division] || 'base_information'
    @bands = Band.include_title.order_yomi
    case @division
    when 'base_information'
      @concerts = @band.concerts.include_title.order_date
    when 'member'
      @band_members_hash = hash_array_from_array(@band.band_members.include_instrumental.order_instrumental_sort_order) { |bm| [ bm.person_id, bm ] }
      @people = Person.ids_are(@band_members_hash.keys).include_title
    end
  end
end
