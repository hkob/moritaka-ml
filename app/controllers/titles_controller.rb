class TitlesController < ApplicationController
  def index
    @head = params[:division]
    @head = "00" unless /^\d\d$/ =~ @head
    @titles = Title.head_value_is(@head).order_yomi
    @heads = @is_ja ? JHeads : EHeads
    @lhead_hash = @is_ja ? LHeadJHash : LHeadEHash
    @subtitle = @lhead_hash.invert[@head]
    @ids = {head: @head}
    at = Title.head_value_is(@head)
    @links = [Activity, Album, Band, Book, Company, Concert, Hall, Instrumental, Person, Prefecture, Single, Song, Video].map do |model|
      Hash[model.joins(:title).merge(at).map { |o| [ o.title_id, o ] } ]
    end
  end
end
