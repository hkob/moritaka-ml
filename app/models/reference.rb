# @!attribute key
#   @return [String] 検索キー
# @!attribute link
#   @return [String] リンク
# @!attribute sort_order
#   @return [Fixnum] 並び順

class Reference < ApplicationRecord
  include Name

  validates :key, :link, :title_id, :sort_order, presence: true
  validates :key, :title_id, uniqueness: true
  validates :sort_order, uniqueness: {scope: :reference_type}

  enum reference_type: { official_site: 1, fan_site: 2 }

  # @return [Title] 対応するタイトル
  belongs_to :title

  scope :order_reference_type, -> { order arel_table[:reference_type] }
  scope :order_sort_order, -> { order arel_table[:sort_order] }
  scope :include_title, -> { includes :title }
end
