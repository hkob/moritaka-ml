# @!attribute list_id
#   @return [Fixnum] ソングリストID
# @!attribute song_id
#   @return [Fixnum] 曲ID
# @!attribute j_title
#   @return [String] 日本語タイトル
# @!attribute e_title
#   @return [String] 英語タイトル
# @!attribute j_ver
#   @return [String] 日本語バージョン名
# @!attribute e_ver
#   @return [String] 英語バージョン名
# @!attribute sort_order
#   @return [Fixnum] 並び順

class ListContent < ApplicationRecord
  validates :list_id, :sort_order, presence: true
  validates :sort_order, uniqueness: { scope: %i[list_id] }

  # @return [List] 対応するソングリスト
  belongs_to :list
  # @return [Song] 対応する曲
  belongs_to :song, optional: true

#  # @return [<BBB>] 対応するYYY
#  has_one :bbb

  # @return [Array<SongPerson>] 対応する曲関係者一覧
  has_many :song_people, dependent: :destroy
  # @return [Array<ConcertVideo>] 対応するコンサートビデオ(コンサート側)
  has_many :concert_to_concert_videos, class_name: :ConcertVideo, foreign_key: :concert_list_content_id, dependent: :destroy
  # @return [Array<ConcertVideo>] 対応するコンサートビデオ(ビデオ側)
  has_many :video_to_concert_videos, class_name: :ConcertVideo, foreign_key: :video_list_content_id, dependent: :destroy

  scope :include_concert, -> { includes(:list).merge(List.include_concert) }
  scope :include_device, -> { includes(:list).merge(List.include_device) }
  scope :include_song, -> { includes(:song).merge(Song.include_title) }
  scope :include_list, -> { includes(:list) }
  scope :include_video_to_concert_videos, -> { includes(:video_to_concert_videos) }
  scope :only_album, -> { joins(list: :device).merge(Device.only_album) }
  scope :only_single, -> { joins(list: :device).merge(Device.only_single) }
  scope :only_video, -> { joins(list: :device).merge(Device.only_video) }
  scope :only_concert, -> { joins(:list).merge(List.only_concert) }
  scope :only_book, -> { joins(:list).merge(List.only_book) }
  scope :only_activity_sub, -> { joins(:list).merge(List.only_activity_sub) }
  scope :order_device_year, -> { joins(list: :device).merge(Device.order_year) }
  scope :order_device_date_desc, -> { joins(list: :device).merge(Device.order_date_desc) }
  scope :order_sort_order, -> { order arel_table[:sort_order] }
  scope :songs_are, -> ss { where arel_table[:song_id].in ss.map(&:id) }
  scope :sort_order_value_is, -> v { where arel_table[:sort_order].eq v }

  # @param [Boolean] is_ja 日本語なら true
  # @return [String] タイトル
  def title(is_ja)
    is_ja ? j_title : e_title
  end

  # @param [Boolean] is_ja 日本語なら true
  # @return [String] タイトル
  def version(is_ja)
    is_ja ? j_ver : e_ver
  end

  def get_arranges
    song_people.include_person.only_arrange.order_person_yomi.map(&:person)
  end

  def arrange_names(is_ja)
    get_arranges.map { |p| p.name(is_ja) }.join(', ')
  end

  def self.title_only(jt, et = nil, jv = nil, ev = nil)
    et ||= jt
    ev ||= jv
    [[nil, [], jv, ev, jt.to_s, et.to_s]]
  end

  def self.title_with_performer(performer, jt, et = nil, jv = nil, ev = nil)
    et ||= jt
    ev ||= jv
    [[nil, performer, jv, ev, jt.to_s, et.to_s]]
  end

  def self.medley_in(jv = nil, ev = nil)
    self.title_only(:MEDLEY_IN, :MEDLEY_IN, jv, ev)
  end

  def comment(is_ja)
    is_ja ? j_comment : e_comment
  end
end
