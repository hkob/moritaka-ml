# @!attribute key
#   @return [String] 検索キー
# @!attribute j_capital
#   @return [String] 県庁所在地
# @!attribute e_capital
#   @return [String] Capital
# @!attribute sort_order
#   @return [Fixnum] 並び順
# @!attribute region
#   @return [Fixnum] 地方
# @!attribute title_id
#   @return [Fixnum] タイトルID

class Prefecture < ApplicationRecord
  include Name

  validates :key, :j_capital, :e_capital, :region, :sort_order, :title_id, presence: true
  validates :key, :title_id, :sort_order, uniqueness: true

  enum region: { Hokkaidou: 1, Touhoku: 2, Kantou: 3, Chuubu: 4, Kinki: 5, Chuugoku: 6, Shikoku: 7, Kyuushuu: 8 }

  # @return [Title] 対応するタイトル
  belongs_to :title

  # @return [Array<Hall>] 対応するホール一覧
  has_many :halls, dependent: :destroy

  scope :include_title, -> { includes :title }
  scope :order_sort_order, -> { order arel_table[:sort_order] }
  scope :sort_order_value_is, -> v { where arel_table[:sort_order].eq v }
  scope :region_value_is, -> v { where arel_table[:region].eq Prefecture.regions[v] }

#  # @param [Boolean] is_ja 日本語なら true
#  # @return [String] 県庁所在地
  def capital(is_ja)
    is_ja ? j_capital : e_capital
  end
end
