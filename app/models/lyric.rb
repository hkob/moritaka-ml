# @!attribute key
#   @return [String] 検索キー
# @!attribute song_id
#   @return [Fixnum] 曲ID
# @!attribute person_id
#   @return [Fixnum] 関係者ID
# @!attribute sort_order
#   @return [Fixnum] 並び順

class Lyric < ApplicationRecord
  validates :key, :song_id, :person_id, :sort_order, presence: true
  validates :key, uniqueness: true
  validates :sort_order, uniqueness: { scope: %i( song_id ) }

  # @return [Song] 対応する曲
  belongs_to :song
  # @return [Person] 対応する関係者
  belongs_to :person

  scope :include_person, -> { includes(:person).merge(Person.include_title) }
  scope :include_song, -> { includes(:song).merge(Song.include_title) }
  scope :include_song_with_event_date, -> { include_song.merge(Song.include_event_date) }
  scope :order_sort_order, -> { order arel_table[:sort_order] }
  scope :order_song_date, -> { joins(:song).merge(Song.order_date) }
  scope :order_song_date_desc, -> { joins(:song).merge(Song.order_date_desc) }
  scope :people_are, -> ps { where arel_table[:person_id].in ps.map(&:id) }
  scope :person_head_value_is, -> v { joins(:person).merge(Person.head_value_is(v)) }
end
