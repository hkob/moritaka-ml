# @!attribute key
#   @return [String] 検索キー
# @!attribute keyword
#   @return [String] Keyword
# @!attribute book_id
#   @return [Fixnum] 書籍ID
# @!attribute device_id
#   @return [Fixnum] デバイスID
# @!attribute concert_id
#   @return [Fixnum] コンサートID
# @!attribute sort_order
#   @return [Fixnum] 並び順

class List < ApplicationRecord
  validates :key, :keyword, :sort_order, presence: true
  validates :key, uniqueness: true

  # @return [Book] 対応するシングル
  belongs_to :book, optional: true
  # @return [Concert] 対応するコンサート
  belongs_to :concert, optional: true
  # @return [Device] 対応するシングル
  belongs_to :device, optional: true
  # @return [Album] 対応するアルバム
  belongs_to :album, foreign_key: :device_id, optional: true
  # @return [Single] 対応するシングル
  belongs_to :single, foreign_key: :device_id, optional: true
  # @return [Single] 対応するビデオ
  belongs_to :video, foreign_key: :device_id, optional: true
  # @return [ActivitySub] 対応するアクティビティ
  belongs_to :activity_sub, optional: true

  # @return [Array<ListContent>] 対応する曲リスト曲一覧
  has_many :list_contents, dependent: :destroy
  # @return [Array<ConcertHall>] 対応するコンサートホール一覧
  has_many :concert_halls, dependent: :destroy

  scope :sort_order_value_is, -> v { where arel_table[:sort_order].eq v }
  scope :book_is, -> b { where arel_table[:book_id].eq b.id }
  scope :concert_is, -> c { where arel_table[:concert_id].eq c.id }
  scope :device_is, -> d { where arel_table[:device_id].eq d.id }
  scope :device_type_value_is, -> t { joins(:device).merge(Device.only_type(t)) }
  scope :include_concert, -> { includes(:concert).merge(Concert.include_title) }
  scope :include_device, -> { includes(:device).merge(Device.include_title) }
  scope :include_album, -> { includes(:album).merge(Album.include_title) }
  scope :include_single, -> { includes(:single).merge(Single.include_title) }
  scope :include_list_contents, -> { includes(:list_contents) }
  scope :only_concert, -> { where.not(arel_table[:concert_id].eq nil) }
  scope :only_book, -> { where.not(arel_table[:book_id].eq nil) }
  scope :only_activity_sub, -> { where.not(arel_table[:activity_sub_id].eq nil) }
  scope :order_sort_order, -> { order arel_table[:sort_order] }
  scope :order_sort_order_desc, -> { order arel_table[:sort_order].desc }

  def person_instrumental_hash
    ans = Hash.new
    list_contents.order_sort_order.each do |sls|
      sps = sls.song_people.include_person.include_instrumental.order_instrumental_sort_order
      sps.each do |sp|
        person = sp.person
        instrumental = sp.instrumental
        ans[person] ||= {}
        ans[person][instrumental] ||= []
        ans[person][instrumental] << sls.sort_order
      end
    end
    ans
  end

  def list_contents_from_array(array)
    array.each.with_index(1) do |line, so|
      sk, iss, jv, ev, jt, et, jc, ec = line
      lc = list_content_factory self, so do
        {
          song_id: sk,
          j_ver: jv,
          e_ver: ev,
          j_title: jt,
          e_title: et,
          j_comment: jc,
          e_comment: ec
        }
      end
      if iss
        iss.each_slice(2) do |i, p|
          song_person_factory(lc, person_factory(p), instrumental_factory(i))
        end
      end
    end
  end
end
