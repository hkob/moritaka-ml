# @!attribute key
#   @return [String] 検索キー
# @!attribute japanese
#   @return [String] 日本語名
# @!attribute english
#   @return [String] 英語名
# @!attribute yomi
#   @return [String] 読み
# @!attribute yomi_suuji
#   @return [String] 読み数字

class Title < ApplicationRecord
  include Yomi
  include Name
  validates :japanese, :english, :yomi, presence: true

  before_save :on_save_validation_method
  # @return [boolean] yomi_suuji が正しく変換できなければ エラーを追加し、false
  def on_save_validation_method
    ans = true
    if yomi
      self.yomi_suuji = make_yomi_suuji(yomi)
      unless yomi_suuji
        errors.add :yomi, 'は，ひらがなと全角スペース以外の文字は使えません'
        ans = false
      end
    end
    ans
  end
  private :on_save_validation_method

  # @return [Band] 対応するバンド
  has_one :band, dependent: :destroy
  # @return [Concert] 対応するコンサート
  has_one :concert, dependent: :destroy
  # @return [Hall] 対応するホール
  has_one :hall, dependent: :destroy
  # @return [Instrumental] 対応する楽器
  has_one :instrumental, dependent: :destroy
  # @return [Person] 対応する関係者
  has_one :person, dependent: :destroy
  # @return [Prefecture] 対応する都道府県
  has_one :prefecture, dependent: :destroy
  # @return [Activity] 対応する活動一覧
  has_many :activities, dependent: :destroy
  # @return [Album] 対応するアルバム
  has_many :albums, dependent: :destroy
  # @return [Single] 対応するシングル
  has_many :singles, dependent: :destroy
  # @return [Song] 対応する曲
  has_one :song, dependent: :destroy
  # @return [Video] 対応するビデオ
  has_many :videos, dependent: :destroy
  # @return [Reference] 対応する参考サイト
  has_many :references, dependent: :destroy

  scope :head_value_is, -> v { where arel_table[:yomi_suuji].matches("#{v}%") }
  scope :order_yomi, -> { order arel_table[:yomi_suuji] }
  scope :order_yomi_desc, -> { order arel_table[:yomi_suuji].desc }

  # @param [Boolean] is_ja 日本語の場合に true
  # @return [String] 名前
  def name(is_ja)
    is_ja ? japanese : english
  end

  # @return [String] 読み数字の頭の二文字
  def head
    yomi_suuji[0,2]
  end
end
