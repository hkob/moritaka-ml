# @!attribute concert_id
#   @return [Fixnum] コンサートID
# @!attribute sort_order
#   @return [Fixnum] 並び順
# @!attribute hall_id
#   @return [Fixnum] ホールID
# @!attribute event_date_id
#   @return [Fixnum] 公演日ID
# @!attribute j_hall_sub
#   @return [String] 日本語ホールサブ
# @!attribute e_hall_sub
#   @return [String] 英語ホールサブ
# @!attribute j_comment
#   @return [String] 日本語コメント
# @!attribute e_comment
#   @return [String] 英語コメント
# @!attribute j_product
#   @return [String] 日本語名産品
# @!attribute e_product
#   @return [String] 英語名産品
# @!attribute song_list_id
#   @return [Fixnum] ソングリストID
# @!attribute device_id
#   @return [Fixnum] デバイスID

class ConcertHall < ApplicationRecord
  validates :concert_id, :sort_order, :hall_id, :event_date_id, presence: true
  validates :sort_order, uniqueness: { scope: %i[concert_id hall_id] }

  # @return [Concert] 対応するコンサート
  belongs_to :concert
  # @return [Device] 対応するデバイス
  belongs_to :device, optional: true
  # @return [EventDate] 対応する公演日
  belongs_to :event_date
  # @return [Hall] 対応するホール
  belongs_to :hall
  # @return [SongList] 対応する曲リスト
  belongs_to :list, optional: true

  # @return [Array<ConcertVideo>] 対応するコンサートビデオ一覧
  has_many :concert_videos, dependent: :destroy

  scope :concert_is, -> c { where arel_table[:concert_id].eq c.id }
  scope :date_is, -> d { where arel_table[:date].eq d }
  scope :include_concert, -> { includes(:concert).merge(Concert.include_title) }
  scope :include_event_date, -> { includes(:event_date).merge(EventDate.include_year) }
  scope :include_hall, -> { includes(:hall).merge(Hall.include_title) }
  scope :include_hall_with_prefecture, -> { include_hall.merge(Hall.include_prefecture) }
  scope :include_list, -> { includes(:list) }
  scope :order_date, -> { joins(:event_date).merge(EventDate.order_date) }
  scope :order_sort_order, -> { order arel_table[:sort_order] }
  scope :order_sort_order_desc, -> { order arel_table[:sort_order].desc }
 scope :prefecture_is, -> p { joins(:hall).merge(Hall.prefecture_is(p)) }
 scope :sort_order_value_is, -> v { where arel_table[:sort_order].eq v }
  scope :year_is, -> y { joins(:event_date).merge(EventDate.year_is(y)) }

#  # @param [String] arg1 説明
#  # @return [ConcertHall] XXX な ConcertHall
#  def self.rrr(arg1)
#  end

#  # @param [String] arg1 説明
#  # @return [ConcertHall] 説明
#  def ttt(arg1)
#  end

  def self.get_object(concert, sort_order)
    concert_is(concert).sort_order_value_is(sort_order).take
  end

  def comment(is_ja)
    is_ja ? j_comment : e_comment
  end

  def event_title
    "#{Concert.model_name.human}(#{Concert.human_attribute_name concert.concert_type})"
  end

  def hall_sub(is_ja)
    is_ja ? j_hall_sub : e_hall_sub
  end

  def product(is_ja)
    is_ja ? j_product : e_product
  end

  def self.mk(edk, hk, list, jp, ep, jhs = nil, ehs = nil, jc = nil, ec = nil)
    ehs ||= jhs
    ec ||= jc
    [edk, hk, list, jhs, ehs, jc, ec, jp, ep]
  end
end
