# @!attribute key
#   @return [String] 検索キー
# @!attribute device_type
#   @return [Fixnum] デバイスタイプ
# @!attribute title_id
#   @return [Fixnum] タイトルID
# @!attribute event_date_id
#   @return [Date] 日付ID
# @!attribute minutes
#   @return [Fixnum] 分
# @!attribute seconds
#   @return [Fixnum] 秒
# @!attribute sort_order
#   @return [Fixnum] 並び順
# @!attribute number
#   @return [String] 番号
# @!attribute singer_id
#   @return [Fixnum] シンガーID
# @!attribute j_comment
#   @return [String] 日本語コメント
# @!attribute e_comment
#   @return [String] 英語コメント
# @!attribute link
#   @return [String] 外部リンク

class Device < ApplicationRecord
  validates :key, :type, :device_type, :title_id, :sort_order, :event_date_id, presence: true
  validates :key, uniqueness: true

  # @return [EventDate] 対応する日付
  belongs_to :event_date
  # @return [People] 対応する歌手
  belongs_to :singer, class_name: :Person, foreign_key: :singer_id
  # @return [Title] 対応するタイトル
  belongs_to :title

  # @return [Array<ConcertHall>] 対応するコンサートホール一覧
  has_many :concert_halls, dependent: :destroy
  # @return [Array<DeviceActivity>] 対応するデバイスアクティビティ一覧
  has_many :device_activities, dependent: :destroy
  # @return [Array<Medium>] 対応するメディア一覧
  has_many :media, dependent: :destroy
  # @return [Array<SongList>] 対応する曲リスト一覧
  has_many :lists, dependent: :destroy
  # @return [Array<ListContent>] 対応するコンテンツ一覧
  has_many :list_contents, through: :lists

  scope :device_type_value_has, -> v { where 'devices.device_type & ? = ?', v, v }
  scope :ids_are, -> array { where arel_table[:id].in array }
  scope :include_event_date, -> { includes(:event_date) }
  scope :include_singer, -> { includes(:singer).merge(Person.include_title) }
  scope :include_title, -> { includes :title }
  scope :only_type, -> v { where arel_table[:type].eq v}
  scope :only_album, -> { only_type :Album }
  scope :order_year, -> { joins(event_date: :year).merge(Year.order_year) }
  scope :only_single, -> { only_type :Single }
  scope :only_video, -> { only_type :Video }
  scope :order_date, -> { joins(:event_date).merge(EventDate.order_date) }
  scope :order_date_desc, -> { joins(:event_date).merge(EventDate.order_date_desc) }
  scope :order_id, -> { order self.arel_table[:id] }
  scope :order_sort_order, -> { order arel_table[:sort_order] }
  scope :order_sort_order_desc, -> { order arel_table[:sort_order].desc }
  scope :singer_head_value_is, -> v { joins(:singer).merge(Person.head_value_is(v)) }
  scope :sort_order_value_is, -> v { where arel_table[:sort_order].eq v }
  scope :year_is, -> y { joins(:event_date).merge(EventDate.year_is(y)) }

  # @param [String] dt デバイスタイプ
  # @param [Fixnum] so 並び順
  # @return [Device] 対応する Device
  def self.get_object(dt, so)
    self.sort_order_value_is(so).device_type_value_has(dt.to_s).take
  end

  # @return [Fixnum] month
  def month
    date ? date.month + 1 : nil
  end

  def device_types
    ans = []
    self.class.num2str.keys.each do |num|
      ans << num unless (device_type & num) == 0
    end
    ans
  end

  def device_type_strs
    hash = self.class.num2str
    self.device_types.map { |num| hash[num] }
  end

  def whole_time(is_ja)
    if is_ja
      minutes ? seconds ? "#{ minutes }分#{ seconds }秒" : "#{minutes}分" : nil
    else
      minutes ? seconds ? %Q(#{ minutes }'#{ seconds }") : "#{minutes}'" : nil
    end
  end

  def comment(is_ja)
    is_ja ? j_comment : e_comment
  end

  def event_title
    "#{device_type_strs.map { |dt| self.class.human_attribute_name dt }.join(', ')}#{Device.human_attribute_name :release}"
  end

  # @note medium を一括作成
  def media_from_array(array)
    array.each.with_index(1) do |line, so|
      key, md, code, r, ns, company, p = line
      medium_factory key do
        {
          device_id: self.id,
          medium_device: md,
          code: code,
          release: r,
          now_sale: ns,
          company_id: company,
          price: p,
          sort_order: so
        }
      end
    end
  end
end
