# @!attribute event_date_id
#   @return [Fixnum] 日付ID
# @!attribute title_id
#   @return [Fixnum] タイトルID
# @!attribute parent_id
#   @return [Fixnum] 親ID

class Song < ApplicationRecord
  include Name
  validates :key, :title_id, :event_date_id, presence: true
  validates :key, :title_id, uniqueness: true

  # @return [EventDate] 対応する発売日
  belongs_to :event_date
  # @return [Title] 対応するタイトル
  belongs_to :title
  # @return [Song] 対応する親
  belongs_to :parent, class_name: :Song, optional: true

  # @return [Array<Activity>] 対応する活動
  has_many :activities, dependent: :destroy
  # @return [Array<Song>] 対応する子供一覧
  has_many :children, class_name: :Song, foreign_key: :parent_id
  # @return [Array<Lyric>] 対応する作詞者一覧
  has_many :lyrics, dependent: :destroy
  # @return [Array<Music>] 対応する作詞者一覧
  has_many :musics, dependent: :destroy
  # @return [Array<ListContent>] 対応する曲リスト曲一覧
  has_many :list_contents, dependent: :destroy
  # @return [Array<ActivitySub>] 対応するアクティビティサブ
  has_many :activity_subs, dependent: :destroy
  # @return [Array<Youtube>] 対応するアクティビティサブ
  has_many :youtubes, dependent: :destroy

  scope :head_value_is, -> v { joins(:title).merge(Title.head_value_is(v)) }
  scope :include_event_date, -> { includes(:event_date).merge(EventDate.include_year) }
  scope :include_lyrics, -> { includes(:lyrics).merge(Lyric.include_person) }
  scope :include_musics, -> { includes(:musics).merge(Music.include_person) }
  scope :include_title, -> { includes :title }
  scope :only_main, -> { where self.arel_table[:parent_id].eq(nil) }
  scope :order_date, -> { joins(:event_date).merge(EventDate.order_date) }
  scope :order_date_desc, -> { joins(:event_date).merge(EventDate.order_date_desc) }
  scope :order_yomi, -> { joins(:title).merge(Title.order_yomi) }
  scope :order_yomi_desc, -> { joins(:title).merge(Title.order_yomi_desc) }
  scope :year_is, -> y { joins(:event_date).merge(EventDate.year_is(y)) }


  def lyric_people
    self.lyrics.include_person.order_sort_order.map { |lyric| lyric.person }
  end

  def lyrics_str(flag)
    lyric_people.map { |person| person.name(flag) }.join(', ')
  end

  def music_people
    self.musics.include_person.order_sort_order.map { |music| music.person }
  end

  def musics_str(flag)
    music_people.map { |person| person.name(flag) }.join(', ')
  end
end
