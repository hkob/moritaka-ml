# @!attribute key
#   @return [String] 検索キー
# @!attribute activity_type
#   @return [Fixnum] アクティビティタイプ
# @!attribute title_id
#   @return [Fixnum] タイトルID
# @!attribute company_id
#   @return [Fixnum] 企業ID (放送局、タイアップ先)
# @!attribute song_id
#   @return [Fixnum] 曲ID
# @!attribute from_id
#   @return [Date] 開始日ID
#   @return [Boolean] 開始日に日がない場合に true
# @!attribute to_id
#   @return [Date] 終了日ID
# @!attribute j_comment
#   @return [String] 日本語コメント
# @!attribute e_comment
#   @return [String] 英語コメント
# @!attribute sort_order
#   @return [Fixnum] 並び順

class Activity < ApplicationRecord
  include Name

  validates :key, :activity_type, :title_id, :sort_order, presence: true
  validates :key, uniqueness: true
  validates :sort_order, uniqueness: { scope: %i( activity_type ) }

  enum activity_type: { cm: 1, regular_tv: 2, regular_radio: 3, tv_program: 4, radio_program: 5, magazine: 6, thema_song: 7, movie: 8, other: 99 }

  # @return [Company] 対応する企業
  belongs_to :company, optional: true
  # @return [EventDate] 対応する開始日
  belongs_to :from, class_name: :EventDate, foreign_key: :from_id, optional: true
  # @return [Song] 対応する曲
  belongs_to :song, optional: true
  # @return [Title] 対応するタイトル
  belongs_to :title
  # @return [EventDate] 対応する終了日
  belongs_to :to, class_name: :EventDate, foreign_key: :to_id, optional: true
  # @return [Array<ActivitySub>] 対応するアクティビティサブ
  has_many :activity_subs, dependent: :destroy
  # @return [Array<DeviceActivity>] 対応するデバイスアクティビティ一覧
  has_many :device_activities, dependent: :destroy

  scope :activity_type_str_is, -> s { where arel_table[:activity_type].eq activity_types[s] }
  scope :from_year_is, -> y { joins(:from).merge(EventDate.year_is(y)) }
  scope :include_from, -> { includes :from }
  scope :include_title, -> { includes :title }
  scope :include_to, -> { includes :to }
  scope :order_from, -> { joins(:from).merge(EventDate.order_date) }
  scope :order_sort_order, -> { order arel_table[:sort_order] }
  scope :order_sort_order_desc, -> { order arel_table[:sort_order].desc }
  scope :order_to, -> { joins(:to).merge(EventDate.order_date) }
  scope :sort_order_value_is, -> v { where arel_table[:sort_order].eq v }
  scope :to_year_is, -> y { joins(:to).merge(EventDate.year_is(y)) }

  def comment(is_ja)
    is_ja ? j_comment : e_comment
  end

  def from_event_title
    Activity.human_attribute_name "#{ activity_type }_from_event_title"
  end

  def to_event_title
    Activity.human_attribute_name "#{ activity_type }_to_event_title"
  end

  def from_or_children_from
    from || activity_subs.order_sort_order.first.try(:from)
  end

  def to_or_children_to
    to || activity_subs.order_sort_order.last.try(:to) || activity_subs.order_sort_order.last.try(:from)
  end

  def table_header
    case activity_type
    when 'tv_program'
      %i[name broadcasting_date song_list comment]
    else
      %i[name term tieup_song comment]
    end
  end
end
