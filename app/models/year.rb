# @!attribute year
#   @return [Fixnum] 年

class Year < ApplicationRecord
  include Name

  validates :year, presence: true
  validates :year, uniqueness: true

  # @return [Array<EventDate>] 対応する日付
  has_many :event_dates, dependent: :destroy

#  has_many :ddds, class_name: 'CCC', foreign_key: :ddd_id

  scope :ids_are, -> ids { where arel_table[:id].in ids }
  scope :order_year, -> { order arel_table[:year] }
  scope :order_year_desc, -> { order arel_table[:year].desc }
  scope :year_value_is, -> y { where arel_table[:year].eq y }
  scope :select_id, -> { select arel_table[:id] }

  # @params [Boolena] is_ja 日本語なら true
  # @return [String] 名前
  def name(is_ja)
    is_ja ? "#{ year }年" : "In #{ year }"
  end

  def self.ynmgdnw(date, noday = false)
    I18n.l date, format: (noday ? :month : :long) if date
  end

  def ynmgdnw(date, noday = false)
    date ? self.class.ynmgdnw(date, noday) : "#{ year }年"
  end

  def self.year_from_date(date)
    y = date.year
    ans = Year.year_value_is(y).take
    ans = Year.create(year: y) unless ans
    ans
  end

  def self.month_str(m, is_ja)
    is_ja ? %w( - 1月 2月 3月 4月 5月 6月 7月 8月 9月 10月 11月 12月 未設定 )[m] : %w( - January February March April May June July August September October November December Unsettled)[m]
  end

  def key
    year
  end
end
