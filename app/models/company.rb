# @!attribute title_id
#   @return [Fixnum] タイトルID

class Company < ApplicationRecord
  include Name
  validates :key, :title_id, presence: true
  validates :key, uniqueness: true

  # @return [Title] 対応するタイトル
  belongs_to :title

#  # @return [<BBB>] 対応するYYY
#  has_one :bbb

  # @return [Array<Activity>] 対応する活動
  has_many :activities, dependent: :destroy
  # @return [Array<Medium>] 対応するメディア一覧
  has_many :media, dependent: :destroy
  # @return [Array<Book>] 対応するメディア一覧
  has_many :published_books, dependent: :destroy, foreign_key: :publisher_id, class_name: :Book
  # @return [Array<Book>] 対応するメディア一覧
  has_many :selled_books, dependent: :destroy, foreign_key: :seller_id, class_name: :Book

  scope :include_title, -> { includes :title }
  scope :order_yomi, -> { joins(:title).merge(Title.order_yomi) }
end
