# @!attribute key
#   @return [String] 検索キー
# @!attribute book_type
#   @return [Fixnum] ブックタイプ
# @!attribute title_id
#   @return [Fixnum] タイトルID
# @!attribute publisher_id
#   @return [Fixnum] 出版社ID
# @!attribute seller_id
#   @return [Fixnum] 販売社ID
# @!attribute author_id
#   @return [Fixnum] 著者ID
# @!attribute photographer_id
#   @return [Fixnum] 撮影者ID
# @!attribute isbn
#   @return [String] ISBN
# @!attribute price
#   @return [String] 価格
# @!attribute event_date_id
#   @return [Fixnum] 発売日ID
# @!attribute sort_order
#   @return [Fixnum] 並び順
# @!attribute j_comment
#   @return [String] 日本語コメント
# @!attribute e_comment
#   @return [String] 英語コメント
# @!attribute link
#   @return [String] リンク

class Book < ApplicationRecord
  include Name
  validates :key, :book_type, :title_id, :sort_order, presence: true
  validates :key, uniqueness: true
  validates :sort_order, uniqueness: { scope: %i( book_type ) }

  enum book_type: { picture_book: 1, score_book: 2, essay: 3, poems: 4 }

  # @return [Person] 対応する著者
  belongs_to :author, foreign_key: :author_id, class_name: :Person, optional: true
  # @return [EventDate] 対応する発売日
  belongs_to :event_date, optional: true
  # @return [Company] 対応する出版社
  belongs_to :publisher, foreign_key: :publisher_id, class_name: :Company
  # @return [Company] 対応する販売社
  belongs_to :seller, foreign_key: :seller_id, class_name: :Company, optional: true
  # @return [Person] 対応する撮影者
  belongs_to :photographer, foreign_key: :photographer_id, class_name: :Person, optional: true
  # @return [Title] 対応するタイトル
  belongs_to :title

  # @return [List] 対応するソングリスト
  has_one :list, dependent: :destroy

  scope :book_type_str_is, -> s { where arel_table[:book_type].eq Book.book_types[s] }
  scope :include_title, -> { includes :title }
  scope :include_event_date, -> { includes(:event_date) }
  scope :sort_order_value_is, -> v { where arel_table[:sort_order].eq v }
  scope :order_sort_order, -> { order arel_table[:sort_order] }
  scope :order_sort_order_desc, -> { order arel_table[:sort_order].desc }
  scope :order_date, -> { joins(:event_date).merge(EventDate.order_date) }
  scope :year_is, -> y { joins(:event_date).merge(EventDate.year_is(y)) }
  scope :photographer_head_value_is, -> v { joins(:photographer).merge(Person.head_value_is(v)) }
  scope :author_head_value_is, -> v { joins(:author).merge(Person.head_value_is(v)) }
  scope :photographer_are, -> ps { where arel_table[:photographer_id].in ps.map(&:id) }
  scope :author_are, -> ps { where arel_table[:author_id].in ps.map(&:id) }

  def event_title
    self.class.human_attribute_name book_type
  end

  def comment(is_ja)
    is_ja ? j_comment : e_comment
  end
end
