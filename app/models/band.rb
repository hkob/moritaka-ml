# @!attribute key
#   @return [String] 検索キー
# @!attribute title_id
#   @return [Fixnum] タイトルID
# @!attribute j_comment
#   @return [String] 日本語コメント
# @!attribute e_comment
#   @return [String] 英語コメント

class Band < ApplicationRecord
  include Name

  validates :key, :title_id, presence: true
  validates :key, :title_id, uniqueness: true

  # @return [Title] 対応するタイトルID
  belongs_to :title

  # @return [Array<Concert>] 対応するコンサート一覧
  has_many :concerts, dependent: :destroy
  # @return [Array<BandMember>] 対応するバンドメンバー一覧
  has_many :band_members, dependent: :destroy

  scope :ids_are, -> array { where arel_table[:id].in array }
  scope :include_title, -> { includes :title }
  scope :order_yomi, -> { joins(:title).merge(Title.order_yomi) }

  def band_members_from_array(array)
    array.each_slice(2) do |i, p|
      band_member_factory(self, p, i)
    end
  end
end
