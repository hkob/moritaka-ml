# @!attribute concert_list_content_id
#   @return [Fixnum] コンサート側のソングリスト曲ID
# @!attribute video_list_content_id
#   @return [Fixnum] ビデオ側のソングリスト曲ID

class ConcertVideo < ApplicationRecord
  validates :concert_list_content_id, :video_list_content_id, :concert_hall_id, presence: true

  # @return [ListContent] 対応するソングリスト曲(コンサート側)
  belongs_to :concert_list_content, class_name: :ListContent, foreign_key: :concert_list_content_id
  # @return [ListContent] 対応するソングリスト曲(ビデオ側)
  belongs_to :video_list_content, class_name: :ListContent, foreign_key: :video_list_content_id
  # @return [ConcertHall] 対応するコンサートホール
  belongs_to :concert_hall

  scope :concert_hall_is, -> o { where arel_table[:concert_hall_id].eq o.id }
  scope :concert_list_content_is, -> o { where arel_table[:concert_list_content_id].eq o.id }
  scope :include_concert_list_content, -> { includes(:concert_list_content) }
  scope :include_concert_list_content_with_song_list, -> { include_concert_list_content.merge(ListContent.include_song_list) }
#  scope :include_video_list_content, -> { includes(:video_list_content).merge(ListContent.include_song_list) }
  scope :include_video_list_content, -> { includes(:video_list_content) }
  scope :video_list_content_is, -> o { where arel_table[:video_list_content_id].eq o.id }

#  # @return [Array<ConcertVideo>] sort_order 順に並んだ ConcertVideo 一覧を得る
#  scope :order_sort_order, -> { order arel_table[:sort_order] }
#  # @return [Array<ConcertVideo>] qqq が v な ConcertVideo 一覧を得る
#  scope :qqq_value_is, -> v { where arel_table[:qqq].eq v }

#  # @param [String] arg1 説明
#  # @return [ConcertVideo] XXX な ConcertVideo
#  def self.rrr(arg1)
#  end

#  # @param [String] arg1 説明
#  # @return [ConcertVideo] 説明
#  def ttt(arg1)
#  end
end
