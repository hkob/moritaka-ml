# @!attribute medium_device
#   @return [String] メディアデバイス
# @!attribute code
#   @return [String] コード
# @!attribute release
#   @return [String] リリース名
# @!attribute now_sale
#   @return [Boolean] 販売中なら true
# @!attribute company_id
#   @return [Fixnum] 企業ID
# @!attribute price
#   @return [String] 価格
# @!attribute sort_order
#   @return [Fixnum] 並び順
# @!attribute device_id
#   @return [Fixnum] デバイスID

class Medium < ApplicationRecord
  validates :key, :medium_device, :sort_order, :device_id, presence: true
  validates :key, uniqueness: true

  # @return [Company] 対応するデバイス
  belongs_to :company
  # @return [Device] 対応するデバイス
  belongs_to :device

#  has_many :ddds, class_name: 'CCC', foreign_key: :ddd_id

  scope :device_is, -> v { where arel_table[:device_id].eq v.id }
  scope :include_company, -> { includes(:company).merge(Company.include_title) }
  scope :include_device, -> { includes(:device).merge(Device.include_title) }
  scope :include_device_with_event_date, -> { include_device.merge(Device.include_event_date) }
  scope :only_album, -> { only_type :Album }
  scope :only_single, -> { only_type :Single }
  scope :only_video, -> { only_type :Video }
  scope :only_type, -> v { joins(:device).merge(Device.only_type(v)) }
  scope :order_code, -> { order arel_table[:code] }
  scope :order_sort_order, -> { order arel_table[:sort_order] }
  scope :sort_order_value_is, -> v { where arel_table[:sort_order].eq v }

#  # @param [String] arg1 説明
#  # @return [Medium] XXX な Medium
#  def self.rrr(arg1)
#  end

#  # @param [String] arg1 説明
#  # @return [Medium] 説明
#  def ttt(arg1)
#  end

  # @param [Device] dv デバイス
  # @param [Fixnum] so 並び順
  # @return [Medium] 対応する Medium
  def self.get_object(dv, so)
    self.device_is(dv).sort_order_value_is(so).take
  end

  def price_str(is_ja)
    price ? is_ja ? "#{price}円" : "#{price} Yen" : nil
  end

end
