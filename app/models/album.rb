# @!attribute type
#   @return [String] タイプ
# @!attribute device_type
#   @return [Fixnum] デバイスタイプ
# @!attribute title_id
#   @return [Fixnum] タイトルID
# @!attribute event_date_id
#   @return [Fixnum] 日付ID
# @!attribute minutes
#   @return [Fixnum] 分
# @!attribute seconds
#   @return [Fixnum] 秒
# @!attribute sort_order
#   @return [Fixnum] 並び順
# @!attribute number
#   @return [String] 番号
# @!attribute singer_id
#   @return [Fixnum] シンガーID
# @!attribute j_comment
#   @return [String] 日本語コメント
# @!attribute e_comment
#   @return [String] 英語コメント
# @!attribute link
#   @return [String] 外部リンク

class Album < Device
  include Name

  AlbumStr2Num = { album: 1, mini_album: 2, karaoke_cd: 4, lyric_album: 8, music_album: 16, cover_album: 32, part_album: 64, compilation_album: 128 }
  AlbumNum2Str = AlbumStr2Num.invert

  def self.num2str
    AlbumNum2Str
  end

  def self.str2num
    AlbumStr2Num
  end
end
