# @!attribute type
#   @return [String] タイプ
# @!attribute device_type
#   @return [Fixnum] デバイスタイプ
# @!attribute title_id
#   @return [Fixnum] タイトルID
# @!attribute date
#   @return [Date] 日付
# @!attribute minutes
#   @return [Fixnum] 分
# @!attribute seconds
#   @return [Fixnum] 秒
# @!attribute sort_order
#   @return [Fixnum] 並び順
# @!attribute number
#   @return [String] 番号
# @!attribute year_id
#   @return [Fixnum] 年ID
# @!attribute singer_id
#   @return [Fixnum] シンガーID
# @!attribute j_comment
#   @return [String] 日本語コメント
# @!attribute e_comment
#   @return [String] 英語コメント

class Single < Device
  include Name

  SingleStr2Num = { ep_single: 1, single: 2, ct_single: 4, lyric_single: 8, music_single: 16, cover_single: 32, part_single: 64 }
  SingleNum2Str = SingleStr2Num.invert


  def self.num2str
    SingleNum2Str
  end

  def self.str2num
    SingleStr2Num
  end

end
