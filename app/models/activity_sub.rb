# @!attribute key
#   @return [String] 検索キー
# @!attribute j_title
#   @return [String] 日本語タイトル
# @!attribute e_title
#   @return [String] 英語タイトル
# @!attribute song_id
#   @return [Fixnum] 曲ID
# @!attribute from_id
#   @return [Date] 開始日ID
#   @return [Boolean] 開始日に日がない場合に true
# @!attribute to_id
#   @return [Date] 終了日ID
# @!attribute j_comment
#   @return [String] 日本語コメント
# @!attribute e_comment
#   @return [String] 英語コメント
# @!attribute sort_order
#   @return [Fixnum] 並び順
class ActivitySub < ApplicationRecord
  include Name

  validates :key, :sort_order, presence: true
  validates :key, uniqueness: true
  validates :sort_order, uniqueness: { scope: %i( activity_id ) }

  # @return [Activity] 対応するアクティビティ
  belongs_to :activity
  # @return [EventDate] 対応する開始日
  belongs_to :from, class_name: :EventDate, foreign_key: :from_id, optional: true
  # @return [Song] 対応する曲
  belongs_to :song, optional: true
  # @return [EventDate] 対応する終了日
  belongs_to :to, class_name: :EventDate, foreign_key: :to_id, optional: true

  # @return [Array<List>] 対応するリスト
  has_one :list, dependent: :destroy

  scope :from_year_is, -> y { joins(:from).merge(EventDate.year_is(y)) }
  scope :include_from, -> { includes :from }
  scope :include_song, -> { includes :song }
  scope :include_title, -> { includes :title }
  scope :include_to, -> { includes :to }
  scope :order_from, -> { joins(:from).merge(EventDate.order_date) }
  scope :order_sort_order, -> { order arel_table[:sort_order] }
  scope :order_sort_order_desc, -> { order arel_table[:sort_order].desc }
  scope :order_to, -> { joins(:to).merge(EventDate.order_date) }
  scope :sort_order_value_is, -> v { where arel_table[:sort_order].eq v }
  scope :to_year_is, -> y { joins(:to).merge(EventDate.year_is(y)) }
  scope :from_date_before, -> d { joins(:from).merge(EventDate.date_before(d)) }
  scope :to_date_after, -> d { joins(:to).merge(EventDate.date_after(d)) }

  def comment(is_ja)
    is_ja ? j_comment : e_comment
  end

  def name(is_ja)
    ans = is_ja ? j_title : e_title
    ans || activity.name(is_ja)
  end

  def from_event_title
    Activity.human_attribute_name "#{ activity.activity_type }_from_event_title"
  end

  def to_event_title
    Activity.human_attribute_name "#{ activity.activity_type }_to_event_title"
  end
end
