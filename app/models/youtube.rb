# @!attribute key
#   @return [String] 検索キー
# @!attribute youtube_type
#   @return [Fixnum] youtube タイプ
# @!attribute number
#   @return [String] 番号
# @!attribute j_title
#   @return [String] 日本語タイトル
# @!attribute e_title
#   @return [String] 英語タイトル
# @!attribute sort_order
#   @return [Fixnum] 並び順
# @!attribute link
#   @return [String] リンク
# @!attribute comment
#   @return [String] コメント

class Youtube < ApplicationRecord
  include Name
  validates :key, :youtube_type, :sort_order, presence: true
  validates :key, uniqueness: true
  validates :sort_order, uniqueness: { scope: %i( youtube_type ) }

  enum youtube_type: { pv: 100, live: 200, sc001: 1001, sc021: 1002, sc041: 1003, sc061: 1004, sc081: 1005, sc101: 1006, sc121: 1007, sc141: 1008, sc161: 1009, sc181: 1010, sc201: 1011 }
  belongs_to :event_date
  belongs_to :song, optional: true

  scope :youtube_type_value_is, -> v { where arel_table[:youtube_type].eq v }
  scope :include_event_date, -> { includes(:event_date) }
  scope :include_song, -> { includes(:song) }
  scope :order_date, -> { joins(:event_date).merge(EventDate.order_date) }
  scope :order_date_desc, -> { joins(:event_date).merge(EventDate.order_date_desc) }
  scope :order_sort_order_desc, -> { order arel_table[:sort_order].desc }
  scope :year_is, -> y { joins(:event_date).merge(EventDate.year_is(y)) }

  def name(is_ja)
    is_ja ? j_title : e_title
  end

  def event_title
    "#{Youtube.model_name.human}(#{Youtube.human_attribute_name youtube_type})"
  end
end
