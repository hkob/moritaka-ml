# @!attribute date
#   @return [Date] 日付
# @!attribute year_id
#   @return [Fixnum] 年ID
# @!attribute month
#   @return [Fixnum] 月
# @!attribute day
#   @return [Fixnum] 日

class EventDate < ApplicationRecord
  validates :key, :date, :year_id, presence: true
  validates :key, uniqueness: true

  # @return [Year] 対応する年
  belongs_to :year

  # @return [Array<Activity>] 対応する活動(開始日)
  has_many :activity_froms, class_name: :Activity, foreign_key: :from_id, dependent: :destroy
  # @return [Array<Activity>] 対応する活動(終了日)
  has_many :activity_tos, class_name: :Activity, foreign_key: :to_id, dependent: :destroy
  # @return [Array<ActivitySub>] 対応する活動(開始日)
  has_many :activity_sub_froms, class_name: :ActivitySub, foreign_key: :from_id, dependent: :destroy
  # @return [Array<ActivitySub>] 対応する活動(終了日)
  has_many :activity_sub_tos, class_name: :ActivitySub, foreign_key: :to_id, dependent: :destroy
  # @return [Array<Album>] 対応するアルバム
  has_many :albums, dependent: :destroy
  # @return [Array<Book>] 対応する書籍
  has_many :books, dependent: :destroy
  # @return [Array<Concert>] 対応するコンサート(開始日)
  has_many :concert_froms, class_name: :Concert, foreign_key: :from_id, dependent: :destroy
  # @return [Array<ConcertHall>] 対応するコンサートホール
  has_many :concert_halls, dependent: :destroy
  # @return [Array<Concert>] 対応するコンサート(終了日)
  has_many :concert_tos, class_name: :Concert, foreign_key: :to_id, dependent: :destroy
  # @return [Array<Single>] 対応するシングル
  has_many :singles, dependent: :destroy
  # @return [Array<Song>] 対応する曲
  has_many :songs, dependent: :destroy
  # @return [Array<Video>] 対応するビデオ
  has_many :videos, dependent: :destroy
  # @return [Array<Youtube>] 対応するビデオ
  has_many :youtubes, dependent: :destroy

  scope :group_month, -> { group(arel_table[:month]) }
  scope :include_year, -> { includes :year }
  scope :no_month, -> { where arel_table[:month].eq nil }
  scope :order_date, -> { order arel_table[:date] }
  scope :order_date_desc, -> { order arel_table[:date].desc }
  scope :year_is, -> y { where arel_table[:year_id].eq y.id }
  scope :month_is, -> m { where arel_table[:month].eq(m == 13 ? nil : m) }
  scope :date_is, -> d { month_is(d.month).where(arel_table[:day].eq d.day) }
  scope :date_after, -> d { where arel_table[:date].gteq d }
  scope :date_before, -> d { where arel_table[:date].lteq d }

  # @return [String] 日付表示
  def name
    I18n.l date, format: month && day ? :long : month ? :month : :y_only
  end

  # @return [Boolean] 日付の比較結果
  def <=>(other)
    date <=> other.date
  end
end
