# @!attribute key
#   @return [String] 検索キー
# @!attribute sort_order
#   @return [Fixnum] 並び順
# @!attribute title_id
#   @return [Fixnum] タイトルID

class Instrumental < ApplicationRecord
  include Name
  validates :key, :sort_order, :title_id, presence: true
  validates :key, :sort_order, :title_id, uniqueness: true

  # @return [Title] 対応するタイトル
  belongs_to :title

  # @return [Array<SongPerson>] 対応する曲関係者一覧
  has_many :song_people, dependent: :destroy
  # @return [Array<BandMember>] 対応するバンドメンバー一覧
  has_many :band_members, dependent: :destroy

  ARRANGE = 1000

  scope :except_arrange, -> { where.not arel_table[:sort_order].eq ARRANGE }
  scope :include_title, -> { includes :title }
  scope :is_arrange, -> { where arel_table[:sort_order].eq ARRANGE }
  scope :order_sort_order, -> { order arel_table[:sort_order] }
end
