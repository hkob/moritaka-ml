# @!attribute title_id
#   @return [Fixnum] タイトルID
# @!attribute prefecture_id
#   @return [Fixnum] 都道府県ID
# @!attribute sort_order
#   @return [Fixnum] 並び順

class Hall < ApplicationRecord
  include Name

  validates :key, :title_id, :sort_order, :prefecture_id, presence: true
  validates :key, :title_id, uniqueness: true
  validates :sort_order, uniqueness: {scope: %i[prefecture_id]}

  # @return [Title] 対応するタイトル
  belongs_to :title
  # @return [Prefecture] 対応する都道府県
  belongs_to :prefecture
  # @return [Array<ConcertHall>] 対応するコンサートホール一覧
  has_many :concert_halls, dependent: :destroy

  scope :include_title, -> { includes :title }
  scope :include_prefecture, -> { includes(:prefecture).merge(Prefecture.include_title) }
  scope :order_sort_order, -> { order arel_table[:sort_order] }
  scope :prefecture_is, -> p { where arel_table[:prefecture_id].eq p.id }
end
