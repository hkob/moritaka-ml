# @!attribute key
#   @return [String] 検索キー
# @!attribute title_id
#   @return [Fixnum] タイトルID
# @!attribute parent_id
#   @return [Fixnum] 親の person_id

class Person < ApplicationRecord
  include Name

  validates :key, :title_id, presence: true
  validates :key, :title_id, uniqueness: true

  # @return [Title] 対応するタイトル
  belongs_to :title
  # @return [Person] 対応する親
  belongs_to :parent, class_name: :Person, optional: true

  # @return [Array<Album>] 対応するアルバム一覧
  has_many :albums, class_name: :Album, foreign_key: :singer_id, dependent: :destroy
  # @return [Array<BandMember>] 対応するバンドメンバー一覧
  has_many :band_members, dependent: :destroy
  # @return [Array<Person>] 対応する子供
  has_many :children, class_name: :Person, foreign_key: :parent_id
  # @return [Array<Lyric>] 対応する作詞者一覧
  has_many :lyrics, dependent: :destroy
  # @return [Array<Music>] 対応する作詞者一覧
  has_many :musics, dependent: :destroy
  # @return [Array<Book>] 対応する撮影した書籍
  has_many :photographed_books, dependent: :destroy, class_name: :Book, foreign_key: :photographer_id
  # @return [Array<Single>] 対応するシングル
  has_many :singles, class_name: :Single, foreign_key: :singer_id, dependent: :destroy
  # @return [Array<SongPerson>] 対応する曲関係者一覧
  has_many :song_people, dependent: :destroy
  # @return [Array<Video>] 対応するビデオ
  has_many :videos, class_name: :Video, foreign_key: :singer_id, dependent: :destroy
  # @return [Array<Book>] 対応する撮影した書籍
  has_many :written_books, dependent: :destroy, class_name: :Book, foreign_key: :author_id

  scope :head_value_is, -> v { joins(:title).merge(Title.head_value_is(v)) }
  scope :ids_are, -> a { where arel_table[:id].in a }
  scope :include_title, -> { includes :title }
  scope :only_main, -> { where self.arel_table[:parent_id].eq(nil) }
  scope :order_yomi, -> { joins(:title).merge(Title.order_yomi) }
  scope :order_yomi_desc, -> { joins(:title).merge(Title.order_yomi_desc) }

end
