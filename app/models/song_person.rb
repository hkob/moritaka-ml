# @!attribute list_content_id
#   @return [Fixnum] ソングリスト曲ID
# @!attribute person_id
#   @return [Fixnum] 関係者ID
# @!attribute instrumental_id
#   @return [Fixnum] 楽器ID

class SongPerson < ApplicationRecord
  validates :list_content_id, :person_id, :instrumental_id, presence: true

  # @return [ListContent] 対応するソングリスト曲
  belongs_to :list_content
  # @return [Person] 対応する関係者
  belongs_to :person
  # @return [ListContent] 対応する楽器
  belongs_to :instrumental

  scope :device_type_is, -> t { joins(list_content: :list).merge(List.device_type_value_is(t)) }
  scope :except_arrange, -> { joins(:instrumental).merge(Instrumental.except_arrange) }
  scope :include_album, -> { include_list.merge(List.include_album) }
  scope :include_device, -> { include_list.merge(List.include_device).merge(Device.include_title) }
  scope :include_device_year, -> { include_device.merge(Device.include_year) }
  scope :include_instrumental, -> { includes(:instrumental).merge(Instrumental.include_title) }
  scope :include_person, -> { includes(:person).merge(Person.include_title) }
  scope :include_single, -> { include_list.merge(List.include_single) }
  scope :include_list, -> { include_list_content.merge(ListContent.include_list) }
  scope :include_list_content, -> { includes(:list_content) }
  scope :only_album, -> { device_type_is :Album }
  scope :only_arrange, -> { joins(:instrumental).merge(Instrumental.is_arrange) }
  scope :only_single, -> { device_type_is :Single }
  scope :only_video, -> { device_type_is :Video }
  scope :order_device_date_id, -> { joins(list_content: { list: :device }).merge(Device.order_date.order_id) }
  scope :order_person_yomi, -> { joins(:person).merge(Person.order_yomi) }
  scope :order_instrumental_sort_order, -> { joins(:instrumental).merge(Instrumental.order_sort_order) }
  scope :people_are, -> ps { where arel_table[:person_id].in ps.map(&:id) }
  scope :person_head_value_is, -> v { joins(:person).merge(Person.head_value_is(v)) }

#  # @param [String] arg1 説明
#  # @return [SongPerson] XXX な SongPerson
#  def self.rrr(arg1)
#  end

#  # @param [String] arg1 説明
#  # @return [SongPerson] 説明
#  def ttt(arg1)
#  end
end
