# @!attribute key
#   @return [String] 検索キー
# @!attribute type
#   @return [String] タイプ
# @!attribute device_type
#   @return [Fixnum] デバイスタイプ
# @!attribute title_id
#   @return [Fixnum] タイトルID
# @!attribute date
#   @return [Date] 日付
# @!attribute minutes
#   @return [Fixnum] 分
# @!attribute seconds
#   @return [Fixnum] 秒
# @!attribute sort_order
#   @return [Fixnum] 並び順
# @!attribute number
#   @return [String] 番号
# @!attribute singer_id
#   @return [Fixnum] シンガーID
# @!attribute j_comment
#   @return [String] 日本語コメント
# @!attribute e_comment
#   @return [String] 英語コメント

class Video < Device
  include Name

  VideoStr2Num = { live_video: 1, cdv: 2, video_clip: 4, video_clips: 8, live_document_and_clip: 16 }
  VideoNum2Str = VideoStr2Num.invert

  def self.num2str
    VideoNum2Str
  end

  def self.str2num
    VideoStr2Num
  end
end
