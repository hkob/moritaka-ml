class DeviceActivity < ApplicationRecord
  validates :device_id, :activity_id, presence: true
  validates :device_id, uniqueness: {scope: :activity_id}

  # @return [Activity] 対応する企業
  belongs_to :activity
  # @return [Device] 対応する企業
  belongs_to :device
end
