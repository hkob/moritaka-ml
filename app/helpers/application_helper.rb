module ApplicationHelper
  # Common hash
  MbStrHash = {true => '○', false => '×'}
  MbOptions = MbStrHash.map { |k, v| [ v, k ] }

  # @params [Array<Array<String, Boolean>>] options 表示する文字列とboolean(省略可)
  # @return [Hash] simple_form 用の hash
  def mb_options_hash(options = MbOptions)
    {
      collection: MbOptions,
      as: :radio_buttons,
      wrapper: :horizontal_radio_and_checkboxes,
      item_wrapper_class: 'radio-inline'
    }
  end

  # @param [Object] model モデルクラス
  # @param [Array<String>] atr 属性名
  def t_ar(model, atr = nil)
    if atr
      return model.human_attribute_name(atr)
    else
      return model.model_name.human
    end
  end

  # @param [Object] model モデルクラス
  # @param [Array<String>] keys 属性名の配列
  # @return [Array<String>] 翻訳後の文字列の配列
  def t_ars(model, keys)
    keys.map { |key| model.human_attribute_name(key) }
  end

  def t_ars_join(model, keys, del = ', ')
    t_ars(model, keys).join(del)
  end

  # @param [Boolean] flag class を設定するかどうか
  # @param [String] class に設定する文字列
  # @return [Hash] flag が true なら { class: str }、false なら {}
  def class_str_or_nil(flag, str = 'info')
    flag ? {class: str} : {}
  end

  # @param [Boolean] flag true or false
  # @return [String] true なら○、false なら×
  def mb(flag)
    flag ? '○' : '×'
  end

  # @param [Boolean] flag true or false
  # @return [String] true なら○、false ならnil
  def mn(flag)
    flag ? '○' : nil
  end

  # @param [Hash] option 変更するオプション
  # @return [String] 再帰パス
  def recursive_path(option = {})
    request.params.merge(option)
  end

  # @param [String] path path名
  # @return [Hash<String>] Routing parameter
  def url_to_params(path, method = :get)
    Rails.application.routes.recognize_path(path, method: method)
  end

  # @param [String] path path 名
  # @param [String,nil] method メソッド名
  # @return [Array<String>] コントローラ名とアクション名の配列
  def controller_and_action_names_from_path(path, method = :get)
    url_to_params(path, method).values_at(*%i[controller action])
  end

  # @param [String] cn コントローラ名
  # @param [String] an アクション名
  # @return [String] タイトル
  def title_from_controller_and_action(cn, an)
    t([ cn.gsub('/', '.'), an, 'title' ].join('.'))
  end

  # @param [String] cn コントローラ名
  # @param [String] an アクション名
  # @return [String] リンクタイトル
  def link_title_from_controller_and_action(cn, an)
    t([ cn.gsub('/', '.'), an, 'link_title' ].join('.'))
  end

  # @param [String] path path 名
  # @param [String,nil] method メソッド名
  # @return [String] タイトル
  def title_from_path(path, method = nil)
    cn, an = controller_and_action_names_from_path(path, method)
    title_from_controller_and_action(cn, an)
  end

  # @param [String] path path 名
  # @param [String,nil] method メソッド名
  # @return [String] タイトル
  def link_title_from_path(path, method = nil)
    cn, an = controller_and_action_names_from_path(path, method)
    link_title_from_controller_and_action(cn, an)
  end

  # @param [String] path path 名
  # @param [String,nil] method メソッド名
  # @return [Array<String>] タイトルと path の配列
  def title_with_path(path, method = nil)
    [ title_from_path(path, method), path ]
  end

  # @param [String] path path 名
  # @param [String,nil] method メソッド名
  # @return [Array<String>] タイトルと path の配列
  def link_title_with_path(path, method = nil)
    [ link_title_from_path(path, method), path ]
  end

  # @param [String] path path 名
  # @param [String,nil] method メソッド名
  # @return [Array<String>] タイトルと path の配列
  def check_title_with_path(path, method = nil)
    [ t('common.check'), path ]
  end

  # @param [Array<String>] array 文字列と URL の配列
  # @note ナビゲーションリンクを表示
  def navi(array)
    render partial: 'shared/navi_link_bar', locals:{navi: array}
  end

  # @param [Array<String>] メニュータイトルの翻訳キーの配列
  # @return [String] メニューのタイトルコンテンツを得る
  MenuTitles = I18n.t(%w[common.link common.detail common.privileges])
  def menu_title(*array)
    array = MenuTitles if array.length == 0
    content_tag :tr do
      array.flatten.each do |str|
        concat content_tag(:th, str)
      end
    end
  end

  # @param [Array<String>] メニュータイトルの翻訳キーの配列
  # @return [String] メニューのタイトルコンテンツを得る
  def menu_title_wc(array)
    menu_title t('common.control'), array
  end

  # @param [Boolean] flag リンクを描画するかどうか
  # @param [String] path URL
  # @param [String] add_info 追加情報
  # @return [String] link 文字列
  def link_or_nil(flag, path, add_info = {})
    link_to *link_title_with_path(path), add_info if flag
  end

  # @param [Boolean] flag リンクを描画するかどうか
  # @param [String] path URL
  # @param [String] add_info 追加情報
  # @return [String] link 文字列
  def link_or_nil_with_title(flag, path, title, add_info = {})
    link_to title, path, add_info if flag
  end

  def spacer
    content_tag(:span) { '・' }
  end

  def normal_link(object: nil, controller_path: nil, title: nil, division: nil, add_title: nil, add_version: nil)
    controller_path ||= object.controller_path(@is_ja, division)
    title ||= object.name(@is_ja) if object && object.class != Class
    title ||= link_title_from_path(controller_path)
    title += "[#{add_title}]" if add_title
    title += " #{add_version}" if add_version
    link_to title, controller_path
  end

  def normal_link_by_title(object: nil, controller_path: nil, title: nil, division: nil, add_title: nil, add_version: nil)
    controller_path ||= object.controller_path(@is_ja, division)
    title ||= title_from_path(controller_path)
    title += "[#{add_title}]" if add_title
    title += "#{add_version}" if add_version
    link_to title, controller_path
  end

  def normal_link_objects(objs, division: nil)
    render inline: <<-HAML, type: :haml, locals: { objs: objs, division: division }
- objs.each_with_index do |p, i|
  = ', ' unless i == 0
  %span<>
    = normal_link object: p, division: division
HAML
  end

  def normal_link_by_title_objects(obj, division: nil)
    render inline: <<-HAML, type: :haml, locals: { obj: obj, division: division }
- obj.each_with_index do |p, i|
  = ', ' unless i == 0
  %span<>
    = normal_link_by_title object: p, division: division
HAML
  end

  def button_link(object: nil, controller_path: nil, title: nil, flag: false, division: nil, add_title: nil, add_version: nil)
    controller_path ||= object.controller_path(@is_ja, division)
    title ||= object.name(@is_ja) if object && object.class != Class
    title ||= link_title_from_path(controller_path)
    title += "[#{add_title}]" if add_title
    title += "#{add_version}" if add_version
    link_to title, controller_path, button_class(flag)
  end

  def button_link_by_title(object: nil, controller_path: nil, title: nil, flag: false, division: nil, add_title: nil, add_version: nil)
    controller_path ||= object.controller_path(@is_ja, division)
    title ||= title_from_path(controller_path)
    title += "[#{add_title}]" if add_title
    title += "#{add_version}" if add_version
    link_to title, controller_path, button_class(flag)
  end

  def year_link(event_date)
    normal_link controller_path: year_path(event_date.year, division: event_date.month), title: event_date.name
  end

  def data_toggle_tab
    {'data-toggle' => :tab}
  end

  def je_link
    link_to t('common.lang'), recursive_path(locale: @is_ja ? :en : :ja)
  end

  def url_link
    link_to t('common.url'), recursive_path
  end

  def external_or_str(str)
    if /^http/ =~ str
      host = str.split('/')[2]
      key = host == 'www.moritaka-chisato.com' ? :official : :external
      link_to "#{t("common.#{key}_link")}(#{host})", str, button_class(true).merge(target: '_blank')
    else
      str
    end
  end

  def button_class(flag)
    {class: "button#{flag ? '' : ' button-outline'}"}
  end
end
