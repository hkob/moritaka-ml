namespace :db do
  desc 'Load the seed data after all data clear'
  task :seed_after_clear => 'db:abort_if_pending_migrations' do
    [Activity, ActivitySub,Album, BandMember, Band, Book, Company, ConcertHall, ConcertVideo, Concert, DeviceActivity, EventDate, Hall, Instrumental, List, ListContent, Lyric, Medium, Music, Person, Prefecture, Reference, Song, SongPerson, Title, Year, Device].each do |c|
      cc = c.count
      print "#{c}: #{cc}... "
      if c.count > 0
        c.all.map(&:destroy)
      else
        print "skip "
      end
      print "#{ActiveRecord::Base.connection.reset_pk_sequence!(c.to_s.underscore.pluralize)}\n"
    end
    seed_file = File.join(Rails.root, 'db/seeds.rb')
    load(seed_file) if File.exist?(seed_file)
  end
end
