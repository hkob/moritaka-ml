<%= class_name %>FactoryHash = {
  one: %w[],
  two: %w[],
  three: %w[]
}

# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [<%= class_name %>] <%= class_name %> FactoryGirl オブジェクト
def <%= singular_table_name %>_factory(key)
  n, c = <%= class_name %>FactoryHash[key.to_sym]
  FactoryBot.find_or_create(
    :<%= singular_table_name %>,
    name: n,
    code: c,
    sort_order: <%= class_name %>.sort_orders[key.to_sym]
  ) if n
end

# @param [Array<Symbol, String>] keys オブジェクトを一意に決定するキーの配列
# @return [Array<<%= class_name %>>] <%= class_name %> FactoryGirl オブジェクトの配列
def <%= singular_table_name %>_factories(keys)
  keys.map { |k| <%= singular_table_name %>_factory(k) }
end

##### ↑ write to spec/support/create_factories/<%= singular_table_name %>_factory.rb
require 'rails_helper'

RSpec.describe <%= class_name %>, type: :model do

  context 'common validation check' do
    subject { <%= singular_table_name %>_factory :<%= singular_table_name %> }

    #it_behaves_like :presence_validates, %i[]
    #it_behaves_like :unique_validates, %i[], -> { <%= singular_table_name %>_factory :other }
    #it_behaves_like :plural_unique_validates, %i[], -> { <%= singular_table_name %>_factory :other }
    #it_behaves_like :destroy_validates
    #it_behaves_like :reject_destroy_validates
    #it_behaves_like :belongs_to, :<%= singular_table_name %>, has_many: %i[], has_one: %i[], children: :optional, child: :optional
    #it_behaves_like :dependent_destroy, :<%= singular_table_name %>, %i[has_many has_one]
    #it_behaves_like :reject_destroy_for_relations, :<%= singular_table_name %>, %i[has_many has_one]
    #it_behaves_like :destroy_nullify_for_relations, :<%= singular_table_name %>, %i[has_many has_one]
  end

  context 'after some <%= plural_table_name %> are registrered' do
    model_keys = %i[]
    let!(:targets) { <%= singular_table_name %>_factories model_keys }

    describe '<%= class_name %> class' do
      subject { <%= class_name %> }

      #it_behaves_like :mst_block, -> t do
      #  {
      #    method1: [v1, a1, v2, a2, ...],
      #    method2: [v1, a1, v2, a2, ...],
      #  }
      #end
      #
      #it_behaves_like :msta_block, -> t do
      #  {
      #    method1: [v1, a1, v2, a2, ...],
      #    method2: [v1, a1, v2, a2, ...],
      #  }
      #end
      #it_behaves_like :mst, :METHOD1, -> { [v1, <%= singular_table_name %>_factories(model_keys.values_at()), v2, <%= singular_table_name %>_factories(model_keys.values_at())] }
      #it_behaves_like :msta, :METHOD1, -> { [v1, <%= singular_table_name %>_factories(model_keys.values_at()), v2, <%= singular_table_name %>_factories(model_keys.values_at())] }
    end

    context '<%= class_name %> instances' do
      subject { targets }

      #it_behaves_like :amst_block, -> t do
      #  {
      #    method1: [v1, a1, v2, a2, ...],
      #    method2: [v1, a1, v2, a2, ...],
      #  }
      #end
      #
      #it_behaves_like :amsta_block, -> t do
      #  {
      #    method1: [v1, a1, v2, a2, ...],
      #    method2: [v1, a1, v2, a2, ...],
      #  }
      #end
      #it_behaves_like :amst, :METHOD1, -> { [v1, a1, v2, a2] }
      #it_behaves_like :amsta, :METHOD1, -> { [v1, a1, v2, a2] }
    end
  end
end
