require 'rails_helper'

RSpec.describe :<%= class_name.pluralize %>, type: :request do
  include Devise::Test::IntegrationHelpers

  let(:return_path) { <%= plural_table_name %>_path(edit_mode: true) }
  context 'login by hkob' do
    let!(:object) { <%= singular_table_name %>_factory :factory_one }
    let!(:others) { <%= singular_table_name %>_factory :factory_other }
    let(:not_mine) { <%= singular_table_name %>_factory :factory_not_mine }
    teacher_login :hkob

    describe 'GET #index' do
      context 'normal mode' do
        subject { -> { get <%= plural_table_name %>_path } }
        it_behaves_like :response_status_check, 200
        it_behaves_like :response_body_includes, %w[XXX一覧: 表示される文字列]
        it_behaves_like :response_body_not_includes, '表示されない文字列'
      end

      context 'edit mode' do
        subject { -> { get <%= plural_table_name %>_path(edit_mode: true) } }
        it_behaves_like :response_status_check, 200
        it_behaves_like :response_body_includes, %w[XXX一覧: 表示される文字列]
      end
    end

    describe 'GET #new' do
      subject { -> { get new_<%= singular_table_name %>_path } }
      it_behaves_like :response_status_check, 200
    end

    describe 'POST #create' do
      before { @attrs = object.attributes; object.destroy }
      subject { -> { post <%= plural_table_name %>_path, params: {<%= singular_table_name %>: @attrs}} }

      context '正しいパラメータに対して' do
        it_behaves_like :response_status_check, 302
        it_behaves_like :increment_object_by_create, <%= class_name %>
        it_behaves_like :redirect_to
        it_behaves_like :flash_notice_message, 'XXXを登録しました．'
      end

      context '不正なパラメータに対して' do
        before { @attrs['name'] = nil }
        it_behaves_like :response_status_check, 200
        it_behaves_like :not_increment_object_by_create, <%= class_name %>
        it_behaves_like :flash_alert_message, 'XXXが登録できません．'
      end
    end

    describe 'GET #edit' do
      context 'owned object' do
        subject { -> { get edit_<%= singular_table_name %>_path(object) } }
        it_behaves_like :response_status_check, 200
        it_behaves_like :response_body_includes, '2300'
      end

      context 'not owned object' do
        subject { -> { get edit_<%= singular_table_name %>_path(not_mine) } }
        it_behaves_like :response_status_check, 302
        it_behaves_like :redirect_to
        it_behaves_like :flash_alert_message, 'XXXを編集する権限がありません．'
      end
    end

    describe 'PATCH #update' do
      before { @attrs = object.attributes }

      context 'owned object' do
        subject { -> { patch <%= singular_table_name %>_path(object), params: {<%= singular_table_name %>: @attrs} } }

        context '正しいパラメータに対して' do
          before { @attrs['name'] = 'new name' }
          it_behaves_like :response_status_check, 302
          it_behaves_like :change_object_value_by_update, <%= class_name %>, :name, 'new name'
          it_behaves_like :redirect_to
          it_behaves_like :flash_notice_message, 'XXXを更新しました．'
        end

        context '不正なパラメータに対して' do
          before { @attrs['name'] = nil }
          it_behaves_like :response_status_check, 200
          it_behaves_like :not_change_object_value_by_update, <%= class_name %>, :name
          it_behaves_like :flash_alert_message, 'XXXが更新できません．'
        end
      end

      context 'not owned object' do
        subject { -> { put <%= singular_table_name %>_path(not_mine), params: {<%= singular_table_name %>: @attrs} } }
        it_behaves_like :response_status_check, 302
        it_behaves_like :redirect_to
        it_behaves_like :flash_alert_message, 'XXXが更新できません．'
      end
    end

    describe 'DELETE #destroy' do
      context 'owned object' do
        subject { -> { delete <%= singular_table_name %>_path(object), params: {<%= singular_table_name %>: @attrs} } }
        it_behaves_like :response_status_check, 302
        it_behaves_like :decrement_object_by_destroy, <%= class_name %>
        it_behaves_like :redirect_to
        it_behaves_like :flash_notice_message, 'XXXを削除しました．'
      end

      context 'now owned object' do
        subject { -> { delete <%= singular_table_name %>_path(not_mine), params: {<%= singular_table_name %>: @attrs} } }
        it_behaves_like :response_status_check, 302
        it_behaves_like :redirect_to
        it_behaves_like :flash_alert_message, 'XXXを削除する権限がありません．'
      end
    end

    describe 'GET #show' do
      context 'owned object' do
        subject { -> { get <%= singular_table_name %>_path(object) } }
        it_behaves_like :response_status_check, 200
        it_behaves_like :response_body_includes, %w[XXX表示: YYY]
      end

      context 'now owned object' do
        subject { -> { get <%= singular_table_name %>_path(not_mine) } }
        it_behaves_like :response_status_check, 302
        it_behaves_like :redirect_to
        it_behaves_like :flash_alert_message, 'XXXを表示する権限がありません．'
      end
    end
  end

  context 'not login' do
    describe 'GET #index' do
      subject { -> { get <%= plural_table_name %>_path } }
      it_behaves_like :response_status_check, 302
      it_behaves_like :response_body_not_includes, %w[表示されない文字列]
    end
  end
end
