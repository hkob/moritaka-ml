function drawGraph(labels, values, title) {
  var ctx = document.getElementById('graph').getContext('2d');
  var data = {
    labels: labels,
    datasets: [{
        label: title,
        data: values,
        backgroundColor: "rgba(250,40,30,0.4)",
        borderColor: "rgba(250,40,30,0.9)"
    }]
  };
  var options = {
    scales: {
      xAxes: [{
        ticks: {
          min: 0
        },
        barThickness: 20,
      }],
      elements: {
        responsive: true,
        maintainAspectRatio: false,
      }
    }
  }
  var myChart = new Chart(ctx, {
    type: 'horizontalBar',
    data: data,
    options: options
  });
}

window.drawGraph = drawGraph
