# vim:set fileencoding=utf-8 filetype=ruby:
###		 albums.db (アルバムデータベース)			###
#							発売日付順
#☆ タグの説明
# FNAME: html ファイル名 = (a*.html)
# HTITLE: html header title = (English Name: [Songs] English title)
# TYPE: アルバムの種類 = (Album|MiniAlbum|KaraokeCD|LyricAlbum|
#				PartAlbum|CompilationAlbum)
#	Album ... アルバム
#	MiniAlbum ... ミニアルバム
#	KaraokeCD ... カラオケアルバム
#	LyricAlbum ... 詞を提供したアルバム
#	CoverAlbum ... カバーアルバム
#	PartAlbum ... 録音に参加したアルバム
#	CompilationAlbum ... コンピレーションアルバム
# JTITLE: アルバムの日本語タイトル
# ETITLE: アルバムの英語タイトル
#	英語のみのタイトルの場合は，JTITLE にそれを書き，ETITLE は書かない．
# SINGER: 歌手の html ファイル名 = (i*.html)
#	`森高千里' はそのまま書く．
# NO: 歌手別の通算番号
# DATE: 初版の発売日付 = (19\d\d/\d+/\d+)
# MINUTES: MM = (\d+)
# SECONDS: SS = (\d+)
#	録音時間 = (MM:SS)
# CODE: AA BBBB CCCCC D
#	AA = (LP|KT|CD)
#	BBBB = CD 製造番号
#	CCCCC = (first|second|third)
#	D = (|x|X) = (通常販売|生産中止|廃盤)
# COMPANY: 発売元
# PRICE: 価格 ( , 付)
# JCOMMENT, ECOMMENT: コメント
# SDIFF: 複数の曲リストがあるかどうか
#	COMMON ... 共通
#	CD, KT, LP 等... 曲リストが異なる場合に連記
# MODIFY: 上記データの最終更新日
#
#☆ 更新条件
# 1. MODIFY が basedate より新しければ，
#	a. update(FNAME, MODIFY);
#	b. update(hDATEYEAR.html, MODIFY);
#	c. pupdate(MODIFY, %data);
#	d. dupdate('albums.db', FNAME, MODIFY);
#	e. aDATE が MODIFY より新しければ aDATE = MODIFY
# 2. 最後に aDATE が baseDate より新しければ
#	c. update(lAlbums.html, aDate);

# r g model album keyword:string album_type:integer j_title:string e_title:string date:date minutes:integer seconds:integer
# r g model song_list keyword:string device_id:integer sort_order:integer

str = <<"EOF"

keyword: ySweetCandy.html
type: SelfCover
number: 1st
date: 2012/7/23
minutes: 4
seconds: 53
code: Youtube http://www.youtube.com/watch?v=2qiPIg707_E first
songlist: COMMON

keyword: yModorenaiNatsu.html
type: SelfCover
number: 2rd
date: 2012/7/28
minutes: 4
seconds: 2
code: Youtube http://www.youtube.com/watch?v=TffLhxKSoxQ first
songlist: COMMON

keyword: yHare.html
type: SelfCover
number: 3rd
date: 2012/7/30
minutes: 2
seconds: 28
code: Youtube http://www.youtube.com/watch?v=vFxCJCWaqUk first
songlist: COMMON

keyword: yCalypsonoMusume.html
type: SelfCover
number: 4th
date: 2012/8/3
minutes: 3
seconds: 48
code: Youtube http://www.youtube.com/watch?v=tuEPhhfV4FA first
songlist: COMMON

keyword: yHeyDog.html
type: SelfCover
number: 5th
date: 2012/8/4
minutes: 3
seconds: 32
code: Youtube http://www.youtube.com/watch?v=tatJfNx4G1s first
songlist: COMMON

keyword: yUmimadeGofun.html
type: SelfCover
number: 6th
date: 2012/8/5
minutes: 4
seconds: 12
code: Youtube http://www.youtube.com/watch?v=nrYXDSHH_5g first
songlist: COMMON

keyword: yPI-A-NO.html
type: SelfCover
number: 7th
date: 2012/8/6
minutes: 2
seconds: 56
code: Youtube http://www.youtube.com/watch?v=KoHT0wQttkE first
songlist: COMMON

keyword: yTanabatanoYoruKiminiAitai.html
type: SelfCover
number: 8th
date: 2012/8/7
minutes: 4
seconds: 38
code: Youtube http://www.youtube.com/watch?v=V8v7dygRqB4 first
songlist: COMMON

keyword: yHachigatsunoKoi.html
type: SelfCover
number: 9th
date: 2012/8/8
minutes: 3
seconds: 38
code: Youtube http://www.youtube.com/watch?v=VueZ2Sm0go0 first
songlist: COMMON

keyword: yMijikaiNatsu.html
type: SelfCover
number: 10th
date: 2012/8/9
minutes: 5
seconds: 24
code: Youtube http://www.youtube.com/watch?v=Z-y4ALlK8Po first
songlist: COMMON

keyword: yNatsunoUmi.html
type: SelfCover
number: 11th
date: 2012/8/10
minutes: 4
seconds: 14
code: Youtube http://www.youtube.com/watch?v=0vmwbSRxAVE first
songlist: COMMON

keyword: yNatsunohi.html
type: SelfCover
number: 12th
date: 2012/8/10
minutes: 3
seconds: 49
code: Youtube http://www.youtube.com/watch?v=LH_dm94bBkU first
songlist: COMMON

keyword: yHaretaNichiyobi.html
type: SelfCover
number: 13th
date: 2012/8/12
minutes: 4
seconds: 25
code: Youtube http://www.youtube.com/watch?v=aKt-QQZj3E8 first
songlist: COMMON

keyword: yShiritagari.html
type: SelfCover
number: 14th
date: 2012/8/13
minutes: 1
seconds: 21
code: Youtube http://www.youtube.com/watch?v=PJU7XICcNR4 first
songlist: COMMON

keyword: yNatsuwaPararaylon.html
type: SelfCover
number: 15th
date: 2012/8/13
minutes: 3
seconds: 36
code: Youtube http://www.youtube.com/watch?v=fJi1rTR5Osk first
songlist: COMMON

keyword: yTokonatsunoParadise.html
type: SelfCover
number: 16th
date: 2012/8/14
minutes: 3
seconds: 41
code: Youtube http://www.youtube.com/watch?v=vQnTxFbskKg first
songlist: COMMON

keyword: yAnohinoPhotograph.html
type: SelfCover
number: 17th
date: 2012/8/15
minutes: 3
seconds: 6
code: Youtube http://www.youtube.com/watch?v=Ily2na-VcbU first
songlist: COMMON

keyword: yBossaMarina.html
type: SelfCover
number: 18th
date: 2012/8/16
minutes: 4
seconds: 23
code: Youtube http://www.youtube.com/watch?v=tm6U8MEy_Zs
songlist: COMMON

keyword: yGoodByeSeason.html
type: SelfCover
number: 19th
date: 2012/8/17
minutes: 4
seconds: 13
code: Youtube http://www.youtube.com/watch?v=KBkrnsJKE6w
songlist: COMMON

keyword: yShizukanaNatsu.html
type: SelfCover
number: 20th
date: 2012/8/18
minutes: 3
seconds: 30
code: Youtube http://www.youtube.com/watch?v=Iyx-vNLc2XA
songlist: COMMON

keyword: yLaLaSunshine.html
type: SelfCover
number: 21th
date: 2012/8/20
minutes: 3
seconds: 41
code: Youtube http://www.youtube.com/watch?v=j4Elh62FM5k
songlist: COMMON

keyword: ySeishun.html
type: SelfCover
number: 22th
date: 2012/8/22
minutes: 4
seconds: 22
code: Youtube http://www.youtube.com/watch?v=islaubxjqWQ
songlist: COMMON

keyword: yKaigan.html
type: SelfCover
number: 23th
date: 2012/8/24
minutes: 4
seconds: 9
code: Youtube http://www.youtube.com/watch?v=Ylovdzm1RHM
songlist: COMMON

-
EOF

include MiscMethod

print "##### seed youtubes #####\n"
hash = {}
sort_order = 0;
str.split("\n").each do |line|
  if line == ""
    if k = hash[:keyword]
      sort_order += 1
      youtube = Youtube.keyword_is(k).first
      youtube = Youtube.new unless youtube
      youtube.keyword = k;
      youtube.device_type = 0
      hash[:type].split(/ /).each do |k|
        youtube.device_type |= Youtube::YoutubeStr2Num[k.intern]
      end
      youtube.j_title = hash[:j_title]
      youtube.e_title = hash[:e_title]
      youtube.number = hash[:number]
      youtube.date = hash[:date] && Date.parse(hash[:date])
      youtube.minutes = hash[:minutes]
      youtube.seconds = hash[:seconds]
      year_num = hash[:year] || (youtube.date && youtube.date.year)
      if year_num
        year = Year.year_from_year_or_create(year_num)
        youtube.year = year
      end
      youtube.sort_order = sort_order
      youtube.media << Medium.medium_from_data(hash, :code, :company, :price, 0) if hash[:code]
      youtube.j_comment = hash[:j_comment]
      youtube.e_comment = hash[:e_comment]
      sl_hash = array_to_hash(youtube.song_lists) { |sl| [ sl.keyword, sl ] }
      if youtube.save
        print "update youtube: #{youtube.jtitle}\n" if Time.now - youtube.updated_at < 10
      else
        p youtube
        print "save error!\n"
      end
      if hash[:songlist]
        hash[:songlist].split(' ').each do |sl|
          unless sl_hash[sl]
            song_list = youtube.song_lists.create(keyword:sl, sort_order:sl_hash.keys.count + 1)
            sl_hash[sl] = song_list
            print "create song_list: #{sl}\n"
          end
        end
      end
      hash = {}
    end
  else
    k, v = line.split(": ")
    ks = k.intern
    case ks
    when :keyword, :type, :j_title, :e_title, :number, :date, :year, :singer, :minutes, :seconds,
      :code, :code2, :code3, :code4, :code5,
      :company, :company2, :company3, :company4, :company5,
      :price, :price2, :price3, :price4, :price5,
      :j_comment, :e_comment, :songlist
      hash[ks] = v
    end
  end
end
