# vim:set fileencoding=utf-8 filetype=ruby:
###		 songsAlbum.db (アルバム収録曲データベース)		###
#							発売日付順
#☆ タグの説明
# PFNAME: アルバム html ファイル名 = (a*.html)
# SFNAME: 収録曲 html ファイル名 = (w*.html)
# SNUM: 曲順 = (AAAA BB)
#	AAAA = (COMMON|LP|CD|CT)
#	BB = (\d+) 曲順
# VOCAL: 歌手の html ファイル名 = (i*.html)
#	`iChisatoMoritaka.html' はそのまま書く．
# ARRANGE: アレンジ者の html ファイル名 = (i*.html)
# 楽器名: 演奏者名の html ファイル名
# MODIFY: 上記データの最終更新日
#
#☆ 更新条件
# 1. MODIFY が baseDate より新しければ，
#	a. update(PFNAME, MODIFY);
#	b. update(SFNAME, MODIFY);
#	c. update(人名(楽器名), MODIFY);
# 2. dAlbums.dbPFNAME が baseDate より新しければ
#	a. update(SFNAME, dAblums.dbPFNAME);
# 3. dsongs.dbSFNAME が baseDate より新しければ
#	a. update(PFNAME, dsongs.dbSFNAME);

# r g model song_list_song song_list_id:integer song_id:integer
# r g model song_individual song_list_song_id:integer individual_id:integer instrumental_id:integer

str = <<"EOF"

pfname: ySweetCandy.html
sfname: wSweetCandy.html
snum: COMMON 1

pfname: yModorenaiNatsu.html
sfname: wModorenaiNatsu.html
snum: COMMON 1

pfname: yHare.html
sfname: wHare.html
snum: COMMON 1

pfname: yCalypsonoMusume.html
sfname: wCalypsonoMusume.html
snum: COMMON 1

pfname: yHeyDog.html
sfname: wHeyDog.html
snum: COMMON 1

pfname: yUmimadeGofun.html
sfname: wUmimadeGofun.html
snum: COMMON 1

pfname: yPI-A-NO.html
sfname: wPI-A-NO.html
snum: COMMON 1

pfname: yTanabatanoYoruKiminiAitai.html
sfname: wTanabatanoYoruKiminiAitai.html
snum: COMMON 1

pfname: yHachigatsunoKoi.html
sfname: wHachigatsunoKoi.html
snum: COMMON 1

pfname: yMijikaiNatsu.html
sfname: wMijikaiNatsu.html
snum: COMMON 1

pfname: yNatsunoUmi.html
sfname: w0NatsunoUmi.html
snum: COMMON 1

pfname: yNatsunohi.html
sfname: w1Natsunohi.html
snum: COMMON 1

pfname: yHaretaNichiyobi.html
sfname: wHaretaNichiyobi.html
snum: COMMON 1

pfname: yShiritagari.html
sfname: wShiritagari.html
snum: COMMON 1

pfname: yNatsuwaPararaylon.html
sfname: wNatsuwaPararaylon.html
snum: COMMON 1

pfname: yTokonatsunoParadise.html
sfname: wTokonatsunoParadise.html
snum: COMMON 1

pfname: yAnohinoPhotograph.html
sfname: wAnohinoPhotograph.html
snum: COMMON 1

pfname: yBossaMarina.html
sfname: wBossaMarina.html
snum: COMMON 1

pfname: yGoodByeSeason.html
sfname: wGoodByeSeason.html
snum: COMMON 1

pfname: yShizukanaNatsu.html
sfname: wShizukanaNatsu.html
snum: COMMON 1

pfname: yLaLaSunshine.html
sfname: wLaLaSunshine.html
snum: COMMON 1

pfname: ySeishun.html
sfname: wSeishun.html
snum: COMMON 1

pfname: yKaigan.html
sfname: wKaigan.html
snum: COMMON 1

-
EOF

include MiscMethod

instrumental_hash = array_to_hash(Instrumental.all) { |i| [ i.keyword.intern, i ] }

print "##### seed songsYoutube #####\n"
hash = {}
sort_order = 0;
str.split("\n").each do |line|
  if line == ""
    if snum = hash[:snum]
      youtube = Youtube.keyword_is(hash[:pfname]).first
      if youtube
        slname, slnum = snum.split(' ')
        song_list = youtube.song_lists.keyword_is(slname).first
        if song_list
          song = Song.keyword_is(hash[:sfname]).first
          j_title = hash[:j_title]
          if song || j_title
            song_list_song = song_list.song_list_songs.sort_order_is(slnum).first || song_list.song_list_songs.build(sort_order:slnum)
            if song
              song_list_song.song = song
            else
              song_list_song.j_title = j_title
              song_list_song.e_title = hash[:e_title]
              song_list_song.a_title = hash[:a_title]
            end
            song_list_song.j_ver = hash[:j_ver] if hash[:j_ver]
            song_list_song.e_ver = hash[:e_ver] if hash[:e_ver]
            if song_list_song.save
              print "update song_list_song: #{song_list_song.jtitle}\n" if Time.now - song_list_song.updated_at < 10
            else
              p song_list_song
              print "save error!\n"
            end
            hash.keys.each do |ks|
              if instrumental_hash[ks]
                hash[ks].each do |v|
                  si = song_list_song.song_individuals.instrumental_keyword_is(ks).individual_keyword_is(v).first
                  unless si
                    individual = Individual.keyword_is(v).first
                    instrumental = Instrumental.keyword_is(ks).first
                    if individual && instrumental
                      si = song_list_song.song_individuals.build(individual_id:individual.id, instrumental_id:instrumental.id)
                      if si.save
                        print "update song_individual: #{instrumental.jtitle}-#{individual.jtitle}\n" if Time.now - si.updated_at < 10
                      else
                        print "save error!\n"
                      end
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
    hash = {}
  else
    k, v = line.split(": ")
    ks = k.intern
    case ks
    when :pfname, :sfname, :snum, :j_ver, :e_ver, :j_title, :e_title, :a_title
      hash[ks] = v
    end
    if instrumental_hash[ks]
      if hash[ks]
        hash[ks] << v
      else
        hash[ks] = [v]
      end
    end
  end
end

