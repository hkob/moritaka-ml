
str = <<'EOF'

iJunTakahashi: ['高橋順', 'Jun Takahashi', 'たかはしじゅん'],
ni0RyubenTsujimura: ['Ryuben Tsujimura', 'Ryuben Tsujimura', 'つじむらりゅうべん'],
iShinHashimoto: ['橋本慎', 'Shin Hashimoto', 'はしもとしん'],
i2YuichiTogashiki: ['Yuichi Togashiki', 'Yuichi Togashiki', 'とがしきゆういち'],

-
EOF

include MiscMethod

print '##### seed individuals #####\n'
hash = {}
sort_order = 0;
str.split('\n').each do |line|
  if line == ''
    if k = hash[:keyword]
      sort_order += 1
      individual3 = nil
      individual2 = nil
      individual = Individual.keyword_is(k).first || Individual.new(keyword:k)
      if hash[:j_title3]
        k3 = k + '_3'
        individual3 = Individual.keyword_is(k3).first || Individual.new(keyword:k3)
        individual3.j_title = hash[:j_title3]
        individual3.e_title = hash[:e_title3] if hash[:e_title3]
        individual3.is_main = false
        if individual3.save
          print 'update individual: #{individual3.jtitle}\n' if Time.now - individual3.updated_at < 10
        else
          p individual3
          print 'save error!\n'
        end
      end
      if hash[:j_title2]
        k2 = k + '_2'
        individual2 = Individual.keyword_is(k2).first || Individual.new(keyword:k2)
        individual2.j_title = hash[:j_title2]
        individual2.e_title = hash[:e_title2] if hash[:e_title2]
        individual2.another_individual = individual3 if individual3
        individual2.is_main = false
        if individual2.save
          print 'update individual: #{individual2.jtitle}\n' if Time.now - individual2.updated_at < 10
        else
          p individual2
          print 'save error!\n'
        end
      end
      individual.j_title = hash[:j_title]
      individual.e_title = hash[:e_title]
      individual.yomi = hash[:yomi]
      individual.another_individual = individual2 if individual2
      individual.is_main = true
      if individual.save
        print 'update individual: #{individual.jtitle}\n' if Time.now - individual.updated_at < 10
      else
        p individual
        print 'save error!\n'
      end
      hash = {}
    end
  else
    k, v = line.split(': ')
    ks = k.intern
    case ks
    when :keyword, :j_title, :e_title, :yomi, :j_title2, :e_title2, :j_title3, :e_title3
      hash[ks] = v
    end
  end
end

