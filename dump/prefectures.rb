
str = <<"EOF"
keyword: pHokkaido.html
j_title: 北海道
e_title: Hokkaidou
jcapital: 札幌
ecapital: Sapporo
region: Hokkaido
: 'ZEPP 札幌', 'ZEPP Sapporo', 'ぜっぷさっぽろ'],

keyword: pAomori.html
j_title: 青森
e_title: Aomori
jcapital: 青森
ecapital: Aomori
region: Touhoku

keyword: pAkita.html
j_title: 秋田
e_title: Akita
jcapital: 秋田
ecapital: Akita
region: Touhoku

keyword: pIwate.html
j_title: 岩手
e_title: Iwate
jcapital: 盛岡
ecapital: Morioka
region: Touhoku
: '盛岡市民文化ホール', 'Morioka Civic Cultural Hall', 'もりおかしみんぶんかほーる'],

keyword: pFukushima.html
j_title: 福島
e_title: Fukushima
jcapital: 福島
ecapital: Fukushima
region: Touhoku

keyword: pYamagata.html
e_title: Yamagata
jcapital: 山形
ecapital: Yamagata
region: Touhoku

keyword: pMiyagi.html
j_title: 宮城
e_title: Miyagi
jcapital: 仙台
ecapital: Sendai
region: Touhoku
: '仙台 BEEB ベースメントシアター', 'Sendai BEEB Basement theater', 'せんだいびーぶべーすめんとしあたー'],

keyword: pIbaraki.html
j_title: 茨城
e_title: Ibaraki
jcapital: 水戸
ecapital: Mito
region: Kantou
MODIFY: 1968/4/11

keyword: pTochigi.html
j_title: 栃木
e_title: Tochigi
jcapital: 宇都宮
ecapital: Utsunomiya
region: Kantou
: '宇都宮大学', 'Utsunomiya University', 'うつのみやだいがく'],
MODIFY: 1968/4/11

keyword: pSaitama.html
j_title: 埼玉
e_title: Saitama
jcapital: さいたま
ecapital: Urawa
region: Kantou
: '戸田市文化会館', 'Toda-shi BUNKA KAIKAN', 'とだしぶんかかいかん'],
: 'サンシティ越谷市民ホール', 'Sun Sity Koshigaya SHIMIN Hall', 'さんしてぃこしがやしみんほーる'],
MODIFY: 1968/4/11

keyword: pYamanashi.html
j_title: 山梨
e_title: Yamanashi
jcapital: 甲府
ecapital: こうふ
region: Chuubu

keyword: pGunma.html
j_title: 群馬
e_title: Gunma
jcapital: 前橋
ecapital: Maebashi
region: Kantou
: '高崎経済大学', 'Takasaki City University of Economics', 'たかさきけいざいだいがく'],
MODIFY: 1998/11/23

keyword: pKanagawa.html
j_title: 神奈川
e_title: Kanagawa
jcapital: 横浜
ecapital: Yokohama
region: Kantou
MODIFY: 1998/11/23

keyword: pChiba.html
j_title: 千葉
e_title: Chiba
jcapital: 千葉
ecapital: Chiba
region: Kantou
MODIFY: 1998/3/21

keyword: pTokyo.html
j_title: 東京
e_title: Tokyo
jcapital: 東京
ecapital: Tokyo
region: Kantou
: '早稲田大学', 'Waseda University', 'わせだだいがく'],
: '渋谷クラブ・クアトロ', 'Shibuya club Quattro', 'しぶやくらぶくあとろ'],
MODIFY: 1968/4/11

keyword: pNagano.html
j_title: 長野
e_title: Nagano
jcapital: 長野
ecapital: Nagano
region: Chuubu
MODIFY: 1968/4/11

keyword: pShizuoka.html
j_title: 静岡
e_title: Shizuoka
jcapital: 静岡
ecapital: Shizuoka
region: Chuubu
MODIFY: 1968/4/11

keyword: pNiigata.html
j_title: 新潟
e_title: Niigata
jcapital: 新潟
ecapital: Niigata
region: Chuubu
: '新潟フェイズ', 'Niigata Phase', 'にいがたふぇいず'],
: '新潟テルサ', 'Niigata TERUSA', 'にいがたてるさ'],
MODIFY: 1968/4/11

keyword: pToyama.html
j_title: 富山
e_title: Toyama
jcapital: 富山
ecapital: Toyama
region: Chuubu
MODIFY: 1968/4/11

keyword: pIshikawa.html
j_title: 石川
e_title: Ishikawa
jcapital: 金沢
ecapital: Kanazawa
region: Chuubu
: '金沢 AZ ホール', 'Kanazawa AZ Hall', 'かなざわあずほーる'],
MODIFY: 1998/11/23

keyword: pAichi.html
j_title: 愛知
e_title: Aichi
jcapital: 名古屋
ecapital: Nagoya
region: Chuubu
: '名古屋クラブ・クアトロ', 'Nagoya club Quattro', 'なごやくらぶくあとろ'],
MODIFY: 1968/4/11

keyword: pGifu.html
j_title: 岐阜
e_title: Gifu
jcapital: 岐阜
ecapital: Gifu
region: Chuubu
MODIFY: 1968/4/11

keyword: pFukui.html
j_title: 福井
e_title: Fukui
ecapital: Fukui
region: Kinki
: '福井フェニックスプラザ', 'Fukui Fenix Plaza', 'ふくいふぇにっくすぷらざ'],
MODIFY: 1968/4/11

keyword: pMie.html
j_title: 三重
e_title: Mie
jcapital: 津
ecapital: Tsu
region: Kinki
: '四日市大学', 'Yokkaichi University', 'よっかいちだいがく'],
MODIFY: 1968/4/11

keyword: pShiga.html
j_title: 滋賀
e_title: Shiga
jcapital: 大津
ecapital: Otsu
region: Kinki
MODIFY: 1968/4/11

keyword: pWakayama.html
j_title: 和歌山
e_title: Wakayama
jcapital: 和歌山
ecapital: Wakayama
region: Kinki
MODIFY: 1968/4/11

keyword: pKyoto.html
j_title: 京都
e_title: Kyoto
jcapital: 京都
ecapital: Kyoto
region: Kinki
MODIFY: 1968/4/11

keyword: pNara.html
j_title: 奈良
e_title: Nara
jcapital: 奈良
ecapital: Nara
region: Kinki
MODIFY: 1968/4/11

keyword: pOsaka.html
j_title: 大阪
e_title: Osaka
jcapital: 大阪
ecapital: Osaka
region: Kinki
: '心斎橋クラブ・クアトロ', 'Shinsai-bashi club Quattro', 'しんさいばしくらぶくあとろ'],
MODIFY: 1998/11/23

keyword: pHyogo.html
j_title: 兵庫
e_title: Hyogo
jcapital: 神戸
ecapital: Kobe
region: Kinki
: '神戸文化ホール', 'Kobe Bunka Hall', 'こうべぶんかほーる'],
: '神戸チキンジョージ', 'Kobe Chiken george', こうべちきんじょーじ'],
MODIFY: 1968/4/11

keyword: pOkayama.html
j_title: 岡山
e_title: Okayama
jcapital: 岡山
ecapital: Okayama
region: Chuugoku
: '岡山市立市民文化ホール', 'Okayama SHIRITSU SHIMIN BUNKA Hall', 'おかやましりつしみんぶんかほーる'
MODIFY: 1968/4/11

keyword: pHiroshima.html
j_title: 広島
e_title: Hiroshima
jcapital: 広島
ecapital: Hiroshima
region: Chuugoku
: '広島アステールプラザ', 'Hiroshima Astel Praza', 'ひろしまあすてーるぷらざ'],
MODIFY: 1998/11/23

keyword: pShimane.html
j_title: 島根
e_title: Shimane
jcapital: 松江
ecapital: Matsue
region: Chuugoku
MODIFY: 1968/4/11

keyword: pTottori.html
j_title: 鳥取
e_title: Tottori
jcapital: 鳥取
ecapital: Tottori
region: Chuugoku
MODIFY: 1998/11/23

keyword: pYamaguchi.html
j_title: 山口
e_title: Yamaguchi
jcapital: 山口
ecapital: Yamaguchi
region: Chuugoku
MODIFY: 1968/4/11

keyword: pKagawa.html
j_title: 香川
e_title: Kagawa
jcapital: 高松
ecapital: Takamatsu
region: Shikoku
MODIFY: 1968/4/11

keyword: pEhime.html
j_title: 愛媛
e_title: Ehime
jcapital: 松山
ecapital: Matsuyama
region: Shikoku
: '松山大学', 'Matsuyama University', 'まつやまだいがく'],
MODIFY: 1968/4/11

keyword: pTokushima.html
j_title: 徳島
e_title: Tokushima
jcapital: 徳島
ecapital: Tokushima
region: Shikoku
: '徳島大学', 'Tokushima University', 'とくしまだいがく'],
MODIFY: 1968/4/11

keyword: pKouchi.html
j_title: 高知
e_title: Kouchi
jcapital: 高知
ecapital: Kouchi
region: Shikoku
MODIFY: 1968/4/11

keyword: pFukuoka.html
j_title: 福岡
e_title: Fukuoka
jcapital: 福岡
ecapital: Fukuoka
region: Kyuushuu
MODIFY: 1998/11/23

keyword: pOita.html
j_title: 大分
e_title: Oita
jcapital: 大分
ecapital: Oita
region: Kyuushuu
MODIFY: 1998/11/23

keyword: pSaga.html
j_title: 佐賀
e_title: Saga
jcapital: 佐賀
ecapital: Saga
region: Kyuushuu
MODIFY: 1968/4/11

keyword: pNagasaki.html
j_title: 長崎
e_title: Nagasaki
jcapital: 長崎
ecapital: Nagasaki
region: Kyuushuu
: '長崎NBCホール', 'Nagasaki NBC Hall', 'ながさきえぬびいしいほーる'
MODIFY: 1968/4/11

keyword: pKumamoto.html
j_title: 熊本
e_title: Kumamoto
jcapital: 熊本
ecapital: Kumamoto
region: Kyuushuu
: '熊本県立劇場', 'Kumamoto Prefectural Theater', 'くまもとけんりつげきじょう'],
MODIFY: 1968/4/11

keyword: pMiyazaki.html
j_title: 宮崎
e_title: Miyazaki
jcapital: 宮崎
ecapital: Miyazaki
region: Kyuushuu
MODIFY: 1968/4/11

keyword: pKagoshima.html
j_title: 鹿児島
e_title: Kagoshima
jcapital: 鹿児島
ecapital: Kagoshima
region: Kyuushuu
MODIFY: 1998/11/23

keyword: pOkinawa.html
e_title: Okinawa
jcapital: 那覇
ecapital: Naha
region: Kyuushuu
MODIFY: 1968/4/11

: '# 不明
: 'HALL: #
: '# その他の言葉
: '#
: '(サブ)', '(Sub)
: '(王子)', '(Oji)
: '第一ホール', 'First Hall
: '第１ホール', 'First Hall
: '第二ホール', 'Second Hall
: '第２ホール', 'Second Hall
: '小ホール', 'Small hall
: '中ホール', 'Medium hall
: '大ホール', 'Big hall
: '(公録)', 'Public recording
: '湘南校舎', 'Shonan Schoolhouse
: '神戸大学医学部', 'Kobe university medical colleage
: '(WPB)', '(WPB)
: '学園祭', 'Festival tour
: 'クボタ', 'KUBOTA
: 'クボタ？', 'KUBOTA?
: ''90', ''90
: '演劇ホール', 'Dramatics Hall
アクトホール', 'Act Hall

-
EOF

print "##### seed prefectures #####\n"
hash = {}
sort_order = 0;
str.split("\n").each do ', 'line|
  if line == ""
    if k = hash[:keyword]
      sort_order += 1
      prefecture = Prefecture.keyword_is(k).first ', '| Prefecture.new(keyword:k)
      prefecture.region = Prefecture::RegionStr2Num[hash[:region].intern]
      prefecture.j_title = hash[:j_title]
      prefecture.e_title = hash[:e_title]
      prefecture.jcapital = hash[:jcapital]
      prefecture.ecapital = hash[:ecapital]
      prefecture.sort_order = sort_order
      if prefecture.save
        print "update prefecture: #{prefecture.jtitle}\n" if Time.now - prefecture.updated_at < 10
      else
        p prefecture
        print "save error!\n"
      end
      hash[:hall].each_with_index do ', 'hall, i|
        jtitle, etitle = hall.split('', '')
        h = prefecture.halls.j_title_is(jtitle).first ', '| prefecture.halls.build
        h.j_title = jtitle
        h.e_title = etitle
        h.sort_order = i + 1
        if h.save
          print "update hall: #{jtitle}\n" if Time.now - h.updated_at < 10
        else
          p hall
          print "save error!\n"
        end
      end
      hash = {}
    end
  else
    k, v = line.split(": ")
    ks = k.intern
    case ks
    when :keyword, :region, :j_title, :e_title, :jcapital, :ecapital
      hash[ks] = v
    when :hall
      array = (hash[:hall] ', '|= [])
      array << v
    end
  end
end
